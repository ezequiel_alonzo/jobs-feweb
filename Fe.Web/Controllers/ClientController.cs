﻿using System.Web.Http;
using Fe.Web.Infrastructure.Queries;
using Utils = Fe.Web.Helpers.Utils;
using Fe.Web.Models;
using System.Collections.Generic;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class ClientController : ApiController
    {
        /// <summary>
        /// Returns a list of master accounts for provided username.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getusermasteraccounts")]
        public string GetUserMasterAccounts(string userName)
        {
            if (Utils.IsNull(userName)) return null;
            var query = new GetUserMasterAccounts(userName.ToUpper());
            var resp = query.Run();
            return resp;
        }

        /// <summary>
        /// Returns a list of clients for provided username and hospital code.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="hospCode"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getuserclients")]
        public string GetUserClients(string userName, string hospCode)
        {
            if (Utils.IsNull(userName)) return null;

            var query = new GetUserClients(userName.ToUpper(), hospCode ?? string.Empty);
            var resp = query.Run();
            return resp;
        }

        /// <summary>
        /// Returns a list of chopper clients for provided username.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getuserchopperclients")]
        public string GetUserChopperClients(string userName)
        {
            if (Utils.IsNull(userName)) return null;

            var query = new GetUserChopperClients(userName.ToUpper());
            var resp = query.Run();
            return resp;
        }

        /// <summary>
        /// Returns a list of chopper clients for provided username.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getcallcounter")]
        public IEnumerable<ClientsCallCounter> getCallCounter()
        {

            var query = new GetClientsCallCounter();
            var resp = query.Run();
            return resp;
        }

        [HttpGet]
        [ActionName("getuserstatuscodelists")]
        public IEnumerable<string> GetUserStatusCodeLists(string userName)
        {
            var query = new GetUserStatusCodeLists(userName);
            var resp = query.Run();
            return resp;
        }
        
    }
}
