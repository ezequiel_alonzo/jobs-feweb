﻿using System.Linq;
using System.Web.Http;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Fe.Web.Infrastructure.Repositories;
using System.Collections.Generic;
using System;
using Fe.Web.Infrastructure.Commands;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class WorkListsController : ApiController
    {
        [HttpGet]
        [ActionName("getworklists")]
        public IEnumerable<object> GetWorkLists(string clientNum)
        {
            var settingTemplates = SettingTemplates.Get("rep-status-special", "filter-settings");

            foreach (var template in settingTemplates.OrderBy(template => template.Name))
            {
                var nameParts = template.Name.Split('_');

                if (nameParts.Count() > 1
                    && nameParts[0].Equals("WORKLIST", StringComparison.OrdinalIgnoreCase))
                    yield return new
                    {
                        TemplateName = template.Name,
                        Type = "WORKLIST",
                        Name = "WORKLIST: " + string.Join("_", nameParts.Skip(1).ToArray()),
                        Template = template.Template
                    };

                if (nameParts.Count() > 2
                    && nameParts[0].Equals("CAMP", StringComparison.OrdinalIgnoreCase)
                    && nameParts[1].Equals(clientNum, StringComparison.OrdinalIgnoreCase))
                    yield return new
                    {
                        TemplateName = template.Name,
                        Type = "CAMPAIGN",
                        Name = "CAMPAIGN: " + string.Join("_", nameParts.Skip(2).ToArray()),
                        Template = template.Template
                    };
            }
        }

        [HttpPost]
        [ActionName("createcampaign")]
        public IHttpActionResult CreateCampaign([FromBody] CreateCampaignRequest request)
        {
            try
            {
                var command = new CreateCampaign(request.CampaignNum, request.ClientNum, request.PatientNums);

                return Ok(command.Run());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }

    public class CreateCampaignRequest
    {
        public string CampaignNum { get; set; }
        public string ClientNum { get; set; }
        public List<int> PatientNums { get; set; }
    }
}

