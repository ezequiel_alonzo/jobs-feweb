﻿using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Newtonsoft.Json;
using System.Text;
using Fe.Web.Infrastructure.Commands;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class EmployeeController : ApiController
    {
        [HttpGet]
        [ActionName("getemployees")]
        public IEnumerable<string> GetEmployees(string userName)
        {
            var query = new GetEmployees(userName);

            return query.Run();
        }


        [HttpGet]
        [ActionName("getemployeeinfo")]
        public IEnumerable<UserInfo> GetEmployeeInfo(string employeeId)
        {
            var query = new GetEmployeeInfo(employeeId);
            var resp = query.Run();
            return resp;
        }


        [HttpPost]
        [ActionName("updateemployeeinfo")]
        public IHttpActionResult UpdateEmployeeInfo(UserInfo uInfo)
        {
            var query = new UpdateEmployeeInfo(uInfo);
            var resp = query.Run();
            return resp;
        }


    }
}
