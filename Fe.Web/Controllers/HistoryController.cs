﻿using System.Web.Http;
using Fe.Web.Infrastructure.Commands;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Utils = Fe.Web.Helpers.Utils;


namespace Fe.Web.Controllers
{
    public class HistoryController : FeApiController
    {
        /// <summary>
        /// Returns History of the current patient.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpatienthistory")]
        public string GetPatientHistory([FromBody] HistoryRequestModel model)
        {
            var query = new GetPatientHistory(model);
            var resp = query.Run();
            return resp;
        }

    }
}