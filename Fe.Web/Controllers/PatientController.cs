﻿using System.Collections.Generic;
using System.Web.Http;
using Fe.Web.Infrastructure.Commands;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Utils = Fe.Web.Helpers.Utils;
using System;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class PatientController : ApiController
    {
        /// <summary>
        /// Returns the patient demographics for provided patient number.
        /// </summary>
        [HttpPost]
        [ActionName("getpatientdemographics")]
        public string GetPatientDemographics([FromBody] PatientRequestModel model)
        {
            
            if (!ModelState.IsValid) return null;
            var query = new GetPatientDemographics(model);
            var resp = query.Run();
            return resp;
        }

        /// <summary>
        /// Updates patient demographics.
        /// </summary>
        [HttpPost]
        [ActionName("updatepatientdemographics")]
        public IHttpActionResult UpdatePatientDemographics([FromBody] PatientDemographics model)
        {
            try
            {
              if (!ModelState.IsValid) return BadRequest("Invalid Model");
                var cmd = new UpdatePatientDemographics(model);
                var errMsg = cmd.Validate();
                if (Utils.IsNotNull(errMsg)) return BadRequest(errMsg);
                cmd.Run();
                return Ok("Success");
            }
            catch
            {
                return BadRequest("Unable to update patient demographics");
            }
        }
        /// <summary>
        /// Insert patient demographics.
        /// </summary>
        [HttpPost]
        [ActionName("inserthfmipatientdemographics")]
        public IHttpActionResult InsertHFMIPatientDemographics([FromBody] HFMIPatientInsertRequestModel model)
        {
            try
            {
                
                var cmd = new HFMIInsertPatient(model);
                var errMsg = cmd.Validate();
                if (Utils.IsNotNull(errMsg)) return BadRequest(errMsg);
                cmd.Run();
                return Ok("Success");
            }
            catch
            {
                return BadRequest("Unable to Insert patient");
            }
        }

        [HttpPost]
        [ActionName("searchpatients")]
        public IEnumerable<PatientWithDetailsModel> SearchPatients([FromBody] SearchPatientsRequestModel model)
        {
            try
            {
                var query = new GetPatientsWithDetailsFiltered(model);

                return query.Run();
            }
            catch (Exception ex) 
            {
                throw ex;
            }
        }
    }
}
