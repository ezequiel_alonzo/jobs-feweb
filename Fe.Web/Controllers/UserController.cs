﻿using Fe.Web.Infrastructure.Commands;
using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Fe.Web.Controllers
{
    public class UserController : ApiController
    {

        /// <summary>
        /// updates user password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatepassword")]
        public async Task<string> updatecheckinstatus([FromBody] UserPasswordRequestModel model)
        {

            var query = new UpdatePassword(model);
            var resp = await query.Run();

            return resp;
        }
    }
}