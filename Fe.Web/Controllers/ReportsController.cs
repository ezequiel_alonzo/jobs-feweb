using System.Linq;
using System.Web.Http;
using Newtonsoft.Json;

namespace Fe.Web.App.reports.Controllers
{
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using System.Web;
    using Telerik.Reporting.Cache.File;
    using Telerik.Reporting.Services;
    using Telerik.Reporting.Services.WebApi;

    //The class name determines the service URL. 
    //ReportsController class name defines /api/report/ service URL.
    public class ReportsController : ReportsControllerBase
    {
        [HttpGet]
        [ActionName("getreportlist")]
        public IEnumerable<object> GetReportList()
        {
            var directory = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Content/reports"));

            return
                from file in directory.GetFiles("*.trdx")
                let xml = XDocument.Load(file.FullName)
                select new
                {
                    ReportName = file.Name.Split('.')[0],
                    FileName = file.Name,
                    Parameters = 
                        from element in xml.Descendants()
                        where element.Name.LocalName == "ReportParameter"
                        select element.Attribute("Name").Value.ToUpper()
                };
        }

        static ReportServiceConfiguration configurationInstance;

        static ReportsController()
        {
            //This is the folder that contains the XML (trdx) report definitions
            //In this case this is the Reports folder
            var appPath = HttpContext.Current.Server.MapPath("~/");
            var reportsPath = Path.Combine(appPath, "Content/reports");

            //Add resolver for trdx report definitions, 
            //then add resolver for class report definitions as fallback resolver; 
            //finally create the resolver and use it in the ReportServiceConfiguration instance.
            var resolver = new ReportFileResolver(reportsPath)
                .AddFallbackResolver(new ReportTypeResolver());

            //Setup the ReportServiceConfiguration
            configurationInstance = new ReportServiceConfiguration
            {
                HostAppId = "Html5App",
                Storage = new FileStorage(),
                ReportResolver = resolver,
                // ReportSharingTimeout = 0,
                // ClientSessionTimeout = 15,
            };
        }

        public ReportsController()
        {
            //Initialize the service configuration
            this.ReportServiceConfiguration = configurationInstance;
        }
    }
}