﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Newtonsoft.Json;
using Fe.Web.Infrastructure.Repositories;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class SettingTemplatesController : ApiController
    {
        [HttpPost]
        [ActionName("savetemplate")]
        public void SaveTemplate([FromBody] SettingTemplate template)
        {
            SettingTemplates.Create(template);
        }

        [HttpDelete]
        [ActionName("deletetemplate")]
        public void DeleteTemplate(int id)
        {
            SettingTemplates.Delete(id);
        }

        [HttpGet]
        [ActionName("gettemplates")]
        public IEnumerable<SettingTemplate> GetTemplates(string parentName, string type) 
        {
            return SettingTemplates.Get(parentName, type);
        }
    }
}
