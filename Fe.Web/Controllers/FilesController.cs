﻿using System;
using System.Web;
using System.Web.Http;
using Fe.Web.Infrastructure.Common;
using Fe.Web.Models;
using System.Collections.Generic;
using Fe.Web.Infrastructure.Queries;
using System.Net.Http;
using System.IO;
using System.Net;
using System.Net.Http.Headers;

namespace Fe.Web.Controllers 
{

    public class FilesController : ApiController
    {
        
        

        /// <summary>
        /// Returns PatientLetter files list
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getfileslist")]
        public IEnumerable<FileUrlInfo> FilesList(string files,string hospCode)
        {
            var query = new GetFilesList( files ,HttpContext.Current.Server.MapPath("~/"), HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/", hospCode );
            var resp = query.Run();
            return resp;
        }


        //[HttpGet]
        //[ActionName("downloadfile")]
        //public HttpResponseMessage DownLoadFile(string fileLocation)
        //{
        //    //var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/test.docx"); ;
        //    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
        //    var stream = new FileStream(fileLocation, FileMode.Open);
        //    result.Content = new StreamContent(stream);
        //    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        //    result.Content.Headers.ContentDisposition.FileName = Path.GetFileName(fileLocation);
        //    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
        //    result.Content.Headers.ContentLength = stream.Length;
        //    return result;      
        //}

        [HttpGet]
        [ActionName("deletefile")]
        public string  Deletefile(string fileLocation)
        {
            HttpResponseMessage result;
            try
            {
                var command = new FileDelete(fileLocation);
                command.Run();
                result  = new HttpResponseMessage(HttpStatusCode.OK);
                return result.ToString();
            }
            catch {
                result = new HttpResponseMessage(HttpStatusCode.BadRequest);
             return  result.ToString() ;

            }
        }
    }
}