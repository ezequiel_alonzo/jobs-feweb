﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using Fe.Web.Helpers;
using Fe.Web.Infrastructure.Choppers;
using Fe.Web.Infrastructure.Common;
using Fe.Web.Models;
using Utils = Fe.Web.Helpers.Utils;
using System.Collections.Generic;

namespace Fe.Web.Controllers
{
    public class ImportController : ApiController
    {
        private string _fileUploadPath = AppDomain.CurrentDomain.BaseDirectory + @"\BatchFiles\Imports\";

        [HttpPost]
        [ActionName("uploadimportfile")]
        public IHttpActionResult UploadImportFile()
        {
            try
            {
                var request = HttpContext.Current.Request;

                var chopperName = request.Form["Chopper"];
                var clientNum = request.Form["ClientNum"];
                var requestId = request.Form["RequestId"];
                var userName = request.Form["UserName"];
                var xx = request.Form["IsRecon"];
                var isRecon = true;

                HttpPostedFile file = request.Files[0];

                var directory = string.Format(@"{0}\{1}", _fileUploadPath, clientNum);

                Directory.CreateDirectory(directory);

                var fileName = string.Format(@"{0}\{1}.import", directory, requestId);

                file.SaveAs(fileName);

                var chopper = GetChopper(fileName, clientNum, chopperName, requestId, userName, isRecon);
                var aa = chopper.GetResult().ToList();
                return Ok(aa);

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [ActionName("processimportfile")]
        public IHttpActionResult ProcessImportFile([FromBody] ProcessUploadedFileRequest request)
        {
            try
            {
                if (!request.IsValid())
                    return BadRequest();

                var fileName = string.Format(@"{0}\{1}\{2}.import", _fileUploadPath, request.ClientNum, request.RequestId);

                new Thread(() =>
                {
                    var chopper = GetChopper(fileName, request.ClientNum, request.ChopperName, request.RequestId, request.UserName, request.isRecon);

                    chopper.Process();

                }).Start();

                return Ok("Processing file");

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private static IChopper<PatientProfile> GetChopper(string fileName, string clientNum, string chopperName, string requestId, string userName, bool isRecon)
        {
            var chopper = Type.GetType("Fe.Web.Infrastructure.Choppers.Chopper" + chopperName);

            if (chopper == null)
                throw new Exception(string.Format("The selected chopper ({0}) is not implemented.", chopperName));

            return (IChopper<PatientProfile>)Activator.CreateInstance(chopper, fileName, clientNum, requestId,userName,isRecon);
        }
    }

    public class ProcessUploadedFileRequest
    {
        public string RequestId { get; set; }
        public string ClientNum { get; set; }
        public string ChopperName { get; set; }
        public string UserName { get; set; }
        public bool isRecon { get; set; }
        public bool IsValid()
        {
            return ClientNum != null && RequestId != null && ChopperName != null;
        }
    }
}
