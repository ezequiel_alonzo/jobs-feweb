﻿using System;
using System.Web.Http;
using Fe.Web.Infrastructure.Common;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class RpcMqController : ApiController
    {
        /// <summary>
        /// Returns next message in provided queue
        /// </summary>
        /// <param name="mq"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("rpcmqreceivemessage")]
        public string RpcMqReceiveMessage([FromBody] MqHandler mq)
        {
            try
            {
                var rpcMqHandler = new RpcMq(mq.Exchange, mq.Queue, mq.RouteKey, MvcApplication._rmqConn);
                var resp = rpcMqHandler.Receive();
                return resp;
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }

        /// <summary>
        /// Returns all messages currently in provided queue
        /// </summary>
        /// <param name="mq"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("rpcmqreceiveallmessages")]
        public IHttpActionResult RpcMqReceiveAllMessages([FromBody] MqHandler mq)
        {
            try
            {
                var rpcMqHandler = new RpcMq(mq.Exchange, mq.Queue, mq.RouteKey, MvcApplication._rmqConn);

                return Ok(rpcMqHandler.ReceiveAll());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Closes channel for the provided exchange
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("rpcmqclosechannel")]
        public IHttpActionResult RpcMqCloseChannel([FromBody] MqHandler mq)
        {
            try
            {
                var rpcMqHandler = new RpcMq(mq.Exchange, mq.Queue, mq.RouteKey, MvcApplication._rmqConn);
                rpcMqHandler.Close();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest("Error");
            }
        }

    }
}
