﻿using System.Web.Http;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Fe.Web.Helpers;
using Fe.Web.Infrastructure.Commands;
using System.Collections.Generic;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class PostingController : ApiController
    {
        /// <summary>
        /// Returns a list of postings for provided patient.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpatientpostings")]
        public string GetPatientPostings([FromBody] PostingRequestModel model)
        {
            if (!ModelState.IsValid) return null;
            var query = new GetPatientPostings(model);
            var resp = query.Run();
            return resp;
        }
        /// <summary>
        /// Returns a list of postings for provided patient.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [ActionName("getpostinguserlist")]
        public string GetPostingUserList()
        {

            var query = new GetPostUserList();
            var resp = query.Run();
            return resp;
        }

        /// <summary>
        /// Adds a patient posting.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("addposting")]
        public IHttpActionResult AddPosting([FromBody] PostingInsertModel model)
        {
            try
               {
                   if (!ModelState.IsValid) return BadRequest("Wrong Entry Verify Dates");
                   if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                   model.UserName = model.UserName.ToUpper();
                   var cmd = new InsertPatientPosting(model);
                   var errMsg = cmd.Validate();
                  if (Utils.IsNotNull(errMsg)) return BadRequest(errMsg);
                   cmd.Run();
                   return Ok("Success");
             }

            catch
            {
                return BadRequest("Unable to insert posting");
            }
        }

        /// <summary>
        /// deletes a patient  posting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deleteposting")]
        public IHttpActionResult DeletePosting([FromBody] PostingDeleteModel model)
        {
            try
            {
                //if (!ModelState.IsValid) return BadRequest("Invalid Model");
              //  if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
              //  model.UserName = model.UserName.ToUpper();
                var cmd = new DeletePatientPosting(model);
                //var errMsg = cmd.Validate();
                //if (Utils.IsNotNull(errMsg)) return BadRequest(errMsg);
                cmd.Run();
                return Ok("Success");
            }
            catch
            {
                return BadRequest("Unable to insert posting");
            }
        }
   }
}
