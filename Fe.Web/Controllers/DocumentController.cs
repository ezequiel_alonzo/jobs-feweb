﻿using System;
using System.Web;
using System.Web.Http;
using Fe.Web.Infrastructure.Common;
using Fe.Web.Models;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class DocumentController : ApiController
    {
        [HttpPost]
        [ActionName("generatedocument")]
        public string GenerateDocument([FromBody] DocumentGenerationRequestModel model)
        {
            if (!ModelState.IsValid) return null;
            var query = new DocumentGeneration(model, HttpContext.Current.Server.MapPath("~/content/documents/"));
            var resp = query.Run();
            resp = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/content/documents/generated/" + resp;
            return resp;
        }
    }
}
