﻿using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using System.Web.Http;
using System.Web.Security;
using Fe.Web.Infrastructure.Common;
using Fe.Web.Models;
using Utils = Fe.Web.Helpers.Utils;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Infrastructure.Commands;
using System.Collections.Generic;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class AccountController : ApiController
    {

        /// <summary>
        /// Attempts to logout user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("logout")]
        public async Task<IHttpActionResult> Logout([FromBody] LocalUserModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");

                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                if (model.Claims == null) return BadRequest("Missing UserName");

                var handler = new LocalUserLogout(model);

                var response = await handler.Logout();

                if (response == "OK") return Ok();

                return BadRequest(response);
            }
            catch
            {
                return BadRequest("Internal Server Error");
            }
        }

        /// <summary>
        /// updates The State Of The User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatecheckinstatus")]
        public bool updatecheckinstatus([FromBody]UserCheckinModel model)
        {
            var aa = HttpContext.Current.Request.UserHostAddress;
            var query = new UpdateCheckinStatus(model, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
            var resp = query.Run();
            return resp;
        }

        /// <summary>
        /// Returns curren user session state.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [ActionName("getsessionstate")]
        public string GetSessionState()
        {
            var session = new UserSessionState
            {
                SessionExpired = HttpContext.Current.Session == null,
                AuthExpired = !User.Identity.IsAuthenticated,
                TimeoutInMinutes = FormsAuthentication.Timeout.TotalMinutes,
                TimeoutInSeconds = FormsAuthentication.Timeout.TotalSeconds
            };

            return JsonConvert.SerializeObject(session);
        }


        /// <summary>
        /// Returns curren user session state.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [ActionName("getcheckinlistbyip")]
        public IEnumerable<UserCheckinIp> GetCheckinListByIp(string userName)
        {
            var query = new GetCheckinListByIp(userName);
            var resp = query.Run();
            return resp;
        }


        /// <summary>
        /// Returns The State Of The User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getcheckedinstatus")]
        public bool getcheckedinstatus(string userName)
        {
            var query = new GetCheckedInStatus(userName.ToUpper());
            var resp = query.Run();
            return resp;
        }

        /// <summary>
        /// Returns The State Of The User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getuserpermissions")]
        public string getUserPermissions([FromBody] LocalUserModel model)
        {
            var query = new GetUserPermissions(model.UserName.ToUpper());
            var resp = query.Run();
            return resp;
        }

        [HttpGet]
        [ActionName("getuserstatuscodelists")]
        public IEnumerable<string> GetUserStatusCodeLists(string userName)
        {
            var query = new GetUserStatusCodeLists(userName.ToUpper());

            return query.Run();
        }
    }
}