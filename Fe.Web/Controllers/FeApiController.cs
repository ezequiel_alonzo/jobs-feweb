﻿using System.Configuration;
using System.Web.Http;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class FeApiController : ApiController
    {
        protected string GetFeConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["FeConnection"].ConnectionString;
        }

        protected string GetEmrConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["EmrConnection"].ConnectionString;
        }
    }
}
