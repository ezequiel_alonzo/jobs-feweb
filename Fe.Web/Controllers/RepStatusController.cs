﻿using System.Web.Http;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class RepStatusController : ApiController
    {

        [HttpPost]
        [ActionName("getrepstatuslist")]
        public string GetRepStatusList([FromBody] RepStatusRequestModel model)
        {
            if (!ModelState.IsValid) return null;

            var query = new GetRepStatusList(model);
            var resp = query.Run();
            return resp;
        }
    }
}

