﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Infrastructure.Commands;
using Fe.Web.Models;
using Newtonsoft.Json;
using Fe.Web.Infrastructure.Common;
using System.Web;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class ExportController : ApiController
    {
        [HttpPost]
        [ActionName("generatemaxbatch")]
        public IHttpActionResult GenerateMaxBatch([FromBody] SearchPatientsRequestModel searchRequest)
        {
            try
            {
                var patientsQuery = new GetPatientsWithDetailsFiltered(searchRequest);

                var patients = patientsQuery.Run();

                var command = new GenerateMaxBatch(AppDomain.CurrentDomain.BaseDirectory + @"\BatchFiles\MaxBatch\", patients);

                return Ok(command.Run());
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [ActionName("generate270")]
        public IHttpActionResult Generate270([FromBody] SearchPatientsRequestModel searchRequest)
        {
            try
            {
                var patientsQuery = new GetPatientsWithDetailsFiltered(searchRequest);

                var patients = patientsQuery.Run();

                var command = new Generate270(AppDomain.CurrentDomain.BaseDirectory + @"\BatchFiles\270\", patients);

                return Ok(command.Run());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

    }
}
