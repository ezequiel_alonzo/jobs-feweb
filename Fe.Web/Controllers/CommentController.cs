﻿using System.Web.Http;
using Fe.Web.Infrastructure.Commands;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Utils = Fe.Web.Helpers.Utils;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class CommentController : ApiController
    {

        /// <summary>
        /// Returns list of activity codes.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getactivitycodes")]
        public string GetActivityCodes()
        {
            var query = new GetActivityCodes();
            var resp = query.Run();
            return resp;
        }

        /// <summary>
        /// Retruns list of comm codes.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getcommcodes")]
        public string GetCommCodes(string clientNum)
        {
            var listId = new GetClientCommCodesList(clientNum).Run();

            var commCodes = new GetCommCodes(listId).Run();

            return JsonConvert.SerializeObject(commCodes);
        }

        /// <summary>
        /// Retruns list of comm codes.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("getcommcodesbylistid")]
        public string GetCommCodesByListId(string listId)
        {
            var commCodes = new GetCommCodes(listId).Run();

            return JsonConvert.SerializeObject(commCodes);
        }

        /// <summary>
        /// Return a list of comments for provided patient number.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpatientcomments")]
        public string GetPatientComments([FromBody] CommentRequestModel model)
        {
            if (!ModelState.IsValid) return null;
            var query = new GetPatientComments(model);
            var resp = query.Run();
            return resp;
        }

        /// <summary>
        /// Adds a patient comment.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("addcomment")]
        public IHttpActionResult AddComment([FromBody] CommentInsertModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest("Invalid Model");
                if (Utils.IsNull(model.UserName)) return BadRequest("Missing UserName");
                model.UserName = model.UserName.ToUpper();
                var cmd = new InsertPatientComment(model);
                var errMsg = cmd.Validate();
                if(Utils.IsNotNull(errMsg)) return BadRequest(errMsg);                
                cmd.Run();
                return Ok("Success");
            }
            catch
            {
                return BadRequest("Unable to insert comment");
            }
        }

        /// <summary>
        /// Adds a patient comment.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("addmulticomment")]
        public IHttpActionResult AddMultiComment([FromBody] MultiCommentInsertModel model)
        {
            try
            {
                model.UserName = model.UserName.ToUpper();
                var cmd = new InsertMultiComment(model);
                //if (Utils.IsNotNull(errMsg)) return BadRequest(errMsg);
                cmd.Run();
                return Ok("Success");
            }
            catch
            {
                return BadRequest("Unable to insert comment");
            }
        }

    }
}
