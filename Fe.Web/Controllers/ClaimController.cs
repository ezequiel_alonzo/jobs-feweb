﻿using System.Web.Http;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;

namespace Fe.Web.Controllers
{
    [Authorize]
    public class ClaimController : ApiController
    {
        /// <summary>
        /// Returns a list of claims for provided subscriber number.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpatientclaims")]
        public string GetPatientClaims([FromBody] ClaimRequestModel model)
        {
            if (!ModelState.IsValid) return null;
            var query = new GetPatientClaims(model);
            var resp = query.Run();
            return resp;
        }
    }
}
