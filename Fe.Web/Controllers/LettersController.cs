﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Infrastructure.Commands;
using Fe.Web.Models;
using Newtonsoft.Json;
using Fe.Web.Infrastructure.Common;
using System.Web;


namespace Fe.Web.Controllers
{
    public class LettersController : ApiController
    {
        [HttpPost]
        [ActionName("getlettersfile")]
        public string GetLettersFile([FromBody] PatientLettersRequestModel model)
        {
            try
            {
                var query = new LettersFileGeneration(model, HttpContext.Current.Server.MapPath("~/content/documents/"), HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));
                var resp = query.Run();
                return resp;

            }
            catch (Exception ex)
            {
                return ex.Message + "__error";
            }
        }

        [HttpDelete]
        [ActionName("clearlettersqueue")]
        public void clearLettersQueue([FromBody] PatientLettersRequestModel model)
        {
                var query = new ClearLettersQueue(model);
                 query.Run();
        }

        [HttpGet]
        [ActionName("getletterstemplates")]
        public IEnumerable<FileUrlInfo> GetLettersTemplates(string clientNum)
        {
            var query = new GetTemplateLetters(clientNum, HttpContext.Current.Server.MapPath("~/content/documents/templates/"));
            var resp = query.Run();

            return resp;
        }

    }
}