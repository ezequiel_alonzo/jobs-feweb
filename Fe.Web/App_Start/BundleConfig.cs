﻿using System.Web.Optimization;

namespace Fe.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js")
                .Include("~/Scripts/jquery-{version}.js")
                .Include("~/Scripts/polyfills.js")
                .Include("~/Scripts/respond.js")
                .Include("~/Scripts/knockout-3.1.0.js")
                .Include("~/Scripts/knockout.mapping.min.js")
                .Include("~/Scripts/knockout.validation.min.js")
                .Include("~/Scripts/knockout.validation.js")
                .Include("~/Scripts/jszip.js")
                .Include("~/Scripts/kendo/2016.1.406/kendo.all.min.js")
                .Include("~/Scripts/telerikReportViewer.js")
                .Include("~/Scripts/toastr.js"));

            bundles.Add(new StyleBundle("~/bundles/css")
                .Include("~/Content/site.css")
                .Include("~/Content/toastr.min.css")
                .Include("~/Content/kendo/2016.1.406/kendo.common.min.css")
                .Include("~/Content/kendo/2016.1.406/kendo.silver.min.css")
                .Include("~/Content/reportviewer/styles/telerikReportViewer.css")
                .Include("~/Content/bootstrap-glypicons.min.css")
                .Include("~/Content/durandal.css"));         

            BundleTable.EnableOptimizations = true;
        }
    }
}
