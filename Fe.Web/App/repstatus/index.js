﻿//******************************
// File: repstatus/index.js
//******************************

define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger"], function (router, $, app, appsecurity, routeconfig, utils, logger) {

    //----------------------------------------------------------------------

    var browserName = new ko.observable();
    var repStatusList = new ko.observableArray([]);

    function showPatientDetails(arg) {
        var rsGrid = $("#fe-rep-status-grid").data("kendoGrid");
        var selectedItem = rsGrid.dataItem(rsGrid.select());
        var patientNum = selectedItem.PatientNumber;
        var subscriberNum = selectedItem.SubscriberNumber;
        
        var rsList = rsGrid._data;
        var newRepStatusList = [];
        $.each(rsList, function (index, item) {
            if (item.SubscriberNumber === subscriberNum) {
                item.HasViewed = true;
            }
            newRepStatusList.push(item);
        });

        rsGrid.dataSource.data = newRepStatusList;
        rsGrid.refresh();

        app.trigger("patient:resetpatientdetails");

        router.navigate("Patient/Emeric")

        var pObj = {
            PatientNumber: patientNum,
            SubscriberNumber: subscriberNum
        };

        app.trigger("patient:getpatientdetails", pObj);

    };

    function buildRepStatusGrid() {

        $("#fe-rep-status-grid").remove();
        $("#fe-rep-status-grid-container").html("<div id=\"fe-rep-status-grid\" style=\"height: calc(100% - 22px);\"></div>");

        var data = repStatusList();

        var vTemplate = "#if(HasViewed){#<input type='checkbox' disabled='disabled' checked='checked' style='margin-right: 0; margin-left: 1px; padding-left: 0; padding-right: 0;'/>#} " +
                        "else { #<input type='checkbox' disabled='disabled' style='margin-right: 0; margin-left: 1px; padding-left: 0; padding-right: 0;'/># }#";

        var cols = [
            { field: "HasViewed", title: " ", width: "25px", template: vTemplate, headerAttributes: { style: "border-width: 0px !important" }, attributes: { style: "border-width: 0px !important" } },
            { field: "PatientName", title: "Name", width: "250px" },
            { field: "ClaimCount", title: "Claim Count", width: "125px" },
            { field: "AmountPaid", title: "Amount Paid", width: "125px", format:"{0:c2}" },
            { field: "SubscriberNumber", title: "Claimant #", width: "125px" },
            { field: "PatientNumber", title: "Patient #", width: "125px" },
            { field: "Status", title: "Status", width: "125px" },
            { field: "StatusDate", type: "date", title: "Status Date", width: "100px", format: "{0:MM/dd/yyyy}"},
            { field: "CallBackDate", type: "date", title: "CB Date", width: "100px", format: "{0:MM/dd/yyyy}" },
            { field: "LastChanged", type: "date", title: "LastChanged", width: "100px", format: "{0:MM/dd/yyyy}" }

        ];

        var gHeight = 242;
        var bName = browserName();
        switch (bName) {
            case "Chrome":
                gHeight = 220;
                break;
            case "Firefox":
                gHeight = 208;
                break;
            default:
                gHeight = 242;
                break;
        }

        $("#fe-rep-status-grid").kendoGrid({
            columns: cols,
            dataSource: {
                data: data,
                pageSize: 20,
                filter: { field: "StatusNum", operator: "lt", value: 900 },
                sort: { field: "CallBackDate", dir: "asc" }
            },
            selectable: "row",
            scrollable: true,
            sortable: true,
            resizable: true,
            pageable: true,
            change: showPatientDetails
        }).data("kendoGrid");

    };

    function populateRepStatusList(data) {
        
        var rslData = [];

        $.each(data, function (index, item) {

            var statusNum = -1;

            if (item.Status) {
                try {
                    statusNum = parseInt(item.Status);
                } catch (e) {

                }
            }

            var tmpItem = {
                HasViewed: item.HasViewed,
                PatientName: item.PatientName,
                ClaimCount: item.ClaimCount,
                AmountPaid: item.AmountPaid,
                PatientNumber: item.PatientNumber,
                SubscriberNumber: item.SubscriberNumber,
                Status: item.Status,
                StatusNum: statusNum,
                StatusDate: item.StatusDate,
                CallBackDate: item.CallBackDate,
                LastChanged: item.LastChanged
            };
            rslData.push(tmpItem);
        });

        repStatusList(rslData);

        buildRepStatusGrid();

    };

    function getRepStatusList() {

        var obj = {
            TemplateName: "emr",
            ClientNumber: appsecurity.getUserClientNumber()
        };

        $.ajax(routeconfig.getRepStatusList, {
            type: "POST",
            dataType: "json",
            data: obj,
            success: function (data, status, xhr) {

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    populateRepStatusList(jObj);
                }
                
            },
            error: function (data, status, xhr) {
                logger.logError(xhr, status, null, true);
            },
            fail: function (data, status, xhr) {
                logger.logError(xhr, status, null, true);
            }
        });

    };

    function initControls() {     
        var bName = utils.getBrowserName();
        browserName(bName);
        getRepStatusList();
    };

    function filterRepStatusList() {

        var ds = $("#fe-rep-status-grid").data("kendoGrid").dataSource;
        var bt = $("#fe-rep-status-filter-button-text").text();

        if (bt === "SHOW ALL") {
            ds.filter({});
            $("#fe-rep-status-grid").data("kendoGrid").dataSource = ds;
            $("#fe-rep-status-filter-button-text").text("SHOW < 900");
        } else {
            ds.filter({ field: "StatusNum", operator: "lt", value: 900 });
            $("#fe-rep-status-grid").data("kendoGrid").dataSource = ds;
            $("#fe-rep-status-filter-button-text").text("SHOW ALL");
        }

    };

    //----------------------------------------------------------------------

    var viewmodel = {

        //State Functions
        canActivate: function() {
            return true;
        },
        activate: function() {

        },
        compositionComplete: function() {
            initControls();
        },

        //Variables

        //Functions
        filterRepStatusList: filterRepStatusList

};

    return viewmodel;
});