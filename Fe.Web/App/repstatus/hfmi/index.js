
define(["plugins/router", "durandal/app", "services/server", "services/appsecurity", "services/routeconfig", "services/gridhelpers", "services/logger", "services/utils"],
    function (router, app, server, appsecurity, routes, gridhelpers, logger, utils) {

        var workListGrid;
        var selectedTemplate = ko.observable();

        var reloadContent = function () {
            $("#templates-dropdown").data("kendoDropDownList").dataSource.read();

            if (!selectedTemplate()) return;

            workListGrid.dataSource.read();
        }

        return {
            selectedTemplate: selectedTemplate,
            reloadContent: reloadContent,

            templates: new kendo.data.DataSource({
                transport: {
                    read: function (operation) {
                        server
                            .get("./api/worklists/getworklists", { clientNum: appsecurity.getUserClientNumber() })
                            .then(operation.success);
                    }
                }
            }),

            downloadLetters: function () {

                var patientLettersRequestData = $("#work-list-grid").data("kendoGrid").dataSource
                    .data()
                    .filter(function (patient) {
                        return patient.letter
                    })
                    .map(function (patientWithLetter) {
                        return {
                            PatNum: patientWithLetter.patientNum,
                            HospName: patientWithLetter.phyName,
                            FirstName: patientWithLetter.firstName,
                            LastName: patientWithLetter.lastName,
                            PAddress: patientWithLetter.patAddress,
                            HospAcct: patientWithLetter.hospAcct,
                            CurrBalance: patientWithLetter.balance ? patientWithLetter.balance : 0.00,
                            AdmitDate: kendo.toString(kendo.parseDate(patientWithLetter.admitDate, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy"),
                            PState: patientWithLetter.patState,
                            PZip: patientWithLetter.patZip,
                            PCity: patientWithLetter.patCity,
                            LetterName: patientWithLetter.letter + ".docx",
                            ClientNum: appsecurity.getUserClientNumber(),
                            PaymentStartDate: kendo.toString(kendo.parseDate(patientWithLetter.cbDate, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy"),
                            PaymentAmount: patientWithLetter.paymentAmount,
                            CurrStatus: utils.lPad(patientWithLetter.currStatus, 3)

                        };
                    });

                if (patientLettersRequestData.length > 0) {
                    logger.log(" Letters for " + patientLettersRequestData.length + " patients, are being processed", "Letters", null, true)
                    $.ajax(routes.getLettersFile, {
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: "POST",
                        data: JSON.stringify({ PatientLetters: patientLettersRequestData, UserName: appsecurity.getUserInfo().userName, DisplayName: appsecurity.getUserInfo().displayName }),
                        success: function (result) {
                            if (result.indexOf("__error") > -1) {
                                logger.log("Template not in server", result, null, true)
                            }
                            else {
                                app.showMessage("do you wish to clear the queue?", "Confirm", ["Yes", "No"])
                                .then(function (answer) {
                                    if (answer !== "Yes") return;
                                    $.ajax(routes.clearLettersQueue, {
                                        contentType: 'application/json; charset=utf-8',
                                        dataType: 'json',
                                        type: "DELETE",
                                        data: JSON.stringify({ PatientLetters: patientLettersRequestData, UserName: appsecurity.getUserInfo().userName, DisplayName: appsecurity.getUserInfo().displayName }),
                                        success: function (result) {
                                            workListGrid.dataSource.read();
                                        }
                                    });
                                });
                                window.open(result);
                            }
                        }
                    });
                }
                else {
                    app.showMessage("No letters to print.");
                }
            },

            loadSelectedTemplate: function () {
                if (!selectedTemplate()) return;

                var template = JSON.parse(selectedTemplate().template);

                template.gridOptions.columns.unshift({
                    field: "hasViewed",
                    title: " ",
                    width: "40px",
                    template: "<input type='checkbox' disabled='disabled' #if (hasViewed) {# checked='checked' #}#/>",
                    attributes: { style: "text-align: center" }
                });

                workListGrid.setOptions(template.gridOptions)
                workListGrid.setOptions({
                    filterable: false,
                    toolbar: false,
                    autoBind: false,
                    dataSource: {
                        transport: {
                            read: function (operation) {
                                server
                                    .post(routes.searchPatients, {
                                        clientNums: template.clientSearch === "CURRENT"
                                            ? [appsecurity.getUserClientNumber()]
                                            : template.clientNums,
                                        statusCodes: template.statusCodes,
                                        spanish: template.spanish,
                                        afterHours: template.afterHours,
                                        fieldVisit: template.fieldVisit,
                                        everHadStatus: template.everHadStatus,
                                        agedUnder20: template.agedUnder20,
                                        postingsOnly: template.postingsOnly,
                                        dateSearch: template.dateSearch,
                                        dateRangeType: template.dateRangeType,
                                        days: template.days,
                                        startDate: template.startDate,
                                        endDate: template.endDate
                                    })
                                    .then(function (patients) {
                                        operation.success(patients.map(function (patient) {
                                            patient.hasViewed = false;
                                            return patient;
                                        }));
                                    });
                            }
                        },
                        pageSize: 100
                    }
                });
                workListGrid.dataSource.read();
            },

            deleteSelectedRows: function () {
                gridhelpers.deleteSelectedRows(workListGrid, "patientNum");
            },

            createCampaign: function () {

                var filters = workListGrid.dataSource.filter();
                var allData = workListGrid.dataSource.data();
                var query = new kendo.data.Query(allData);
                var data = query.filter(filters).data;

                if (!data || data.length === 0) return;

                logger.log("Creating Campaign: " + selectedTemplate().templateName, "Campaign", null, true);

                server
                    .post("./api/worklists/createcampaign", {
                        campaignNum: selectedTemplate().templateName,
                        clientNum: appsecurity.getUserClientNumber(),
                        patientNums: data.map(function (patient) {
                            return patient.patientNum;
                        })
                    })
                    .then(function (result) {
                        logger.log(result + " Patient(s) added to " + selectedTemplate().templateName, "Campaign", null, true);
                    }, function (error) {
                        logger.log("An error ocurred creating campaign: " + selectedTemplate().templateName, "Campaign", null, true);
                    });
            },

            compositionComplete: function () {
                workListGrid = $("#work-list-grid").kendoGrid({
                    dataBound: function (e) {
                        $("#work-list-grid .has-qletter").parents("tr").addClass("has-qletter");
                        $("#work-list-grid .has-manager-status").parents("tr").addClass("has-manager-status");
                        $("#work-list-grid .has-hosp-alert").parents("td").addClass("has-hosp-alert");
                        $("#work-list-grid .has-fin-class-change").parents("td").addClass("has-fin-class-change");
                        $("#work-list-grid .no-recon-balance").parents("td").addClass("no-recon-balance");
                        $("#work-list-grid .recon-balance-differs").parents("td").addClass("recon-balance-differs");
                    }
                }).data("kendoGrid");

                $("#work-list-grid").on("dblclick", "tr.k-state-selected", function () {
                    var patient = workListGrid.dataItem(workListGrid.select());

                    patient.set("hasViewed", true);

                    if (patient.clientNum !== appsecurity.getUserClientNumber())
                        appsecurity.setUserClient(patient.hospCode, patient.clientNum, patient.phyName);

                    router.navigate("Patient/HFMI/" + patient.patientNum);
                });

                app.on("view:changeclientinfo").then(function (clientInfo) {
                    $("#templates-dropdown").data("kendoDropDownList").dataSource.read();
                });
            }
        };
    });