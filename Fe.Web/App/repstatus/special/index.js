﻿define(["durandal/app", "plugins/router", "services/routeconfig", "services/server", "services/gridhelpers", "services/appsecurity", "services/logger", "moment", "jquery", "modals/columnselector/index", "modals/inputmodal/index", "modals/selectmodal/index", "modals/commentmodal/index"],
    function (app, router, routes, server, gridhelpers, appsecurity, logger, moment, $, columnSelector, inputModal, selectModal, commentModal) {

        var codes;
        var clients;
        var patientsGrid;
        var currentClient = ko.observable(appsecurity.getUserClientNumber());
        var statusCodeListIds = appsecurity.getUserInfo().statusCodeListIds;
        var selectedStatusCodeListId = ko.observable(statusCodeListIds[0]);

        var hospAlertTemplate = "#if (hospAlert) {#<span class='glyphicon glyphicon-ok'></span>#} " +
                                "else { #<span></span># }#";

        var managerTemplate = "#if (managerStatus) {#<span class='has-hosp-alert glyphicon glyphicon-ok'></span>#} " +
                        "else { #<span></span># }#";

        var reconDiff = "#if (reconDiff) {#<span class='glyphicon glyphicon-ok'></span>#} " +
                                "else { #<span></span># }#";

        var noReconBalance = "#if (noReconBalance) {#<span class='glyphicon glyphicon-ok'></span>#} " +
                               "else { #<span></span># }#";

        var transfer = "#if (transfer) {#<span class='glyphicon glyphicon-ok'></span>#} " +
                               "else { #<span></span># }#";

        var status800Recon = "#if (status800Recon) {#<span class='glyphicon glyphicon-ok'></span>#} " +
                               "else { #<span></span># }#";

        var qletterTemplate = "#if(letter){#<span class='has-qletter'>#:letter#</span>#} else {#<span></span>#}#";

        var finClassTemplate = "#if(finClass !== prevFinClass){#<span class='has-fin-class-change'>#= (finClass || '') #</span>#} else {#<span>#= (finClass || '') #</span>#}#"

        var finClassChange = "#if (finClassChange) {#<span class='glyphicon glyphicon-ok'></span>#} " +
                               "else { #<span></span># }#";

        var balance = "#if(noReconBalance){#<span class='no-recon-balance'>#:balance#</span>#}else if(reconDiff){#<span class='recon-balance-differs'>#:balance#</span>#} "
                        +"else {#<span>#:balance#</span>#}#";

        var fields = {
            lastName: { title: "Last Name", type: "string", width: "180px" },
            firstName: { title: "First Name", type: "string", width: "180px" },
            balance: { title: "Balance", type: "number", width: "100px", format: "{0:c}", template : balance },
            admitDate: { title: "Admit Date", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            currStatus: { title: "Status", type: "number", width: "100px", format: "{0:000}" },
            cbDate: { title: "Cb Date", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            hospAcct: { title: "Hosp Acct", type: "string", width: "180px" },
            patientNum: { title: "Patient Num", type: "string", width: "180px" },
            remDate: { title: "Rem Date", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            cbUser: { title: "Cb User", type: "string", width: "100px" },
            clientNum: { title: "Client Num", type: "string", width: "180px" },
            insCode: { title: "Ins Code", type: "string", width: "180px" },
            mrn: { title: "MRN", type: "string", width: "180px" },
            applicationDate: { title: "Application Date", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            caidNum: { title: "Caid Num", type: "string", width: "180px" },
            placeDate: { title: "Place Date", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            postType: { title: "Post Type", type: "string", width: "180px" },
            sex: { title: "Sex", type: "string", width: "180px" },
            dischDate: { title: "Discharge Date", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            patPhone: { title: "Phone", type: "string", width: "180px" },
            dob: { title: "DOB", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            ssn: { title: "SSN", type: "string", width: "180px" },
            minor: { title: "Minor", type: "string", width: "100px" },
            patAddress: { title: "Address", type: "string", width: "250px" },
            patState: { title: "State", type: "string", width: "180px" },
            patZip: { title: "Zip", type: "string", width: "180px" },
            respWPhone: { title: "Work Phone", type: "string", width: "180px" },
            respCellPhone: { title: "Cell Phone", type: "string", width: "180px" },
            finClass: { title: "FC", width: "100px", template: finClassTemplate },
            prevFinClass: { title: "Prev FC", type: "string", width: "100px" },
            placeAmount: { title: "Place Amount", type: "number", width: "180px", format: "{0:c}" },
            paymentStartDate: { title: "Payment Start Date", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            paymentLastDate: { title: "Payment Last Date", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            paymentAmount: { title: "Payment Amount", type: "number", width: "100px", format: "{0:c}" },
            returnDate: { title: "Return Date", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            prevStatus: { title: "Prev Status", type: "number", width: "100px", format: "{0:000}" },
            daysIn300: { title: "Days in 300", type: "number", width: "120px" },
            daysIn500: { title: "Days in 500", type: "number", width: "120px" },
            daysIn700: { title: "Days in 700", type: "number", width: "120px" },
            date800: { title: "800 Date", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            letter: { title: "QLetter", width: "100px", attributes: { style: "text-align: center;" }, template: qletterTemplate },
            printQ: { title: "Print Q Letter", type: "date", width: "100px", format: "{0:MM-dd-yyyy}" },
            hospAlert: { title: "HAlert", width: "100px", type: "boolean", attributes: { style: "text-align: center;" }, template: hospAlertTemplate },
            managerStatus: { title: "Manager", width: "100px", type: "boolean", attributes: { style: "text-align: center;" }, template: managerTemplate },
            reconBalance: { title: "Recon Balance", type: "number", width: "180px", format: "{0:c}" },
            reconDiff: { title: "Recon Balance Mismatch", width: "230px", type: "boolean", attributes: { style: "text-align: center;" }, template: reconDiff },
            noReconBalance: { title: "No Recon Balance", width: "190px", type: "boolean", attributes: { style: "text-align: center;" }, template: noReconBalance },
            transfer: { title: "Transfer", width: "120px", type: "boolean", attributes: { style: "text-align: center;" }, template: transfer },
            status800Recon: { title: "800+ Recon Status", width: "250px", type: "boolean", attributes: { style: "text-align: center;" }, template: status800Recon },
            finClassChange: { title: "Finclass Change", width: "250px", type: "boolean", attributes: { style: "text-align: center;" }, template: finClassChange },
            
        };

        var getSelectedIds = function (listview) {
            return $(listview)
                .find(".k-state-selected")
                .map(function () {
                    return $(this).attr("data-item-id");
                })
                .get();
        };

        var selectIds = function (listview, ids) {
            $(listview)
                .find(".fe-list-item")
                .each(function () {
                    ids.indexOf($(this).attr("data-item-id")) === -1
                        ? $(this).removeClass("k-state-selected")
                        : $(this).addClass("k-state-selected")
                });
        };

        var filters = ko.mapping.fromJS({
            clientSearch: "ALL",
            allCodes: true,
            spanish: false,
            afterHours: false,
            fieldVisit: false,
            everHadStatus: false,
            agedUnder20: false,
            postingsOnly: false,
            dateSearch: "PLACEMENT",
            dateRangeType: "DAYS",
            days: 0,
            startDate: null,
            endDate: null,
            exportOption: "NONE",
        });

        var getRequestData = function () {
            return $.extend(ko.mapping.toJS(filters), {

                clientNums: filters.clientSearch() === "ALL"
                    ? clients.map(function (client) { return client.ClientNum; })
                    : filters.clientSearch() === "CURRENT"
                        ? [appsecurity.getUserClientNumber()]
                        : getSelectedIds("#fe-client-list"),

                statusCodes: filters.allCodes()
                    ? codes.map(function (code) { return code.CcCode; })
                    : getSelectedIds("#fe-codes-list")
            });
        };

        selectedStatusCodeListId.subscribe(function (listId) {
            $("#fe-client-list").data("kendoListView").dataSource.read();
            $("#fe-codes-list").data("kendoListView").dataSource.read();
        });

        return {
            filters: filters,
            currentClient: currentClient,

            statusCodeListIds: statusCodeListIds,
            selectedStatusCodeListId: selectedStatusCodeListId,

            clients: new kendo.data.DataSource({
                transport: {
                    read: function (operation) {
                        server
                            .get(routes.getUserClients, { userName: appsecurity.getUserInfo().userName, hospCode: "" })
                            .then(function (result) {
                                clients = JSON.parse(result).filter(function (client) {
                                    return client.StatusCodeListId === selectedStatusCodeListId();
                                });
                                operation.success(clients);
                            });
                    }
                }
            }),

            addMultiComment: function () {
                var patients = patientsGrid.dataSource.data()
                    .map(function (patient) {
                        return patient.patientNum;
                    });
                if (patients.length > 0){
                    commentModal.open("Multi Status Update", "Multi Status Update", null, patients);
                }
                else {
                    logger.logError("No Patients Selected", "Error", null, true);
                }
            },

            codes: new kendo.data.DataSource({
                transport: {
                    read: function (operation) {
                        server
                            .get(routes.getCommCodesByListId, { listId: selectedStatusCodeListId() })
                            .then(function (result) {
                                codes = JSON.parse(result);
                                operation.success(codes);
                            });
                    }
                }
            }),

            searchPatients: function () {

                switch (filters.exportOption()) {
                    case "MAXBATCH":

                        server
                            .post(routes.generateMaxBatch, getRequestData())
                            .then(function (result) {
                                logger.log(result + " MaxBatch file(s) had been generated", "MaxBatch Export", null, true);
                            });

                        break;

                    case "270":

                        server
                            .post(routes.generate270, getRequestData())
                            .then(function (result) {
                                logger.log(result + " 270 file(s) had been generated", "270 Export", null, true);
                            });

                        break;

                    default:

                        patientsGrid.dataSource.read();
                        patientsGrid.resize();
                }
            },
            deleteRows: function () {
                gridhelpers.deleteSelectedRows(patientsGrid, "patientNum");
            },
            openColumnSelector: function () {
                columnSelector.open(patientsGrid);
            },
            saveFiltersTemplate: function () {
                inputModal
                    .open("Template Name", "Please enter a name")
                    .then(function (result) {
                        if (!result.hasInput || !result.input) return;

                        server.post(routes.saveTemplate, {
                            name: result.input,
                            parentName: "rep-status-special",
                            type: "filter-settings",
                            template: JSON.stringify($.extend(getRequestData(), {
                                gridOptions: patientsGrid.getOptions()
                            }))
                        });
                    });
            },
            loadFilterTemplate: function () {
                var modal = this;

                server
                    .get(routes.getTemplates, {
                        parentName: "rep-status-special",
                        type: "filter-settings"
                    })
                    .then(function (templates) {
                        selectModal
                            .open("Template", "Please select a template", {
                                textField: "name",
                                valueField: "template",
                                items: templates,
                                deleteItem: function (template) {
                                    server.remove(routes.deletetemplate, { id: template.id });
                                }
                            })
                            .then(function (result) {
                                if (!result.hasSelectedItem || !result.selectedItem) return;

                                var template = JSON.parse(result.selectedItem.template);

                                ko.mapping.fromJS(template, modal.filters);

                                if (modal.filters.clientSearch() === "SELECT") selectIds("#fe-client-list", template.clientNums);
                                if (!modal.filters.allCodes()) selectIds("#fe-codes-list", template.statusCodes);

                                patientsGrid.setOptions(template.gridOptions);
                            });
                    });
            },
            compositionComplete: function () {

                patientsGrid = $("#patients-grid").kendoGrid({
                    dataSource: {
                        transport: {
                            read: function (operation) {
                                server
                                    .post(routes.searchPatients, getRequestData())
                                    .then(operation.success, operation.error);
                            }
                        },
                        schema: {
                            model: {
                                id: "patientNum",
                                fields: fields
                            }
                        },
                        pageSize: 100
                    },
                    toolbar: ["excel"],
                    excel: { fileName: "patients.xlsx", allPages: true },
                    selectable: "multiple",
                    sortable: true,
                    filterable: true,
                    resizable: true,
                    reorderable: true,
                    autoBind: false,
                    pageable: {
                        pageSizes: true,
                        buttonCount: 5,
                        pageSizes: [100, 500, 1000, "All"]
                    },
                    columns: Object
                        .keys(fields)
                        .map(function (field) {
                            return {
                                field: field,
                                title: fields[field].title,
                                width: fields[field].width,
                                format: fields[field].format,
                                template: fields[field].template,
                                attributes: fields[field].attributes
                            };
                        })
                }).data("kendoGrid");

                $("#patients-grid").on("dblclick", "tr.k-state-selected", function () {
                    var patient = patientsGrid.dataItem(patientsGrid.select());

                    appsecurity.setUserClient(patient.hospCode, patient.clientNum, patient.phyName);

                    router.navigate(appsecurity.getUserTemplate() === "emr"
                        ? "Patient/Emeric/" + patient.hospAcct
                        : "Patient/HFMI/" + patient.patientNum);
                });

                app.on("view:changeclientinfo").then(function (clientInfo) {
                    currentClient(appsecurity.getUserClientNumber());
                });
            }
        }
    });