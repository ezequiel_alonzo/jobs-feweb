﻿define(["durandal/app", "plugins/router", "services/server", "services/routeconfig", "services/appsecurity", "modals/clientselector/index"],
    function (app, router, server, routes, appsecurity, clientSelector) {

        var isCheckedIn = ko.observable();
        var currentTemplate = ko.observable(appsecurity.getUserTemplate());
        checkingRemind = false;
        return {
            router: router,
            isCheckedIn: isCheckedIn,
            currentTemplate: currentTemplate,
            clientSelector: clientSelector,
            appsecurity: appsecurity,

            activate: function () {
                return router.map([
                    { route: "", moduleId: "noclient/index", title: "Select Client", nav: false },
                    { route: "Patient/HFMI(/:patientNum)", moduleId: "patient/demographic/hfmi/index", title: "Patient", nav: true, iconClass: "glyphicon glyphicon-user", onlyFor: "hfmi", hash: "#Patient/HFMI", permission: "view:patient" },
                    { route: "Patient/Emeric(/:hospAcct)", moduleId: "patient/demographic/emeric/index", title: "Claimant", nav: true, iconClass: "glyphicon glyphicon-user", onlyFor: "emr", hash: "#Patient/Emeric", permission: "view:patient" },
                    { route: "RepStatus/HFMI", moduleId: "repstatus/hfmi/index", title: "Work List", nav: true, iconClass: "glyphicon glyphicon-tasks", onlyFor: "hfmi", permission: "view:rep status list" },
                    { route: "RepStatus/Emeric", moduleId: "repstatus/index", title: "Rep Status", nav: true, iconClass: "glyphicon glyphicon-tasks", onlyFor: "emr", permission: "view:rep status list" },
                    { route: "RepStatus/Special", moduleId: "repstatus/special/index", title: "Special", nav: true, iconClass: "glyphicon glyphicon-list-alt", onlyFor: false, permission: "view:rep status special" },
                    { route: "Reports", moduleId: "reports/index", title: "Reports", nav: true, iconClass: "glyphicon glyphicon-stats", onlyFor: false, permission: "view:reports" },
                    { route: "Tools*Details", moduleId: "tools/index", title: "Tools" }
                ])
                .buildNavigationModel()
                .activate();
            },

            compositionComplete: function () {
                server
                    .get(routes.isCheckedIn, JSON.parse(appsecurity.getUserModel()))
                    .then(function (response, error, statusText) {
                        isCheckedIn(response);
                    });
                
                if (currentTemplate() !== "emr" && currentTemplate() !== "hfmi")
                    clientSelector.open();

                app.on("view:changeclientinfo").then(function (clientInfo) {

                    if (!clientInfo) {
                        currentTemplate(null);
                        router.navigate("");

                        return;
                    }

                    currentTemplate(clientInfo.template);

                    var viewConfig = router.activeInstruction().config;

                    if (viewConfig.route === "" || (viewConfig.onlyFor && viewConfig.onlyFor !== currentTemplate())) {
                        router.navigate(currentTemplate() === "hfmi" ? 'Patient/HFMI' : "Patient/Emeric");
                    }
                });
               
            },

            updateCheckinStatus: function () {
                server
                    .post(routes.updateCheckInstatus, { userName: appsecurity.getUserInfo().userName, isCheckedIn: isCheckedIn()})
                    .then(function () {
                        isCheckedIn(!isCheckedIn());
                    });
            },

            getUserDisplayName: function () {
                return appsecurity.getUserInfo().displayName;
            },

            checkViewAccess: function (view) {
                return (!view.permission || appsecurity.hasPermission(view.permission))
                    && (!view.onlyFor || view.onlyFor === currentTemplate());
            }
        };
    });