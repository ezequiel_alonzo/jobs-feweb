﻿define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger", "modals/selectmodal/index", "services/gridhelpers", "moment"],
    function (router, $, app, appsecurity, routeconfig, utils, logger, selectmodal, gridhelpers, moment) {

        var checkinByIpGrid;

        var checkinbyipDataSource =
            new kendo.data.DataSource({
                pageSize: 20,
                transport: {

                    read: function (operation) {

                        $.ajax(routeconfig.getCheckinListByIp, {
                            type: "GET",
                            dataType: "json",
                            data: {userName : appsecurity.getUserInfo().userName },
                            success: function (data) {
                                operation.success(data.map(function (item) {
                                    return {
                                        username: item.username,
                                        datein: item.datein ? moment(item.datein).format("MM-DD-YYYY h:mm:ss a") : "",
                                        dateout: item.dateout ? moment(item.dateout).format("MM-DD-YYYY h:mm:ss a") : "",
                                        ipaddress: item.ipaddress
                                    }
                                }));
                            }
                        });
                    }
                }
            });



        function buildCheckinByIpGrid() {

            checkinByIpGrid = $("#checkin-by-ip-grid").kendoGrid({
                columns: [
                   { field: "username", title: "Username", width: "100px" },
                   { field: "datein", title: "Checked in", width: "180px" },
                   { field: "dateout", title: "Checked Out", width: "180px" },
                   { field: "ipaddress", title: "Last IP" }
                ],
                dataSource: checkinbyipDataSource,
                sortable: true,
                toolbar: ["excel"],
                excel: { fileName: "checkinbyip.xlsx", allPages: true },
                resizable: true,
                pageable: true,
                selectable: "multiple row",
                pageSize: 20,

                autoBind: false,
            }).data("kendoGrid");
        }

        function reloadContent() {
            checkinbyipDataSource.read();
        }

        

        function initControls() {
            buildCheckinByIpGrid();
            checkinbyipDataSource.read();

        }

        var viewmodel = {

            activate: function () {
                initControls();

            },
            compositionComplete: function () {
                initControls();
            },

            //Functions
            reloadContent: reloadContent

        };

        return viewmodel;

    });

