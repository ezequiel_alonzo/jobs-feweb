﻿define(["plugins/router", "services/appsecurity"], function (router, appsecurity) {
    return {
        checkViewAccess2: function (view) {
            return (!view.permission || appsecurity.hasPermission(view.permission))
        },
        router: router
            .createChildRouter()
            .makeRelative({
                moduleId: "tools",
                fromParent: true
            })
            .map([
               // { route: "WorkLists", moduleId: "worklists/index", title: "Work Lists Manager", nav: true },
                { route: "Files", moduleId: "files/index", title: "File Manager", nav: true },
                { route: "Imports", moduleId: "imports/index", title: "Imports", nav: true, permission: "view:imports" },
                { route: "CallCounter", moduleId: "callcounter/index", title: "Call Counter", nav: true },
                { route: "UserSettings", moduleId: "usersettings/index", title: "User Settings", nav: true },
                { route: "CheckInByIP", moduleId: "checkinbyip/index", title: "CheckIn List", nav: true },
                { route: "Employees", moduleId: "employees/index", title: "Employees", nav: true, permission: "view:employee" }
            ])
            .buildNavigationModel()
        
    };
});
