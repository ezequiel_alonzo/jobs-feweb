﻿define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger", "modals/selectmodal/index"],
    function (router, $, app, appsecurity, routeconfig, utils, logger, selectmodal ) {

        var fileTypeToRead = ko.observable("letters");

        var filesDataSource =
            new kendo.data.DataSource({
                pageSize: 10,
                transport: {
                    read: function (operation) {

                        $.ajax(routeconfig.getFilesList, {
                            type: "GET",
                            dataType: "json",
                            data: {
                                hospCode: appsecurity.getUserHospCode(),
                                files: fileTypeToRead(),
                            },
                            success: function (data) {
                                operation.success(data);
                            },
                            cache: false
                        });
                    }
                }
            });


        var buildFileTypes = function () {
            
            $("#tools-filetypes-dropdown").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select file type",
                dataSource: [
                    { text: "Letters", value: "letters" },
                    { text: "270", value: "270" },
                    { text: "MaxBatch", value: "maxbatch" }],
                change: function (e) {
                    fileTypeToRead(e.sender.dataItem(e.sender.select()).value);
                    filesDataSource.read();
                }
            });
        }
        function deleteFile(e) {
            e.preventDefault();
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

            $.ajax(routeconfig.deleteFile, {
                type: "GET",
                dataType: "json",
                data: { fileLocation: dataItem.basePath + dataItem.fileName },
                success: function (data, status, xhr) {
                    filesDataSource.read();
                }
            });
        }

        function buildLetterFilesListGrid() {

            $("#files-manager-grid").kendoGrid({
                columns: [
                   { field: "fileName", title: "File Name" },
                   { command: { text: "Download", click: downloadFile }, title: " ", width: "100px" },
                   { command: { text: "Delete", click: deleteFile }, title: " ", width: "85px" }],
                dataSource: filesDataSource,
                sortable: true,
                resizable: true,
                pageable: true,
                pageSize: 10,
                editable: true,
                autoBind: false,
            }).data("kendoGrid");
        }

        function downloadFile(e) {
            e.preventDefault();
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            
            window.open(dataItem.baseUrl)
        }

        function initControls() {
            buildLetterFilesListGrid();
            buildFileTypes();
        }

        var viewmodel = {

            activate: function () {
                app.on("view:changeclientinfo").then(function (clientInfo) {
                    initControls();
                    filesDataSource.read();
                });


            },
            attached: function () {
                initControls();
            },

            //Functions
            
        };

        return viewmodel;

    });
