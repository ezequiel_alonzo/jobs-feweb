﻿define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger", "modals/selectmodal/index", "services/gridhelpers"],
    function (router, $, app, appsecurity, routeconfig, utils, logger, selectmodal, gridhelpers) {

        var currentTab = ko.observable('security');
        

        var securityInfo = ko.mapping.fromJS({
            userName: "",
            oldPassword: "",
            newPassword: "",
            confirmNewPassword: "",
        });

        function updateSecuritySettings() {
            if (!securityInfo.oldPassword()) {
                logger.log("Old password can not be empty","password",null, true);
                return;
            }
            if (!securityInfo.newPassword()) {
                logger.log("New password can not be empty", "password", null, true);
                return;
            }
            if (!securityInfo.confirmNewPassword()) {
                logger.log("Please confirm the new password", "password", null, true);
                return;
            }

            $.ajax(routeconfig.updatePassword, {
                type: "POST",
                dataType: "json",
                data: ko.mapping.toJS(securityInfo),
                success: function (jqXhr, error, statusText) {
                    if(!jqXhr){
                        app.showMessage("Password Updated");
                        securityInfo.oldPassword("");
                        securityInfo.newPassword("");
                        securityInfo.confirmNewPassword("");
                    }
                    else {
                        logger.log(JSON.parse(jqXhr).Message,"password", null, true);
                    }
                },
                error: function (jqXhr, error, statusText) {

                    
                }
            });

            
        }


        function initControls() {
            securityInfo.userName(appsecurity.getUserInfo().userName);
            //buildCallCounterGrid();
            //callCounterDataSource.read();

        }



        var viewmodel = {

            activate: function () {
                //app.on("view:changeclientinfo").then(function (clientInfo) {
                //    callCounterDataSource.read();
                //});


            },
            attached: function () {
                initControls();
            },
            //variables
            currentTab: currentTab,
            securityInfo:securityInfo,

            //Functions
            updateSecuritySettings: updateSecuritySettings
            //deleteRows: deleteRows,
            //reloadContent: reloadContent
            

        };

        return viewmodel;

    });