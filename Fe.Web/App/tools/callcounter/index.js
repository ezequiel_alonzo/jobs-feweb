﻿define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger", "modals/selectmodal/index", "services/gridhelpers"],
    function (router, $, app, appsecurity, routeconfig, utils, logger, selectmodal, gridhelpers) {

        var callCounterGrid;

        var callCounterDataSource =
            new kendo.data.DataSource({
                pageSize: 20,
                autoSync: true,
                schema: {
                    model: {
                        fields: {
                            numleft: { type: "number" },
                            time: { type: "string" },
                            campaign: { type: "string" }
                        }
                    }
                },
                aggregate: [{ field: "numleft", aggregate: "sum" }],
                transport: {

                    read: function (operation) {

                        $.ajax(routeconfig.getCallCounter, {
                            type: "GET",
                            dataType: "json",
                            
                            success: operation.success,
                            cache: false
                        }
                        );
                        }
                }
            });



        function buildCallCounterGrid() {

             callCounterGrid = $("#callcounter-grid").kendoGrid({
                columns: [
                   { field: "numleft", title: "Num Left", width: "100px", aggregates: ["sum"], footerTemplate: "Total: #=sum#" },
                   { field: "time", title: "Time", width: "180px" },
                   { field: "campaign", title: "Campaign" }
                ],
                dataSource: callCounterDataSource,
                sortable: true,
                toolbar: ["excel"],
                excel: { fileName: "callcounter.xlsx", allPages: true },
                resizable: true,
                pageable: true,
                selectable: "multiple row",
                pageSize: 20,
                
                autoBind: false,
            }).data("kendoGrid");
        }

        function reloadContent() {
            callCounterDataSource.read();
        }

        function deleteRows() {
            gridhelpers.deleteSelectedRows(callCounterGrid, "campaign");
        }
        
        function initControls() {
            buildCallCounterGrid();
            callCounterDataSource.read();
            
        }

        var viewmodel = {

            activate: function () {
                initControls();
                app.on("view:changeclientinfo").then(function (clientInfo) {
                    callCounterDataSource.read();
                });


            },
            attached: function () {
                initControls();
            },

            //Functions
            deleteRows: deleteRows,
            reloadContent: reloadContent

        };

        return viewmodel;

    });
