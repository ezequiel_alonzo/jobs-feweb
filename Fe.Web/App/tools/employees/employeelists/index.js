﻿define(['plugins/dialog', 'patient/demographic/hfmi/index', 'durandal/app', 'services/appsecurity', 'services/routeconfig'], function (dialog, patient, app, appsecurity, routeconfig) {
    var employeeListsResults = function (title) {
        this.title = title;

    };
    var resetViewModel = function (viewModel) {
        for (var prop in viewModel) {
            if (viewModel.hasOwnProperty(prop) && ko.isObservable(viewModel[prop])) {
                viewModel[prop]("");
            }
        }
    };
    var listType = ko.observable('ALL');

    var employeeListDataSource = new kendo.data.DataSource({
        schema:  {
            model: {
                fields: {
                    userName: { type: "string" },
                    startDate: { type: "date" },
                    personalRate: { type: "string" },
                    vacationRate: { type: "string" },
                    lastChanged: { type: "date" },
                    location: { type: "string" },
                    rate: { type: "string" },
                    supvName: { type: "string" },
                    type: { type: "string" },
                    region: { type: "string" },
                    coachingFreq: { type: "string" },
                    termDate: { type: "date" },
                    payType: { type: "string" },
                    lastName: { type: "string" },
                    firstName: { type: "string" },
                }
            }
    },
        transport: {

            read: function (operation) {

                $.ajax(routeconfig.getEmployeeInfo, {
                    type: "GET",
                    dataType: "json",
                    data: { employeeId: listType },
                    success: 
                        operation.success
                });
            }
        },
        pageSize: 10
    });


    employeeListsResults.prototype.attached = function () {

        var modal = this;
        
        var cols = [{ field: "userName", title: "Empl", width: "80px" },
            { title: "Name", width: "125px", template: "#= lastName #, #=firstName #" },
            {field: "startDate", title: "Start Date", width: "125px", type: "date", format: "{0:MM-dd-yyyy}"},
            { field: "personalRate", title: "Pers.Rate", width: "125px" },
            { field: "vacationRate", title: "Vac. Rate", width: "100px" },
            { field: "lastChanged", title: "Last Changed", width: "125px", type: "date", format: "{0:MM-dd-yyyy}" },
            { field: "location", title: "location", width: "100px" },
            { field: "rate", title: "Rate", width: "70px" },
            { field: "supvName", title: "Supv", width: "70px" },
            { field: "type", title: "F/P", width: "70px" },
            { field: "region", title: "region", width: "90px" },
            { field: "coachingFreq", title: "CouchHrs", width: "90px" },
            { field: "termDate", title: "Term Date", width: "125px", type: "date", format: "{0:MM-dd-yyyy}"},
            { field: "payType", title: "PayType", width: "90px" },


        ];

        $('#fe-employees-grid-container').show();

        $("#fe-employees-grid").kendoGrid({
            columns: cols,
            dataSource: employeeListDataSource,
            height: 300,
            selectable: "row",
            sortable: true,
            resizable: true,
            pageable: true,
            //change: function () {

            //    var currentDataItem = $("fe-employees-grid").data("kendoGrid").dataItem(this.select())

            //    if (currentDataItem.ClientNum !== appsecurity.getUserClientNumber()) {
            //        appsecurity.setUserClient(currentDataItem.ActHospCode, currentDataItem.ClientNum, appsecurity.getUserClientName(), "hfmi", false);
            //    }

            //    app.trigger("patient:getpatientdetailshfmi", { PatNum: currentDataItem.PatNum });
            //    dialog.close(modal);
            //}
        }).data("kendoGrid");
    }


    employeeListsResults.prototype.close = function () {
        dialog.close(this, this.model);
    };

    employeeListsResults.show = function (type) {
        listType(type);
        employeeListDataSource.read();
        return dialog.show(new employeeListsResults('Search Results'));
    };

    return employeeListsResults;
});
