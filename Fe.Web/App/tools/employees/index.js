﻿/// <reference path="../../services/routeconfig.js" />
define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger", "modals/selectmodal/index", "moment", "tools/employees/employeelists/index"],
    function (router, $, app, appsecurity, routeconfig, utils, logger, selectmodal, moment,employeelists) {


        var currentTab = ko.observable("payrollinfo");
        var currentEmployee = ko.mapping.fromJS({
            userName: ""
            , firstName: ""
            , lastName: ""
            ,startDate: ""
            ,personalRate: ""
            ,vacationRate: ""
            ,type: ""
            ,lastChanged: ""
            ,percentage: ""
            ,location: ""
            ,rate: ""
            , supvName: ""
            , region: ""
            , coachingFreq: ""
            , termDate: ""
            , payType: ""
            , payrollEmail: ""
            , msgText:""
            , hospClientNum:""
            , alertFlag:""
            , trainer:""
            , payChexEmpNum:""
            , mgrCoach: ""
            , phoneRptFlag: ""
            , cardName: ""
            , cardTitle: ""
            , card1Left: ""
            , card2Left: ""
            , card3Left: ""
            , card4Left: ""
            , card1Right: ""
            , card2Right: ""
            , card3Right: ""
            , card4Right: ""
            , name :""
            , addr1 :""
            , addr2 :""
            , city :""
            , state :""
            , zipcode :""
            , ssn :""
            , drivLic :""
            , hPhone :""
            , wPhone :""
             ,dob :""
            , eContact :""
            , ePhone :""
            , miscInfo :""
        });

        var resetViewModel = function (viewModel) {
            for (var prop in viewModel) {
                if (viewModel.hasOwnProperty(prop) && ko.isObservable(viewModel[prop])) {
                    viewModel[prop]("");
                }
            }
        };


        var regionTypes = [{ text: "01:LAKER", value: "01" },
                { text: "10:ADMIN", value: "10" },
                { text: "20:TAMMY", value: "20" },
                { text: "30:", value: "30" },
                { text: "35:JODI", value: "35" },
                { text: "40:CINDY", value: "40" },
                { text: "50:WENDY", value: "50" },
                { text: "60:RIKKI", value: "60" },
                { text: "70:WAYNE", value: "70" },
                { text: "75:", value: "75" },
                { text: "80:WENDY", value: "80" },
                { text: "83:JAN", value: "83" },
                { text: "85:MARK", value: "85" },
                { text: "88:**INACTIVE**", value: "88" },
                { text: "90:WENDY", value: "90" },
                { text: "95:**INACTIVE**", value: "95" },
                { text: "99:TEST", value: "99" },
        ];

        var phoneRepTypes = [{ text: "0:ADMIN", value: "0" },
                { text: "1:NOBLE 9", value: "1" },
                { text: "2:REP", value: "2" }
        ];
        var userListDataSource;
   

    function userListType(){
        if (appsecurity.hasPermission('view:admin')) {
            userListDataSource = new kendo.data.DataSource({
                transport: {

                    read: function (operation) {

                        $.ajax(routeconfig.getpostinguserlist, {
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                operation.success(JSON.parse(data).map(function (item) {
                                    return { text: item, value: item };
                                }))
                            }
                        });
                    }
                }
            })
        }
        else {
            userListDataSource = new kendo.data.DataSource({
                data: [{ text: appsecurity.getUserInfo().userName.toUpperCase(), value: appsecurity.getUserInfo().userName.toUpperCase() }]
            });
        }
        }


        function OpenEmployeeListAll() {
            employeelists.show('ALL');
        }

        function OpenEmployeeListAct() {
            employeelists.show('ACT');
        }


        function currentEmployeeUpdate(employeeId) {
            $.ajax(routeconfig.getEmployeeInfo, {
                type: "GET",
                dataType: "json",
                data: { employeeId: employeeId },
                success: function (data) {
                    
                    if (data.length > 0) {
                        data[0].startDate ? data[0].startDate = moment(data[0].startDate).format("MM-DD-YYYY") : "";
                        data[0].termDate ? data[0].termDate = moment(data[0].termDate).format("MM-DD-YYYY") : "";
                        data[0].lastChanged ? data[0].lastChanged = moment(data[0].lastChanged).format("MM-DD-YYYY") : "";
                        data[0].dob ? data[0].dob = moment(data[0].dob).format("MM-DD-YYYY") : "";
                        data[0].alertFlag ? data[0].alertFlag = true : data[0].alertFlag = false;
                        data[0].trainer ? data[0].trainer = true : data[0].trainer = false;
                        data[0].mgrCoach ? data[0].mgrCoach = true : data[0].mgrCoach = false;

                        ko.mapping.fromJS(data[0], currentEmployee);

                        $("#employee-region-dropdown").data("kendoDropDownList").value(data[0].region);
                        $("#phonerep-type-dropdown").data("kendoDropDownList").value(data[0].phoneRptFlag);
                    }
                    else {
                        resetViewModel(currentEmployee);
                    }
                }
            });
        }
        
        function saveEmployeeInfo() {
            var uInfo = ko.mapping.toJS(currentEmployee);
            if (uInfo.startDate) uInfo.startDate = moment(uInfo.startDate, ['MM/DD/YYYY', 'MMDDYYYY']).format("MM-DD-YYYY");
            if (uInfo.termDate) uInfo.termDate = moment(uInfo.termDate, ['MM/DD/YYYY', 'MMDDYYYY']).format("MM-DD-YYYY")
            if (uInfo.trainer) uInfo.trainer = 'Y';
            if (uInfo.mgrCoach) uInfo.mgrCoach = 'Y';
            if (uInfo.alertFlag) uInfo.alertFlag = 'Y';
            uInfo= utils.compactObject(uInfo);

            $.ajax(routeconfig.UpdateEmployeeInfo, {
                type: "POST",
                dataType: "json",
                data: uInfo,
                success: function (data) {
                    logger.log(" Employee Updated Correctly", "Employee", null, true);
                }
            });
        }

        function initControls() {

            userListDataSource.read();
            $("#tools-userlist-dropdown").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: userListDataSource,
                optionLabel: "--Select --",
                change: function (e) {
                    resetViewModel(currentEmployee);
                    currentEmployeeUpdate(e.sender.dataItem(e.sender.select()).value);
                }
            });

            
            $("#employee-region-dropdown").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: regionTypes,
            });

            $("#phonerep-type-dropdown").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: phoneRepTypes,
            });

        }

        var viewmodel = {

            activate: function () {
                userListType();
                initControls();

            },
            attached: function () {
                userListType();
                initControls();
            },

            //Functions

            //Variables 
            userListType:userListType,
            currentTab: currentTab,
            currentEmployee: currentEmployee,
            OpenEmployeeListAll: OpenEmployeeListAll,
            OpenEmployeeListAct: OpenEmployeeListAct,
            saveEmployeeInfo: saveEmployeeInfo
        };

        return viewmodel;

    });