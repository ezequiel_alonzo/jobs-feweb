﻿//******************************
// File: imports/index.js
//******************************

define(["services/routeconfig", "services/logger", "services/utils", "services/appsecurity", "jqueryFileUpload"], function (routeconfig, logger, utils, appsecurity) {

    var statusTemplate =
        "#if (!hospAcct) {#<span class=\"glyphicon glyphicon-warning-sign\" title=\"#=importMessage#\" />#} " +
        "else if (importStatus === 'PENDING') {#<span class=\"glyphicon glyphicon-time\" title=\"#=importMessage#\" />#} " +
        "else if (importStatus === 'SUCCESS') {#<span class=\"glyphicon glyphicon-ok\" title=\"#=importMessage#\" />#} " +
        "else if (importStatus === 'DUPLICATE') {#<span class=\"glyphicon glyphicon-warning-sign\" title=\"#=importMessage#\" />#} " +
        "else if (importStatus === 'ERROR') {#<span class=\"glyphicon glyphicon-remove\" title=\"#=importMessage#\" />#} #";

    var fields = {
        importStatus: { template: statusTemplate, width: "32px", title: " " },
        patientNum: { type: "string", field: "patientNum", width: "180px" },
        clientNum: { type: "string", field: "clientNum", width: "180px" },
        hospAcct: { type: "string", field: "hospAcct", width: "180px" },
        ssn: { type: "string", field: "ssn", width: "180px" },
        dob: { type: "date", format: "{0:MM-dd-yyyy}", field: "dob", width: "180px" },
        sex: { type: "string", field: "sex", width: "180px" },
        lastName: { type: "string", field: "lastName", width: "180px" },
        firstName: { type: "string", field: "firstName", width: "180px" },
        patPhone: { type: "string", field: "patPhone", width: "180px" },
        patAddress: { type: "string", field: "patAddress", width: "180px" },
        patCity: { type: "string", field: "patCity", width: "180px" },
        patState: { type: "string", field: "patState", width: "180px" },
        patZip: { type: "string", field: "patZip", width: "180px" },
        empName: { type: "string", field: "empName", width: "180px" },
        empPhone: { type: "string", field: "empPhone", width: "180px" },
        empAddress: { type: "string", field: "empAddress", width: "180px" },
        empCity: { type: "string", field: "empCity", width: "180px" },
        empState: { type: "string", field: "empState", width: "180px" },
        empZip: { type: "string", field: "empZip", width: "180px" },
        respName: { type: "string", field: "respName", width: "180px" },
        respPhone: { type: "string", field: "respPhone", width: "180px" },
        respAddress: { type: "string", field: "respAddress", width: "180px" },
        respWPhone: { type: "string", field: "respWPhone", width: "180px" },
        respCity: { type: "string", field: "respCity", width: "180px" },
        respState: { type: "string", field: "respState", width: "180px" },
        respZip: { type: "string", field: "respZip", width: "180px" },
        minor: { type: "string", field: "minor", width: "180px" },
        placeDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "placeDate", width: "180px" },
        placeAmount: { type: "number", field: "placeAmount", width: "180px" },
        admitDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "admitDate", width: "180px" },
        dischDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "dischDate", width: "180px" },
        caidNum: { type: "string", field: "caidNum", width: "180px" },
        bDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "bDate", width: "180px" },
        ep: { type: "string", field: "ep", width: "180px" },
        cbDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "cbDate", width: "180px" },
        cbUser: { type: "string", field: "cbUser", width: "180px" },
        currBalance: { type: "number", field: "currBalance", width: "180px" },
        inhdsch: { type: "string", field: "inhdsch", width: "180px" },
        currStatus: { type: "string", field: "currStatus", width: "180px" },
        remDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "remDate", width: "180px" },
        postedDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "postedDate", width: "180px" },
        amount: { type: "number", field: "amount", width: "180px" },
        payType: { type: "string", field: "payType", width: "180px" },
        postDesc: { type: "string", field: "postDesc", width: "180px" },
        relCode: { type: "string", field: "relCode", width: "180px" },
        relId: { type: "string", field: "relId", width: "180px" },
        lastChanged: { type: "date", format: "{0:MM-dd-yyyy}", field: "lastChanged", width: "180px" },
        periodStart: { type: "date", format: "{0:MM-dd-yyyy}", field: "periodStart", width: "180px" },
        periodEnd: { type: "date", format: "{0:MM-dd-yyyy}", field: "periodEnd", width: "180px" },
        periodBal: { type: "number", field: "periodBal", width: "180px" },
        visitType: { type: "string", field: "visitType", width: "180px" },
        finClassDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "finClassDate", width: "180px" },
        clientLoc: { type: "string", field: "clientLoc", width: "180px" },
        finClass: { type: "string", field: "finClass", width: "180px" },
        dcf: { type: "string", field: "dcf", width: "180px" },
        dcfDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "dcfDate", width: "180px" },
        managerStatus: { type: "string", field: "managerStatus", width: "180px" },
        hospAlert: { type: "string", field: "hospAlert", width: "180px" },
        mrn: { type: "string", field: "mrn", width: "180px" },
        insCode: { type: "string", field: "insCode", width: "180px" },
        actionType: { type: "string", field: "actionType", width: "180px" },
        spanish: { type: "string", field: "spanish", width: "180px" },
        afterHours: { type: "string", field: "afterHours", width: "180px" },
        fieldVisit: { type: "string", field: "fieldVisit", width: "180px" },
        transfer: { type: "string", field: "transfer", width: "180px" },
        starFlag: { type: "string", field: "starFlag", width: "180px" },
        webConfo: { type: "number", field: "webConfo", width: "180px" },
        webConfoFlag: { type: "string", field: "webConfoFlag", width: "180px" },
        respSsn: { type: "string", field: "respSsn", width: "180px" },
        prevFinClass: { type: "string", field: "prevFinClass", width: "180px" },
        altFinClass: { type: "string", field: "altFinClass", width: "180px" },
        currStatusQ: { type: "string", field: "currStatusQ", width: "180px" },
        printQ: { type: "date", format: "{0:MM-dd-yyyy}", field: "printQ", width: "180px" },
        preBadDebt: { type: "date", format: "{0:MM-dd-yyyy}", field: "preBadDebt", width: "180px" },
        connanceId: { type: "string", field: "connanceId", width: "180px" },
        applicationDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "applicationDate", width: "180px" },
        encounter: { type: "string", field: "encounter", width: "180px" },
        respCellPhone: { type: "string", field: "respCellPhone", width: "180px" },
        patAddress2: { type: "string", field: "patAddress2", width: "180px" },
        paymentStartDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "paymentStartDate", width: "180px" },
        paymentAmount: { type: "number", field: "paymentAmount", width: "180px" },
        paymentLastDate: { type: "date", format: "{0:MM-dd-yyyy}", field: "paymentLastDate", width: "180px" }
    };

    var importGrid;
    var progressBar;

    var mqMessages = [];
    var formData;

    var requestInProgress = ko.observable(false);
    var currentRequestId = ko.observable();
    var selectedClient = ko.observable();
    var selectedChopper = ko.observable();
    var selectedFile = ko.observable();
    var isRecon = ko.observable(false);

    currentRequestId.subscribe(function (requestId) {
        $("#import-select-client").data("kendoDropDownList").enable(!requestId);
    });

    var checkRequestProgress = function () {
        $.ajax(routeconfig.rpcMqReceiveAllMessages, {
            type: "POST",
            dataType: "json",
            data: {
                Exchange: "fe.import",
                Queue: "fe.chopper",
                RouteKey: currentRequestId()
            },
            success: function (messages) {
                messages = messages.map(function (message) {
                    return JSON.parse(message);
                });

                mqMessages = mqMessages.concat(messages);

                importGrid.dataSource.data(importGrid
                    .dataSource.data()
                    .map(function (patient) {
                        var patientMessage = messages.find(function (message) {
                            return message.hospAcct === patient.hospAcct;
                        });

                        if (patientMessage) {
                            patient.importStatus = patientMessage.status;
                            patient.importMessage = patientMessage.message || "";
                        };

                        return patient;
                    }));

                progressBar.value((100 / importGrid.dataSource.total()) * mqMessages.length)

                if (importGrid.dataSource.total() > mqMessages.length) {
                    setTimeout(checkRequestProgress, 3000);
                }
            },

            error: function (error) {
                logger.log("There was a problem getting the request progress: " + JSON.parse(error.responseText).exceptionMessage, "Import", null, true);

                progressBar.value(100);
                currentRequestId(null);
                requestInProgress(false);
            }
        });
    };

    return {

        selectedClient: selectedClient,
        selectedChopper: selectedChopper,
        selectedFile: selectedFile,
        currentRequestId: currentRequestId,
        requestInProgress: requestInProgress,
        isRecon: isRecon,

        compositionComplete: function () {

            importGrid = $("#fe-import-file-grid").kendoGrid({
                pageable: true,
                resizable: true,
                autoBind: false,
                columns: Object
                    .keys(fields)
                    .map(function (field) {
                        return {
                            field: field,
                            title: fields[field].title || field,
                            width: fields[field].width,
                            format: fields[field].format,
                            template: fields[field].template
                        };
                    }),
                dataSource: {
                    transport: {
                        read: function (operation) {
                           
                            var requestId = utils.generateGuid().toUpperCase();

                            formData.formData = {
                                ClientNum: selectedClient().ClientNum,
                                Chopper: selectedChopper(),
                                RequestId: requestId
                            };

                            formData
                                .submit()
                                .then(function (patients) {
                                    currentRequestId(requestId);

                                    operation.success(patients.map(function (patient) {
                                        patient.importStatus = "PENDING";
                                        patient.importMessage = "";
                                        return patient;
                                    }));
                                },
                                function (error) {
                                    logger.log(JSON.parse(error.responseText).exceptionMessage, "Import", null, true);

                                    currentRequestId(null);

                                    operation.error();
                                });
                        }
                    },
                    pageSize: 50,
                    schema: {
                        model: {
                            id: "hospAcct",
                            fields: fields
                        }
                    },
                }
            }).data("kendoGrid");

            progressBar = $("#fe-import-progress-bar").kendoProgressBar({
                type: "percent",
                animation: {
                    duration: 600
                },
                complete: function (e) {
                    requestInProgress(false);
                    currentRequestId(null);
                    mqMessages = [];
                }
            }).data("kendoProgressBar");

            $("#fe-import-form").fileupload({
                replaceFileInput: false,
                limitMultiFileUploads: 1,

                add: function (e, data) {
                    formData = data;
                }
            });
        },

        updateChopper: function () {
            selectedChopper(selectedClient() && selectedClient().Chopper);
        },

        previewFile: function () {
            if (!selectedClient() || !selectedChopper() || !selectedFile()) {
                logger.log("Must select a client, chopper and file.", "Validation", null, true);
                return;
            }

            progressBar.value(0);
            importGrid.dataSource.read();
        },

        importFile: function () {
            if (!selectedClient() || !selectedChopper() || !selectedFile()) {
                logger.log("Must select a client, chopper and file.", "Validation", null, true);
                return;
            }

            requestInProgress(true);

            $.ajax({
                type: "POST",
                url: "./api/import/processimportfile",
                data: {
                    clientNum: selectedClient().ClientNum,
                    chopperName: selectedChopper(),
                    requestId: currentRequestId(),
                    userName: appsecurity.getUserInfo().userName,
                    isRecon: isRecon()
                },
                success: function () {
                    checkRequestProgress();
                },
                error: function (error) {
                    logger.log(JSON.parse(error.responseText).exceptionMessage, "Import", null, true);

                    currentRequestId(null);
                    requestInProgress(false);
                }
            });
        },

        clients: new kendo.data.DataSource({
            transport: {
                read: function (operation) {
                    $.ajax(routeconfig.getUserChopperClients + "?userName=" + appsecurity.getUserInfo().userName, {
                        type: "GET",
                        dataType: "json",
                        success: function (result) {
                            operation.success(JSON.parse(result));
                        }
                    });
                }
            }
        }),

        reset: function () {
            currentRequestId(null);

            importGrid.dataSource.data([]);
        },

        browse: function () {
            $("#fe-browse-import-file").trigger("click");
        },

        readImportFile: function () {
            selectedFile($("#fe-browse-import-file").val().split("\\").pop());
        }
    };
});