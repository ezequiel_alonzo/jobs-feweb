requirejs.config({
    paths: {
        'text': '../Scripts/text',
        'durandal': '../Scripts/durandal',
        'plugins': '../Scripts/durandal/plugins',
        'transitions': '../Scripts/durandal/transitions',
        'moment': '../Scripts/moment.min',
        'jquery': '../Scripts/jquery-1.10.2',
        'jquery.ui': '../Scripts/jquery-ui-1.8.24',
        'jquery.ui.widget': '../Scripts/jquery.ui.widget',
        'jqueryFileUpload': '../Scripts/jquery.fileupload',
        'jqueryIframeTransport': '../Scripts/jquery.iframe-transport'
    },
    shims: {

        'jqueryIframeTransport': {
            deps: ['jquery'],
            exports: 'jQuery.jqueryIframeTransport'
        },
        'jqueryFileUpload': {
            deps: ['jquery', 'jquery.ui.widget', 'jqueryIframeTransport'],
            exports: 'jQuery.jqueryFileUpload'
        },
        'jquery.ui': {
            deps: ['jquery'],
            exports: 'jQuery.jquery.ui'
        }
    }
});

define('jquery', function () { return jQuery; });
define('knockout', ko);

define(['durandal/system', 'durandal/app', 'durandal/binder', 'durandal/viewLocator', 'services/appsecurity'],
    function (system, app, binder, viewLocator, appsecurity) {
        //>>excludeStart("build", true);
        system.debug(true);
        //>>excludeEnd("build");

        app.title = 'Fiscal Express';

        //$.ajax(routeconfig.instanceName, {
        //    type: "GET"
        //}).done(function (jqXhr) {
        //    app.title = jqXhr;
        //    $('head title', window.parent.document).text(jqXhr);
        //});

        app.configurePlugins({
            router: true,
            dialog: true
        });

        app.start().then(function () {

            kendo.ns = "kendo-";

            binder.binding = function (obj, view) {
                kendo.bind(view, obj.viewModel || obj);
            };

            // Configure ko validation
            ko.validation.init({
                decorateElement: true,
                errorElementClass: "has-error",
                registerExtenders: true,
                messagesOnModified: true,
                insertMessages: true,
                parseInputAttributes: true,
                messageTemplate: null
            });

            //Show the app by setting the root view model for our application with a transition.
            var uInfo = appsecurity.getUserInfo();
            if (uInfo) {
                if (uInfo.isAuthenticated) {
                    if (uInfo.changePassword) {
                        app.setRoot('account/login/index', 'entrance');
                    }
                    else {
                        app.setRoot('shell', 'entrance');
                    }
                }
                else {
                    app.setRoot('account/login/index', 'entrance');
                }
            }
            else {
                app.setRoot('account/login/index', 'entrance');
            }

        });

    });