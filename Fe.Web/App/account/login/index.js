//******************************
// File: account/login/index.js
//******************************

define(["plugins/router", "jquery", "durandal/app", "moment", "services/appsecurity", "services/routeconfig", "services/logger", "services/utils", "services/server"], function (router, $, app, moment, appsecurity, routeconfig, logger, utils,server) {

    //----------------------------------------------------------------------

    var userName = ko.observable();

    function initControls() {
        $("#fe-login-username").focus();
    }

    function login() {

        $("#fe-login-wait").css("display", "inline-block");

        var uName = $("#fe-login-username").val();

        var obj = {
            grant_type: "password",
            username: uName,
            password: $("#fe-login-pwd").val(),
            client_id: "feweb"
        };

        $.ajax(routeconfig.login, {
            type: "POST",
            dataType: "json",
            data: obj,
            success: function (jqXhr, error, statusText) {

                $("#fe-login-wait").hide();

                if (jqXhr) {
                    logger.logSuccess('Login Successful', jqXhr, null, false);
                    var IdentityServLoginData = jqXhr;

                    var permissionsRequest = $.ajax(routeconfig.getUserPermissions, {
                        type: "POST",
                        dataType: "json",
                        data: obj
                    });

                    var statusCodesListIdsRequest = $.ajax(routeconfig.getUserStatusCodeLists, {
                        type: "GET",
                        dataType: "json",
                        data: { userName: uName }
                    });

                    $.when(permissionsRequest, statusCodesListIdsRequest).done(function (permissions, statusCodeListIds) {
                        var perms = JSON.parse(permissions[0]);

                            if (perms) {
                                userName(uName);
                            appsecurity.setUserInfo(uName, statusCodeListIds[0], perms, true, false, IdentityServLoginData.access_token, false, IdentityServLoginData[".expires"], IdentityServLoginData.userModel);

                                router.reset();
                                router.deactivate();
                                
                                app.setRoot('shell', 'entrance');
                            }
                    })

                } else {
                    logger.logError(statusText, error, null, true);
                    $("#me-login-username").focus().select();
                }
            },
            error: function (jqXhr, error, statusText) {

                $("#fe-login-wait").hide();

                if (jqXhr) {
                    var jObj = utils.parseJson(jqXhr.responseText);

                    if (jObj !== false) {
                        var resp = jObj.error_description.split("\"").join("");
                        logger.logError(resp, error, null, true);
                    } else {
                        logger.logError(statusText, error, null, true);
                    }
                }
                $("#fe-login-username").focus().select();
            }
        });

    };

    //----------------------------------------------------------------------

    var viewmodel = {

        //State Functions
        canActivate: function () {
            return true;
        },
        activate: function () {

        },
        compositionComplete: function () {
            initControls();
        },

        //Variables

        //Functions
        login: login
    };

    return viewmodel;
});