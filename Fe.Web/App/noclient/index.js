﻿define(["plugins/router", "services/appsecurity"], function (router, appsecurity) {
    return {
        canActivate: function () {
            if (appsecurity.getUserTemplate() === "hfmi") return { redirect: "Patient/HFMI" };

            if (appsecurity.getUserTemplate() === "emr") return { redirect: "Patient/Emeric" };

            return true;
        }
    };
})