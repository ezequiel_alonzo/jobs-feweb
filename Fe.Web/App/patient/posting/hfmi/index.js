﻿define(['plugins/dialog', 'patient/demographic/hfmi/index', 'durandal/app', 'services/appsecurity', 'services/routeconfig', 'services/utils', 'services/logger', 'modals/commentmodal/index','moment'], function (dialog, patient, app, appsecurity, routeconfig, utils, logger, commentmodal,moment) {



    var patientNum = new ko.observable();
    var postingTypes = [{ value: "1", text: "PAYMENT TO HOSPITAL" },
                        { value: "3", text: "CONTRACTUAL ALLOWANCE" },
                        { value: "4", text: "MISC ADJUSTMENT" },
                        { value: "5", text: "HFMI - CASH COLLECTED" },
                        { value: "6", text: "POS HFMI COLLECTIONS" },
                        { value: "8", text: "SPECIAL CASH" },

    ];

    var posting = function (title) {
        this.title = title;

    };

    function submitPatientPosting(posting) {
        this.posting = posting
        posting.RemitDate = moment(posting.RemitDate, ['MM/DD/YYYY', 'MMDDYYYY']).format("MM-DD-YYYY");
        posting.PostingDate = moment(posting.PostingDate, ['MM/DD/YYYY', 'MMDDYYYY']).format("MM-DD-YYYY");


        $.ajax(routeconfig.addPosting, {
            type: "POST",
            dataType: "json",
            data: posting,
            success: function (data, status, xhr) {
                
                var pObj = {
                    PatNum: patientNum(),
                };

                app.trigger("patient:getpatientpostings");
            },
            error: function (data, status, xhr) {
                logger.logError(xhr, status, null, true);
            },
            fail: function (data, status, xhr) {
                logger.logError(xhr, status, null, true);
            }
        });

    }
    function openCommentsPopup(patientNum) {
        this.patientNum = patientNum;


        var postingCommentInfo ={
            TemplateName: 'hfmi',
            PatientNum: patientNum,
            DefaultAc: 0,
            DefaultSt: 800 ,
            };


        commentmodal.open("posting comment", "posting comment", postingCommentInfo);
    }
  
    posting.prototype.attached = function () {
        var cData = [];
        Date.prototype.toDateInputValue = (function () {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0, 10);
        });

        $("#fe-posting-type-dropbox").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: postingTypes,
        });

        $.ajax(routeconfig.getpostinguserlist, {
            type: "GET",
            dataType: "json",
            success: function (data, status, xhr) {

                var jObj = utils.parseJson(data);

                if (jObj !== false) {

                    $.each(jObj, function (index, item) {
                        var tmpItem = {
                            text: item,
                            value: item
                        };
                        cData.push(tmpItem);
                    });
                    // create dropdown from input HTML element
                    $("#fe-posting-userlist-dropbox").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: cData,
                    });
                    var cn = appsecurity.getUserInfo().userName;
                    var xx = utils.lookupIndex(cData, 'value', cn);
                    $("#fe-posting-userlist-dropbox").data("kendoDropDownList").select(xx);
                }
            }
        });
        
    }

    posting.prototype.close = function () {
        dialog.close(this);
        
    };

    posting.prototype.submit = function () {
        var pp = patient;
        var posting = {};
        posting.Type = $("#fe-posting-type-dropbox").data("kendoDropDownList").dataItem().value;
        //posting.Description = $("#fe-posting-type-dropbox").data("kendoDropDownList").dataItem().text;
        posting.PostingDate = $('#fe-posting-date').val();
        posting.RemitDate = $('#fe-posting-remitdate').val();
        posting.Amount = $('#fe-posting-amount').val();
        posting.UserName = appsecurity.getUserInfo().userName;
        posting.AgencyComm = 0;
        posting.HfmiRep = "";
        posting.HfmiComm = 0;
        posting.PatientNum = patientNum();
        posting.ClientNum = appsecurity.getUserClientNumber();

        
        if (posting.PostingDate === "") {
                logger.logError("Posting  Date Required", "Posting  Date Required", null, true);
                return;
            }
        if (posting.RemitDate === "") {
                logger.logError("Remit  Date Required", "Remit  Date Required", null, true);
                return;
            }
        if (posting.Amount === "" || !utils.isValidAmount(posting.Amount)) {
                logger.logError("Invalid Amount", "Amount Required", null, true);
                return;
            }
        if (utils.convertDate(posting.PostingDate) === "") {
                logger.logError("Invalid Posting Date", "Invalid Posting Date", null, true);
                return;
            }
        if (utils.convertDate(posting.RemitDate) === "") {
                logger.logError("Invalid Remit Date", "Invalid Remit Date", null, true);
                return;
            }


        

        switch(posting.Type){
            case "1":
                submitPatientPosting(posting);
                $("#fe-posting-amount").val($('#fe-act-currbalance').val()-$('#fe-posting-amount').val());
                var xx = utils.lookupIndex(postingTypes, 'value', "3");
                $("#fe-posting-type-dropbox").data("kendoDropDownList").select(xx);
                break;
            case "3":
                submitPatientPosting(posting);
                dialog.close(this);
                openCommentsPopup(posting.PatientNum);
                break;

            case "4":
                submitPatientPosting(posting);
                dialog.close(this);
                break;
                
            case "5":
                posting.HfmiRep = $("#fe-posting-userlist-dropbox").data("kendoDropDownList").dataItem().value;
                submitPatientPosting(posting);
                dialog.close(this, this.model);

                break;
            case "6":
                posting.HfmiRep =  $("#fe-posting-userlist-dropbox").data("kendoDropDownList").dataItem().value;
                submitPatientPosting(posting);
                dialog.close(this, this.model);
                break;
            case "8":
                submitPatientPosting(posting);
                dialog.close(this, this.model);
                break;
        }
        

    };

    posting.prototype.compositionComplete = function () {
    
            var todayDate = kendo.toString(kendo.parseDate(new Date()), 'MM/dd/yyyy');
            $("#fe-posting-date").val(todayDate);
    
            $('#fe-posting-date').kendoDatePicker({
                    format: "M/d/yyyy",
                    parseFormats: ["MMMM yyyy"] //format also will be added to parseFormats
                });
        $('#fe-posting-remitdate').kendoDatePicker({
                format: "M/d/yyyy",
                parseFormats: ["MMMM yyyy"] //format also will be added to parseFormats
            });
    }

    posting.show = function (pNum) {

        this.pNum = pNum;
        patientNum(pNum);
        return dialog.show(new posting('Add Posting'));
    };


    return posting;
});