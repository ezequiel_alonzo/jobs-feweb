﻿
define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger", 'plugins/dialog'], function (router, $, app, appsecurity, routeconfig, utils, logger, dialog) {

    //----------------------------------------------------------------------   

    //var confirmMultiComment = require('~/App/');
    var browserName = new ko.observable();
    var historyPatientList = new ko.observableArray([]);
    var patientNum = new ko.observable();
    var initialObject = new ko.observable();
    var templateName = new ko.observable();
    var subscriberNum = new ko.observable('');
    var ccCode = new ko.observable();
    var cText = new ko.observable();
    var acCode = new ko.observable();

    function closePopup() {
        app.trigger("view:closepopup");
    };

    function getHistoryPatientList() {

        var obj = {
            MRN: $("#fe-act-mrn").val(),
            SSN: $("#fe-patient-ssn").val(),
            MedicaidNum: '',
            Template: appsecurity.getUserTemplate(),
            ClientNum: appsecurity.getUserClientNumber(),
            HospCode: appsecurity.getUserHospCode(),
            PatientNum: $("#fe-patient-num").val()
        };

        $.ajax(routeconfig.getpatienthistory, {
            type: "POST",
            dataType: "json",
            data: obj,
            success: function (data, status, xhr) {

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    populateHistoryPatientList(jObj);
                }

            },
            error: function (data, status, xhr) {
                logger.logError(xhr, status, null, true);
            },
            fail: function (data, status, xhr) {
                logger.logError(xhr, status, null, true);
            }
        });
    }

    function populateHistoryPatientList(data) {
        var historyPatientData = [];
        $.each(data, function (index, item) {
            var tempItem = {
                MRN: item.mrn,
                PatientNum: item.PatientNum,
                HospAcct: item.HospAcct,
                PatName: item.PatName,
                CurrStatus: item.CurrStatus,
                CurrBalance: item.CurrBalance,
                AdmitDate: item.AdmitDate,
                DischDate: item.DischDate,
                FinClass: item.FinClass,
                ClientNum: item.ClientNum,
                SSN: item.SSN
            }
            historyPatientData.push(tempItem);
        });
        historyPatientList(historyPatientData);

        buildHistoryPatientGrid();

    }
    function buildHistoryPatientGrid() {
        $("#fe-multicomment-patientlist-grid").remove();
        $("#fe-multicomment-patientlist-grid-container").html("<div id=\"fe-multicomment-patientlist-grid\"></div>");
        
        
        var data = historyPatientList();
        actualpn = $('#fe-patient-num').val();
        
        var updateCheckBoxTemplate = "#if( actualpn === PatientNum){#<input type='checkbox' checked/>#} " +
                                        "else {#<input type='checkbox' /> # }#";

        var cols = [
                { field: "update ", title: " ", template: updateCheckBoxTemplate, width: "20px" },
                { field: "HospAcct", title: "HospAcct", width: "90px" },
                { field: "PatName", title: "Patient Name", width: "120px" },
                { field: "CurrStatus", title: "CurrStatus", width: "50px" },
                { field: "FinClass", title: "FinClass", width: "70px" },
                { field: "AdmitDate", type: "date", title: "AdmitDate", width: "120px", format: "{0:MM/dd/yyyy}", headerAttributes: { style: "text-align: center;" }, attributes: { style: "text-align: center;" } },
                { field: "DischDate", type: "date", title: "DischDate", width: "120px", format: "{0:MM/dd/yyyy}", headerAttributes: { style: "text-align: center;" }, attributes: { style: "text-align: center;" } },
                { field: "CurrBalance", title: "CurrBalance", width: "70px" },
                { field: "ClientNum", title: "ClientNum", width: "70px" },

        ];


        var gHeight = 200;

        var bName = browserName();
        switch (bName) {
            case "Chrome":
                gHeight = 178;
                break;
            case "Firefox":
                gHeight = 166;
                break;
            default:
                gHeight = 200;
                break;
        }

        $("#fe-multicomment-patientlist-grid").kendoGrid({
            columns: cols,
            dataSource: {
                data: data
            },
            height: gHeight,
            selectable: "multiple row",
            sortable: true,
            resizable: true,
            pageable: false
        }).data("kendoGrid");

        $("#fe-multicomment-patientlist-grid").on("dblclick", "tr.k-state-selected", function () {
            var rsGrid = $("#fe-multicomment-patientlist-grid").data("kendoGrid");
            var selectedItem = rsGrid.dataItem(rsGrid.select());
            var patientNum = selectedItem.PatientNum;
            var subscriberNum = selectedItem.HospAcct;
            if (selectedItem.ClientNum !== appsecurity.getUserClientNumber()) {
                appsecurity.setUserClient(appsecurity.getUserHospCode(), selectedItem.ClientNum, appsecurity.getUserClientName(), "hfmi", false);
            }
            var pObj = {
                PatNum: patientNum,
                ActHospAct: subscriberNum
            };

            app.trigger("patient:getpatientdetailshfmi", pObj);

        });
    }
     
    function saveMultiComment(comment,patientlist) {
        var uProf = appsecurity.getUserInfo();
        var grid = $("#fe-multicomment-patientlist-grid").data("kendoGrid")
        var mainPatient = {
            TemplateName: templateName(),
            PatientNum: patientNum(),
            SubscriberNum: subscriberNum()
        };
        
        var selectedPatients = grid.tbody.find("input:checked").closest("tr").each(function (index) {
            var row = grid.tbody.find("input:checked").closest("tr")[index];
            var actualPatient = grid.dataItem(row);

            var obj = {
                UserName: uProf.userName,
                TemplateName: templateName(),
                PatientNum: actualPatient.PatientNum,
                SubscriberNum: subscriberNum(),
                Status: ccCode(),
                Comment: cText(),
                ActionCode: acCode()
            };
          

            $.ajax(routeconfig.addComment, {
                type: "POST",
                dataType: "json",
                data: obj,
                success: function (data, status, xhr) {

                    app.trigger("patient:refreshcomments", mainPatient);
                },
                error: function (data, status, xhr) {
                    logger.logError(xhr, status, null, true);
                },
                fail: function (data, status, xhr) {
                    logger.logError(xhr, status, null, true);
                }
            });
            
        });
        
        closePopup();
    }


    function initControls() {
        getHistoryPatientList();
    }


    var viewmodel = {

        //State Functions
        canActivate: function () {
            return true;
        },
        activate: function (settings) {

            patientNum(settings.PatientNum);
            templateName(settings.TemplateName);
            ccCode(settings.CcCode);
            cText(settings.cText.toUpperCase());
            acCode(settings.AcCode);

        },
        compositionComplete: function () {
            initControls();
        },

        //Variables
    

        //Functions
        closePopup: closePopup,
        saveMultiComment: saveMultiComment


    };

    return viewmodel;
});