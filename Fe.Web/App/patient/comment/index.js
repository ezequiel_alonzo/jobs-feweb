﻿//*********************************
// File: patient/comment/index.js
//*********************************

define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger"], function (router, $, app, appsecurity, routeconfig, utils, logger) {

    //----------------------------------------------------------------------   

    var templateName = new ko.observable();
    var patientNum = new ko.observable();
    var subscriberNum = new ko.observable();
    var activityCodes = new ko.observableArray([]);
    var commCodes = new ko.observableArray([]);
    var acText = new ko.observable();
    var ccText = new ko.observable();
    var defaultAc = new ko.observable();
    var defaultSt = new ko.observable();
    var repStatus = new ko.observable(false);

    function addAcTextToComment(arg) {
        var targetItem = arg.sender;
        var currItem = targetItem.dataItem(this.select());
        if (currItem.AcCode === 0) {
            acText("");
            var cText1 = ccText();
            if (cText1) $("#fe-comment-text").val(cText1);
            if (!cText1) $("#fe-comment-text").val("");
        }
        if (currItem.AcCode !== 0) {
            acText(currItem.AcCodeDescription);
            var aText = acText();
            var cText = ccText();
            if (aText && cText) $("#fe-comment-text").val(aText + " " + cText);
            if (aText && !cText) $("#fe-comment-text").val(aText);
            if (!aText && cText) $("#fe-comment-text").val(cText);
            if (!aText && !cText) $("#fe-comment-text").val("");
        }

        $("#fe-comment-text").focus();
    };

    function addMultiComment() {
        var ac = false;
        var cc = false;
        var ct = false;

        var uProf = appsecurity.getUserInfo();

        var acGrid = $("#fe-comment-ac-grid").data("kendoGrid");
        var ccGrid = $("#fe-comment-cc-grid").data("kendoGrid");
        var cText = $("#fe-comment-text").val();
        var acItem = acGrid.dataItem(acGrid.select());
        var ccItem = ccGrid.dataItem(ccGrid.select());

        if (acItem) ac = true;
        if (ccItem) cc = true;
        if (cText) ct = true;
        var patientList = [];

        if (ac && cc && ct) {
            var obj = {
                UserName: uProf.userName,
                TemplateName: templateName(),
                PatientNum: patientNum(),
                SubscriberNum: subscriberNum(),
                Status: ccItem.CcCode,
                Comment: cText.toUpperCase(),
                ActionCode: acItem.AcCode
            };
            patientNum($('#fe-patient-num').val());
            var pNum = patientNum();

            var aHeader = "<div style=\"padding: 4px;\"><span>Comment</span></div>" +
                          "<span class=\"glyphicon glyphicon-remove glyphicon-button\" " +
                          "style=\"position: absolute; top: 4px; right: 4px; \" " +
                          "data-bind=\"click: closePopup\">" +
                          "</span>";

            var aHtml = "<div data-bind=\"compose : { model: 'patient/comment/multicomment/index', " +
                "activationData: { " +
                "TemplateName: 'hfmi'," +
                "CcCode: '" + pNum + "', " +
                "CcCode: '" + ccItem.CcCode + "', " +
                "cText: '" + cText + "', " +
                "AcCode: '" + acItem.AcCode + "'} " +
                "}, style: {height: '100%'}\"></div>";

            $("#fe-main-popup-header").html(aHeader);
            $("#fe-main-popup-view").html(aHtml);
            ko.cleanNode(document.getElementById("fe-main-popup"));
            ko.applyBindings(viewmodel, document.getElementById("fe-main-popup"));

            $("#fe-main-popup-overlay").show();
            $("#fe-main-popup").show();



        } else {
            var msg = "Missing required field(s): ";
            if (!ac && !cc && !ct) msg += "Action, Status, Comment";
            if (!ac && !cc && ct) msg += "Action, Status";
            if (!ac && cc && !ct) msg += "Action, Comment";
            if (ac && !cc && !ct) msg += "Status, Comment";
            if (!ac && cc && ct) msg += "Action";
            if (ac && !cc && ct) msg += "Status";
            if (ac && cc && !ct) msg += "Comment";

            logger.logError(msg, "Error", null, true);
        }

    };

    function buildActivityCodeGrid(data) {

        var cols = [{ field: "AcCode", title: "Code", width: "30px" },
                    { field: "AcCodeDescription", title: "Description" }];

        if (!data) data = [];

        var gHeight = 200;

        var grid = $("#fe-comment-ac-grid").kendoGrid({
            columns: cols,
            dataSource: {
                data: data
            },
            height: gHeight,
            selectable: "row",
            sortable: false,
            resizable: true,
            pageable: false,
            change: addAcTextToComment
        }).data("kendoGrid");

        var view = grid.dataSource.view();
        var row = $.grep(view, function (item) {
            return item.AcCode === 0;
        }).map(function (item) {
            return grid.tbody.find("[data-uid=" + item.uid + "]");
        });

        grid.select(row);

    };

    function getActivityCodes() {

        $.ajax(routeconfig.getActivityCodes, {
            type: "GET",
            dataType: "json",
            success: function (data, status, xhr) {

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    activityCodes(jObj);

                    var acData = [];

                    $.each(jObj, function (index, item) {
                        var tmpItem = {
                            AcCode: item.AcCode,
                            AcCodeDescription: item.AcCodeDescription
                        };
                        acData.push(tmpItem);
                    });

                    buildActivityCodeGrid(acData);

                }

            },
            error: function (data, status, xhr) {

            },
            fail: function (data, status, xhr) {

            }
        });

    };

    function addCcTextToComment(arg) {
        var targetItem = arg.sender;
        var currItem = targetItem.dataItem(this.select());
        ccText(currItem.CcDescription);
        var aText = acText();
        var cText = ccText();
        if (aText && cText) $("#fe-comment-text").val(aText + " " + cText);
        if (aText && !cText) $("#fe-comment-text").val(aText);
        if (!aText && cText) $("#fe-comment-text").val(cText);
        if (!aText && !cText) $("#fe-comment-text").val("");

        $("#fe-comment-text").focus();
    };

    function buildCommCodeGrid(data) {

        var cols = [{ field: "CcCode", title: "Code", width: "50px" },
                    { field: "CcDescription", title: "Description" }];

        if (!data) data = [];

        var gHeight = 200;

        $("#fe-comment-cc-grid").kendoGrid({
            columns: cols,
            dataSource: {
                data: data
            },
            height: gHeight,
            selectable: "row",
            sortable: false,
            resizable: true,
            pageable: false,
            change: addCcTextToComment
        }).data("kendoGrid");

    };

    function getCommCodes(clientNum) {

        var eSeg = "?clientNum=" + clientNum;

        $.ajax(routeconfig.getCommCodes + eSeg, {
            type: "GET",
            dataType: "json",
            success: function (data, status, xhr) {

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    commCodes(jObj);

                    var ccData = [];

                    $.each(jObj, function (index, item) {
                        var tmpItem = {
                            CcCode: item.CcCode,
                            CcDescription: item.CcDescription,
                            CcComments: item.CcComments
                        };
                        ccData.push(tmpItem);
                    });

                    buildCommCodeGrid(ccData);
                }

            },
            error: function (data, status, xhr) {

            },
            fail: function (data, status, xhr) {

            }
        });

    };

    function initControls() {

        var tName = templateName();
        if (tName) tName = tName.toLowerCase();
        switch (tName) {
            case "hfmi":
                getActivityCodes();
                getCommCodes(appsecurity.getUserClientNumber());
                break;
            case "emr":
                $("#fe-comment-ac-container").hide();
                $("#fe-comment-cc-container").css("width", "calc(100% - 4px)");
                getActivityCodes();
                getCommCodes("90414A");
                break;
            default:
                getActivityCodes();
                getCommCodes(appsecurity.getUserClientNumber());
                break;
        }

    };

    function closePopup() {
        app.trigger("view:closepopup");
    };

    function addComment() {


    };

    function formatCommCode(item) {

        if (item) {
            return item.CcCode + ": " + item.CcDescription;
        }

        return "";
    };

    function formatActivityCode(item) {

        if (item) {
            return item.AcCode + ": " + item.AcCodeDescription;
        }

        return "";

    };

    //----------------------------------------------------------------------

    var viewmodel = {

        //State Functions
        canActivate: function () {
            return true;
        },
        activate: function (settings) {

            if (settings) {
                if (settings.TemplateName) templateName(settings.TemplateName);
                if (settings.PatientNum) {
                    patientNum(settings.PatientNum);
                }
                if (settings.SubscriberNum) subscriberNum(settings.SubscriberNum);
                activityCodes([]);
                commCodes([]);
                acText("");
                ccText("");
                if (settings.DefaultAc) defaultAc(settings.DefaultAc);
                if (settings.DefaultSt) defaultSt(settings.DefaultSt);
            }

        },
        compositionComplete: function () {
            initControls();
        },

        //Variables
        activityCodes: activityCodes,
        commCodes: commCodes,

        //Functions
        closePopup: closePopup,
        addComment: addComment,
        formatCommCode: formatCommCode,
        formatActivityCode: formatActivityCode,
        addMultiComment: addMultiComment

    };

    return viewmodel;
});