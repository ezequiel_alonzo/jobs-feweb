//******************************************
// File: patient/demographic/hfmi/index.js
//******************************************

define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger", "patient/demographic/hfmi/searchresults/index", "patient/posting/hfmi/index", "modals/selectmodal/index", "modals/commentmodal/index","moment"],
    function (router, $, app, appsecurity, routeconfig, utils, logger, searchresults, posting, selectmodal, commentmodal,moment) {

        var currentClient = ko.observable();
        var browserName = new ko.observable();
        var patientNum = new ko.observable();
        var currentTab = new ko.observable('comments');
        var commentList = new ko.observableArray([]);
        var postingList = new ko.observableArray([]);
        var historyPatientList = new ko.observableArray([]);
        var hasPatientLoaded = new ko.observable(false);
        var hasPostingDeletePermission = new ko.observable(false);

        var demographics = ko.mapping.fromJS({
            PatNum: "",
            PatLastName: "",
            PatFirstName: "",
            PatAddress: "",
            PatCity: "",
            PatState: "",
            PatZip: "",
            PatPhone: "",
            PatDob: "",
            PatGender: "",
            PatSsn: "",
            RespName: "",
            RespAddress: "",
            RespCity: "",
            RespState: "",
            RespZip: "",
            RespPhone: "",
            RespFc: "",
            RespWorkPhone: "",
            RespSsn: "",
            ActHospAcct: "",
            ActMrn: "",
            ActInsCode: "",
            ActCurrBalance: "",
            ActPlaceAmt: "",
            ActPlaceDate: "",
            ActAdmitDate: "",
            ActDischDate: "",
            ActApplDate: "",
            ActEncounter: "",
            ActMedicaidNum: "",
            CmtCurrStatus: "",
            CmtStatusDate: "",
            CmtCallBackUser: "",
            CmtCallBackDate: "",
            CmtLastChanged: "",
            PaymentAmount: "",
            Spanish: "",
            FieldVisit: "",
            AfterHours:""
        });

        var resetViewModel = function (viewModel) {
            for (var prop in viewModel) {
                if (viewModel.hasOwnProperty(prop) && ko.isObservable(viewModel[prop])) {
                    viewModel[prop]("");
                }
            }
        };

        var historyDataSource = new kendo.data.DataSource({
            transport: {
                read: function (operation) {
                    if (!hasPatientLoaded()) {
                        operation.success([]);
                        return;
                    };

                    $.ajax(routeconfig.getpatienthistory, {
                        type: "POST",
                        dataType: "json",
                        data: {
                            MRN: demographics.ActMrn(),
                            SSN: /^(.)\1+$/.test(demographics.PatSsn()) ?  "" : demographics.PatSsn() ,
                            MedicaidNum: '',
                            Template: appsecurity.getUserTemplate(),
                            ClientNum: appsecurity.getUserClientNumber(),
                            HospCode: appsecurity.getUserHospCode(),
                            PatientNum: demographics.PatNum()
                        },
                        success: function (data, status, xhr) {

                            operation.success(JSON.parse(data)
                                .map(function (patHis) {
                                    patHis.AdmitDate = kendo.toString(kendo.parseDate(patHis.AdmitDate, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy");
                                    patHis.DischDate = kendo.toString(kendo.parseDate(patHis.DischDate, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy");

                                    return patHis;
                                })
                            );
                        }
                    });
                }
            }
        });

        var commentsDataSource = new kendo.data.DataSource({
            transport: {
                read: function (operation) {
                    if (!hasPatientLoaded()) {
                        operation.success([]);
                        return;
                    };

                    $.ajax(routeconfig.getPatientComments, {
                        type: "POST",
                        dataType: "json",
                        data: {
                            TemplateName: "hfmi",
                            PatientNum: demographics.PatNum()
                        },
                        success: function (data, status, xhr) {

                            operation.success(JSON
                                .parse(data)
                                .map(function (comment) {
                                    return {
                                        User: comment.CmtUser,
                                        DateTime: comment.CmtDate,
                                        Status: comment.CmtStatus,
                                        ActionCode: comment.CmtActionCode,
                                        Comment: comment.CmtComment
                                    }
                                }));
                        }
                    });
                }
            }
        });

        var postingsDataSource = new kendo.data.DataSource({
            transport: {
                read: function (operation) {
                    if (!hasPatientLoaded()) {
                        operation.success([]);
                        return;
                    };

                    $.ajax(routeconfig.getPatientPostings, {
                        type: "POST",
                        dataType: "json",
                        data: {
                            TemplateName: "hfmi",
                            PatientNum: demographics.PatNum()
                        },
                        success: function (data, status, xhr) {

                            operation.success(JSON
                                .parse(data)
                                .map(function (posting) {
                                    return {
                                        Code: posting.PstCode,
                                        RemitDate: posting.PstRemitDate,
                                        PostDate: posting.PstPostDate,
                                        Amount: posting.PstAmount,
                                        Commish: posting.PstCommish,
                                        User: posting.PstUserName,
                                        HFMI: posting.PstHfmiRep,
                                        Description: posting.PstDescription,
                                        LineNum: posting.LineNum,
                                        PatientNum: posting.PatientNum
                                    };
                                }));
                        }
                    });
                }
            }
        });

        var clientsDataSource = new kendo.data.DataSource({
            transport: {
                read: function (operation) {
                    var eSeg = "?userName=" + appsecurity.getUserInfo().userName + "&hospCode=" + appsecurity.getUserHospCode();

                    $.ajax(routeconfig.getUserClients + eSeg, {
                        type: "GET",
                        dataType: "json",
                        success: function (data, status, xhr) {
                            operation.success(JSON.parse(data));

                            $("#fe-client-dropdown").data("kendoDropDownList").select(function (client) {
                                return client.ClientNum === currentClient();
                            });
                        }
                    });
                }
            }
        });

        function buildHistoryPatientGrid() {

            var cols = [
                { field: "HospAcct", title: "Account number", width: "140px" },
                { field: "PatName", title: "Patient Name", width: "250px" },
                { field: "CurrStatus", title: "Status", width: "70px" },
                { field: "FinClass", title: "FC", width: "90px" },
                { field: "AdmitDate", type: "date", title: "Admit Date", width: "120px", format: "{0:MM/dd/yyyy}", headerAttributes: { style: "text-align: center;" }, attributes: { style: "text-align: center;" } },
                { field: "DischDate", type: "date", title: "Discharge Date", width: "135px", format: "{0:MM/dd/yyyy}", headerAttributes: { style: "text-align: center;" }, attributes: { style: "text-align: center;" } },
                { field: "CurrBalance", title: "Balance", width: "80px" },
                { field: "ClientNum", title: " Client Number" }
            ];

            var gHeight = 200;

            var bName = browserName();
            switch (bName) {
                case "Chrome":
                    gHeight = 178;
                    break;
                case "Firefox":
                    gHeight = 166;
                    break;
                default:
                    gHeight = 200;
                    break;
            }

            $("#fe-history-grid").kendoGrid({
                //dataBound: function(){
                //    var grid = $("#fe-history-grid").data("kendoGrid");
                //    for (var i = 0; i < grid.columns.length; i++) {
                //        grid.autoFitColumn(i);
                //    }
                //},
                columns: cols,
                dataSource: historyDataSource,
                height: gHeight,
                selectable: "multiple row",
                sortable: true,
                resizable: true,
                pageable: false,
                //scrollable: false,
                autoBind: false
            }).data("kendoGrid");

            $("#fe-history-grid").on("dblclick", "tr.k-state-selected", function () {
                var rsGrid = $("#fe-history-grid").data("kendoGrid");
                var selectedItem = rsGrid.dataItem(rsGrid.select());

                if (selectedItem.ClientNum !== appsecurity.getUserClientNumber()) {
                    appsecurity.setUserClient(appsecurity.getUserHospCode(), selectedItem.ClientNum, appsecurity.getUserClientName(), "hfmi", false);
                }

                app.trigger("patient:getpatientdetailshfmi", {
                    PatNum: selectedItem.PatientNum,
                    ActHospAct: selectedItem.SubscriberNumber
                });
            });
        }

        function setCurrentUserClient() {
            var dropdown = $("#fe-client-dropdown").data("kendoDropDownList");

            var selectedClient = dropdown.dataItem(dropdown.select());

            appsecurity.setUserClient(appsecurity.getUserHospCode(), selectedClient.ClientNum, selectedClient.Name);
        }

        function getPatientHistory() {
            
            historyDataSource.read();
        }

        function buildCommentGrid() {

            var cols = [
                    { field: "User", title: "User", width: "50px" },
                    { field: "DateTime", title: "Date", width: "140px" },
                    { field: "Status", title: "Status", width: "70px" },
                    { field: "ActionCode", title: "AC", width: "40px" },
                    { field: "Comment", title: "Comment" }
            ];

            var data = commentList();

            var gHeight = 200;
            var bName = browserName();
            switch (bName) {
                case "Chrome":
                    gHeight = 178;
                    break;
                case "Firefox":
                    gHeight = 166;
                    break;
                default:
                    gHeight = 200;
                    break;
            }

            $("#fe-comment-grid").kendoGrid({
                columns: cols,
                dataSource: commentsDataSource,
                height: gHeight,
                selectable: "row",
                sortable: false,
                resizable: true,
                pageable: false,
                autoBind: false
            }).data("kendoGrid");

        };

        function updatePatientDetailshfmi() {

            var obj = utils.compactObject($.extend(ko.mapping.toJS(demographics), {
                TemplateName: appsecurity.getUserTemplate(),
                ClientNum: currentClient()
            }));

            if (!obj.ActHospAcct) {
                if (utils.isWhitespaceOrEmpty(obj.ActHospAcct)) {
                    logger.log(" Hospital Account can not be empty", "HospAct", null, true);
                }
                else {
                    logger.log(" Can not save a patient without Hosp Account", "HospAct", null, true);
                }
                return;
            }
            if (demographics.PatGender() && !(new RegExp("(M|F)","i")).test(demographics.PatGender()))
            { 
                logger.log(" Invalid Gender(use \"M\" or \"F\")", "Gender", null, true);
                return;
            }
            if (obj.ActDischDate) obj.ActDischDate = moment(obj.ActDischDate, ['MM/DD/YYYY', 'MMDDYYYY']).format("MM-DD-YYYY");
            if (obj.ActPlaceDate) obj.ActPlaceDate= moment(obj.ActPlaceDate, ['MM/DD/YYYY', 'MMDDYYYY']).format("MM-DD-YYYY");
            if (obj.ActAdmitDate) obj.ActAdmitDate = moment(obj.ActAdmitDate, ['MM/DD/YYYY', 'MMDDYYYY']).format("MM-DD-YYYY");
            if (obj.PatDob) obj.PatDob = moment(obj.PatDob, ['MM/DD/YYYY', 'MMDDYYYY']).format("MM-DD-YYYY");
            if (obj.ActApplDate) obj.ActApplDate = moment(obj.ActApplDate, ['MM/DD/YYYY', 'MMDDYYYY']).format("MM-DD-YYYY");
            if (obj.PatSsn) obj.PatSsn = utils.removeSpecialCharacters(obj.PatSsn);  
            if (obj.RespSsn) obj.RespSsn = utils.removeSpecialCharacters(obj.RespSsn);

            $.ajax(routeconfig.updatePatientDemographics, {
                type: "POST",
                dataType: "json",
                data: utils.compactObject(obj),
                success: function () {
                    app.trigger("patient:getpatientdetailshfmi", {
                        PatNum: obj.PatNum
                    });
                }
            });
        }

        function saveNewPatientDetails() {

            var obj = $.extend(ko.mapping.toJS(demographics), {
                TemplateName: appsecurity.getUserTemplate(),
                ClientNum: currentClient(),
                Username: appsecurity.getUserInfo().userName
            });

            $.ajax(routeconfig.inserthfmipatientdemographics, {
                type: "POST",
                dataType: "json",
                data: obj,
                success: function () {
                    app.trigger("patient:getpatientdetailshfmi", {
                        PatNum: obj.PatNum
                    });
                }
            });
        }

        function validateIfNewPatient() {
            app.showMessage('This hospital account doesn\'t exists. Do you wish to add it?', 'New User', ['Yes', 'No']).then(function (dialogResult) {
                if (dialogResult !== "Yes") return;

                saveNewPatientDetails();
            });
        }

        function buildPostingGrid() {

            var cols = [
                { field: "Code", title: "Code", width: "60px" },
                    { field: "RemitDate", title: "Remit Date", width: "140px" },
                    { field: "PostDate", title: "Post Date", width: "140px" },
                    { field: "Amount", title: "Amount", width: "100px" },
                    { field: "Commish", title: "Commission", width: "120px" },
                    { field: "User", title: "User", width: "70px" },
                    { field: "HFMI", title: "HFMI", width: "70px" },
                    { field: "Description", title: "Description" }
            ];

            var gHeight = 200;
            var bName = browserName();
            switch (bName) {
                case "Chrome":
                    gHeight = 178;
                    break;
                case "Firefox":
                    gHeight = 166;
                    break;
                default:
                    gHeight = 200;
                    break;
            }

            $("#fe-posting-grid").kendoGrid({
                columns: cols,
                dataSource: postingsDataSource,
                height: gHeight,
                selectable: "row",
                sortable: false,
                resizable: true,
                autobind: false
            }).data("kendoGrid");

        };

        function openMoveModal() {


            selectmodal
                .open('Move Clients', 'Select a client', {
                    textField: "Name",
                    valueField: "ClientNum",
                    items: clientsDataSource.data(),
                    selectedItem: currentClient()
                })
                .then(function (result) {
                    if (!result.hasSelectedItem || !result.selectedItem) return;

                    app.showMessage("The patient will be moved. Are you sure?", "Confirm", ["Yes", "No"])
                        .then(function (answer) {
                            if (answer !== "Yes") return;

                            currentClient(result.selectedItem);

                            var obj = {
                                UserName: appsecurity.getUserInfo().userName,
                                TemplateName: 'hfmi',
                                PatientNum: $('#fe-patient-num').val(),
                                SubscriberNum: $('#fe-act-hospacct').val(),
                                Status: $('#fe-cmt-currstatus').val() !== "" ? $('#fe-cmt-currstatus').val() : "102",
                                Comment: "Moved from client " + appsecurity.getUserClientNumber() + " to " + currentClient(),
                                ActionCode: "0"
                            };

                            $.ajax(routeconfig.addComment, {
                                type: "POST",
                                dataType: "json",
                                data: obj,
                                success: function (data, status, xhr) {

                                    updatePatientDetailshfmi();
                                    app.trigger("patient:getpatientdetailshfmi", { PatNum: obj.PatientNum });

                                    if (obj.ClientNum !== appsecurity.getUserClientNumber()) {
                                        appsecurity.setUserClient(appsecurity.getUserHospCode(), obj.ClientNum, appsecurity.getUserClientName(), "hfmi", false);
                                    }
                                }
                            });


                        });
                });
        }

        function searchResultsShow(data) {
            searchresults.show(data);
        }

        function populatePatientDemographics(data) {

            if (data.length > 1) {
                searchResultsShow(data);
            }
            if (!data[1]) {
                if (data[0].ClientNum !== appsecurity.getUserClientNumber()) {
                    appsecurity.setUserClient(data[0].ActHospCode, data[0].ClientNum, appsecurity.getUserClientName(), "hfmi", false);
                }
                data[0].ActPlaceDate = kendo.toString(kendo.parseDate(data[0].ActPlaceDate, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy");
                data[0].ActAdmitDate = kendo.toString(kendo.parseDate(data[0].ActAdmitDate, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy");
                data[0].ActDischDate = kendo.toString(kendo.parseDate(data[0].ActDischDate, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy");
                data[0].CmtCallBackDate = kendo.toString(kendo.parseDate(data[0].CmtCallBackDate, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy");
                data[0].CmtStatusDate = kendo.toString(kendo.parseDate(data[0].CmtStatusDate, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy");
                data[0].CmtLastChanged = kendo.toString(kendo.parseDate(data[0].CmtLastChanged, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy");
                data[0].PatDob = kendo.toString(kendo.parseDate(data[0].PatDob, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy");
                data[0].ActApplDate = kendo.toString(kendo.parseDate(data[0].ActApplDate, "yyyy-MM-ddTHH:mm:ss"), "MM/dd/yyyy");
                ko.mapping.fromJS(data[0], demographics);
                patientNum(demographics.PatNum());
            }
        };
        function searchAcrossAllClients() {
            getPatientDemographics("all");
        }

        function getPatientDemographics(clientCriteria) {

            demographics.ActHospAcct(demographics.ActHospAcct().replace(/ /g, ''));

            var tPatientCriteria = {
                PatNum: demographics.PatNum(),
                ActHospCode: appsecurity.getUserHospCode()
            };

            if (tPatientCriteria.PatNum === "") {

                tPatientCriteria.ActHospAcct = demographics.ActHospAcct();

                if (tPatientCriteria.ActHospAcct === "") {

                    tPatientCriteria = utils.compactObject(ko.mapping.toJS(demographics));
                }
            }

            if (utils.isEmpty(tPatientCriteria)) {
                logger.log("No Search Criteria Added", "No criteria", null, true);

            } else {
                tPatientCriteria = $.extend(tPatientCriteria, {
                    TemplateName: appsecurity.getUserTemplate(),
                    ClientNum: currentClient(),
                    ActHospCode: appsecurity.getUserHospCode()
                })

                if (clientCriteria === "all") tPatientCriteria.ClientNum = 'all';
                
                $.ajax(routeconfig.getPatientDemographics, {
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    data: JSON.stringify({ patient: tPatientCriteria, userName: appsecurity.getUserInfo().userName }),
                    success: function (data, status, xhr) {

                        var jObj = utils.parseJson(data);

                        if (jObj !== false) {
                            populatePatientDemographics(jObj);
                            currentTab("comments");
                            currentClient(jObj[0].ClientNum);
                            hasPatientLoaded(true);
                            commentsDataSource.read();
                            postingsDataSource.read();
                            clientsDataSource.read();


                        }
                        else {
                            if (demographics.ActHospAcct()) {
                                validateIfNewPatient();
                            }
                            else {
                                app.showMessage("No Results Found");
                            }
                        }

                    }
                });
            }
        };

        function copyPatientInfo() {
            if (browserName() === "Microsoft Internet Explorer") {

                var patInfo = ";" + demographics.ActHospAcct() + ";;" + demographics.PatLastName() + ";" + demographics.PatFirstName() + ";" +
                            demographics.PatGender() + ";" + demographics.PatDob() + ";" + demographics.PatSsn() + ";;" + demographics.ActAdmitDate() +
                            ";" + demographics.PatNum() + ";;";


                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val(patInfo).select();
                document.execCommand("copy");
                $temp.remove();

            } else {
                logger.log("Clipboard Copy is not supported in " + browserName(), "Not Supported", null, true);
            }


        }

        function deletePosting() {
            var grid = $("#fe-posting-grid").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());
            var patientNum = selectedItem.PatientNum;
            var lineNum = selectedItem.LineNum;

            var obj = {
                PatientNum: patientNum,
                LineNum: lineNum
            }

            $.ajax(routeconfig.deletePosting, {
                type: "POST",
                dataType: "json",
                data: obj,
                success: function (data, status, xhr) {

                    postingsDataSource.read();

                }
            });


        }
        function openPostingModal() {

            posting.show(demographics.PatNum());
        }

        function resetPatientDetailshfmi() {
            hasPatientLoaded(false);
            currentTab("comments");

            resetViewModel(demographics);

            historyDataSource.data([]);
            commentsDataSource.data([]);
            postingsDataSource.data([]);
        };

        function openCommentPopup() {
            commentmodal.open("Comments", "Comments", {
                patientNum: demographics.PatNum(),
                patient: ko.mapping.toJS(demographics),
                TemplateName: 'hfmi'
            });
        };

        function initControls() {
            browserName(utils.getBrowserName());
            currentClient(appsecurity.getUserClientNumber());
            hasPostingDeletePermission(appsecurity.hasPermission("save:deleteposting"));

            buildCommentGrid();
            buildHistoryPatientGrid();
            buildPostingGrid();


            clientsDataSource.read();
        };

        function getPatientDetailshfmi() {
            getPatientDemographics();
        };

        function openPrintModal() {

            $.ajax(routeconfig.getLettersTemplates, {
                type: "GET",
                dataType: "json",
                data: {
                    clientNum: appsecurity.getUserClientNumber()
                },
                success: function (letters) {

                    selectmodal
                        .open('Print Letter', 'Select a Letter', {
                            textField: "fileName",
                            valueField: "fileName",
                            items: letters
                        })
                        .then(function (result) {
                            if (!result.hasSelectedItem || !result.selectedItem) return;

                            var patientLettersRequestData = [{
                                patNum: demographics.PatNum(),
                                hospName: appsecurity.getUserClientName(),
                                firstName: demographics.PatFirstName(),
                                lastName: demographics.PatLastName(),
                                pAddress: demographics.PatAddress(),
                                hospAcct: demographics.ActHospAcct(),
                                currBalance: demographics.ActCurrBalance() ? demographics.ActCurrBalance(): '0.00',
                                admitDate: demographics.ActAdmitDate(),
                                pState: demographics.PatState(),
                                pZip: demographics.PatZip(),
                                pCity: demographics.PatCity(),
                                letterName: result.selectedItem.fileName,
                                clientNum: appsecurity.getUserClientNumber(),
                                displayName: appsecurity.getUserInfo().displayName,
                                dischDate: demographics.ActDischDate(),
                                PaymentStartDate: demographics.CmtCallBackDate(),
                                PaymentAmount: demographics.PaymentAmount(),
                            }];

                            $.ajax(routeconfig.getLettersFile, {
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                type: "POST",
                                data: JSON.stringify({ PatientLetters: patientLettersRequestData, UserName: appsecurity.getUserInfo().userName, DisplayName: appsecurity.getUserInfo().displayName }),
                                success: function (result) { window.open(result); }
                            });

                        });

                }

            });


        }


        function submitClipboardLoader() {

            if (browserName() === "Microsoft Internet Explorer") {

                var details = window.clipboardData.getData("Text");

                resetPatientDetailshfmi();

                demographics.PatNum(details);

                getPatientDetailshfmi();

            } else {
                logger.log("Clipboard Loader is not supported in " + browserName(), "Not Supported", null, true);
            }

        };

        function scanPat() {
            if (browserName() === "Microsoft Internet Explorer") {

                var patInfo = "ix-PAT-" + demographics.PatNum() + ".pdf";


                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val(patInfo).select();
                document.execCommand("copy");
                $temp.remove();

            } else {
                logger.log("Clipboard Copy is not supported in " + browserName(), "Not Supported", null, true);
            }
        }

        function getpatientpostings() {

            postingsDataSource.read();
        }

        //----------------------------------------------------------------------

        var viewmodel = {

            activate: function (patientNum) {

                if (patientNum) {
                    resetPatientDetailshfmi();
                    demographics.PatNum(patientNum);
                    getPatientDetailshfmi();
                    router.navigate("Patient/HFMI");
                }

                app.on("patient:refreshcomments").then(function (model) {
                    getPatientComments(model);
                });

                app.on("patient:resetpatientdetailshfmi").then(function () {
                    resetPatientDetailshfmi();
                });

                app.on("patient:openCommentPopup").then(function () {
                    openCommentPopup();
                });

                app.on("patient:getpatientdetailshfmi").then(function (model) {
                    demographics.PatNum(model.PatNum);
                    getPatientDetailshfmi();
                });

                app.on("patient:getpatientpostings").then(function () {
                    getpatientpostings();
                });

                app.on("view:changeclientinfo").then(function (clientInfo) {
                    resetPatientDetailshfmi();

                    if (!clientInfo) return;

                    currentClient(clientInfo.clientNum);
                    clientsDataSource.read();
                });

            },
            compositionComplete: function () {
                initControls();
            },

            //Variables
            hasPostingDeletePermission: hasPostingDeletePermission,
            currentTab: currentTab,
            hasPatientLoaded: hasPatientLoaded,
            demographics: demographics,
            clientsDataSource: clientsDataSource,


            //Functions
            getPatientDetailshfmi: getPatientDetailshfmi,
            resetPatientDetailshfmi: resetPatientDetailshfmi,
            openCommentPopup: openCommentPopup,
            saveNewPatientDetails: saveNewPatientDetails,
            updatePatientDetailshfmi: updatePatientDetailshfmi,
            populatePatientDemographics: populatePatientDemographics,
            openPostingModal: openPostingModal,
            deletePosting: deletePosting,
            openMoveModal: openMoveModal,
            getPatientHistory: getPatientHistory,
            setCurrentUserClient: setCurrentUserClient,
            submitClipboardLoader: submitClipboardLoader,
            copyPatientInfo: copyPatientInfo,
            openPrintModal: openPrintModal,
            getpatientpostings: getpatientpostings,
            searchAcrossAllClients: searchAcrossAllClients,
            scanPat: scanPat
        };

        return viewmodel;
    });