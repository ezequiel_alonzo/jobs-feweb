﻿define(['plugins/dialog', 'patient/demographic/hfmi/index', 'durandal/app', 'services/appsecurity'], function (dialog, patient, app, appsecurity) {
    var confirmMultiComment = function (title) {
        this.title = title;

    };

    var searchResultsArray = ko.observableArray([]);

    confirmMultiComment.prototype.attached = function () {

        var modal = this;

        var cols = [{ field: "ActHospAcct", title: "HostPital Account", width: "125px" },
            { field: "PatFirstName", title: "Name", width: "125px" },
            { field: "PatLastName", title: "LastName", width: "125px" },
            { field: "RespSsn", title: "Social Security", width: "125px" },
            { field: "ClientNum", title: "ClientNum", width: "70px" }


        ];

        $('#fe-results-grid-container').show();

        $("#fe-results-grid").kendoGrid({
            columns: cols,
            dataSource: {
                data: searchResultsArray(),

            },
            height: 300,
            selectable: "row",
            sortable: true,
            resizable: true,
            pageable: {
                pageSize: 10
            },
            change: function () {

                var currentDataItem = $("#fe-results-grid").data("kendoGrid").dataItem(this.select())

                if (currentDataItem.ClientNum !== appsecurity.getUserClientNumber()) {
                    appsecurity.setUserClient(currentDataItem.ActHospCode, currentDataItem.ClientNum, appsecurity.getUserClientName(), "hfmi", false);
                }

                app.trigger("patient:getpatientdetailshfmi", { PatNum: currentDataItem.PatNum });
                dialog.close(modal);
            }
        }).data("kendoGrid");
    }


    confirmMultiComment.prototype.close = function () {
        dialog.close(this, this.model);
        app.trigger("patient:resetpatientdetailshfmi");
    };

    confirmMultiComment.show = function (data) {

        this.data = data;
        searchResultsArray(data);
        return dialog.show(new confirmMultiComment('Search Results'));
    };

    return confirmMultiComment;
});