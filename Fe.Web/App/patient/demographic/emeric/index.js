﻿//********************************************
// File: patient/demographic/emeric/index.js
//********************************************

define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger", "modals/commentmodal/index", "moment"], function (router, $, app, appsecurity, routeconfig, utils, logger, commentmodal, moment) {

    //----------------------------------------------------------------------

    var browserName = new ko.observable();
    var subscriberNum = new ko.observable();
    var commentList = new ko.observableArray([]);
    var claimList = new ko.observableArray([]);
    //var claimDisplay = new ko.observableArray([]);

    function buildCommentGrid() {

        $("#fe-comment-grid").remove();
        $("#fe-comment-grid-container").html("<div id=\"fe-comment-grid\"></div>");

        var cols = [{ field: "User", title: "User", width: "50px" },
                    { field: "DateTime", title: "Date", width: "125px" },
                    { field: "Status", title: "Status", width: "60px" },
                    { field: "Comment", title: "Comment" }];

        var data = commentList();

        var gHeight = 200;
        var bName = browserName();
        switch (bName) {
            case "Chrome":
                gHeight = 178;
                break;
            case "Firefox":
                gHeight = 166;
                break;
            default:
                gHeight = 200;
                break;
        }

        $("#fe-comment-grid").kendoGrid({
            columns: cols,
            dataSource: {
                data: data
            },
            height: gHeight,           
            selectable: "row",
            scrollable: true,
            sortable: false,
            resizable: true,
            pageable: false
        }).data("kendoGrid");

    };

    function buildClaimGrid() {

        $("#fe-claim-grid").remove();
        $("#fe-claim-grid-container").html("<div id=\"fe-claim-grid\"></div>");

        var data = claimList();
        //var showColumns = claimDisplay();

        var cols = [
            { field: "ClientName", title: "Client Name", width: "250px" },
            { field: "ClaimNumber", title: "Claim #", width: "150px" },
            { field: "DateOfInjury", type: "date", title: "Date of Injury", width: "125px", format: "{0:MM/dd/yyyy}" },
            { field: "DateOfService", type: "date", title: "Date of Service", width: "125px", format: "{0:MM/dd/yyyy}" },
            { field: "ProviderName", title: "Provider Name", width: "200px" },
            { field: "AmountBilled", title: "Amount Billed", width: "125px", format: "{0:c2}" },
            { field: "AmountPaid", title: "Amount Paid", width: "125px", format: "{0:c2}" },
            { field: "NdcNumber", title: "NDC #", width: "125px" },
            { field: "Units", title: "Units", width: "75px" },           
            { field: "BillType", title: "Bill Type", width: "125px" },
            { field: "BillNumber", title: "Bill #", width: "125px" },
            { field: "LineNumber", title: "Line #", width: "125px" },            
            { field: "ServiceDescription", title: "Service Description", width: "250px" },                       
            { field: "FsReduction", title: "FS Reduction", width: "125px", format: "{0:c2}" },
            { field: "AuditReduction", title: "Audit Reduction", width: "125px", format: "{0:c2}" },
            { field: "PpoReduction", title: "PPO Reduction", width: "125px", format: "{0:c2}" },
            { field: "PrintDate", type: "date", title: "Print Date", width: "125px", format: "{0:MM/dd/yyyy}" },
            { field: "Network", title: "Network", width: "125px" },
            { field: "ProviderTin", title: "Provider TIN", width: "125px" },
            { field: "ProviderNpi", title: "Provider TIN", width: "125px" },           
            { field: "ProviderAddress", title: "Provider Address", width: "250px" },
            { field: "ProviderCity", title: "Provider City", width: "200px" },
            { field: "ProviderState", title: "Provider State", width: "125px" },
            { field: "ProviderZip", title: "Provider Zip", width: "125px" },
            { field: "PrescriberName", title: "Prescriber Name", width: "200px" },
            { field: "PrescriberDeaNumber", title: "Prescriber DEA #", width: "200px" }
        ];

        var gHeight = 200;
        var bName = browserName();
        switch (bName) {
            case "Chrome":
                gHeight = 178;
                break;
            case "Firefox":
                gHeight = 166;
                break;
            default:
                gHeight = 200;
                break;
        }

        var grid = $("#fe-claim-grid").kendoGrid({
            columns: cols,
            dataSource: {
                data: data
            },
            height: gHeight,
            selectable: "row",
            scrollable: true,
            sortable: true,
            resizable: true,
            pageable: false
        }).data("kendoGrid");

        //if (data.length > 0) {
        //    if (!showColumns[0]) grid.hideColumn("ClientName");
        //    if (!showColumns[1]) grid.hideColumn("ClaimNumber");
        //    if (!showColumns[2]) grid.hideColumn("DateOfInjury");
        //    if (!showColumns[3]) grid.hideColumn("DateOfService");
        //    if (!showColumns[4]) grid.hideColumn("ProviderName");
        //    if (!showColumns[5]) grid.hideColumn("AmountBilled");
        //    if (!showColumns[6]) grid.hideColumn("AmountPaid");
        //    if (!showColumns[7]) grid.hideColumn("NdcNumber");
        //    if (!showColumns[8]) grid.hideColumn("Units");
        //    if (!showColumns[9]) grid.hideColumn("BillType");
        //    if (!showColumns[10]) grid.hideColumn("BillNumber");
        //    if (!showColumns[11]) grid.hideColumn("LineNumber");
        //    if (!showColumns[12]) grid.hideColumn("ServiceDescription");
        //    if (!showColumns[13]) grid.hideColumn("FsReduction");
        //    if (!showColumns[14]) grid.hideColumn("AuditReduction");
        //    if (!showColumns[15]) grid.hideColumn("PpoReduction");
        //    if (!showColumns[16]) grid.hideColumn("PrintDate");
        //    if (!showColumns[17]) grid.hideColumn("Network");
        //    if (!showColumns[18]) grid.hideColumn("ProviderTin");
        //    if (!showColumns[19]) grid.hideColumn("ProviderNpi");
        //    if (!showColumns[20]) grid.hideColumn("ProviderAddress");
        //    if (!showColumns[21]) grid.hideColumn("ProviderCity");
        //    if (!showColumns[22]) grid.hideColumn("ProviderState");
        //    if (!showColumns[23]) grid.hideColumn("ProviderZip");
        //    if (!showColumns[24]) grid.hideColumn("PrescriberName");
        //    if (!showColumns[25]) grid.hideColumn("PrescriberDeaNumber");
        //}

    };

    function changeTabs(data, target) {

        var t = target.currentTarget.childNodes[1].innerText;
        var commentTab = document.getElementById("fe-comment-tab");
        var claimTab = document.getElementById("fe-claim-tab");
        var commentContainer = document.getElementById("fe-comment-container");
        var claimContainer = document.getElementById("fe-claim-container");

        switch (t) {

            case "Claims":

                if (claimTab.classList.contains("active")) break;

                commentTab.classList.remove("active");
                commentContainer.style.display = "none";
                claimTab.classList.add("active");
                claimContainer.style.display = "block";
                buildClaimGrid();
                break;

            case "Comments":

                if (commentTab.classList.contains("active")) break;

                claimTab.classList.remove("active");
                claimContainer.style.display = "none";
                commentTab.classList.add("active");
                commentContainer.style.display = "block";
                buildCommentGrid();
                break;

        }

    };

    function populatePatientDemographics(dataObj) {

        var commentTab = document.getElementById("fe-comment-tab");
        var claimTab = document.getElementById("fe-claim-tab");
        var commentContainer = document.getElementById("fe-comment-container");
        var claimContainer = document.getElementById("fe-claim-container");

        claimTab.classList.remove("active");
        claimContainer.style.display = "none";
        commentTab.classList.add("active");
        commentContainer.style.display = "block";

        var emrData = dataObj.EmrPatientDemographics;
        var data = dataObj.FePatientDemographics;

        if (emrData.length === 1) {
            
            subscriberNum($("#fe-subscriber-num").val());
            if (emrData[0].PatFirstName && emrData[0].PatLastName) {
                $("#fe-patient-name").text(emrData[0].PatFirstName + " " + emrData[0].PatLastName);
                $("#fe-patient-name1").val(emrData[0].PatFirstName + " " + emrData[0].PatLastName);
                
                $("#fe-patient-name-container").css("display", "inline-block");
                $("#fe-toolbar-button-container").css("display", "inline-block");
            }
            if (emrData[0].PatFirstName && !emrData[0].PatLastName) {
                $("#fe-patient-name").text(emrData[0].PatFirstName);
                $("#fe-patient-name-container").css("display", "inline-block");
                $("#fe-toolbar-button-container").css("display", "inline-block");
            }
            if (!emrData[0].PatFirstName && emrData[0].PatLastName) {
                $("#fe-patient-name").text(emrData[0].PatLastName);
                $("#fe-patient-name-container").css("display", "inline-block");
                $("#fe-toolbar-button-container").css("display", "inline-block");
            }
            if (emrData[0].PatAddress) $("#fe-patient-address1").val(emrData[0].PatAddress);
            if (emrData[0].PatCity) $("#fe-patient-city1").val(emrData[0].PatCity);
            if (emrData[0].PatState) $("#fe-patient-state1").val(emrData[0].PatState);
            if (emrData[0].PatZip) $("#fe-patient-zip1").val(emrData[0].PatZip);
            if (emrData[0].PatPhone) $("#fe-patient-phone1").val(emrData[0].PatPhone);

        }

        if (data.length === 1) {
            
            if (data[0].PatFirstName && data[0].PatLastName) $("#fe-patient-name2").val(data[0].PatFirstName + " " + data[0].PatLastName);
            if (data[0].PatAddress) $("#fe-patient-address2").val(data[0].PatAddress);
            if (data[0].PatCity) $("#fe-patient-city2").val(data[0].PatCity);
            if (data[0].PatState) $("#fe-patient-state2").val(data[0].PatState);
            if (data[0].PatZip) $("#fe-patient-zip2").val(data[0].PatZip);
            if (data[0].PatPhone) $("#fe-patient-phone2").val(data[0].PatPhone);

            if (data[0].RespName) $("#fe-patient-name3").val(data[0].RespName);
            if (data[0].RespAddress) $("#fe-patient-address3").val(data[0].RespAddress);
            if (data[0].RespCity) $("#fe-patient-city3").val(data[0].RespCity);
            if (data[0].RespState) $("#fe-patient-state3").val(data[0].RespState);
            if (data[0].RespZip) $("#fe-patient-zip3").val(data[0].RespZip);
            if (data[0].RespPhone) $("#fe-patient-phone3").val(data[0].RespPhone);

            if (data[0].CmtCurrStatus) $("#fe-cmt-currstatus").val(data[0].CmtCurrStatus);
            if (data[0].CmtStatusDate) $("#fe-cmt-statusdate").val(data[0].CmtStatusDate);
            if (data[0].CmtCallBackDate) $("#fe-cmt-callbackdate").val(moment(data[0].CmtCallBackDate).format('MM/DD/YYYY'));
            if (data[0].CmtLastChanged) $("#fe-cmt-lastchanged").val(moment(data[0].CmtLastChanged).format('MM/DD/YYYY'));
        }       
    };

    function getPatientDemographics() {

        $(".fe-address-loader").css("display", "inline-block");

        subscriberNum($("#fe-subscriber-num").val());
        var sNum = $("#fe-subscriber-num").val();

        utils.copyToClipboard(sNum);

        if (!sNum) {
            logger.logError("Required: Claimant Number", "Required", null, true);
            $(".fe-address-loader").css("display", "none");
             return;
        }

        var obj = {
            TemplateName: "emr",
            SubscriberNum: sNum,
            ClientNum: appsecurity.getUserClientNumber()
        };

        $.ajax(routeconfig.getPatientDemographics, {
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            data: JSON.stringify({ patient: obj, userName: appsecurity.getUserInfo().userName }),
            success: function (data, status, xhr) {

                $(".fe-address-loader").css("display", "none");

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    populatePatientDemographics(jObj);
                }

            },
            error: function (data, status, xhr) {
                $(".fe-address-loader").css("display", "none");
                logger.logError(xhr, status, null, true);
            },
            fail: function (data, status, xhr) {
                $(".fe-address-loader").css("display", "none");
                logger.logError(xhr, status, null, true);
                
            }
        });

    };

    function populatePatientComments(data) {

        var cmtData = [];

        $.each(data, function (index, item) {
            var tmpItem = {
                User: item.CmtUser,
                DateTime: item.CmtDate,
                Status: item.CmtStatus,
                ActionCode: item.CmtActionCode,
                Comment: item.CmtComment
            };
            cmtData.push(tmpItem);
        });

        commentList(cmtData);

        buildCommentGrid();

        $("#fe-add-comment-button").css("display", "inline-block");

    };

    function getPatientComments(model) {

        $(".fe-comment-loader").css("display", "inline-block");

        var sNum = "";

        if (model) {

            if (model.SubscriberNum) {
                sNum = model.SubscriberNum;
            } else {
                sNum = $("#fe-subscriber-num").val();
            }

        } else {
            sNum = $("#fe-subscriber-num").val();
        }

        if (!sNum) {
            $(".fe-comment-loader").css("display", "none");
             return;
        }

        var obj = {
            TemplateName: "emr",
            SubscriberNum: sNum
        };

        $.ajax(routeconfig.getPatientComments, {
            type: "POST",
            dataType: "json",
            data: obj,
            success: function (data, status, xhr) {

                $(".fe-comment-loader").css("display", "none");

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    populatePatientComments(jObj);
                }

            },
            error: function (data, status, xhr) {
                $(".fe-comment-loader").css("display", "none");
                logger.logError(xhr, status, null, true);               
            },
            fail: function (data, status, xhr) {
                $(".fe-comment-loader").css("display", "none");
                logger.logError(xhr, status, null, true);             
            }
        });
    };

    function populatePatientClaims(data) {

        var clmData = [];
        //var clmDisplay = [false, false, false, false, false, false, false, false,
        //                  false, false, false, false, false, false, false, false,
        //                  false, false, false, false, false, false, false, false,
        //                  false, false];
        
        $.each(data, function (index, item) {
            var tmpItem = {
                ClientName: item.ClientName,
                ClaimNumber: item.ClaimNumber,
                DateOfInjury: item.DateOfInjury,
                DateOfService: item.DateOfService,
                BillType: item.BillType,
                BillNumber: item.BillNumber,
                LineNumber: item.LineNumber,
                NdcNumber: item.NdcNumber,
                ServiceDescription: item.ServiceDescription,

                Units: item.Units,
                AmountBilled: item.AmountBilled,
                FsReduction: item.FsReduction,
                AuditReduction: item.AuditReduction,
                PpoReduction: item.PpoReduction,
                AmountPaid: item.AmountPaid,
                PrintDate: item.PrintDate,
                Network: item.Network,
                ProviderTin: item.ProviderTin,
                ProviderNpi: item.ProviderNpi,
                ProviderName: item.ProviderName,
                ProviderAddress: item.ProviderAddress,
                ProviderCity: item.ProviderCity,
                ProviderState: item.ProviderState,
                ProviderZip: item.ProviderZip,
                PrescriberName: item.PrescriberName,
                PrescriberDeaNumber: item.PrescriberDeaNumber
            };
            clmData.push(tmpItem);

            //if (item.ClientName) clmDisplay[0] = true;
            //if (item.ClaimNumber) clmDisplay[1] = true;
            //if (item.DateOfInjury) clmDisplay[2] = true;
            //if (item.DateOfService) clmDisplay[3] = true;
            //if (item.ProviderName) clmDisplay[4] = true;
            //if (item.AmountBilled) clmDisplay[5] = true;
            //if (item.AmountPaid) clmDisplay[6] = true;
            //if (item.NdcNumber) clmDisplay[7] = true;
            //if (item.Units) clmDisplay[8] = true;
            //if (item.BillType) clmDisplay[9] = true;
            //if (item.BillNumber) clmDisplay[10] = true;
            //if (item.LineNumber) clmDisplay[11] = true;
            
            //if (item.ServiceDescription) clmDisplay[12] = true;                    
            //if (item.FsReduction) clmDisplay[13] = true;
            //if (item.AuditReduction) clmDisplay[14] = true;
            //if (item.PpoReduction) clmDisplay[15] = true;           
            //if (item.PrintDate) clmDisplay[16] = true;
            //if (item.Network) clmDisplay[17] = true;
            //if (item.ProviderTin) clmDisplay[18] = true;
            //if (item.ProviderNpi) clmDisplay[19] = true;            
            //if (item.ProviderAddress) clmDisplay[20] = true;
            //if (item.ProviderCity) clmDisplay[21] = true;
            //if (item.ProviderState) clmDisplay[22] = true;
            //if (item.ProviderZip) clmDisplay[23] = true;
            //if (item.PrescriberName) clmDisplay[24] = true;
            //if (item.PrescriberDeaNumber) clmDisplay[25] = true;
        });

        claimList(clmData);
        //claimDisplay(clmDisplay);

        buildClaimGrid();

        $("#fe-add-claim-button").attr("disabled", false);

    };

    function getPatientClaims(model) {

        $(".fe-claim-loader").css("display", "inline-block");

        var sNum = "";

        if (model) {

            if (model.SubscriberNum) {
                sNum = model.SubscriberNum;
            } else {
                sNum = $("#fe-subscriber-num").val();
            }

        } else {
            sNum = $("#fe-subscriber-num").val();
        }

        if (!sNum) {
            $(".fe-claim-loader").css("display", "none");
            return;
        }

        var obj = {
            TemplateName: "emr",
            SubscriberNum: sNum
        };

        $.ajax(routeconfig.getPatientClaims, {
            type: "POST",
            dataType: "json",
            data: obj,
            success: function (data, status, xhr) {

                $(".fe-claim-loader").css("display", "none");

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    populatePatientClaims(jObj);
                }

            },
            error: function (data, status, xhr) {
                $(".fe-claim-loader").css("display", "none");
                logger.logError(xhr, status, null, true);
            },
            fail: function (data, status, xhr) {
                $(".fe-claim-loader").css("display", "none");
                logger.logError(xhr, status, null, true);
            }
        });
    };

    function resetPatientDetails() {

        $("#fe-patient-name").text("");
        $("#fe-patient-name1").val("");
        $("#fe-patient-address1").val("");
        $("#fe-patient-city1").val("");
        $("#fe-patient-state1").val("");
        $("#fe-patient-zip1").val("");
        $("#fe-patient-phone1").val("");
        $("#fe-patient-name2").val("");
        $("#fe-patient-address2").val("");
        $("#fe-patient-city2").val("");
        $("#fe-patient-state2").val("");
        $("#fe-patient-zip2").val("");
        $("#fe-patient-phone2").val("");
        $("#fe-patient-name3").val("");
        $("#fe-patient-address3").val("");
        $("#fe-patient-city3").val("");
        $("#fe-patient-state3").val("");
        $("#fe-patient-zip3").val("");
        $("#fe-patient-phone3").val("");
        $("#fe-cmt-currstatus").val("");
        $("#fe-cmt-statusdate").val("");
        $("#fe-cmt-callbackuser").val("");
        $("#fe-cmt-callbackdate").val("");
        $("#fe-cmt-lastchanged").val("");

        $("#fe-add-comment-button").css("display", "none");

        subscriberNum("");
        commentList([]);
        claimList([]);
        //claimDisplay([]);

        buildCommentGrid();
        buildClaimGrid();

        $("#fe-patient-name-container").css("display", "none");
        $("#fe-toolbar-button-container").css("display", "none");

        var commentTab = document.getElementById("fe-comment-tab");
        var claimTab = document.getElementById("fe-claim-tab");
        var commentContainer = document.getElementById("fe-comment-container");
        var claimContainer = document.getElementById("fe-claim-container");

        $(".fe-address-loader").css("display", "none");
        $(".fe-comment-loader").css("display", "none");
        $(".fe-claim-loader").css("display", "none");

        claimTab.classList.remove("active");
        claimContainer.style.display = "none";
        commentTab.classList.add("active");
        commentContainer.style.display = "block";

        $("#fe-subscriber-num").val("");
        $("#fe-subscriber-num").focus();

    };

    function closePopup() {
        app.trigger("view:closepopup");
    };

    function openCommentPopup() {

        commentmodal.open("Comments", "Comments", {
            SubscriberNum: $('#fe-subscriber-num').val(),
            TemplateName: 'emr'
        });

    };

    function openGenerateDocumentPopup() {

        var sNum = subscriberNum();

        var aHeader = "<div style=\"padding: 4px;\"><span>Document Generation</span></div>" +
                      "<span class=\"glyphicon glyphicon-remove glyphicon-button\" " +
                      "style=\"position: absolute; top: 4px; right: 4px; \" " +
                      "data-bind=\"click: closePopup\">" +
                      "</span>";

        var aHtml = "<div data-bind=\"compose : { model: 'patient/doc/index', " +
            "activationData: { " +
            "SubscriberNum: '" + sNum + "'} " +
            "}, style: {height: '100%'}\"></div>";

        $("#fe-main-popup-header").html(aHeader);
        $("#fe-main-popup-view").html(aHtml);
        ko.cleanNode(document.getElementById("fe-main-popup"));
        ko.applyBindings(viewmodel, document.getElementById("fe-main-popup"));

        $("#fe-main-popup-overlay").show();
        $("#fe-main-popup").show();

    };

    function getPatientDetails() {
        getPatientDemographics();
        getPatientComments();
        getPatientClaims();
    };

    function savePatientDetails() {
        
        $(".fe-address-loader").css("display", "inline-block");

        subscriberNum($("#fe-subscriber-num").val());
        var sNum = $("#fe-subscriber-num").val();

        if (!sNum) {
            logger.logError("Unable to save patient details", "Error", null, true);
            $(".fe-address-loader").css("display", "none");
            return;
        }

        var obj = {
            TemplateName: "emr",
            SubscriberNum: sNum,
            PatAddress: $("#fe-patient-address2").val(),
            PatCity: $("#fe-patient-city2").val(),
            PatState: $("#fe-patient-state2").val(),
            PatZip: $("#fe-patient-zip2").val(),
            PatPhone: $("#fe-patient-phone2").val(),
            PatEmail: $("#fe-patient-email2").val(),
            RespName: $("#fe-patient-name3").val(),
            RespAddress: $("#fe-patient-address3").val(),
            RespCity: $("#fe-patient-city3").val(),
            RespState: $("#fe-patient-state3").val(),
            RespZip: $("#fe-patient-zip3").val(),
            RespPhone: $("#fe-patient-phone3").val(),
            RespEmail: $("#fe-patient-email3").val()
        };

        $.ajax(routeconfig.updatePatientDemographics, {
            type: "POST",
            dataType: "json",
            data: obj,
            success: function (data, status, xhr) {

                $(".fe-address-loader").css("display", "none");
                logger.logSuccess("Saved patient details", "Success", null, true);

            },
            error: function (data, status, xhr) {
                $(".fe-address-loader").css("display", "none");
                logger.logError(xhr, status, null, true);
            },
            fail: function (data, status, xhr) {
                $(".fe-address-loader").css("display", "none");
                logger.logError(xhr, status, null, true);

            }
        });

    };

    function refreshPatientClaims() {
        getPatientClaims();
    };

    function initControls() {
        var bName = utils.getBrowserName();
        browserName(bName);
        commentList([]);
        claimList([]);
        buildCommentGrid();
        buildClaimGrid();

        $("#fe-subscriber-num").focus();
    };

    //----------------------------------------------------------------------

    var viewmodel = {

        //State Functions
        canActivate: function () {
            return true;
        },
        activate: function (hospAcct) {

            if (hospAcct) {
                resetPatientDetails();
                $("#fe-subscriber-num").val(hospAcct);
                getPatientDetails();
            }

            app.on("patient:refreshcomments").then(function (model) {
                getPatientComments(model);
            });

            app.on("patient:resetpatientdetails").then(function () {
                resetPatientDetails();
            });

            app.on("patient:getpatientdetails").then(function (model) {
                $("#fe-subscriber-num").val(model.SubscriberNumber);
                getPatientDetails();
            });

        },
        compositionComplete: function () {
            initControls();
        },

        //Variables

        //Functions
        changeTabs: changeTabs,
        getPatientDetails: getPatientDetails,
        resetPatientDetails: resetPatientDetails,
        closePopup: closePopup,
        openCommentPopup: openCommentPopup,
        openGenerateDocumentPopup: openGenerateDocumentPopup,
        savePatientDetails: savePatientDetails,
        refreshPatientClaims: refreshPatientClaims
    };

    return viewmodel;
});