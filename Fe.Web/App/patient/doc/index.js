﻿//******************************
// File: doc/index.js
//******************************

define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/logger", "services/utils"], function (router, $, app, appsecurity, routeconfig, logger, utils) {

    //----------------------------------------------------------------------
    var patientNumber = new ko.observable();
    var subscriberNumber = new ko.observable();
    var selectedAddress = new ko.observable();

    function closePopup() {
        app.trigger("view:closepopup");
    };

    function buildDocumentGrid(data) {
        
        var cols = [{ field: "DocumentName", title: "Name" }];

        if (!data) data = [];

        var gHeight = 200;

        var grid = $("#fe-doc-generate-list-grid").kendoGrid({
            columns: cols,
            dataSource: {
                data: data
            },
            height: gHeight,
            selectable: "row",
            sortable: false,
            resizable: true,
            pageable: false
        }).data("kendoGrid");

        if (data.length === 1) {
            var row = $("#fe-doc-generate-list-grid").find('tbody:first').find('tr:first');
            grid.select(row);
        }

    };

    function populateDocumentGrid() {

        var dData = [];

        dData.push({ DocumentName: "Letter To Doctor Out Of Network" });
        dData.push({ DocumentName: "Letter To Claimant Out Of Network" });

        buildDocumentGrid(dData);

    };

    function getDocumentList() {
        populateDocumentGrid();
    };

    function generateDoc() {

        var selectedAddress = "";

        if ($("#fe-doc-generate-address1").hasClass("radio-selected")) {
            selectedAddress = "Pharmscreen";
        }

        if ($("#fe-doc-generate-address2").hasClass("radio-selected")) {
            selectedAddress = "Alternate";
        }

        if ($("#fe-doc-generate-address3").hasClass("radio-selected")) {
            selectedAddress = "Third Party";
        }

        var dGrid = $("#fe-doc-generate-list-grid").data("kendoGrid");
        var dItem = dGrid.dataItem(dGrid.select());

        if (!dItem) {
            logger.logError("Missing selected document", "Error", null, true);
        }

        var obj =
        {
            DocumentName: dItem.DocumentName,
            SubscriberNumber: subscriberNumber(),
            SelectedAddress: selectedAddress
        };

        $.ajax(routeconfig.generateDocument, {
            type: "POST",
            dataType: "json",
            data: obj,
            success: function (data, status, xhr) {
                closePopup();
                window.open(data, "_blank", "status = no, toolbar = no, location = no, left = 9999, top = 9999, width = 1, height = 1");
            },
            error: function (data, status, xhr) {
                logger.logError(xhr, status, null, true);
            },
            fail: function (data, status, xhr) {
                logger.logError(xhr, status, null, true);
            }
        });

    };

    function changeAddressSelection(data,target) {

        var a = target.currentTarget;
        if (a.classList.contains("radio-selected")) return;

        if ($("#fe-doc-generate-address1").hasClass("radio-selected")) {
            $("#fe-doc-generate-address1").removeClass("radio-selected");
            $("#fe-doc-generate-address1").find("span:first").removeClass("glyphicon glyphicon-check");
            $("#fe-doc-generate-address1").find("span:first").addClass("glyphicon glyphicon-unchecked");
        }
        
        if ($("#fe-doc-generate-address2").hasClass("radio-selected")) {
            $("#fe-doc-generate-address2").removeClass("radio-selected");
            $("#fe-doc-generate-address2").find("span:first").removeClass("glyphicon glyphicon-check");
            $("#fe-doc-generate-address2").find("span:first").addClass("glyphicon glyphicon-unchecked");
        }

        if ($("#fe-doc-generate-address3").hasClass("radio-selected")) {
            $("#fe-doc-generate-address3").removeClass("radio-selected");
            $("#fe-doc-generate-address3").find("span:first").removeClass("glyphicon glyphicon-check");
            $("#fe-doc-generate-address3").find("span:first").addClass("glyphicon glyphicon-unchecked");
        }

        $("#" + a.id).addClass("radio-selected");
        $("#" + a.id).find("span:first").removeClass("glyphicon glyphicon-unchecked");
        $("#" + a.id).find("span:first").addClass("glyphicon glyphicon-check");

        selectedAddress(a.outerText);
    };

    function initControls() {
        getDocumentList();
    };

    //----------------------------------------------------------------------

    var viewmodel = {

        //State Functions
        canActivate: function () {
            return true;
        },
        activate: function (settings) {

            if (settings) {
                if (settings.SubscriberNum) subscriberNumber(settings.SubscriberNum);
            }

        },
        compositionComplete: function () {
            initControls();
        },

        //Variables

        //Functions
        generateDoc: generateDoc,
        closePopup: closePopup,
        changeAddressSelection: changeAddressSelection

    };

    return viewmodel;
});