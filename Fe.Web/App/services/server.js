
define(['jquery', "services/appsecurity"], function ($, appsecurity) {

    var dataToQuery = function (data) {
        return Object
            .keys(data)
            .map(function (key) {
                return key + "=" + data[key];
            })
            .join("&");
    }

    return {

        get: function (url, data) {
            return $.ajax({
                url: url + (data ? "?" + dataToQuery(data) : ""),
                type: 'GET',
                header: appsecurity.getSecurityHeaders(),
                cache: false
            });
        },

        post: function (url, data, options) {
            if (Array.isArray(data)){
                return $.ajax({
                    url: url,
                    data: JSON.stringify(data),
                    contentType:'application/json',
                    crossDomain: true,
                    type: 'POST',
                    header: appsecurity.getSecurityHeaders(),
                    cache: false
                });
            }
            return $.ajax({
                url: url,
                data: data,
                type: 'POST',
                header: appsecurity.getSecurityHeaders(),
                cache: false
            });
        },

        remove: function (url, data) {
            return $.ajax({
                url: url + "?" + dataToQuery(data),
                type: 'DELETE',
                header: appsecurity.getSecurityHeaders(),
                cache: false
            });
        }
    };
});
