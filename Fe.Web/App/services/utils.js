﻿
define(['moment'], function (moment) {

    function convertDate(value) {

        var fDate = "";
        var shortFormats = ["M-D", "M/D", "MMDD"];
        var longFormats = [
            "MMDDYY", "MMDDYYYY", "M-D-YY", "MM-DD-YY", "M/D/YY", "MM/DD/YY",
            "M-D-YYYY", "MM-DD-YYYY", "M/D/YYYY", "MM/DD/YYYY"
        ];

        if (moment(value, shortFormats, true).isValid()) {

            var cYear = new Date().getFullYear();
            var eMonth = moment(value, shortFormats, true).format('M');
            var eDay = moment(value, shortFormats, true).format('D');

            var nDate = new Date(cYear, eMonth - 1, eDay, 0, 0, 0, 0);

            return moment(nDate).format("MM/DD/YYYY");

        }

        if (moment(value, longFormats, true).isValid()) {

            var cYear = moment(value, longFormats, true).format('YYYY');
            var eMonth = moment(value, longFormats, true).format('M');
            var eDay = moment(value, longFormats, true).format('D');

            var nDate = new Date(cYear, eMonth - 1, eDay, 0, 0, 0, 0);

            return moment(nDate).format("MM/DD/YYYY");
        }

        return "";

    };

    function isValidAmount(amount) {
        
        return new RegExp(/^[1-9]\d{0,6}(\.\d{0,3})*(,\d+)?$/).test(amount) || new RegExp(/^[0-9]+$/).test(amount);
       
        
    }

    function lookupIndex(array, prop, value) {
        for (var i = 0, len = array.length; i < len; i++)
            if (array[i] && array[i][prop] === value) return i;
    }
    function parseJson(jsonString) {
        try {

            var o = JSON.parse(jsonString);

            if (o && typeof o === "object" && o !== null) {
                return o;
            }
        } catch (e) {
        }

        return false;
    };

    function isWhitespaceOrEmpty(text) {
        return !/[^\s]/.test(text);
    }

    function lPad(value, padding) {
        var zeroes = new Array(padding + 1).join("0");
        return (zeroes + value).slice(-padding);
    }

    function compactObject(someObject) {

        for (var key in someObject) {
            if (!someObject[key] || someObject[key] == null) {
                delete someObject[key];
            }
            else {
                someObject[key] = $.trim(someObject[key]);
            }
        }
        return someObject;
    }

    function getBrowserName() {

        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browserName = navigator.appName;
        var fullVersion = '' + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;

        // In Opera, the true version is after "Opera" or after "Version"
        if ((verOffset = nAgt.indexOf("Opera")) != -1) {
            browserName = "Opera";
            fullVersion = nAgt.substring(verOffset + 6);
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                fullVersion = nAgt.substring(verOffset + 8);
        }
            // In MSIE, the true version is after "MSIE" in userAgent
        else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
            browserName = "Microsoft Internet Explorer";
            fullVersion = nAgt.substring(verOffset + 5);
        }
            // In Chrome, the true version is after "Chrome" 
        else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
            browserName = "Chrome";
            fullVersion = nAgt.substring(verOffset + 7);
        }
            // In Safari, the true version is after "Safari" or after "Version" 
        else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
            browserName = "Safari";
            fullVersion = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                fullVersion = nAgt.substring(verOffset + 8);
        }
            // In Firefox, the true version is after "Firefox" 
        else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
            browserName = "Firefox";
            fullVersion = nAgt.substring(verOffset + 8);
        }
            // In most other browsers, "name/version" is at the end of userAgent 
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            browserName = nAgt.substring(nameOffset, verOffset);
            fullVersion = nAgt.substring(verOffset + 1);
            if (browserName.toLowerCase() == browserName.toUpperCase()) {
                browserName = navigator.appName;
            }
        } else {
            browserName = "Microsoft Internet Explorer";
        }

        return browserName;
    };

    function copyToClipboard(value) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(value).select();
        document.execCommand("copy");
        $temp.remove();
    };

    function isEmpty(obj) {
        for (var prop in obj) {
            if (obj[prop] !== "")
                return false;
        }

        return true;
    }

    function generateGuid() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    };

    function removeSpecialCharacters(str) {
        return str.replace(/[^a-zA-Z0-9 ]/g, "");
    }

    return {
        convertDate: convertDate, parseJson: parseJson, getBrowserName: getBrowserName,
        copyToClipboard: copyToClipboard, isEmpty: isEmpty, generateGuid: generateGuid,
        lookupIndex: lookupIndex, compactObject: compactObject, isWhitespaceOrEmpty: isWhitespaceOrEmpty,
        isValidAmount: isValidAmount, lPad: lPad, removeSpecialCharacters: removeSpecialCharacters
    };

});