﻿/** 
 * @module Authentication module. 
 *         All the interactions with users should start from this module
 * @requires system
 * @requires router
 * @requires routeconfig
 * @requires utils
 */

define(["durandal/system", "durandal/app", "plugins/router", "services/routeconfig", "services/logger", "services/settings", "services/utils"],
    function (system, app, router, routeconfig, logger, settings, utils) {

        var self = this;

        var userInfo = ko.observable();

        // Get the Authorization header for include in requests
        function getUserInfo() {
            var uInfo = sessionStorage["userDetails"] || localStorage["userDetails"];

            var uObj = utils.parseJson(uInfo);

            if (uObj !== false) {
                return uObj;
            }

            return {};
        };

        // Get the user client number
        function getUserClientNumber() {
            var userClientNum = sessionStorage["userClientNum"] || localStorage["userClientNum"];

            if (userClientNum) {
                return userClientNum;
            }

            return {};
        };

        // Get the user client name
        function getUserClientName() {
            var userClientName = sessionStorage["userClientName"] || localStorage["userClientName"];

            if (userClientName) {
                return userClientName;
            }

            return {};
        };

        // Get the user hospital code
        function getUserHospCode() {
            var userHospCode = sessionStorage["userHospCode"] || localStorage["userHospCode"];

            if (userHospCode) {
                return userHospCode;
            }

            return {};
        };

        // Get the user client number
        function getUserTemplate() {
            var userTemplate = sessionStorage["userTemplate"] || localStorage["userTemplate"];

            if (userTemplate) {
                return userTemplate;
            }

            return {};
        };

        // Get the Authorization header for include in requests
        function getSecurityHeaders() {
            var accessToken = sessionStorage["accessToken"] || localStorage["accessToken"];

            if (accessToken) {
                return { "Authorization": "Bearer " + accessToken };
            }

            return {};
        };

        // Get the user model for logout
        function getUserModel() {
            var userModel = sessionStorage["userModel"] || localStorage["userModel"];

            if (userModel) {
                return userModel;
            }

            return {};
        };

        // Get token expiration
        function getTokenExpire() {
            var tokenExpire = sessionStorage["tokenExpire"] || localStorage["tokenExpire"];

            if (tokenExpire) {
                return tokenExpire;
            }

            return {};
        };

        //function logOut() {
        //    var uObj = appsecurity.getUserModel();

        //    var obj = utils.parseJson(uObj);

        //    if (obj !== false) {

        //        $.ajax(routeconfig.logout, {
        //            type: "POST",
        //            data: obj,
        //            header: appsecurity.getSecurityHeaders(),
        //            success: function (jqXhr, error, statusText) {

        //                if (statusText) {

        //                    if (statusText.statusText === "OK") {
        //                        logger.logSuccess('Logout Successful', statusText.statusText, null, false);

        //                        appsecurity.clearUserInfo();
        //                        app.setRoot('account/login/index', 'entrance');
        //                    } else {
        //                        logger.logError(error, error, null, true);
        //                    }

        //                } else {
        //                    logger.logError(error, error, null, true);
        //                }
        //            },
        //            error: function (jqXhr, error, statusText) {

        //                if (jqXhr) {
        //                    var jObj = utils.parseJson(jqXhr.responseText);

        //                    if (jObj !== false) {

        //                        logger.logError(jObj.Message, error, null, true);
        //                    } else {
        //                        logger.logError(statusText, error, null, true);
        //                    }
        //                }
        //            }
        //        });
        //    }
        //}

        // Clear stored token expiration from local and session storage
        function clearUserInfo() {

            userInfo(null);

            localStorage.removeItem("userDetails");
            localStorage.removeItem("accessToken");
            localStorage.removeItem("tokenExpire");
            localStorage.removeItem("userModel");
            localStorage.removeItem("userHospCode");
            localStorage.removeItem("userClientNum");
            localStorage.removeItem("userClientName");
            localStorage.removeItem("userTemplate");

            sessionStorage.removeItem("userDetails");
            sessionStorage.removeItem("accessToken");
            sessionStorage.removeItem("tokenExpire");
            sessionStorage.removeItem("userModel");
            sessionStorage.removeItem("userHospCode");
            sessionStorage.removeItem("userClientNum");
            sessionStorage.removeItem("userClientName");
            sessionStorage.removeItem("userTemplate");

            logger.logSuccess('Cleared User Info', null, null, null);

            app.trigger("view:changeclientinfo", null);
        };

        function setUserInfo(userName, statusCodeListIds, perms, isAuthenticated, changePassword, accessToken, persistent, tokenExpire, userModel) {

            var uName = "";
            var uModel = utils.parseJson(userModel);

            if (uModel !== false) { if (uModel.Claims) { if (uModel.Claims.FirstName) { uName = uModel.Claims.FirstName; } } }

            var obj = { userName: userName, displayName: uName, statusCodeListIds: statusCodeListIds, perms: perms, isAuthenticated: isAuthenticated, changePassword: changePassword };
            userInfo(obj);

            if (persistent) {
                localStorage["userDetails"] = JSON.stringify(obj);
                localStorage["accessToken"] = accessToken;
                localStorage["tokenExpire"] = tokenExpire;
                localStorage["userModel"] = userModel;
            } else {
                sessionStorage["userDetails"] = JSON.stringify(obj);
                sessionStorage["accessToken"] = accessToken;
                sessionStorage["tokenExpire"] = tokenExpire;
                sessionStorage["userModel"] = userModel;
            }

            logger.logSuccess('Set User Info', obj, null, null);

        };

        function setUserClient(hospCode, clientNum, clientName, template, persistent) {

            if (persistent) {
                localStorage["userHospCode"] = hospCode;
                localStorage["userClientNum"] = clientNum;
                localStorage["userClientName"] = clientName;
                localStorage["userTemplate"] = clientNum === "90414A" || clientNum === "90414B" ? "emr" : "hfmi";
            } else {
                sessionStorage["userHospCode"] = hospCode;
                sessionStorage["userClientNum"] = clientNum;
                sessionStorage["userClientName"] = clientName;
                sessionStorage["userTemplate"] = clientNum === "90414A" || clientNum === "90414B" ? "emr" : "hfmi";
            }

            app.trigger("view:changeclientinfo", {
                hospCode: hospCode,
                clientNum: clientNum,
                clientName: clientName,
                template: clientNum === "90414A" || clientNum === "90414B" ? "emr" : "hfmi"
            });

            logger.logSuccess('Set User Client', null, null, null);

        };

        function hasRole(role) {

            var cUser = userInfo();

            if (cUser.userName === "admin") return true;

            var uRoles = cUser.roles;
            var found = false;

            $.each(uRoles, function (index, item) {
                if (item === role) found = true;
            });

            if (found) return true;

            return false;

        };

        function hasPermission(perm) {
            var access = perm.split(":")[0];
            var name = perm.split(":")[1];
            var user = getUserInfo();

            return user.userName === "admin" || user.perms.some(function (permission) {
                return permission.PermName.toLowerCase() === name &&
                    ((access === "view" && permission.ViewPerm === "Y") ||
                        (access === "save" && permission.SavePerm === "Y"));
            });
        };

        function checkSession(callback) {

            $.ajax(routeconfig.getSessionState, {
                type: "GET",
                dataType: "json",
                async: false,
                success: callback
            });
        };

        function logOut() {

            var uObj = getUserModel();

            var obj = utils.parseJson(uObj);

            if (obj !== false) {

                $.ajax(routeconfig.logout, {
                    type: "POST",
                    data: obj,
                    header: getSecurityHeaders(),
                    success: function (jqXhr, error, statusText) {

                        if (statusText) {

                            if (statusText.statusText === "OK") {
                                logger.logSuccess('Logout Successful', statusText.statusText, null, false);

                                app.setRoot('account/login/index', 'entrance');

                                clearUserInfo();

                            } else {
                                logger.logError(error, error, null, true);
                            }

                        } else {
                            logger.logError(error, error, null, true);
                        }
                    },
                    error: function (jqXhr, error, statusText) {

                        if (jqXhr) {
                            var jObj = utils.parseJson(jqXhr.responseText);

                            if (jObj !== false) {

                                logger.logError(jObj.Message, error, null, true);
                            } else {
                                logger.logError(statusText, error, null, true);
                            }
                        }
                    }
                });
            }
        };

        function hasViewPermission(perm) {

            var cUser = getUserInfo();

            if (cUser.userName == "admin") return true;

            var uPerms = cUser.perms;
            var found = false;

            if (!uPerms) return false;

            $.each(uPerms, function (index, item) {
                if (item.PermName === perm && item.ViewPerm === 'Y') found = true;
            });

            if (found) return true;

            return false;

        };

        function hasSavePermission(perm) {

            var cUser = getUserInfo();

            if (cUser.userName == "admin") return true;

            var uPerms = cUser.perms;
            var found = false;

            if (!uPerms) return false;

            $.each(uPerms, function (index, item) {
                if (item.PermName === perm && item.SavePerm === 'Y') found = true;
            });

            if (found) return true;

            return false;

        };


        var securitymodel = {

            //Variables
            userInfo: userInfo,

            //Functions
            setUserInfo: setUserInfo,
            clearUserInfo: clearUserInfo,
            setUserClient: setUserClient,
            hasRole: hasRole,
            hasPermission: hasPermission,
            checkSession: checkSession,
            getUserInfo: getUserInfo,
            getSecurityHeaders: getSecurityHeaders,
            getUserModel: getUserModel,
            getTokenExpire: getTokenExpire,
            getUserClientNumber: getUserClientNumber,
            getUserClientName: getUserClientName,
            getUserHospCode: getUserHospCode,
            getUserTemplate: getUserTemplate,
            logOut: logOut,
            hasViewPermission: hasViewPermission,
            hasSavePermission: hasSavePermission,
        };

        return securitymodel;
    });