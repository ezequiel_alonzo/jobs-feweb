define(function () {

    $(window).resize(function () {
        $(".k-grid")
            .filter(function () {
                return this.style.height.indexOf("%") > -1;
            })
            .each(function () {
                $(this).data("kendoGrid").resize();
            });
    });

    return {
        deleteSelectedRows: function (grid, id) {

            var selectedItemIds = grid.select()
                .map(function () { return grid.dataItem($(this))[id]; })
                .get();

            var data = grid.dataSource.data().filter(function (item) {
                return selectedItemIds.indexOf(item[id]) === -1;
            });

            grid.dataSource.data(data);
        }
    }
})