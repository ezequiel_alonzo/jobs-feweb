﻿/** 
 * @module Route table
 */
define(function () {

    var routes = {

        //*************************************************************
        //Breeze Routes                                              **
        //*************************************************************
        lookupUrl: "durandalauth/lookups",
        saveChangesUrl: "durandalauth/savechanges",
        publicArticlesUrl: "durandalauth/publicarticles",
        privateArticlesUrl: "durandalauth/privatearticles",
        categoriesUrl: "durandalauth/categories",

        //*************************************************************
        // Account Controller                                        **
        //*************************************************************
        login: "./token",
        logout: "./api/account/logout",
        getCurrentUserName: "./api/account/getcurrentusername",
        isAuthenticatedUser: "./api/account/isauthenticateduser",
        getSessionState: "./api/account/getsessionstate",
        isCheckedIn: "./api/account/getcheckedinstatus",
        updateCheckInstatus: "./api/account/updatecheckinstatus",
        getUserPermissions: "./api/account/getuserpermissions",
        getCheckinListByIp: "./api/account/getcheckinlistbyip",
        //*************************************************************
        // Account Controller                                        **
        //*************************************************************
        updatePassword: "./api/user/updatepassword",

        //*************************************************************
        // Client Controller                                         **
        //*************************************************************
        getUserMasterAccounts: "./api/client/getusermasteraccounts",
        getUserClients: "./api/client/getuserclients",
        getUserChopperClients: "./api/client/getuserchopperclients",
        getCallCounter: "./api/client/getcallcounter",
        getUserStatusCodeLists: "./api/client/getuserstatuscodelists",

        //*************************************************************
        // Patient Controller                                        **
        //*************************************************************
        getPatientDemographics: "./api/patient/getpatientdemographics",
        updatePatientDemographics: "./api/patient/updatepatientdemographics",
        inserthfmipatientdemographics: "./api/patient/inserthfmipatientdemographics",
        updatehfmipatientdemographics: "./api/patient/updatehfmipatientdemographics",
        searchPatients: "./api/patient/searchpatients",

        //*************************************************************
        // History Controller                                        **
        //*************************************************************
        getpatienthistory: "./api/history/getpatienthistory",

        //*************************************************************
        // Comment Controller                                        **
        //*************************************************************
        getActivityCodes: "./api/comment/getactivitycodes",
        getCommCodes: "./api/comment/getcommcodes",
        getCommCodesByListId: "./api/comment/getcommcodesbylistid",
        getPatientComments: "./api/comment/getpatientcomments",
        addComment: "./api/comment/addcomment",
        addMultiComment: "./api/comment/addmulticomment",

        //*************************************************************
        // Posting Controller                                        **
        //*************************************************************
        getPatientPostings: "./api/posting/getpatientpostings",
        getpostinguserlist: "./api/posting/getpostinguserlist",
        addPosting: "./api/posting/addposting",
        deletePosting: "./api/posting/deleteposting",

        //*************************************************************
        // Claim Controller                                          **
        //*************************************************************
        getPatientClaims: "./api/claim/getpatientclaims",

        //*************************************************************
        // Rep Status Controller                                     **
        //*************************************************************
        getRepStatusList: "./api/repstatus/getrepstatuslist",

        //*************************************************************
        // Document Controller                                       **
        //*************************************************************
        generateDocument: "./api/document/generateDocument",        
        
        //*************************************************************
        // Reports Controller                                        **
        //*************************************************************
        reportService: "./api/reports/",
        getReportList: "./api/reports/getreportlist",

        //*************************************************************
        // Employee Controller                                       **
        //*************************************************************
        getEmployees: "./api/employee/getemployees",
        getEmployeeInfo: "./api/employee/getemployeeinfo",
        getEmployeeInfoList: "./api/employee/getemployeeinfolist",
        UpdateEmployeeInfo: "./api/employee/updateemployeeinfo",
        //*************************************************************
        // Export Controller                                         **
        //*************************************************************
        generateMaxBatch: "./api/export/generatemaxbatch",
        generate270: "./api/export/generate270",

        //*************************************************************
        // Letters Controller                                         **
        //*************************************************************
        getLettersFile: "./api/letters/getlettersfile",
        getLettersTemplates: "./api/letters/getletterstemplates",
        clearLettersQueue: "./api/letters/clearlettersqueue",

        //*************************************************************
        // Setting Templates Special Controller                      **
        //*************************************************************
        saveTemplate: "./api/settingtemplates/savetemplate",
        getTemplates: "./api/settingtemplates/gettemplates",
        deletetemplate: "./api/settingtemplates/deletetemplate",

        //*************************************************************
        // RpcMq Controller                                         **
        //*************************************************************
        rpcMqReceiveMessage: "./api/rpcmq/rpcmqreceivemessage",
        rpcMqReceiveAllMessages: "./api/rpcmq/rpcmqreceiveallmessages",
        rpcMqCloseChannel: "./api/rpcmq/rpcmqclosechannel",

        //*************************************************************
        // Files Controller                                          **
        //*************************************************************
        getFilesList: "./api/files/getfileslist",
        downloadFile: "./api/files/downloadfile",
        deleteFile: "./api/files/deletefile"



    };

    return routes;

});