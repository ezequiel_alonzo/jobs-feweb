﻿//******************************
// File: header/index.js
//******************************

define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/logger", "services/utils"], function (router, $, app, appsecurity, routeconfig, logger, utils) {

    //----------------------------------------------------------------------
    var currentTemplate = new ko.observable();
    var isCheckedIn = ko.observable();

    function logOut() {
        var uObj = appsecurity.getUserModel();

        var obj = utils.parseJson(uObj);

        if (obj !== false) {

            $.ajax(routeconfig.logout, {
                type: "POST",
                data: obj,
                header: appsecurity.getSecurityHeaders(),
                success: function (jqXhr, error, statusText) {

                    if (statusText) {

                        if (statusText.statusText === "OK") {
                            logger.logSuccess('Logout Successful', statusText.statusText, null, false);

                            appsecurity.clearUserInfo();

                            app.setRoot('account/login/index', 'entrance');
                        } else {
                            logger.logError(error, error, null, true);
                        }

                    } else {
                        logger.logError(error, error, null, true);
                    }
                },
                error: function (jqXhr, error, statusText) {

                    if (jqXhr) {
                        var jObj = utils.parseJson(jqXhr.responseText);

                        if (jObj !== false) {

                            logger.logError(jObj.Message, error, null, true);
                        } else {
                            logger.logError(statusText, error, null, true);
                        }
                    }
                }
            });
        }
    };

    function getCheckedInStatus() {


        var uObj = appsecurity.getUserModel();

        var obj = utils.parseJson(uObj);

        if (obj !== false) {

            $.ajax(routeconfig.isCheckedIn, {
                type: "POST",
                data: obj,
                header: appsecurity.getSecurityHeaders(),
                success: function (jqXhr, error, statusText) {

                    if (statusText.responseText === 'true') {
                        isCheckedIn(true);
                    }
                    else {
                        isCheckedIn(false);
                    }


                },
                error: function (jqXhr, error, statusText) {
                }
            });


        }
    }

    function updateCheckinStatus(actualCheckinstatus) {

        var uObjChecked = appsecurity.getUserModel();


        var obj = utils.parseJson(uObjChecked);
        obj.checkInStatus = actualCheckinstatus;

        if (obj !== false) {

            $.ajax(routeconfig.updateCheckInstatus, {
                type: "POST",
                data: obj,
                header: appsecurity.getSecurityHeaders(),
                success: function (jqXhr, error, statusText) {

                    if (actualCheckinstatus === 'checkedIn') {
                        $('#fe-checkin').show();
                        $('#fe-checkout').hide();
                    }
                    else {
                        $('#fe-checkin').hide();
                        $('#fe-checkout').show();
                    }

                },
                error: function (jqXhr, error, statusText) {
                    //fail
                }
            });
        }

    }

    function showPatientView() {
        $("#fe-patient-view").show();
        $("#fe-rep-status-view").hide();
        $("#fe-rep-status-special-view").hide();
        $("#fe-reports-view").hide();
        $("#fe-imports-view").hide();
    };

    function showRepStatusView() {
        $("#fe-patient-view").hide();
        $("#fe-rep-status-view").show();
        $("#fe-rep-status-special-view").hide();
        $("#fe-reports-view").hide();
        $("#fe-imports-view").hide();
        if ($("#fe-rep-status-grid").data("kendoGrid")) $("#fe-rep-status-grid").data("kendoGrid").resize();
    };

    function showRepStatusSpecialView() {
        $("#fe-patient-view").hide();
        $("#fe-rep-status-view").hide();
        $("#fe-rep-status-special-view").show();
        $("#fe-reports-view").hide();
        $("#fe-imports-view").hide();
    };

    function showReportsView() {
        $("#fe-patient-view").hide();
        $("#fe-rep-status-view").hide();
        $("#fe-rep-status-special-view").hide();
        $("#fe-reports-view").show();
        $("#fe-imports-view").hide();
    };

    function showImportsView() {
        $("#fe-patient-view").hide();
        $("#fe-rep-status-view").hide();
        $("#fe-rep-status-special-view").hide();
        $("#fe-reports-view").hide();
        $("#fe-imports-view").show();
    };

    function getUserDisplayName() {

        var uModel = appsecurity.getUserInfo();
        if (uModel.displayName) {
            return uModel.displayName.toUpperCase();
        }
        return "";

    };

    function openClientSelectPopup() {

        var aHeader = "<div style=\"padding: 4px;\"><span>Client</span></div>" +
                      "<span class=\"glyphicon glyphicon-remove glyphicon-button\" " +
                      "style=\"position: absolute; top: 4px; right: 4px; \" " +
                      "data-bind=\"click: closePopup\">" +
                      "</span>";

        var aHtml = "<div data-bind=\"compose : 'user/client/index', style: {height: '100%'}\"></div>";

        $("#fe-main-popup-header").html(aHeader);
        $("#fe-main-popup-view").html(aHtml);
        $("#fe-main-popup").css("min-width", "400px");
        ko.cleanNode(document.getElementById("fe-main-popup"));
        ko.applyBindings(viewmodel, document.getElementById("fe-main-popup"));

        $("#fe-main-popup-overlay").show();
        $("#fe-main-popup").show();

    };

    function closePopup() {
        app.trigger("view:closepopup");
    };

    //----------------------------------------------------------------------

    var viewmodel = {

        //State Functions
        canActivate: function () {
            return true;
        },
        activate: function () {

        },
        compositionComplete: function () {
            getCheckedInStatus();
            currentTemplate(appsecurity.getUserTemplate());
        },

        //Variables
        isCheckedIn: isCheckedIn,
        currentTemplate: currentTemplate,

        //Functions
        logOut: logOut,
        showPatientView: showPatientView,
        showRepStatusView: showRepStatusView,
        showRepStatusSpecialView: showRepStatusSpecialView,
        showReportsView: showReportsView,
        showImportsView: showImportsView,
        getUserDisplayName: getUserDisplayName,
        openClientSelectPopup: openClientSelectPopup,
        closePopup: closePopup,
        updateCheckinStatus: updateCheckinStatus
    };

    return viewmodel;
});