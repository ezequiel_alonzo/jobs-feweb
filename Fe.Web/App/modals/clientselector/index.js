﻿define(["durandal/app", "plugins/dialog", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger", "plugins/router", "services/server"],
    function (app, dialog, appsecurity, routeconfig, utils, logger,router,server) {

        var ClientSelector = function () {
            this.title = "Select Client";

            this.masterAccountList = new ko.observableArray([]);
            this.clientList = new ko.observableArray([]);
            this.selectedMaster = new ko.observable();
            this.selectedClient = new ko.observable();
            var hasPatientLoaded = new ko.observable(false);
            

            this.buildUserClientGrid = function (data) {

                $("#fe-user-client-sub-grid").remove();
                $("#fe-user-client-sub-container").append("<div id=\"fe-user-client-sub-grid\" class=\"display-block\" style=\"width: 100%;\"></div>");

                var cols = [{ field: "ClientNum", title: "Client Number", width: "80px" },
                            { field: "Name", title: "Name" }];

                if (!data) data = [];

                var gHeight = 200;

                var grid = $("#fe-user-client-sub-grid").kendoGrid({
                    columns: cols,
                    dataSource: {
                        data: data
                    },
                    height: gHeight,
                    selectable: "row",
                    sortable: false,
                    resizable: true,
                    pageable: false
                }).data("kendoGrid");

                if (data.length === 1) {
                    var row = $("#fe-user-client-sub-grid").find('tbody:first').find('tr:first');
                    grid.select(row);
                }

            };

            this.getUserClients = function () {
                var modal = this;
                var uProf = appsecurity.getUserInfo();

                var maGrid = $("#fe-user-client-master-grid").data("kendoGrid");
                var selectedItem = maGrid.dataItem(maGrid.select());
                var hospCode = selectedItem.HospCode;
                modal.selectedMaster(hospCode);

                var eSeg = "?userName=" + uProf.userName + "&hospCode=" + hospCode;

                $.ajax(routeconfig.getUserClients + eSeg, {
                    type: "GET",
                    dataType: "json",
                    success: function (data, status, xhr) {

                        var jObj = utils.parseJson(data);

                        if (jObj !== false) {

                            modal.clientList(jObj);

                            var cData = [];

                            $.each(jObj, function (index, item) {
                                var tmpItem = {
                                    ClientNum: item.ClientNum,
                                    Name: item.Name
                                };
                                cData.push(tmpItem);
                            });

                            modal.buildUserClientGrid(cData);

                        }

                    },
                    error: function (data, status, xhr) {

                    },
                    fail: function (data, status, xhr) {

                    }
                });

            };

            this.buildUserMasterAccountGrid = function (data) {

                var cols = [{ field: "HospCode", title: "Code", width: "80px" },
                            { field: "Name", title: "Name" }];

                if (!data) data = [];

                var gHeight = 200;

                var grid = $("#fe-user-client-master-grid").kendoGrid({
                    columns: cols,
                    dataSource: {
                        data: data
                    },
                    height: gHeight,
                    selectable: "row",
                    sortable: false,
                    resizable: true,
                    pageable: false,
                    change: this.getUserClients.bind(this)
                }).data("kendoGrid");

                if (data.length === 1) {
                    var row = $("#fe-user-client-master-grid").find('tbody:first').find('tr:first');
                    grid.select(row);
                }

            };

            this.getUserMasterAccounts = function () {

                var modal = this;

                var uProf = appsecurity.getUserInfo();

                var eSeg = "?userName=" + uProf.userName;

                $.ajax(routeconfig.getUserMasterAccounts + eSeg, {
                    type: "GET",
                    dataType: "json",
                    success: function (data, status, xhr) {

                        var jObj = utils.parseJson(data);

                        if (jObj !== false) {
                            modal.masterAccountList(jObj);
                            modal.selectedMaster(null);

                            var maData = [];

                            $.each(jObj, function (index, item) {
                                var tmpItem = {
                                    HospCode: item.HospCode,
                                    Name: item.Name
                                };
                                maData.push(tmpItem);
                            });

                            modal.buildUserMasterAccountGrid(maData);

                        }

                    },
                    error: function (data, status, xhr) {

                    },
                    fail: function (data, status, xhr) {

                    }
                });

            };

            this.selectUserClient = function () {

                var master = this.selectedMaster();

                if (!master) {
                    logger.logError("Missing Master Account", "Error", null, true);
                    return;
                }

                var cGrid = $("#fe-user-client-sub-grid").data("kendoGrid");
                var selectedItem = cGrid.dataItem(cGrid.select());

                if (!selectedItem) {
                    logger.logError("Missing Client", "Error", null, true);
                    return;
                }
                
                appsecurity.setUserClient(this.selectedMaster(), selectedItem.ClientNum, selectedItem.Name);
                
                this.close();
                if (!checkingRemind && this.selectedMaster() != '90414') {
                    server
                                .get(routeconfig.isCheckedIn, JSON.parse(appsecurity.getUserModel()))
                                .then(function (response, error, statusText) {
                                    if (!response) {
                                        app.showMessage("Don't forget to check in !!!");
                                        checkingRemind = true;
                                    }
                                });
                }
            };

            this.compositionComplete = function () {
                this.buildUserClientGrid();
                this.getUserMasterAccounts();
            };

            this.close = function () {
                dialog.close(this);
            };
        };

        ClientSelector.open = function () {
            return dialog.show(new ClientSelector());
        };

        return ClientSelector;
    });