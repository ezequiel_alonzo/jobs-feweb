define(["plugins/dialog", "durandal/app"], function (dialog, app) {

    var InputModal = function (title, label, data) {
        this.title = title;
        this.label = label;

        this.enableDelete = !!data.deleteItem;
        this.items = new kendo.data.DataSource({ data: data.items });
        this.selectedItem = ko.observable(data.selectedItem);

        this.attached = function () {
            var dropDown = $("#items-dropdown").data("kendoDropDownList");

            dropDown.setOptions({
                dataTextField: data.textField,
                dataValueField: data.valueField
            });
            dropDown.value(data.selectedItem);
        };
        this.deleteItem = function () {
            var modal = this;
            
            app.showMessage("The item will be deleted. Are you sure?", "Confirm", ["Yes", "No"]).then(function (result) {
                if (result !== "Yes") return;

                data.deleteItem(modal.selectedItem());
                modal.items.remove(modal.selectedItem());
                modal.selectedItem(null);
            });
        };
    };

    InputModal.prototype.ok = function () {
        var item = this.selectedItem()

        dialog.close(this, {
            selectedItem: item,
            hasSelectedItem: true
        });
    };

    InputModal.prototype.close = function () {
        dialog.close(this, {
            hasSelectedItem: false
        });
    };

    InputModal.open = function (title, label, items) {
        return dialog.show(new InputModal(title, label, items));
    };

    return InputModal;
});