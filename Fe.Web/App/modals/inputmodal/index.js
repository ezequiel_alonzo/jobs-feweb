define(["plugins/dialog"], function (dialog) {

    var InputModal = function (title, label) {
        this.title = title;
        this.label = label;

        this.input = ko.observable();
    };
    
    InputModal.prototype.ok = function () {
        dialog.close(this, {
            input: this.input(),
            hasInput: true
        });
    };

    InputModal.prototype.close = function () {
        dialog.close(this, {
            hasInput: false
        });
    };
    
    InputModal.open = function (title, label) {
        return dialog.show(new InputModal(title, label));
    };

    return InputModal;
});