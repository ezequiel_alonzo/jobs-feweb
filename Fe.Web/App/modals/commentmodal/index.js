﻿/// <reference path="index.js" />
define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/utils", "services/logger", "plugins/dialog",
    "modals/commentmodal/multicommentmodal/index", "moment", "services/server"], function (router, $, app, appsecurity, routeconfig, utils, logger, dialog, multicommentmodal, moment, server) {
    var templateName = new ko.observable();
    var patientNum = new ko.observable();
    var subscriberNum = new ko.observable();
    var activityCodes = new ko.observableArray([]);
    var commCodes = new ko.observableArray([]);
    var acText = new ko.observable();
    var ccText = new ko.observable();
    var defaultAc = new ko.observable();
    var defaultSt = new ko.observable();
    var isSpecialCBDate = new ko.observable(false);
    var specialCbDatesStatus = ["705", "210"];
    this.isSpecialCBDate = isSpecialCBDate;
    var spanishPat = ko.observable(false);
    this.spanishPat = spanishPat;
    var afterHoursPat = ko.observable(false);
    this.afterHoursPat = afterHoursPat;
    var fieldVisitPat = ko.observable(false);
    this.fieldVisitPat = fieldVisitPat;
    var cbDate = ko.observable();
    this.cbDate = cbDate;
    this.width = "750px";
    this.bodyHeight = "360px";
    var patient = ko.observable();
    var repstatusSpecial = ko.observable(false);
    this.repstatusSpecial = repstatusSpecial;
    var repSpecialPatients;
    var patInfoUpdateBar = ko.observable(true);
    this.patInfoUpdateBar = patInfoUpdateBar;
    var cbMaxDatesDict = [{ 705: 30 },{ 210: 10 }];

    var commentModal = function (title, label, settings) {
        this.title = title;
        this.label = label;
                
        cbDate("");

        if (settings) {
            if (settings.TemplateName) {
                templateName(settings.TemplateName)
                if (templateName() === 'emr') patInfoUpdateBar(false);
            }
            if (settings.PatientNum) {
                patientNum(settings.PatientNum);
            }
            if (settings.patient) {
                patient(settings.patient);
                if (settings.patient.Spanish === "1") {
                    spanishPat(true);
                }
                if (settings.patient.FieldVisit === "1") {
                    fieldVisitPat(true);
                }
                if (settings.patient.AfterHours === "1") {
                    afterHoursPat(true);
                }
                
            }
            if (settings.SubscriberNum) subscriberNum(settings.SubscriberNum);
            activityCodes([]);
            commCodes([]);
            acText("");
            ccText("");
            if (settings.DefaultAc) defaultAc(settings.DefaultAc);
            if (settings.DefaultSt) defaultSt(settings.DefaultSt);

        }
    }

    commentModal.prototype.addMultiCommentRepStatusSpecial = function () {


        modal = this;

        var acGrid = $("#fe-comment-ac-grid").data("kendoGrid");
        var ccGrid = $("#fe-comment-cc-grid").data("kendoGrid");
        var cText = $("#fe-comment-text").val();
        var acItem = acGrid.dataItem(acGrid.select());
        var ccItem = ccGrid.dataItem(ccGrid.select());
        var uProf = appsecurity.getUserInfo();

        var ac = false;
        var cc = false;
        var ct = false;

        if (ccItem) cc = true;
        if (cText) ct = true;
        if (acItem) ac = true;

        if (ac && cc && ct) {
            
            server.post(routeconfig.addMultiComment, {
                patientList: repSpecialPatients, status: ccItem.CcCode, comment: cText.toUpperCase(),
                actionCode: acItem.AcCode, userName: uProf.userName.toUpperCase(), spanishPat: spanishPat(),
                fieldVisitPat: fieldVisitPat(),afterHoursPat: afterHoursPat()
            }).then(function () {
                repstatusSpecial(false);
                dialog.close(modal);
                app.showMessage("Patients updated");
            });
        }
        else {
            var msg = "Missing required field(s): ";
            if (!ac && !cc && !ct) msg += "Action, Status, Comment";
            if (!ac && !cc && ct) msg += "Action, Status";
            if (!ac && cc && !ct) msg += "Action, Comment";
            if (ac && !cc && !ct) msg += "Status, Comment";
            if (!ac && cc && ct) msg += "Action";
            if (ac && !cc && ct) msg += "Status";
            if (ac && cc && !ct) msg += "Comment";

            logger.logError(msg, "Error", null, true);
        }
    }

    commentModal.prototype.attached = function () {

        var tName = templateName();
        if (tName) tName = tName.toLowerCase();
        switch (tName) {
            case "hfmi":
                getActivityCodes();
                getCommCodes(appsecurity.getUserClientNumber());
                break;
            case "emr":
                $("#fe-comment-ac-container").hide();
                $("#fe-comment-cc-container").css("width", "calc(100% - 4px)");
                getActivityCodes();
                getCommCodes("90414A");
                break;
            default:
                getActivityCodes();
                getCommCodes(appsecurity.getUserClientNumber());
                break;
        }

    }

    commentModal.prototype.addMultiComment = function () {

        var acGrid = $("#fe-comment-ac-grid").data("kendoGrid");
        var ccGrid = $("#fe-comment-cc-grid").data("kendoGrid");
        var cText = $("#fe-comment-text").val();
        var acItem = acGrid.dataItem(acGrid.select());
        var ccItem = ccGrid.dataItem(ccGrid.select());
        var uProf = appsecurity.getUserInfo();

        var ac = false;
        var cc = false;
        var ct = false;

        if (ccItem) cc = true;
        if (cText) ct = true;

        if (templateName() === 'hfmi') {

            if (acItem) ac = true;

            var patientList = [];

            if (ac && cc && ct) {
                var obj = {
                    UserName: uProf.userName,
                    TemplateName: templateName(),
                    PatientNum: patientNum(),
                    SubscriberNum: subscriberNum(),
                    Status: ccItem.CcCode,
                    Comment: cText.toUpperCase(),
                    ActionCode: acItem.AcCode
                };
                patientNum($('#fe-patient-num').val());
                var pNum = patientNum();


                var patientCommentInfo = {
                    TemplateName: 'hfmi',
                    PatientNum: pNum,
                    CcCode: ccItem.CcCode,
                    cText: cText,
                    AcCode: acItem.AcCode,
                    CbDate: cbDate(),
                    SpanishPat: spanishPat(),
                    FieldVisitPat: fieldVisitPat(),
                    AfterHoursPat: afterHoursPat()
                };

                if (specialCbDatesStatus.indexOf(patientCommentInfo.CcCode) > -1) {
                    if (!cbDate()) {
                        logger.logError("For this status callback date is required.", "cbDate", null, true);
                        return;
                    }
                    if (!utils.convertDate(cbDate()) && specialCbDatesStatus.indexOf(patientCommentInfo.CcCode) > -1) {
                        logger.logError("Invalid callback date", "Invalid callback Date", null, true);
                        return;
                    }
                    if (moment(cbDate(), 'M/D/YYYY').diff(new Date(), 'days') < 0) {
                        logger.logError("callback day can not be prior to the current date", "Invalid callback Date", null, true);
                        return;
                    }

                    if (moment(cbDate(), 'M/D/YYYY').diff(new Date(), 'days') > cbMaxDatesDict.filter(function (code) {
                         return code.hasOwnProperty(patientCommentInfo.CcCode)
                    })
                         [0][patientCommentInfo.CcCode]) {
                        logger.logError("Callback date can not be more than " + cbMaxDatesDict.filter(function (code) {
                            return code.hasOwnProperty(patientCommentInfo.CcCode)
                        })
                         [0][patientCommentInfo.CcCode] + " days from the current date", "Invalid callback Date", null, true);
                        return;
                    }
                }
                isSpecialCBDate(false);
                fieldVisitPat(false);
                afterHoursPat(false);
                spanishPat(false);
                dialog.close(this);
                multicommentmodal.open("select the patients", "patients", patientCommentInfo);

            } else {
                var msg = "Missing required field(s): ";
                if (!ac && !cc && !ct) msg += "Action, Status, Comment";
                if (!ac && !cc && ct) msg += "Action, Status";
                if (!ac && cc && !ct) msg += "Action, Comment";
                if (ac && !cc && !ct) msg += "Status, Comment";
                if (!ac && cc && ct) msg += "Action";
                if (ac && !cc && ct) msg += "Status";
                if (ac && cc && !ct) msg += "Comment";

                logger.logError(msg, "Error", null, true);
            }
        }
        else if (templateName() === 'emr') {
            var obj = {
                UserName: uProf.userName,
                TemplateName: templateName(),
                PatientNum: patientNum(),
                SubscriberNum: subscriberNum(),
                Status: ccItem.CcCode,
                Comment: cText.toUpperCase(),
                ActionCode: 0
            };


            $.ajax(routeconfig.addComment, {
                type: "POST",
                dataType: "json",
                data: obj,
                success: function (data, status, xhr) {

                    //app.trigger("patient:refreshcomments", { SubscriberNum: obj.SubscriberNum });
                    app.trigger("patient:getpatientdetails", { SubscriberNumber: obj.SubscriberNum });

                },
                error: function (data, status, xhr) {
                    logger.logError(xhr, status, null, true);
                },
                fail: function (data, status, xhr) {
                    logger.logError(xhr, status, null, true);
                }
            });
            dialog.close(this);
        }

    };

    function addAcTextToComment(arg) {
        var targetItem = arg.sender;
        var currItem = targetItem.dataItem(this.select());
        if (currItem.AcCode === 0) {
            acText("");
            var cText1 = ccText();
            if (cText1) $("#fe-comment-text").val(cText1);
            if (!cText1) $("#fe-comment-text").val("");
        }
        if (currItem.AcCode !== 0) {
            acText(currItem.AcCodeDescription);
            var aText = acText();
            var cText = ccText();
            if (aText && cText) $("#fe-comment-text").val(aText + " " + cText);
            if (aText && !cText) $("#fe-comment-text").val(aText);
            if (!aText && cText) $("#fe-comment-text").val(cText);
            if (!aText && !cText) $("#fe-comment-text").val("");
        }

        $("#fe-comment-text").focus();
    };

    function addCcTextToComment(arg) {

        var targetItem = arg.sender;
        var currItem = targetItem.dataItem(this.select());
        if (specialCbDatesStatus.indexOf(currItem.CcCode) > -1) {
            isSpecialCBDate(true);
        }
        else {
            isSpecialCBDate(false);
        }
        ccText(currItem.CcDescription);
        var aText = acText();
        var cText = ccText();
        if (aText && cText) $("#fe-comment-text").val(aText + " " + cText);
        if (aText && !cText) $("#fe-comment-text").val(aText);
        if (!aText && cText) $("#fe-comment-text").val(cText);
        if (!aText && !cText) $("#fe-comment-text").val("");

        $("#fe-comment-text").focus();
    };

    function buildActivityCodeGrid(data) {

        var cols = [{ field: "AcCode", title: "Code", width: "35px" },
                    { field: "AcCodeDescription", title: "Description" }];

        if (!data) data = [];

        var gHeight = 200;

        var grid = $("#fe-comment-ac-grid").kendoGrid({
            columns: cols,
            dataSource: {
                data: data
            },
            height: gHeight,
            selectable: "row",
            sortable: false,
            resizable: true,
            pageable: false,
            change: addAcTextToComment
        }).data("kendoGrid");

        var view = grid.dataSource.view();
        var row = $.grep(view, function (item) {
            return item.AcCode === 0;
        }).map(function (item) {
            return grid.tbody.find("[data-uid=" + item.uid + "]");
        });

        grid.select(row);

    };

    function buildCommCodeGrid(data) {

        var cols = [{ field: "CcCode", title: "Code", width: "50px" },
                    { field: "CcDescription", title: "Description" }];

        if (!data) data = [];

        var gHeight = 200;

        $("#fe-comment-cc-grid").kendoGrid({
            columns: cols,
            dataSource: {
                data: data
            },
            height: gHeight,
            selectable: "row",
            sortable: false,
            resizable: true,
            pageable: false,
            change: addCcTextToComment
        }).data("kendoGrid");

    };

    function getActivityCodes() {

        $.ajax(routeconfig.getActivityCodes, {
            type: "GET",
            dataType: "json",
            success: function (data, status, xhr) {

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    activityCodes(jObj);

                    var acData = [];

                    $.each(jObj, function (index, item) {
                        var tmpItem = {
                            AcCode: item.AcCode,
                            AcCodeDescription: item.AcCodeDescription
                        };
                        acData.push(tmpItem);
                    });

                    buildActivityCodeGrid(acData);

                }

            },
            error: function (data, status, xhr) {

            },
            fail: function (data, status, xhr) {

            }
        });

    };


    function getCommCodes(clientNum) {

        var eSeg = "?clientNum=" + clientNum;

        $.ajax(routeconfig.getCommCodes + eSeg, {
            type: "GET",
            dataType: "json",
            success: function (data, status, xhr) {

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    commCodes(jObj);

                    var ccData = [];

                    $.each(jObj, function (index, item) {
                        var tmpItem = {
                            CcCode: item.CcCode,
                            CcDescription: item.CcDescription,
                            CcComments: item.CcComments
                        };
                        ccData.push(tmpItem);
                    });

                    buildCommCodeGrid(ccData);
                }

            },
            error: function (data, status, xhr) {

            },
            fail: function (data, status, xhr) {

            }
        });

    };


    function addComment() {


    };

    function formatCommCode(item) {

        if (item) {
            return item.CcCode + ": " + item.CcDescription;
        }

        return "";
    };

    function formatActivityCode(item) {

        if (item) {
            return item.AcCode + ": " + item.AcCodeDescription;
        }

        return "";

    };



    commentModal.prototype.isSpecialCb = function () {
        return isSpecialCBDate();
    }

    commentModal.prototype.close = function () {
        isSpecialCBDate(false);
        cbDate("");
        dialog.close(this, {
            hasSelectedItem: false
        });
    };

    commentModal.open = function (title, label, items, patients) {
        if (patients) {
            repSpecialPatients = patients;
            repstatusSpecial(true);
        }
        return dialog.show(new commentModal(title, label, items));
    };

    return commentModal;
});