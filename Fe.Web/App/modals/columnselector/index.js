define(["plugins/dialog", "durandal/app", "modals/inputmodal/index", "services/routeconfig"], function (dialog, app, inputModal, routes) {

    var ColumnSelector = function (grid) {
        this.grid = grid;
        this.columns = ko.observableArray(grid.columns);
        this.width = "500px";
        this.bodyHeight = "400px";

        this.templates = new kendo.data.DataSource({
            transport: {
                read: function (operation) {
                    $.ajax(routes.getTemplates + "?parentName=patients-grid&type=grid-columns", {
                        type: "GET",
                        dataType: "json",
                        success: function (templates) {
                            operation.success(templates);
                        }
                    });
                }
            }
        })
        this.selectedTemplate = ko.observable();
    };

    ColumnSelector.prototype.saveAsTemplate = function () {
        modal = this;

        inputModal
            .open("Template Name", "Please enter a name")
            .then(function (result) {
                if (!result.hasInput || !result.input) return;

                $.ajax(routes.saveTemplate, {
                    type: "POST",
                    data: {
                        Name: result.input,
                        ParentName: "patients-grid",
                        Type: "grid-columns",
                        Template: $("#column-list")
                            .find("input[type=checkbox]:checked")
                            .map(function () { return $(this).attr("id"); })
                            .get()
                            .join(",")
                    },
                    dataType: "json",
                    success: function (result) {
                        modal.templates.read();
                    }
                });
            });
    }

    ColumnSelector.prototype.deleteTemplate = function () {
        var modal = this;

        app.showMessage("The template will be deleted. Are you sure?", "Confirm", ["Yes", "No"]).then(function (result) {
            if (result !== "Yes") return;

            $.ajax(routes.deletetemplate + "?id=" + modal.selectedTemplate().id, {
                type: "DELETE",
                dataType: "json",
                success: function (result) {
                    modal.templates.read();
                    modal.selectedTemplate(null);
                }
            });
        });
    }

    ColumnSelector.prototype.loadSelectedTemplate = function () {
        var grid = this.grid;

        if (!this.selectedTemplate()) return;

        var templateFields = this.selectedTemplate().template.split(",");

        var columns = templateFields
            .map(function (field) {
                return grid.columns.filter(function (column) {
                    return column.field === field;
                }).shift();
            })
            .filter(function (column) {
                return !!column;
            })
            .concat(grid.columns.filter(function (column) {
                return templateFields.indexOf(column.field) === -1;
            }));

        this.columns(columns.map(function (column) {
            return {
                title: column.title,
                field: column.field,
                hidden: templateFields.indexOf(column.field) === -1
            };
        }));
    };

    ColumnSelector.prototype.close = function () {
        dialog.close(this);
    };

    ColumnSelector.prototype.saveGrid = function () {
        var grid = this.grid;

        $("#column-list").find("input[type=checkbox]").each(function (newIndex) {
            var field = $(this).attr("id");

            grid.reorderColumn(newIndex, grid.columns.filter(function (column) {
                return column.field === field;
            }).shift());

            $(this).is(":checked")
                ? grid.showColumn(newIndex)
                : grid.hideColumn(newIndex);
        });

        this.close();
    };

    ColumnSelector.prototype.attached = function () {
        $("#column-list").kendoSortable({
            hint: function (element) {
                return element.clone().addClass("hint");
            },
            placeholder: function (element) {
                return element.clone().addClass("placeholder").text("drop here");
            },
            cursorOffset: {
                top: -10,
                left: -10
            }
        });
    };

    ColumnSelector.open = function (columns) {
        return dialog.show(new ColumnSelector(columns));
    };

    return ColumnSelector;
});