﻿//******************************
// File: view/index.js
//******************************

define(["plugins/router", "jquery", "durandal/app", "services/appsecurity", "services/routeconfig", "services/logger", "services/utils"], function (router, $, app, appsecurity, routeconfig, logger, utils) {

    //---------------------------------------------------------------------- 

    var masterAccountList = new ko.observableArray([]);
    var clientList = new ko.observableArray([]);

    function openClientSelectPopup() {

        var aHeader = "<div style=\"padding: 4px;\"><span>Client</span></div>" +
                      "<span class=\"glyphicon glyphicon-remove glyphicon-button\" " +
                      "style=\"position: absolute; top: 4px; right: 4px; \" " +
                      "data-bind=\"click: closePopup\">" +
                      "</span>";

        var aHtml = "<div data-bind=\"compose : 'user/client/index', style: {height: '100%'}\"></div>";

        $("#fe-main-popup-header").html(aHeader);
        $("#fe-main-popup-view").html(aHtml);
        $("#fe-main-popup").css("min-width", "400px");
        ko.cleanNode(document.getElementById("fe-main-popup"));
        ko.applyBindings(viewmodel, document.getElementById("fe-main-popup"));

        $("#fe-main-popup-overlay").show();
        $("#fe-main-popup").show();

    };

    function changeView(template) {

        if (!template) appsecurity.getUserTemplate();

        switch (template) {

            case "hfmi":

                var aHtml = "<div id=\"fe-patient-view\" data-bind=\"compose : { model: 'patient/demographic/hfmi/index' " +
                            "}, style: {height: '100%'}\"></div>" +
                            "<div id=\"fe-rep-status-view\" data-bind=\"compose : { model: 'repstatus/hfmi/index' " +
							"}, style: {height: '100%', display: 'none'}\"></div>" +
							"<div id=\"fe-rep-status-special-view\" data-bind=\"compose : { model: 'repstatus/special/index' " +
                            "}, style: {height: '100%', display: 'none'}\"></div>" +
                            "<div id=\"fe-reports-view\" data-bind=\"compose : { model: 'reports/index' " +
                            "}, style: {height: '100%', display: 'none'}\"></div>" + 
                            "<div id=\"fe-imports-view\" data-bind=\"compose : { model: 'imports/index' " +
                            "}, style: {height: '100%', display: 'none'}\"></div>";
                $("#fe-main-view").html(aHtml);
                ko.cleanNode(document.getElementById("fe-main-view"));
                ko.applyBindings(viewmodel, document.getElementById("fe-main-view"));

                break;

            case "emr":

                var bHtml = "<div id=\"fe-patient-view\" data-bind=\"compose : { model: 'patient/demographic/emeric/index' " +
                            "}, style: {height: '100%'}\"></div>" +
                            "<div id=\"fe-rep-status-view\" data-bind=\"compose : { model: 'repstatus/index' " +
                            "}, style: {height: '100%', display: 'none'}\"></div>" +
                            //"<div id=\"fe-rep-status-special-view\" data-bind=\"compose : { model: 'repstatus/special/index' " +
                            //"}, style: {height: '100%', display: 'none'}\"></div>" +
                            "<div id=\"fe-reports-view\" data-bind=\"compose : { model: 'reports/index' " +
                            "}, style: {height: '100%', display: 'none'}\"></div>" +
                            "<div id=\"fe-imports-view\" data-bind=\"compose : { model: 'imports/index' " +
                            "}, style: {height: '100%', display: 'none'}\"></div>";
                $("#fe-main-view").html(bHtml);
                ko.cleanNode(document.getElementById("fe-main-view"));
                ko.applyBindings(viewmodel, document.getElementById("fe-main-view"));

                break;

            default:

                openClientSelectPopup();

                break;

        }
    };

    function getUserClients() {

        var uProf = appsecurity.getUserInfo();

        var maList = masterAccountList();

        if (maList.length !== 1) return;

        var eSeg = "?userName=" + uProf.userName + "&hospCode=" + maList[0].HospCode;

        $.ajax(routeconfig.getUserClients + eSeg, {
            type: "GET",
            dataType: "json",
            success: function (data, status, xhr) {

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    clientList(jObj);

                    if (jObj.length === 1) {

                        if (jObj[0].ClientNum === "90414A") {
                            appsecurity.setUserClient(maList[0].HospCode, jObj[0].ClientNum, jObj[0].ClientName, "emr", false);
                            changeView("emr");
                        } else {
                            appsecurity.setUserClient(maList[0].HospCode, jObj[0].ClientNum, jObj[0].ClientName, "hfmi", false);
                            changeView("hfmi");
                        }

                    } else {
                        changeView(null);
                    }

                } else {
                    changeView(null);
                }

            },
            error: function (data, status, xhr) {
                changeView(null);
            },
            fail: function (data, status, xhr) {
                changeView(null);
            }
        });

    };

    function checkUserClientsForOnlyOneClient() {

        var uProf = appsecurity.getUserInfo();

        var eSeg = "?userName=" + uProf.userName;

        $.ajax(routeconfig.getUserMasterAccounts + eSeg, {
            type: "GET",
            dataType: "json",
            success: function (data, status, xhr) {

                var jObj = utils.parseJson(data);

                if (jObj !== false) {
                    masterAccountList(jObj);

                    if (jObj.length === 1) {
                        getUserClients();
                    } else {
                        changeView(null);
                    }
                }

            },
            error: function (data, status, xhr) {
                changeView(null);
            },
            fail: function (data, status, xhr) {
                changeView(null);
            }
        });

    };

    function initControls(template) {

        if (!template) {
            template = appsecurity.getUserTemplate();
        }

        if (!template.length) {
            checkUserClientsForOnlyOneClient();
        } else {
            changeView(template);
        }
    };

    function closePopup() {

        $("#fe-main-popup").hide();
        $("#fe-main-popup-overlay").hide();
        $("#fe-main-popup-header").html("");
        $("#fe-main-popup-view").html("");

    };

    function resizeGrid() {
        if ($("#fe-rep-status-grid").data("kendoGrid")) $("#fe-rep-status-grid").data("kendoGrid").resize();
        if ($("#fe-comment-grid").data("kendoGrid")) $("#fe-comment-grid").data("kendoGrid").resize();
        if ($("#fe-posting-grid").data("kendoGrid")) $("#fe-posting-grid").data("kendoGrid").resize();
        if ($("#fe-claim-grid").data("kendoGrid")) $("#fe-claim-grid").data("kendoGrid").resize();
    };

    $(window).resize(function () {
        resizeGrid();
    });

    //----------------------------------------------------------------------

    var viewmodel = {

        //State Functions
        canActivate: function () {
            return true;
        },
        activate: function () {

            app.on("view:closepopup").then(function () {
                closePopup();
            });

            app.on("view:changeview").then(function (template) {
                changeView(template);
            });

        },
        compositionComplete: function () {
            initControls();
        },

        //Variables

        //Functions
        closePopup: closePopup
    };

    return viewmodel;
});