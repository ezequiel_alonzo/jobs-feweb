﻿define(["services/routeconfig", "services/appsecurity", "services/server", "moment"], function (routes, appsecurity, server, moment) {

    var clients = [],
        codes = [],
        users = [],
        selectedSingleClient = ko.observable({ ClientNum: '88881A' }), 
        selectedReport = ko.observable(),
        statusCodeListIds = appsecurity.getUserInfo().statusCodeListIds,
        selectedStatusCodeListId = ko.observable(statusCodeListIds[0]);


    var filters = {
        startDate: ko.observable(),
        endDate: ko.observable(),

        allClients: ko.observable(true),
        allCodes: ko.observable(true),
        allUsers: ko.observable(true),
    };

    var getSelectedIds = function (listview) {
        return $(listview)
            .find(".k-state-selected")
            .map(function () {
                return $(this).attr("data-item-id");
            })
            .get();
    };

    selectedStatusCodeListId.subscribe(function (listId) {
        $("#reports-client-list").data("kendoListView").dataSource.read();
        $("#reports-codes-list").data("kendoListView").dataSource.read();
    });

    return {
        selectedSingleClient: selectedSingleClient,
        statusCodeListIds: statusCodeListIds,
        selectedStatusCodeListId: selectedStatusCodeListId,

        selectedReport: selectedReport,
        filters: filters,

        reports: new kendo.data.DataSource({
            transport: {
                read: function (operation) {
                    server
                        .get(routes.getReportList)
                        .then(operation.success);
                }
            }
        }),

        clients: new kendo.data.DataSource({
            transport: {
                read: function (operation) {
                    server
                        .get(routes.getUserClients, { userName: appsecurity.getUserInfo().userName, hospCode: "" })
                        .then(function (result) {
                            clients = JSON.parse(result).filter(function (client) {
                                return client.StatusCodeListId === selectedStatusCodeListId();
                            });
                            operation.success(clients);
                        });
                }
            }
        }),

        codes: new kendo.data.DataSource({
            transport: {
                read: function (operation) {
                    server
                        .get(routes.getCommCodesByListId, { listId: selectedStatusCodeListId() })
                        .then(function (result) {
                            codes = JSON.parse(result);
                            operation.success(codes);
                        });
                }
            }
        }),

        users: new kendo.data.DataSource({
            transport: {
                read: function (operation) {
                    server
                        .get(routes.getEmployees, { userName: appsecurity.getUserInfo().userName })
                        .then(function (result) {
                            users = result.sort();
                            operation.success(users);
                        });
                }
            }
        }),

        clearReport: function () {
            $("#fe-report-viewer").empty();
        },

        refreshReport: function () {
            var viewer = $("#fe-report-viewer").data("telerik_ReportViewer");

            if (!viewer) return;

            viewer.refreshReport();
        },

        loadReport: function () {

            $("#fe-report-container").html("<div id='fe-report-viewer' />");

            if (!selectedReport()) return;
                 
            $("#fe-report-viewer").telerik_ReportViewer({
                serviceUrl: routes.reportService,
                reportSource: {
                    report: selectedReport().fileName,
                    parameters: {
                        DATEFROM: moment.utc(filters.startDate()).format('l LT'),
                        DATETO: moment.utc(filters.endDate()).add(86399, 's').format('l LT'),
                        CLIENTNUM: selectedSingleClient().ClientNum,
                        CLIENTNUMS: (filters.allClients()
                            ? clients.map(function (client) { return client.ClientNum; })
                            : getSelectedIds("#reports-client-list")).join(", "),
                        USERNAMES: (filters.allUsers()
                            ? users
                            : getSelectedIds("#fe-user-list")).join(", ")
                    }
                },
                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                ready: function () { }
            });
        },

        containsFilter: function (filter) {
            return selectedReport() && selectedReport().parameters.indexOf(filter) > -1;
        }
    }
})