﻿using System;
using System.Collections.Generic;
using Dapper;
using Devart.Data.Oracle;
using System.Configuration;

namespace Fe.Web.Infrastructure.Repositories.Libraries
{
    public static class Oracle
    {
        private static string GetFeConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["FeConnection"].ConnectionString;
        }

        private static string GetEmrConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["EmrConnection"].ConnectionString;
        }

        public static void Execute(string command, Dictionary<string, object> parameters = null)
        {
            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                db.Execute(command, parameters);
            }
        }

        public static void Execute(string command, object parameters = null)
        {
            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                db.Execute(command, parameters);
            }
        }

        public static IEnumerable<T> Query<T>(string command, object parameters = null)
        {
            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                return db.Query<T>(command, parameters);
            }
        }

        public static IEnumerable<T> Query<T>(string command, Dictionary<string, object> parameters)
        {
            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                return db.Query<T>(command, parameters);
            }
        }
    }

    public class GuidTypeHandler : SqlMapper.TypeHandler<Guid>
    {
        public override Guid Parse(object value)
        {
            var inVal = (byte[])value;
            byte[] outVal = new byte[] { inVal[3], inVal[2], inVal[1], inVal[0], inVal[5], inVal[4], inVal[7], inVal[6], inVal[8], inVal[9], inVal[10], inVal[11], inVal[12], inVal[13], inVal[14], inVal[15] };
            return new Guid(outVal);
        }

        public override void SetValue(System.Data.IDbDataParameter parameter, Guid value)
        {
            var inVal = value.ToByteArray();
            byte[] outVal = new byte[] { inVal[3], inVal[2], inVal[1], inVal[0], inVal[5], inVal[4], inVal[7], inVal[6], inVal[8], inVal[9], inVal[10], inVal[11], inVal[12], inVal[13], inVal[14], inVal[15] };
            parameter.Value = outVal;
        }
    }
}