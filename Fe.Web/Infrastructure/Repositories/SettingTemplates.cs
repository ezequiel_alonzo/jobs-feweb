﻿using System;
using System.Collections.Generic;
using Fe.Web.Infrastructure.Repositories.Libraries;

namespace Fe.Web.Infrastructure.Repositories
{
    public class SettingTemplates
    {
        public static void Create(SettingTemplate template)
        {
            const string command = "INSERT INTO SETTINGTEMPLATES (NAME, PARENTNAME, TYPE, TEMPLATE) " +
                                   "VALUES (:NAME, :PARENTNAME, :TYPE, :TEMPLATE)";

            Oracle.Execute(command, new
            {
                NAME = template.Name,
                PARENTNAME = template.ParentName,
                TYPE = template.Type,
                TEMPLATE = template.Template
            });
        }

        public static void Update(SettingTemplate template)
        {
            throw new NotImplementedException();
        }

        public static void Delete(int id)
        {
            const string command = "DELETE FROM SETTINGTEMPLATES " +
                                   "WHERE ID = :ID";

            Oracle.Execute(command, new { ID = id });
        }

        public static SettingTemplate Get(int id)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<SettingTemplate> Get(string parentName = null, string type = null)
        {
            const string command = "SELECT * FROM SETTINGTEMPLATES " +
                                   "WHERE (:PARENTNAME IS NULL OR PARENTNAME = PARENTNAME) " +
                                   "  AND (:TYPE IS NULL OR TYPE = :TYPE)";

            return Oracle.Query<SettingTemplate>(command, new
            {
                PARENTNAME = parentName,
                TYPE = type
            });
        }
    }

    public class SettingTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ParentName { get; set; }
        public string Type { get; set; }
        public string Template { get; set; }
    }
}