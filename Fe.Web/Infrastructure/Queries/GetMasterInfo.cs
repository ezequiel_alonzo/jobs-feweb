﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;
using Fe.Web.Models;


namespace Fe.Web.Infrastructure.Queries
{
    public class GetMasterInfo : FeApiController
    {
        private readonly string _ClientNum;

        public GetMasterInfo(string clientNum)
        {
            _ClientNum = clientNum;
        }

        public IEnumerable<HospInfo> Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"clientNum", Utils.IsNotNull(_ClientNum) ? _ClientNum : string.Empty}
                    };
                    var sql = BuildHospInfo();

                    return db.Query<HospInfo>(sql, dict);
                }
            }
            catch
            {
                return null;
            }
        }


        private static string BuildHospInfo()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT hp.HospCode,hp.Name as HospName, c.phyaddress as HospAddress, c.phyphone as HospPhone, c.phycity as HospCity, ")
            .Append(" c.phystate as HospState,c.phyzip as HospZip ")
            .Append("FROM HospMaster hp  ")
                .Append("Join clients c on hp.HospCode=c.HospCode")
                .Append(" WHERE c.clientNum=:clientNum");

            return sb.ToString();
        }
    }

   
}