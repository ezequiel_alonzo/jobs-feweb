﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;
using Fe.Web.Models;
namespace Fe.Web.Infrastructure.Queries
{
    public class GetTemplateLetters : FeApiController
    {
        private readonly string _clientNum;
        private readonly string _path;

        public GetTemplateLetters(string clientNum, string path)
        {
            _path = path;
            _clientNum = clientNum;
        }


        public IEnumerable<FileUrlInfo> Run()
        {
            try
            {
                var getHospInfo = new GetMasterInfo(_clientNum);

                var Hospinfo = getHospInfo.Run().First();

                return new GetFilesList("lettertemplates", _path , "_", Hospinfo.HospCode).Run();
            }
            catch
            {
                return null;
            }
        }
    }
}