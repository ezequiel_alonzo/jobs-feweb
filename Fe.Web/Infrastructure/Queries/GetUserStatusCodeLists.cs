﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Text;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetUserStatusCodeLists : FeApiController
    {
        private readonly string _userName;

        public GetUserStatusCodeLists(string userName)
        {
            _userName = userName;
        }

        public IEnumerable<string> Run()
        {
            var userClientsQuery = new GetUserClients(_userName, null);

            var query =
                @"WITH 
                    CLIENTNUMS AS (
                      SELECT regexp_substr(:CLIENTNUMS, '[^,]+', 1, level) AS CLIENTNUM FROM DUAL
                      CONNECT BY regexp_substr(:CLIENTNUMS, '[^,]+', 1, level) IS NOT NULL
                    )
                    SELECT DISTINCT NVL(LISTID, 'HFMI2')
                    FROM CLIENTS C 
                    LEFT JOIN CLIENTCOMMCODESASSIGN CC
                        ON C.CLIENTNUM = CC.CLIENTNUM 
                    WHERE C.CLIENTNUM IN (SELECT CLIENTNUM FROM CLIENTNUMS)";

            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                return db.Query<string>(query, new 
                {
                    CLIENTNUMS = string.Join(",", userClientsQuery
                        .Get()
                        .Select(uc => uc.ClientNum)
                        .ToArray()) 
                });
            }
        }
    }
}