﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetPatientClaims : FeApiController
    {
        private readonly ClaimRequestModel _model;

        public GetPatientClaims(ClaimRequestModel model)
        {
            _model = model;
        }

        public string Run()
        {
            try
            {
                if (Utils.IsNull(_model.TemplateName)) return null;
                var tName = _model.TemplateName.ToLower();
                switch (tName)
                {
                    case "emr":
                        return RunEmrGetPatientClaims();
                    default:
                        return null;
                }

            }
            catch
            {
                return null;
            }
        }

        private string RunEmrGetPatientClaims()
        {
            try
            {
                var query = new GetClaimNumFromSubscriberNum(_model.SubscriberNum);
                var claimNum = query.Run();

                using (var db = new OracleConnection(GetEmrConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"claimNum", Utils.IsNotNull(claimNum) ? claimNum : string.Empty}
                    };

                    var sql = BuildEmrClaimSql();

                    var patPst = db.Query<ClaimModel>(sql, dict).ToList();

                    var formatPstDetails = new List<ClaimModel>();

                    if (patPst.Any()) formatPstDetails = FormatEmrClaimDetails(patPst);

                    return JsonConvert.SerializeObject(formatPstDetails);

                }
            }
            catch
            {
                return null;
            }
        }

        private static string BuildEmrClaimSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT ")
                .Append("clientname as ClientName,claimid as ClaimNumber,injdate as DateOfInjury,dos as DateOfService,")
                .Append(
                    "ndc as NdcNumber,qty as Units,amountbilled as AmountBilled,amountpaid as AmountPaid,printdate as PrintDate,")
                .Append("providertin as ProviderTin,providernpi as ProviderNpi,providername as ProviderName,")
                .Append(
                    "provideraddress as ProviderAddress,providercity as ProviderCity,providerstate as ProviderState,")
                .Append("providerzip as ProviderZip,prescriberfirstname || ' ' || prescriberlastname as PrescriberName,")
                .Append("prescriberdea as PrescriberDeaNumber ")
                .Append("FROM sqlmgr.oonpclaims WHERE claimid=:claimNum ORDER BY dos DESC");

            return sb.ToString();
        }

        private static List<ClaimModel> FormatEmrClaimDetails(IEnumerable<ClaimModel> cList)
        {
            return cList.Select(clm => new ClaimModel()
            {
                ClientName = clm.ClientName,
                ClaimNumber = clm.ClaimNumber,
                DateOfInjury = clm.DateOfInjury,
                DateOfService = clm.DateOfService,
                BillType = clm.BillType,
                BillNumber = clm.BillNumber,
                LineNumber = clm.LineNumber,
                NdcNumber = clm.NdcNumber,
                ServiceDescription = clm.ServiceDescription,
                Units = clm.Units,
                AmountBilled = clm.AmountBilled,
                FsReduction = clm.FsReduction,
                AuditReduction = clm.AuditReduction,
                PpoReduction = clm.PpoReduction,
                AmountPaid = clm.AmountPaid,
                PrintDate = clm.PrintDate,
                Network = clm.Network,
                ProviderTin = clm.ProviderTin,
                ProviderNpi = clm.ProviderNpi,
                ProviderName = clm.ProviderName,
                ProviderAddress = clm.ProviderAddress,
                ProviderCity = clm.ProviderCity,
                ProviderState = clm.ProviderState,
                ProviderZip = clm.ProviderZip,
                PrescriberName = clm.PrescriberName,
                PrescriberDeaNumber = clm.PrescriberDeaNumber
            }).ToList();
        }

    }
}