﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetRepStatusList : FeApiController
    {
        private readonly RepStatusRequestModel _model;

        public GetRepStatusList(RepStatusRequestModel model)
        {
            _model = model;
        }

        public string Run()
        {
            try
            {
                var rsList = new List<RepStatusModel>();
                var cdList = new List<RepStatusClaimDetailModel>();
                var cList = new List<string>();
                var tmpList = new List<string>();
                var strList = new List<string>();
                var cItems = "";

                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"clientNum", Utils.IsNotNull(_model.ClientNumber) ? _model.ClientNumber : string.Empty}
                    };

                    var sql = BuildRepStatusListSql();

                    rsList = db.Query<RepStatusModel>(sql, dict).ToList();

                }              

                foreach (var rs in rsList
                    .Where(rs => !Utils.IsNull(rs.SubscriberNumber))
                    .Where(rs => !cList.Contains(rs.SubscriberNumber)))
                {
                    cList.Add(rs.SubscriberNumber);
                    tmpList.Add(rs.SubscriberNumber);
                    cItems += "'" + rs.SubscriberNumber + "',";

                    if (tmpList.Count() != 1000) continue;

                    cItems = cItems.Substring(0, cItems.Length - 1);
                    strList.Add(cItems);
                    tmpList = new List<string>();
                    cItems = "";
                }

                if (cItems.Length > 0)
                {
                    cItems = cItems.Substring(0, cItems.Length - 1);
                    strList.Add(cItems);
                }

                using (var db = new OracleConnection(GetEmrConnectionString()))
                {
                    foreach (var tList in strList.Select(BuildRepClaimDetailsSql)
                        .Select(sql => db.Query<RepStatusClaimDetailModel>(sql).ToList()))
                    {
                        cdList.AddRange(tList);
                    }

                }

                var rList = FormatRepStatusList(rsList, cdList);

                return JsonConvert.SerializeObject(rList);
            }
            catch
            {
                return null;
            }
        }

        private static string BuildRepStatusListSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT ")
                .Append("0 as HasViewed,lastname || ', ' || firstname as PatientName,patientnum as PatientNumber,")
                .Append("hospacct as SubscriberNumber,currstatus as Status,")
                .Append("remdate as StatusDate,cbdate as CallBackDate,lastchanged as LastChanged ")
                .Append("FROM patient WHERE clientnum = :clientNum");

            return sb.ToString();
        }

        private static string BuildRepClaimDetailsSql(string claimIds)
        {
            var sb = new StringBuilder();

            sb.Append("SELECT ")
                .Append("s.subscribernum as SubscriberNumber, COUNT(o.id) as ClaimCount, SUM(o.amountpaid) AS AmountPaid ")
                .Append("FROM sqlmgr.oonpclaims o LEFT OUTER JOIN sqlmgr.subscriber s on o.claimid = s.claimid ")
                .AppendFormat("WHERE s.subscribernum IN ({0}) GROUP BY s.subscribernum ORDER BY s.subscribernum", claimIds);

            return sb.ToString();
        }

        private static List<RepStatusModel> FormatRepStatusList(IEnumerable<RepStatusModel> rsList, List<RepStatusClaimDetailModel> cdList)
        {
            var nList = new List<RepStatusModel>();

            foreach (var rs in rsList)
            {
                var tmpItem = new RepStatusModel()
                {
                    HasViewed = rs.HasViewed,
                    PatientName = rs.PatientName,
                    PatientNumber = rs.PatientNumber,
                    SubscriberNumber = rs.SubscriberNumber,
                    Status = rs.Status,
                    StatusDate = rs.StatusDate,
                    CallBackDate = rs.CallBackDate,
                    LastChanged = rs.LastChanged
                };

                var tmpClaim = cdList.FirstOrDefault(a => a.SubscriberNumber == rs.SubscriberNumber);

                if (tmpClaim != null)
                {
                    tmpItem.ClaimCount = tmpClaim.ClaimCount;
                    tmpItem.AmountPaid = tmpClaim.AmountPaid;
                }

                nList.Add(tmpItem);
            }

            return nList;
        }

    }
}