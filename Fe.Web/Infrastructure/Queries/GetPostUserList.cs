﻿using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Newtonsoft.Json;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetPostUserList : FeApiController
    {
        public string Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {

                    var sql = BuildPostingUserListgSql();

                    var postingUserList= db.Query<string>(sql).ToList();

                    return JsonConvert.SerializeObject(postingUserList);

                }
            }
            catch
            {
                return null;
            }
        }
        public string BuildPostingUserListgSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT UserName from UserMaster")
                .Append(" order by username");
            return sb.ToString();
        }
    }
}