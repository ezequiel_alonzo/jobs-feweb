﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Text;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetClientsCallCounter : FeApiController
    {
        public IEnumerable<ClientsCallCounter> Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var command = new StringBuilder();
                    

                    command.Append("SELECT campaign, numLeft , ")
                        .Append("to_char(chkTime, 'mm/dd/yyyy hh24:mi:ss') as Time ")
                        .Append("FROM CAMPAIGNMONITOR_SF ")
                        .Append("ORDER BY campaign ");


                    return db.Query<ClientsCallCounter>(command.ToString());
                }

                
            }
            catch(Exception ex)
            { 
            return null;
            }
        }

    }
}