﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetEmployees : FeApiController
    {
        private readonly string _userName;

        public GetEmployees(string userName)
        {
            _userName = userName;
        }

        public IEnumerable<string> Run()
        {
            const string query = "SELECT EMPNUM FROM EMPLOYEE WHERE SUPVNAME = :USERNAME OR (SELECT SECURITYLVL FROM USERMASTER WHERE USERNAME = :USERNAME) = '4'";

            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    return db.Query<string>(query, new
                    {
                        USERNAME = _userName.ToUpper()
                    });
                }
            }
            catch
            {
                return Enumerable.Empty<string>();
            }
        }
    }
}