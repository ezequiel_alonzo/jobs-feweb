﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using System.Text;
using Fe.Web.Helpers;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetEmployeeInfo : FeApiController
    {

        private readonly string _employeeId;

        public GetEmployeeInfo(string employeeId)
        {
            _employeeId = employeeId;
        }

        public IEnumerable<UserInfo> Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {

                    var sql = BuildEmployeeInfoGetSql();

                    var dict = new Dictionary<string,object>{
                      {"employeeId", Utils.IsNotNull(_employeeId) ? _employeeId : string.Empty}
                      };


                    return db.Query<UserInfo>(sql, dict);
                }
            }
            catch
            {
                return Enumerable.Empty<UserInfo>();
            }
        }


        private string BuildEmployeeInfoGetSql()
        {
            var sb = new StringBuilder();

            sb.Append("select empnum as UserName,")
                .Append("u.firstname,u.lastname,")
                .Append("to_char(startdate,'yyyy/mm/dd') as StartDate,")
                .Append("to_char(personalrate,'0.0000') as PersonalRate,")
                .Append("to_char(vacationrate,'0.0000') as VacationRate,")
                .Append("type,")
                .Append("to_char(e.lastchanged,'mm/dd/yyyy hh24:mm') LastChanged")
                .Append(",percentage ")
                .Append(",location ")
                .Append(",Rate ")
                .Append(",supvname,region")
                .Append(", coachingFreq ")
                .Append(",to_char(e.termdate,'yyyy/mm/dd') as TermDate ")
                .Append(",e.PayType ")
                .Append(",e.PAYROLLEMAIL")
                .Append(",e.MSGTEXT")
                .Append(",e.HOSPCLIENTNUM")
                .Append(",e.ALERTFLAG")
                .Append(",e.TRAINER")
                .Append(",e.PAYCHEXEMPNUM")
                .Append(",e.MGRCOACH")
                .Append(",e.PHONERPTFLAG")
                .Append(",CardName")
                .Append(",e.CardTitle")
                .Append(",e.Card1Left")
                .Append(",e.Card2Left")
                .Append(",e.Card3Left")
                .Append(",e.Card4Left")
                .Append(",e.Card1Right")
                .Append(",e.Card2Right")
                .Append(",e.Card3Right")
                .Append(",e.Card4Right")
                .Append(",e.NAME")
                .Append(",e.ADDR1")
                .Append(",e.ADDR2")
                .Append(",e.CITY")
                .Append(",e.STATE")
                .Append(",e.ZIPCODE")
                .Append(",e.SSN")
                .Append(",e.DRIVLIC")
                .Append(",e.HPHONE")
                .Append(",e.WPHONE")
                .Append(",e.DOB")
                .Append(",e.ECONTACT")
                .Append(",e.EPHONE")
                .Append(",e.MISCINFO")
                .Append(" from employee e,usermaster u")
                .Append(" where empnum = u.username");

            if (_employeeId != "ALL" && _employeeId !="ACT") sb.Append("  and empnum =:employeeId");
            if (_employeeId != "ALL") sb.Append(" and e.type != 'I' ");
            
            sb.Append(" order by empnum");

            return sb.ToString();
        }

    }
}