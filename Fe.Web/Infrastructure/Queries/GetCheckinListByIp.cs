﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Text;
using Fe.Web.Helpers;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetCheckinListByIp : FeApiController
    {
        private readonly string _userName;

        public GetCheckinListByIp(string userName)
        {
            _userName = userName;
        }
        public IEnumerable<UserCheckinIp> Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var sql = BuildGetCheckInListByIp();
                    var dict = new Dictionary<string, object> {
                {"userName", (Utils.IsNotNull(_userName) ? _userName.ToUpper() : string.Empty).ToUpper()}
            };

                    return db.Query<UserCheckinIp>(sql, dict);
                }
                
            }
            catch
            {
                return Enumerable.Empty<UserCheckinIp>();
            }
        }

        public string BuildGetCheckInListByIp()
        {
            var sb = new StringBuilder();

            sb.Append("select Username,Datein,Dateout,Ipaddress from TIMECARDS ")
            .Append("where USERNAME=:userName order by linenum desc");
            

            return sb.ToString();
        }
    }
}