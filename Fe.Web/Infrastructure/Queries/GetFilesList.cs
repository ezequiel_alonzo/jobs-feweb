﻿using Fe.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetFilesList
    {
        private readonly string _files;
        private readonly string _basePath;
        private readonly string _hospCode;
        private readonly string _baseUrl;
        public static Dictionary<string, string> dict = new Dictionary<string, string> {
            {"letters","content/documents/generated/"},
            {"maxbatch","BatchFiles/MaxBatch/"},
            {"270","BatchFiles/270/"},
            {"lettertemplates",""}
            };

        public GetFilesList(string files, string basePath, string baseUrl,string hospCode)
        {
            _files = files;
            _basePath = basePath;
            _hospCode = hospCode;
            _baseUrl = baseUrl;
            
        }

        public IEnumerable<FileUrlInfo> Run()
        {
            string uriAuthority;
            string baseFolder = dict[_files];

            uriAuthority = ConfigurationManager.AppSettings["SiteVersion"] != null ? ConfigurationManager.AppSettings["SiteVersion"] : _baseUrl;

            try
            {
                return
                    from file in Directory.EnumerateFiles(_basePath + baseFolder + "\\" + _hospCode + "\\", "*.*", SearchOption.AllDirectories)
                    let fileName = Path.GetFileName(file)
                    select new FileUrlInfo(uriAuthority + baseFolder + _hospCode + "/" + fileName, fileName, _basePath + baseFolder.Replace("/", "\\") + _hospCode + "/");
            }
            catch
            {
                return Enumerable.Empty<FileUrlInfo>();
            }
        } 
            
    }

    public class FileUrlInfo{
        public FileUrlInfo(string bu,string fn ,string bp)
        {
            BaseUrl = bu;
            FileName = fn;
            BasePath = bp;
        }
        public string BaseUrl { get; set; }
        public string FileName { get; set; }
        public string BasePath { get; set; }
    }
}