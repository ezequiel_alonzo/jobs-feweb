﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;
using Fe.Web.Models;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetCommCodes : FeApiController
    {

        private readonly string _listId;

        public GetCommCodes(string listId)
        {
            _listId = listId;
        }

        public List<CommCodeModel> Run()
        {
            var query = @"SELECT CODE CcCode, DESCRIPTION CcDescription, COMMENTS CcComments FROM CLIENTCOMMCODES WHERE LISTID = :LISTID ORDER BY CODE";

            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                return db.Query<CommCodeModel>(query, new { LISTID = _listId }).ToList();
            }
        }
    }
}