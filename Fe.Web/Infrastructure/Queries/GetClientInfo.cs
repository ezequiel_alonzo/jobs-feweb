﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Text;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetClientInfo : FeApiController
    {
        private readonly string _clientNum;

        public GetClientInfo(string clientNum)
        {
            _clientNum = clientNum;
        }

        public IEnumerable<ClientInfo> Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"clientNum", _clientNum}
                    };

                    var sql = BuildClientInfoGet();

                    var clientInfo = db.Query<ClientInfo>(sql, dict);

                    return clientInfo;
                }
            }
            catch
            {
                return null;

            }
        }

        public string BuildClientInfoGet()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT Clientnum ,Phyname ,Phyaddress ,Phycity ,Phystate ,Phyzip ,Phycontact ,Phyphone ,Phytitle ,Rptname ,Rptaddress ")
                .Append(",Rptcity ,Rptstate ,Rptzip ,Rptcontact ,Rptphone ,Rpttitle ,Billname ,Billaddress ,Billcity ,Billstate ,Billzip ")
                .Append(",Billcontact ,Billphone ,Billtitle ,Providernum ,Minimum ,Cap ,Continhouse ,Contdschrg ,Flatrate ,Pgm ,Importchopper")
                .Append(" ,Inactive ,Optmevs ,Statecode ,Patscreenopt ,Regioncode ,Userrequired ,Sortgroup ,Formflags ,Formsorder ,Cbdaysadd ")
                .Append(" ,Comments ,Tpg ,Serialnum ,Applid ,Mastate ,Hosppgm ,Hosptimeout ,Phonemsgs ,Keproclient ,Campuscode ,Touchstarbase ")
                .Append(" ,Starautoclose ,Acctnolength ,Importlimit ,Clipboardchopper ,Basebillamt ,Testclient ,Altletters ,Budget ,Chain ")
                .Append(" ,Hospname ,Hospid ,Basebudget ,Pdforder ,Pt_custid ,Pt_description ,Hospnamefull ,Name1 ,Title1 ,Email1 ,Name2 ,")
                .Append(" Title2 ,Email2 ,Name3 ,Title3 ,Email3 ,Restricted ,Clienttype ,Commissionovr ,Noletterq ,Defssconfig ,Startdate ")
                .Append(" ,Hospcode ,Npi ,Taxatomynbr ,Socsvcurl ,Socsvcfacility ,Socsvcusername ,Socsvcpassword ,Recon ,Reconquestion ,")
                .Append(" Ltr1q ,Ltr2q ,Ltr3q ,Actioncodes ,Bitflag ")
            .Append("FROM clients WHERE clientnum = :clientNum");

            return sb.ToString();
        }
    }
}