﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;
using System;
using System.Data;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetPatientDemographics : FeApiController
    {
        private readonly PatientDemographics _patient;
        private readonly string _userName;

        public GetPatientDemographics(PatientRequestModel model)
        {
            _patient = model.patient;
            _userName = model.userName;
        }

        public string Run()
        {
            try
            {
                if (Utils.IsNull(_patient.TemplateName)) return null;
                var tName = _patient.TemplateName.ToLower();
                switch (tName)
                {
                    case "hfmi":
                        return RunHfmiGetPatientDemographics();
                    case "emr":
                        return RunEmrGetPatientDemographics();
                    default:
                        return null;
                }

            }
            catch
            {
                return null;
            }
        }

        private string RunHfmiGetPatientDemographics()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var sql = _patient.ClientNum != "all" ? BuildHfmiPatientSql(_patient) : BuildHfmiPatientAllSql(_patient, _userName);

                    var param = new DynamicParameters();
                    if (_patient.ClientNum != "all") param.Add(name: "hospcode", value: _patient.ActHospCode, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "hospacct", value: _patient.ActHospAcct != null ? _patient.ActHospAcct.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patNum", value: _patient.PatNum, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patFirstName", value: _patient.PatFirstName != null ? _patient.PatFirstName.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patLastName", value: _patient.PatLastName != null ? _patient.PatLastName.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "ssn", value: _patient.PatSsn != null ? _patient.PatSsn.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "mrn", value: _patient.ActMrn != null ? _patient.ActMrn.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "caidNum", value: _patient.ActMedicaidNum != null ? _patient.ActMedicaidNum.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patZip", value: _patient.PatZip != null ? _patient.PatZip.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patCity", value: _patient.PatCity != null ? _patient.PatCity.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patAddress", value: _patient.PatAddress != null ? _patient.PatAddress.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patState", value: _patient.PatState != null ? _patient.PatState.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "dob", value: _patient.PatDob, direction: ParameterDirection.Input, dbType: DbType.Date);
                    param.Add(name: "patPhone", value: !string.IsNullOrEmpty(_patient.PatPhone) ? _patient.PatPhone.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patPhone1", value: !string.IsNullOrEmpty(_patient.PatPhone) ? _patient.PatPhone.ToUpper().Replace("-", ""): null , direction: ParameterDirection.Input, dbType: DbType.String);
                    if (!string.IsNullOrEmpty(_patient.PatPhone))
                    {
                       if(_patient.PatPhone.Length == 10) param.Add(name: "patPhone2", value: _patient.PatPhone.Substring(0, 3) + "-" + _patient.PatPhone.Substring(3, 3) + "-" + _patient.PatPhone.Substring(6, 4), direction: ParameterDirection.Input, dbType: DbType.String);
                    }

                    var patDetails = db.Query<PatientDemographics>(sql, param).ToList();

                    if (!patDetails.Any()) return null;

                    if (patDetails.Count() > 1)
                    {
                        var formatMpatDetails = FormatHfmiPatientDetails(patDetails);
                        return JsonConvert.SerializeObject(formatMpatDetails);
                    }

                    var formatPatDetails = FormatHfmiPatientDetails(patDetails);

                    return JsonConvert.SerializeObject(formatPatDetails);
                }
            }
            catch
            {
                return null;
            }

        }

        private static string BuildHfmiPatientAllSql(PatientDemographics model, string userName)
        {

            IEnumerable<UserClientModel> userClients = new GetUserClients(userName, "").Get();
            var clients = string.Join(",", from userClient in userClients
                                           select "'" + userClient.ClientNum + "'");

            var sb = new StringBuilder();

            sb.Append("SELECT ")
        .Append("p.patientnum as PatNum,p.lastname as PatLastName,p.firstname as PatFirstName,p.pataddress as PatAddress,p.patcity as PatCity,")
        .Append("p.patstate as PatState,patzip as PatZip,patphone as PatPhone,dob as PatDob,sex as PatGender, ")
        .Append("p.ssn as PatSsn,p.respname as RespName,p.respaddress as RespAddress,p.respcity as RespCity,")
        .Append("p.respstate as RespState,p.respzip as RespZip,p.respphone as RespPhone,p.finclass as RespFc,")
        .Append("p.respwphone as RespWorkPhone,respssn as RespSsn,hospacct as ActHospAcct,mrn as ActMrn,")
        .Append("p.inscode as ActInsCode,p.currbalance as ActCurrBalance,p.placeamount as ActPlaceAmt,")
        .Append("p.placedate as ActPlaceDate,p.admitdate as ActAdmitDate,p.dischdate as ActDischDate,")
        .Append("p.applicationdate as ActApplDate,p.encounter as ActEncounter,p.caidnum as ActMedicaidNum,")
        .Append("p.currstatus as CmtCurrStatus,remdate as CmtStatusDate,cbuser as CmtCallBackUser,")
        .Append("p.cbdate as CmtCallBackDate,p.lastchanged as CmtLastChanged,p.clientnum, c.hospcode as ActHospCode,p.PaymentAmount,spanish, fieldvisit, afterhours ")
        .Append("FROM patient p join clients c ON p.clientnum=c.clientnum ")
        .Append("WHERE")
        .AppendFormat(" c.clientnum IN ({0}) ", clients)
        .Append("AND (:patNum is null or p.patientnum=:patNum) ")
        .Append("AND (:hospacct is null or hospacct = :hospacct) ")
        .Append("AND (:patFirstName is null or firstname LIKE :patFirstName ||'%' ) ")
        .Append(" AND (:patLastName is null or  lastname LIKE :patLastName ||'%' )")
        .Append("AND (:ssn is null or ssn =:ssn) ")
        .Append("AND (:mrn is null or mrn =:mrn) ")
        .Append("AND (:caidNum is null or caidnum = :caidNum) ")
        .Append("AND (:patZip  is null or patzip =:patZip) ")
        .Append("AND (:patCity is null or patcity =:patCity) ")
        .Append("AND (:patAddress is null or pataddress =  :patAddress) ")
        .Append("AND ( :patState  is null or patstate = :patState) ")
        .Append("AND (:dob is null or dob =:dob) ")
        .Append("AND (:patphone is null or patphone = :patphone or patphone= :patphone1");
            if (!string.IsNullOrEmpty(model.PatPhone))
            {
                if (model.PatPhone.Length == 10)
                {
                    sb.Append("or patphone =:patphone2) ");
                }
                else
                {
                    sb.Append(")");
                }
            }
            else
            {
                sb.Append(")");
            }



            return sb.ToString();
        }

        private static string BuildHfmiPatientSql(PatientDemographics model)
        {
            var sb = new StringBuilder();

            sb.Append("SELECT ")
                .Append("p.patientnum as PatNum,p.lastname as PatLastName,p.firstname as PatFirstName,p.pataddress as PatAddress,p.patcity as PatCity,")
                .Append("p.patstate as PatState,patzip as PatZip,patphone as PatPhone,dob as PatDob,sex as PatGender, ")
                .Append("p.ssn as PatSsn,p.respname as RespName,p.respaddress as RespAddress,p.respcity as RespCity,")
                .Append("p.respstate as RespState,p.respzip as RespZip,p.respphone as RespPhone,p.finclass as RespFc,")
                .Append("p.respwphone as RespWorkPhone,respssn as RespSsn,hospacct as ActHospAcct,mrn as ActMrn,")
                .Append("p.inscode as ActInsCode,p.currbalance as ActCurrBalance,p.placeamount as ActPlaceAmt,")
                .Append("p.placedate as ActPlaceDate,p.admitdate as ActAdmitDate,p.dischdate as ActDischDate,")
                .Append("p.applicationdate as ActApplDate,p.encounter as ActEncounter,p.caidnum as ActMedicaidNum,")
                .Append("p.currstatus as CmtCurrStatus,remdate as CmtStatusDate,cbuser as CmtCallBackUser,")
                .Append("p.cbdate as CmtCallBackDate,p.lastchanged as CmtLastChanged,p.clientnum, c.hospcode as ActHospCode,p.PaymentAmount,spanish, fieldvisit, afterhours  ")
                .Append("FROM patient p join clients c ON p.clientnum=c.clientnum WHERE ")
                .Append("(:hospcode is null or hospcode = :hospcode) ")
                .Append("AND (:hospacct is null or hospacct = :hospacct)")
                .Append("AND (:patNum is null or p.patientnum=:patNum) ")
                .Append("AND (:patFirstName is null or firstname LIKE :patFirstName ||'%' ) ")
                .Append(" AND (:patLastName is null or  lastname LIKE :patLastName ||'%' )")
                .Append("AND (:ssn is null or ssn =:ssn) ")
                .Append("AND (:mrn is null or mrn =:mrn) ")
                .Append("AND (:caidNum is null or caidnum = :caidNum) ")
                .Append("AND (:patZip  is null or patzip =:patZip) ")
                .Append("AND (:patCity is null or patcity =:patCity) ")
                .Append("AND (:patAddress is null or pataddress =  :patAddress) ")
                .Append("AND ( :patState  is null or patstate = :patState) ")
                .Append("AND (:dob is null or dob =:dob) ")
                .Append("AND (:patphone is null or patphone = :patphone or patphone= :patphone1 ");
            if (!string.IsNullOrEmpty(model.PatPhone))
            {
                if (model.PatPhone.Length == 10)
                {
                    sb.Append("or patphone =:patphone2) ");
                }
                else
                {
                    sb.Append(")");
                }
            }
            else
            {
                sb.Append(")");
            }


            return sb.ToString();

        }

        private static List<PatientDemographics> FormatHfmiPatientDetails(IEnumerable<PatientDemographics> pList)
        {
            return pList.Select(model => new PatientDemographics()
            {
                PatNum = model.PatNum,
                PatLastName = model.PatLastName,
                PatFirstName = model.PatFirstName,
                PatAddress = model.PatAddress,
                PatCity = model.PatCity,
                PatState = model.PatState,
                PatZip = model.PatZip,
                PatPhone = Utils.FormatPhoneString(model.PatPhone),
                PatDob = model.PatDob,
                PatGender = model.PatGender,
                PatSsn = model.PatSsn,
                RespName = model.RespName,
                RespAddress = model.RespAddress,
                RespCity = model.RespCity,
                RespState = model.RespState,
                RespZip = model.RespZip,
                RespPhone = Utils.FormatPhoneString(model.RespPhone),
                RespFc = model.RespFc,
                RespWorkPhone = Utils.FormatPhoneString(model.RespWorkPhone),
                RespSsn = model.RespSsn,
                ActHospAcct = model.ActHospAcct,
                ActMrn = model.ActMrn,
                ActInsCode = model.ActInsCode,
                ActCurrBalance = Utils.FormatMoneyString(model.ActCurrBalance),
                ActPlaceAmt = Utils.FormatMoneyString(model.ActPlaceAmt),
                ActPlaceDate = model.ActPlaceDate,
                ActAdmitDate = model.ActAdmitDate,
                ActDischDate = model.ActDischDate,
                ActApplDate = model.ActApplDate,
                ActEncounter = model.ActEncounter,
                ActMedicaidNum = model.ActMedicaidNum,
                CmtCurrStatus = model.CmtCurrStatus,
                CmtStatusDate = model.CmtStatusDate,
                CmtCallBackUser = model.CmtCallBackUser,
                CmtCallBackDate = model.CmtCallBackDate,
                CmtLastChanged = model.CmtLastChanged,
                ClientNum = model.ClientNum,
                ActHospCode = model.ActHospCode,
                PaymentAmount = model.PaymentAmount,
                Spanish = model.Spanish,
                FieldVisit = model.FieldVisit,
                AfterHours = model.AfterHours
            }).ToList();
        }

        private string RunEmrGetPatientDemographics()
        {
            try
            {
                List<PatientDemographics> pDemo;
                List<EmrPatientDemographics> emrDemo;

                var dict = new Dictionary<string, object>
                    {
                        {"subscriberNum", Utils.IsNotNull(_patient.SubscriberNum) ? _patient.SubscriberNum : string.Empty},
                        {"clientNum",  Utils.IsNotNull(_patient.ClientNum) ? _patient.ClientNum : string.Empty}
                    };

                using (var db = new OracleConnection(GetFeConnectionString()))
                {


                    var sql = BuildEmrFePatientSql();

                    var patDetails = db.Query<PatientDemographics>(sql, dict).ToList();

                    if (!patDetails.Any()) return null;

                    if (patDetails.Count() > 1)
                    {
                        var formatMpatDetails = FormatEmrFePatientDetails(patDetails);

                        var mpModel = new PatientDetails()
                        {
                            FePatientDemographics = formatMpatDetails,
                            EmrPatientDemographics = new List<EmrPatientDemographics>()
                        };

                        return JsonConvert.SerializeObject(mpModel);
                    }

                    var formatPatDetails = FormatEmrFePatientDetails(patDetails);

                    pDemo = formatPatDetails;
                }

                using (var db1 = new OracleConnection(GetEmrConnectionString()))
                {


                    dict = new Dictionary<string, object>
                    {
                        {"subscriberNum", Utils.IsNotNull(_patient.SubscriberNum) ? _patient.SubscriberNum : string.Empty}
                    };

                    var sql = BuildEmrPatientSql();

                    var patDetails = db1.Query<EmrPatientDemographics>(sql, dict).ToList();

                    if (!patDetails.Any()) return null;

                    if (patDetails.Count() > 1)
                    {
                        var formatMpatDetails = FormatEmrPatientDetails(patDetails);

                        var mpModel = new PatientDetails()
                        {
                            FePatientDemographics = new List<PatientDemographics>(),
                            EmrPatientDemographics = formatMpatDetails
                        };

                        return JsonConvert.SerializeObject(mpModel);
                    }

                    var formatPatDetails = FormatEmrPatientDetails(patDetails);

                    emrDemo = formatPatDetails;
                }

                var pModel = new PatientDetails()
                {
                    FePatientDemographics = pDemo,
                    EmrPatientDemographics = emrDemo
                };

                return JsonConvert.SerializeObject(pModel);
            }
            catch
            {
                return null;
            }
        }

        private static string BuildEmrFePatientSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT ")
                .Append("lastname as PatLastName,firstname as PatFirstName,pataddress as PatAddress,patcity as PatCity,")
                .Append("patstate as PatState,patzip as PatZip,patphone as PatPhone,dob as PatDob,sex as PatGender, ")
                .Append("ssn as PatSsn,respname as RespName,respaddress as RespAddress,respcity as RespCity,")
                .Append("respstate as RespState,respzip as RespZip,respphone as RespPhone,finclass as RespFc,")
                .Append("respwphone as RespWorkPhone,respssn as RespSsn,hospacct as ActHospAcct,mrn as ActMrn,")
                .Append("inscode as ActInsCode,currbalance as ActCurrBalance,placeamount as ActPlaceAmt,")
                .Append("placedate as ActPlaceDate,admitdate as ActAdmitDate,dischdate as ActDischDate,")
                .Append("applicationdate as ActApplDate,encounter as ActEncounter,caidnum as ActMedicaidNum,")
                .Append("currstatus as CmtCurrStatus,remdate as CmtStatusDate,cbuser as CmtCallBackUser,")
                .Append("cbdate as CmtCallBackDate,lastchanged as CmtLastChanged ")
                .Append("FROM patient WHERE hospacct=:subscriberNum AND clientnum=:clientNum");

            return sb.ToString();
        }

        private static List<PatientDemographics> FormatEmrFePatientDetails(IEnumerable<PatientDemographics> pList)
        {
            return pList.Select(model => new PatientDemographics()
            {
                PatNum = model.PatNum,
                PatLastName = model.PatLastName,
                PatFirstName = model.PatFirstName,
                PatAddress = model.PatAddress,
                PatCity = model.PatCity,
                PatState = model.PatState,
                PatZip = model.PatZip,
                PatPhone = Utils.FormatPhoneString(model.PatPhone),
                PatDob = model.PatDob ?? DateTime.MinValue,
                PatGender = model.PatGender,
                PatSsn = model.PatSsn,
                RespName = model.RespName,
                RespAddress = model.RespAddress,
                RespCity = model.RespCity,
                RespState = model.RespState,
                RespZip = model.RespZip,
                RespPhone = Utils.FormatPhoneString(model.RespPhone),
                RespFc = model.RespFc,
                RespWorkPhone = Utils.FormatPhoneString(model.RespWorkPhone),
                RespSsn = model.RespSsn,
                ActHospAcct = model.ActHospAcct,
                ActMrn = model.ActMrn,
                ActInsCode = model.ActInsCode,
                ActCurrBalance = Utils.FormatMoneyString(model.ActCurrBalance),
                ActPlaceAmt = model.ActPlaceAmt,
                ActPlaceDate = model.ActPlaceDate ?? DateTime.MinValue,
                ActAdmitDate = model.ActAdmitDate ?? DateTime.MinValue,
                ActDischDate = model.ActDischDate ?? DateTime.MinValue,
                ActApplDate = model.ActApplDate ?? DateTime.MinValue,
                ActEncounter = model.ActEncounter,
                ActMedicaidNum = model.ActMedicaidNum,
                CmtCurrStatus = model.CmtCurrStatus,
                CmtStatusDate = Utils.FormatDateString(model.CmtStatusDate),
                CmtCallBackUser = model.CmtCallBackUser,
                CmtCallBackDate = model.CmtCallBackDate ?? DateTime.MinValue,
                CmtLastChanged = model.CmtLastChanged ?? DateTime.MinValue
            }).ToList();
        }

        private static string BuildEmrPatientSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT ")
                .Append("lastname as PatLastName, firstname as PatFirstName, addr1 as PatAddress, city as PatCity,")
                .Append(" state as PatState, zip as PatZip ")
                .Append("FROM sqlmgr.subscriber WHERE subscribernum=:subscriberNum");

            return sb.ToString();
        }

        private static List<EmrPatientDemographics> FormatEmrPatientDetails(IEnumerable<EmrPatientDemographics> pList)
        {
            return pList.Select(model => new EmrPatientDemographics()
            {
                PatLastName = model.PatLastName,
                PatFirstName = model.PatFirstName,
                PatAddress = model.PatAddress,
                PatCity = model.PatCity,
                PatState = model.PatState,
                PatZip = model.PatZip,
                PatPhone = Utils.FormatPhoneString(model.PatPhone)
            }).ToList();
        }

    }

    internal class PatientDetails
    {
        public List<PatientDemographics> FePatientDemographics { get; set; }
        public List<EmrPatientDemographics> EmrPatientDemographics { get; set; }
    }



    internal class EmrPatientDemographics
    {
        public string PatLastName { get; set; }
        public string PatFirstName { get; set; }
        public string PatAddress { get; set; }
        public string PatCity { get; set; }
        public string PatState { get; set; }
        public string PatZip { get; set; }
        public string PatPhone { get; set; }
    }

}