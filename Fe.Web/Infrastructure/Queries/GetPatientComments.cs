﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetPatientComments : FeApiController
    {
        private readonly CommentRequestModel _model;

        public GetPatientComments(CommentRequestModel model)
        {
            _model = model;
        }

        public string Run()
        {
            try
            {
                if (Utils.IsNull(_model.TemplateName)) return null;
                var tName = _model.TemplateName.ToLower();
                switch (tName)
                {
                    case "hfmi":
                        return RunHfmiGetPatientComments();
                    case "emr":

                        if (Utils.IsNull(_model.PatientNum) && Utils.IsNull(_model.SubscriberNum)) return null;

                        if (Utils.IsNull(_model.PatientNum))
                        {
                            var query = new GetPatientNumFromHospAcct(_model.SubscriberNum);
                            _model.PatientNum = query.Run();
                        }
                        return RunEmrGetPatientComments();

                    default:
                        return null;
                }

            }
            catch
            {
                return null;
            }
        }

        public string RunHfmiGetPatientComments()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"patientNum", Utils.IsNotNull(_model.PatientNum) ? _model.PatientNum : string.Empty}
                    };

                    var sql = BuildHfmiCommentSql();

                    var patCmts = db.Query<PatientComment>(sql, dict).ToList();

                    var formatCmtDetails = new List<PatientComment>();

                    if (patCmts.Any()) formatCmtDetails = FormatHfmiCommentDetails(patCmts);

                    return JsonConvert.SerializeObject(formatCmtDetails);
                }
            }
            catch
            {
                return null;
            }
        }

        private static string BuildHfmiCommentSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT ")
                .Append("username as CmtUser,comdate as CmtDate,status as CmtStatus,actioncode as CmtActionCode,")
                .Append("comments as CmtComment FROM comments WHERE patientnum = :patientNum ORDER BY linenum DESC");

            return sb.ToString();
        }

        private static List<PatientComment> FormatHfmiCommentDetails(IEnumerable<PatientComment> cList)
        {
            return cList.Select(cmt => new PatientComment()
            {
                CmtUser = cmt.CmtUser,
                CmtActionCode = cmt.CmtActionCode,
                CmtComment = cmt.CmtComment,
                CmtDate = Utils.FormatDateTimeString(cmt.CmtDate),
                CmtStatus = cmt.CmtStatus
            }).ToList();
        }

        public string RunEmrGetPatientComments()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"patientNum", Utils.IsNotNull(_model.PatientNum) ? _model.PatientNum : string.Empty}
                    };

                    var sql = BuildEmrCommentSql();

                    var patCmts = db.Query<PatientComment>(sql, dict).ToList();

                    var formatCmtDetails = new List<PatientComment>();

                    if (patCmts.Any()) formatCmtDetails = FormatEmrCommentDetails(patCmts);

                    return JsonConvert.SerializeObject(formatCmtDetails);
                }
            }
            catch
            {
                return null;
            }
        }

        private static string BuildEmrCommentSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT ")
                .Append("username as CmtUser,comdate as CmtDate,status as CmtStatus,actioncode as CmtActionCode,")
                .Append("comments as CmtComment FROM comments WHERE patientnum = :patientNum ORDER BY linenum DESC");

            return sb.ToString();
        }

        private static List<PatientComment> FormatEmrCommentDetails(IEnumerable<PatientComment> cList)
        {
            return cList.Select(cmt => new PatientComment()
            {
                CmtUser = cmt.CmtUser,
                CmtActionCode = cmt.CmtActionCode,
                CmtComment = cmt.CmtComment,
                CmtDate = Utils.FormatDateTimeString(cmt.CmtDate),
                CmtStatus = cmt.CmtStatus
            }).ToList();
        }

    }

    internal class PatientComment
    {
        public string CmtUser { get; set; }
        public string CmtDate { get; set; }
        public string CmtStatus { get; set; }
        public string CmtActionCode { get; set; }
        public string CmtComment { get; set; }
    }

}