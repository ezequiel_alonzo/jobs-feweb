﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetPatientNumFromHospAcct : FeApiController
    {
        private readonly string _hospAcct;

        public GetPatientNumFromHospAcct(string hospAcct)
        {
            _hospAcct = hospAcct;
        }

        public string Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"hospacct", Utils.IsNotNull(_hospAcct) ? _hospAcct : string.Empty}
                    };

                    var sql = BuildGetPatientNumFromHospAcctSql();

                    var patNum = db.Query<string>(sql, dict).FirstOrDefault();

                    return patNum;
                }
            }
            catch
            {
                return null;
            }
        }

        private static string BuildGetPatientNumFromHospAcctSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT patientnum FROM patient WHERE hospacct=:hospacct");

            return sb.ToString();
        }
    }
}