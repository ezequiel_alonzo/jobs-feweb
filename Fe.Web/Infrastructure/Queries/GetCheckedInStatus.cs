﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Fe.Web.Helpers;


namespace Fe.Web.Infrastructure.Queries
{
    public class GetCheckedInStatus : FeApiController
    {
        private readonly string _userName;
 
        public GetCheckedInStatus(string userName)
        {
            _userName = userName;
        }


        public Boolean Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var sql = BuildGetCheckedInStatusSql();

                    var dict = new Dictionary<string, object>
                    {
                        {"userName", Utils.IsNotNull(_userName) ? _userName : string.Empty}
                    };

                    var resp = db.Query(sql, dict).ToList();
                    if (resp.Count > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        private static string BuildGetCheckedInStatusSql()
        {
            var sb = new StringBuilder();
            
            sb.Append("SELECT ")
                .Append("* ")
                .Append("FROM TIMECARDS ")
                .Append("WHERE ROWNUM = 1 and USERNAME= :userName and DATEOUT is null order by USERNAME,LINENUM desc");

            return sb.ToString();
        }


    }
}