﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetUserClients : FeApiController
    {
        private readonly string _userName;
        private readonly string _hospCode;

        public GetUserClients(string userName, string hospCode)
        {
            _userName = userName.ToUpper();
            _hospCode = hospCode;
        }

        public string Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                 

                    var dict2 = new Dictionary<string, object>
                    {
                       {"userName", Utils.IsNotNull(_userName) ? _userName : string.Empty}
                        //{"hospCode", Utils.IsNotNull(_hospCode) ? _hospCode : string.Empty}
                    };
                    var sql = BuildUserHospTypes();

                    var userHospTypesClients = db.Query<UserMasterHospClientTypesModel>(sql, dict2).ToList();

                     sql = BuildUserClientSql(_userName ?? string.Empty, _hospCode ?? string.Empty, userHospTypesClients);

                    var maDetails = db.Query<UserClientModel>(sql).ToList();

                    return JsonConvert.SerializeObject(maDetails);
                }
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<UserClientModel> Get()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {


                    var dict2 = new Dictionary<string, object>
                    {
                       {"userName", Utils.IsNotNull(_userName) ? _userName : string.Empty}
                        //{"hospCode", Utils.IsNotNull(_hospCode) ? _hospCode : string.Empty}
                    };
                    var sql = BuildUserHospTypes();

                    var userHospTypesClients = db.Query<UserMasterHospClientTypesModel>(sql, dict2).ToList();

                    sql = BuildUserClientSql(_userName ?? string.Empty, _hospCode ?? string.Empty, userHospTypesClients);

                    var maDetails = db.Query<UserClientModel>(sql).ToList();

                    return maDetails;
                }
            }
            catch
            {
                return null;
            }
        }

        private static string BuildUserHospTypes()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT CLIENT as HospTypeClient FROM USERCLIENTS WHERE")
                .Append(" CLIENT LIKE 'ALL%' and USERNAME=:userName");

            return sb.ToString();
        }

        private static string BuildUserClientSql(string userName, string hospCode, List<UserMasterHospClientTypesModel> userHospTypesClients)
        {
            var sb = new StringBuilder();

            sb.Append("SELECT DISTINCT C.CLIENTNUM, C.PHYNAME AS NAME, NVL(CC.LISTID, 'HFMI2') AS STATUSCODELISTID FROM CLIENTS C ")
                .Append("JOIN USERCLIENTS UC ON C.CLIENTNUM=UC.CLIENT ")
                .Append("LEFT JOIN CLIENTCOMMCODESASSIGN CC ")
                .Append("ON C.CLIENTNUM=CC.CLIENTNUM ")
                .AppendFormat("WHERE UC.USERNAME= '{0}' ", userName);
                if(hospCode !=""){
                    sb.AppendFormat("and C.HOSPCODE= '{0}' ",hospCode );
                }
                if(userHospTypesClients.Any(e=> e.HospTypeClient.Contains("PRODUCTION"))){
                    sb.Append(" UNION ")
                       .Append("SELECT  C.CLIENTNUM, C.PHYNAME AS NAME, NVL(CC.LISTID, 'HFMI2') AS STATUSCODELISTID ")
                       .Append("FROM CLIENTS C JOIN HOSPMASTER HP ")
                       .Append("ON HP.HOSPCODE=C.HOSPCODE ")
                       .Append("LEFT JOIN CLIENTCOMMCODESASSIGN CC ")
                       .Append("ON C.CLIENTNUM=CC.CLIENTNUM ")
                       .Append("WHERE HP.HOSPTYPE='PRODUCTION' ");
                        if(hospCode!=""){
                            sb.AppendFormat("and HP.hospCode= '{0}' ",hospCode);
                        }
                    }
                if (userHospTypesClients.Any(e => e.HospTypeClient.Contains("SALES")))
                {
                    sb.Append(" UNION ")
                       .Append("SELECT  C.CLIENTNUM, C.PHYNAME AS NAME, NVL(CC.LISTID, 'HFMI2') AS STATUSCODELISTID ")
                       .Append("FROM CLIENTS C JOIN HOSPMASTER HP ")
                       .Append("ON HP.HOSPCODE=C.HOSPCODE ")
                       .Append("LEFT JOIN CLIENTCOMMCODESASSIGN CC ")
                       .Append("ON C.CLIENTNUM=CC.CLIENTNUM ")
                       .Append("WHERE HP.HOSPTYPE='SALES' ");
                    if (hospCode != "")
                    {
                        sb.AppendFormat("and HP.hospCode= '{0}' ", hospCode);
                    }
                }
                if (userHospTypesClients.Any(e => e.HospTypeClient.Contains("RECRUITING")))
                {
                    sb.Append(" UNION ")
                       .Append("SELECT  C.CLIENTNUM, C.PHYNAME AS NAME, NVL(CC.LISTID, 'HFMI2') AS STATUSCODELISTID ")
                       .Append("FROM CLIENTS C JOIN HOSPMASTER HP ")
                       .Append("ON HP.HOSPCODE=C.HOSPCODE ")
                       .Append("LEFT JOIN CLIENTCOMMCODESASSIGN CC ")
                       .Append("ON C.CLIENTNUM=CC.CLIENTNUM ")
                       .Append("WHERE HP.HOSPTYPE='RECRUITING' ");
                    if (hospCode != "")
                    {
                        sb.AppendFormat("and HP.hospCode= '{0}' ", hospCode);
                    }
                }
                sb.Append("ORDER BY 1,2");
            return sb.ToString();
        }
    }

    public class UserClientModel
    {
        public string ClientNum { get; set; }
        public string Name { get; set; }
        public string StatusCodeListId { get; set; }
    }

    internal class UserMasterHospClientTypesModel
    {
          public string HospTypeClient { get; set; }
    }

}