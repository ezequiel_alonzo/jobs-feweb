﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetUserMasterAccounts : FeApiController
    {
        private readonly string _userName;

        public GetUserMasterAccounts(string userName)
        {
            _userName = userName;
        }

        public string Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"userName", Utils.IsNotNull(_userName) ? _userName : string.Empty}
                    };
                    var sql = BuildUserHospTypes();

                    var userHospTypes = db.Query<UserMasterHospTypesModel>(sql, dict).ToList();

                    sql = BuildUserMasterAccountSql(userHospTypes);

                    var maDetails = db.Query<UserMasterAccountModel>(sql, dict).ToList();

                    return JsonConvert.SerializeObject(maDetails);
                }
            }
            catch
            {
                return null;
            }
        }
        private static string BuildUserHospTypes()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT CLIENT as HospType FROM USERCLIENTS WHERE")
                .Append(" CLIENT LIKE 'ALL%' and USERNAME=:userName");

            return sb.ToString();
        }
        private static string BuildUserMasterAccountSql(List<UserMasterHospTypesModel> userHospTypes)
        {

            var sb = new StringBuilder();

            sb.Append("  SELECT HospCode, Name From Hospmaster ")
                .Append("WHERE ")
                .Append("hospcode in ")
                .Append("(select ")
                .Append("case when substr(client,length(client),1) between 'A' and 'Z' and client <> 'TEST' then ")
                .Append("substr(client,1,length(client)-1) else client end as client1 ")
                .Append("From userclients ")
                .Append("where username = :userName) ");
                

            if (userHospTypes.Any(e => e.HospType.Contains("PRODUCTION")))
            {
                sb.Append("UNION")
                    .Append("  SELECT HospCode, Name From Hospmaster ")
                    .Append("WHERE HospType LIKE '%PRODUCTION%'");
            }
            if (userHospTypes.Any(e => e.HospType.Contains("SALES")))
            {
                sb.Append("UNION")
                    .Append("  SELECT HospCode, Name From Hospmaster ")
                    .Append("WHERE HospType LIKE '%SALES%'");
            }
            if (userHospTypes.Any(e => e.HospType.Contains("RECRUITING")))
            {
                sb.Append("UNION")
                    .Append("  SELECT HospCode, Name From Hospmaster ")
                   .Append("WHERE HospType LIKE '%RECRUITING%'");
            }
            sb.Append("order by Name ");
            return sb.ToString();
        }
    }

    internal class UserMasterHospTypesModel
    {
        public string HospType { get; set; }
    }
    internal class UserMasterAccountModel
    {
        public string HospCode { get; set; }
        public string Name { get; set; }
    }
}