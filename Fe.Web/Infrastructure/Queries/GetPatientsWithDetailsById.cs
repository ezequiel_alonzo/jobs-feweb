﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using Fe.Web.Properties;
using System.Data;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetPatientsWithDetailsById : FeApiController
    {
        private readonly string[] _patientNums;

        public GetPatientsWithDetailsById(params string[] patientNums)
        {
            _patientNums = patientNums;
        }

        public GetPatientsWithDetailsById(IEnumerable<string> patientNums)
        {
            _patientNums = patientNums.ToArray();
        }

        public IEnumerable<PatientWithDetailsModel> Run()
        {
            var parameters = new DynamicParameters();

            parameters.Add("PATIENTNUMS", value: string.Join(",", _patientNums), dbType: DbType.AnsiString);

            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                return db.Query<PatientWithDetailsModel>(Resources.GetPatientsWithDetailsById, param: parameters);
            }
        }
    }
}