﻿using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Newtonsoft.Json;

namespace Fe.Web.Infrastructure.Queries
{

    public class GetActivityCodes : FeApiController
    {

        public string Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var sql = BuildActivityCodeSql();

                    var acDetails = db.Query<ActivityCodeModel>(sql).ToList();

                    return JsonConvert.SerializeObject(acDetails);
                }
            }
            catch
            {
                return null;
            }
        }

        private static string BuildActivityCodeSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT ")
                .Append("code as AcCode,codedescription as AcCodeDescription ")
                .Append("FROM activitycodes ORDER BY code");

            return sb.ToString();
        }

    }

    internal class ActivityCodeModel
    {
        public int? AcCode { get; set; }
        public string AcCodeDescription { get; set; }
    }

}