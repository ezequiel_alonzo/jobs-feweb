﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetPatientHistory : FeApiController
    {
        private readonly HistoryRequestModel _model;

        public GetPatientHistory(HistoryRequestModel model)
        {
            _model = model;
        }

        public string Run()
        {

            try
            {
                if (Utils.IsNull(_model.Template)) return null;

                var tName = _model.Template.ToLower();

                switch (tName)
                {
                    case "hfmi":
                        return RunHFMIGetPatientHistory();
                    case "env":
                        return RunHFMIGetPatientHistory();
                    case "emr":
                        return null;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        public string RunHFMIGetPatientHistory()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {

                    var sql = BuildHfmiPatientHistorySql();

                    var patHistoryRecords = db.Query<HistoryModel>(sql).ToList();
                    var formatPatientHistory = new List<HistoryModel>();

                    if (patHistoryRecords.Any()) formatPatientHistory = FormatHfmiHistoryPatient(patHistoryRecords);

                    return JsonConvert.SerializeObject(formatPatientHistory);
                }

            }
            catch
            {
                return null;
            }
        }

        public string BuildHfmiPatientHistorySql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT p.patientnum as PatientNum, p.hospacct as HospAcct, p.lastname || ', ' || p.firstname as PatName, ")
                .Append(" p.currstatus , p.finclass , p.admitdate, p.dischdate, p.currbalance, p.clientnum, '' as ClientName, ssn ")
                .Append(" FROM patient p join clients c ON c.clientnum = p.clientnum")
                .AppendFormat(" WHERE ssn = '{0}' and ssn is not null and ssn not like '%999999%' and ssn not like '%9%9-99-9%9%'", _model.SSN)
                .AppendFormat(" and hospcode LIKE  '%{0}%' ", _model.HospCode)
                .Append(" UNION ")
                .Append(" SELECT patientnum as PatientNum, hospacct as HospAcct, lastname || ', ' || firstname as PatName, ")
                .Append(" currstatus , finclass , admitdate, dischdate, currbalance, p.clientnum, '' as ClientName, ssn ")
                .Append(" FROM patient p join clients c on p.clientnum=c.clientnum ")
                .AppendFormat(" WHERE patientnum = {0} OR (", _model.PatientNum)
                .AppendFormat("mrn = '{0}' and mrn is not null ", _model.MRN)
                .AppendFormat(" and hospcode LIKE  '%{0}%' ) ", _model.HospCode)

                .Append(" order by admitdate desc ");


            return sb.ToString();
        }

        private static List<HistoryModel> FormatHfmiHistoryPatient(IEnumerable<HistoryModel> cList)
        {
            return cList.Select(patHis => new HistoryModel()
            {
                AdmitDate = patHis.AdmitDate,
                ClientName = patHis.ClientName,
                ClientNum = patHis.ClientNum,
                CurrBalance = patHis.CurrBalance,
                CurrStatus = patHis.CurrStatus,
                DischDate = patHis.DischDate,
                FinClass = patHis.FinClass,
                HospAcct = patHis.HospAcct,
                MRN = patHis.MRN,
                PatientNum = patHis.PatientNum,
                PatName = patHis.PatName,
                SSN = patHis.SSN
            }).ToList();
        }

    }
}