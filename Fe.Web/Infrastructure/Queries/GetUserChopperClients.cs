﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetUserChopperClients : FeApiController
    {
        private readonly string _userName;
        private readonly string _hospCode;

        public GetUserChopperClients(string userName, string hospCode = null)
        {
            _userName = userName;
            _hospCode = hospCode;
        }

        public string Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict2 = new Dictionary<string, object>
                    {
                       {"userName", Utils.IsNotNull(_userName) ? _userName : string.Empty}
                        //{"hospCode", Utils.IsNotNull(_hospCode) ? _hospCode : string.Empty}
                    };
                    var sql = BuildUserHospTypes();

                    var userHospTypesClients = db.Query<UserMasterHospClientTypeModel>(sql, dict2).ToList();

                    sql = BuildUserClientSql(_userName ?? string.Empty, _hospCode ?? string.Empty, userHospTypesClients);

                    var maDetails = db.Query<UserChopperClientModel>(sql).ToList();

                    return JsonConvert.SerializeObject(maDetails);
                }
            }
            catch
            {
                return null;
            }
        }

        private static string BuildUserHospTypes()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT CLIENT as HospTypeClient FROM USERCLIENTS WHERE")
                .Append(" CLIENT LIKE 'ALL%' and USERNAME=:userName");

            return sb.ToString();
        }

        private static string BuildUserClientSql(string userName, string hospCode, List<UserMasterHospClientTypeModel> userHospTypesClients)
        {
            var sb = new StringBuilder();

            sb.Append("SELECT * FROM (SELECT C.CLIENTNUM, UPPER(C.CLIENTNUM) || ' : ' || UPPER(C.PHYNAME) AS NAME, C.IMPORTCHOPPER AS CHOPPER FROM CLIENTS C ")
                .Append("JOIN USERCLIENTS UC ON C.CLIENTNUM=UC.CLIENT ")
                .AppendFormat("WHERE UC.USERNAME= '{0}' ", userName);
            if (Utils.IsNotNull(hospCode))
            {
                sb.AppendFormat("and C.HOSPCODE= '{0}' ", hospCode);
            }
            if (userHospTypesClients.Any(e => e.HospTypeClient.Contains("PRODUCTION")))
            {
                sb.Append(" UNION ")
                   .Append("SELECT  C.CLIENTNUM, UPPER(C.CLIENTNUM) || ' : ' || UPPER(C.PHYNAME) AS NAME, C.IMPORTCHOPPER AS CHOPPER ")
                   .Append("FROM CLIENTS C JOIN HOSPMASTER HP ")
                   .Append("ON HP.HOSPCODE=C.HOSPCODE ")
                   .Append("WHERE HP.HOSPTYPE='PRODUCTION' ");
                if (Utils.IsNotNull(hospCode))
                {
                    sb.AppendFormat("and HP.hospCode= '{0}' ", hospCode);
                }
            }
            if (userHospTypesClients.Any(e => e.HospTypeClient.Contains("SALES")))
            {
                sb.Append(" UNION ")
                   .Append("SELECT  C.CLIENTNUM, UPPER(C.CLIENTNUM) || ' : ' || UPPER(C.PHYNAME) AS NAME, C.IMPORTCHOPPER AS CHOPPER ")
                   .Append("FROM CLIENTS C JOIN HOSPMASTER HP ")
                   .Append("ON HP.HOSPCODE=C.HOSPCODE ")
                   .Append("WHERE HP.HOSPTYPE='SALES' ");
                if (Utils.IsNotNull(hospCode))
                {
                    sb.AppendFormat("and HP.hospCode= '{0}' ", hospCode);
                }
            }
            if (userHospTypesClients.Any(e => e.HospTypeClient.Contains("RECRUITING")))
            {
                sb.Append(" UNION ")
                   .Append("SELECT  C.CLIENTNUM, UPPER(C.CLIENTNUM) || ' : ' || UPPER(C.PHYNAME) AS NAME, C.IMPORTCHOPPER AS CHOPPER ")
                   .Append("FROM CLIENTS C JOIN HOSPMASTER HP ")
                   .Append("ON HP.HOSPCODE=C.HOSPCODE ")
                   .Append("WHERE HP.HOSPTYPE='RECRUITING' ");
                if (Utils.IsNotNull(hospCode))
                {
                    sb.AppendFormat("and HP.hospCode= '{0}' ", hospCode);
                }
            }
            sb.Append(") GROUP BY CLIENTNUM, NAME, CHOPPER ")
            .Append(@"ORDER BY TO_NUMBER(REGEXP_SUBSTR(CLIENTNUM, '\A\d+')),REGEXP_SUBSTR(CLIENTNUM, '^\D*'),REGEXP_SUBSTR(CLIENTNUM, '\D+')");
            return sb.ToString();
        }
    }

    internal class UserChopperClientModel
    {
        public string ClientNum { get; set; }
        public string Name { get; set; }
        public int? Chopper { get; set; }
    }
    internal class UserMasterHospClientTypeModel
    {
        public string HospTypeClient { get; set; }
    }
}