﻿using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Newtonsoft.Json;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetUserPermissions : FeApiController
    {
        private readonly string _userName;

        public  GetUserPermissions(string userName)
        {
            _userName = userName;
        }

        public string Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var sql = BuildUserPermissionsSql(_userName.ToUpper());

                    var userPermissions = db.Query<UserPermission>(sql).ToList();

                    //var userPermissionsList = new List<UserPermission>();

                    //if (userPermissions.Any()) userPermissionsList = formatPermissions(userPermissions);
                    return JsonConvert.SerializeObject(userPermissions);
                }
            }
            catch
            {
                return null;
            }
        }

        public string BuildUserPermissionsSql(string userName)
        {
            var sb = new StringBuilder();

            sb.Append("select PERMNAME, VIEWPERM, SAVEPERM ")
                .Append(" from USERPERM UP join USERMASTER UM on UP.USERKEY=UM.USERKEY ")
                .AppendFormat("where USERNAME = '{0}' ", userName);

                return sb.ToString();
        }

       
    }
    internal class UserPermission
    {
        public string PermName { get; set; }
        public string ViewPerm { get;set; }
        public string SavePerm {get;set;}
    }
}