﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{

    public class GetPatientPostings : FeApiController
    {
        private readonly PostingRequestModel _model;

        public GetPatientPostings(PostingRequestModel model)
        {
            _model = model;
        }

        public string Run()
        {
            try
            {
                if (Utils.IsNull(_model.TemplateName)) return null;
                var tName = _model.TemplateName.ToLower();
                switch (tName)
                {
                    case "hfmi":
                        return RunHfmiGetPatientPostings();
                    default:
                        return null;
                }

            }
            catch
            {
                return null;
            }
        }

        private string RunHfmiGetPatientPostings()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"patientNum", Utils.IsNotNull(_model.PatientNum) ? _model.PatientNum : string.Empty}
                    };

                    var sql = BuildHfmiPostingSql();

                    var patPst = db.Query<PostingModel>(sql, dict).ToList();

                    var formatPstDetails = new List<PostingModel>();

                    if (patPst.Any()) formatPstDetails = FormatHfmiPostingDetails(patPst);

                    return JsonConvert.SerializeObject(formatPstDetails);

                }
            }
            catch
            {
                return null;
            }
        }

        private static string BuildHfmiPostingSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT ")
                .Append("p.patientnum as PatientNum, pc.code as PstCode,p.remitdate as PstRemitDate,p.postingdate as PstPostDate,")
                .Append("p.amount as PstAmount,p.agencycomm as PstCommish,")
                .Append("pc.description as PstDescription, username as PstUserName, hfmirep as PstHfmiRep, LineNum  ")
                .Append("FROM posting p JOIN postcodes pc on p.type = pc.code ")
                .Append("WHERE patientnum = :patientNum");

            return sb.ToString();
        }

        private static List<PostingModel> FormatHfmiPostingDetails(IEnumerable<PostingModel> pList)
        {
            return pList.Select(pst => new PostingModel()
            {
                PatientNum = pst.PatientNum,
                PstCode = pst.PstCode,
                PstRemitDate = Utils.FormatDateString(pst.PstRemitDate),
                PstPostDate = Utils.FormatDateString(pst.PstPostDate),
                PstAmount = Utils.FormatMoneyString(pst.PstAmount),
                PstCommish = Utils.FormatMoneyString(pst.PstCommish),
                PstDescription = pst.PstDescription,
                PstUserName = pst.PstUserName,
                PstHfmiRep = pst.PstHfmiRep,
                LineNum = pst.LineNum
            }).ToList();
        }

    }

}