﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;

namespace Fe.Web.Infrastructure.Queries
{
    public class CheckPatientExists : FeApiController
    {
        private readonly string _hospAcct;
        private readonly string _clientNum;

        public CheckPatientExists(string hospAcct, string clientNum)
        {
            _hospAcct = hospAcct;
            _clientNum = clientNum;
        }

        public bool Run()
        {
            const string query =
                @"SELECT PATIENTNUM 
                FROM PATIENT P
                WHERE P.HOSPACCT = :HOSPACCT
                    AND P.CLIENTNUM LIKE :CLIENTNUM || '%'";

            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                return db.Query<PatientProfile>(query, new
                {
                    HOSPACCT = _hospAcct,
                    CLIENTNUM = _clientNum.Substring(0, _clientNum.Length - 1)
                })
                .Any();
            }
        }

        public string GetPatientNum()
        {
            const string query =
                @"SELECT PATIENTNUM 
                FROM PATIENT P
                WHERE P.HOSPACCT = :HOSPACCT
                    AND P.CLIENTNUM LIKE :CLIENTNUM || '%'";

            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                return db.Query<PatientProfile>(query, new
                {
                    HOSPACCT = _hospAcct,
                    CLIENTNUM = _clientNum.Substring(0, _clientNum.Length - 1)
                })
                .FirstOrDefault().PatientNum;
            }
        }
    }
}