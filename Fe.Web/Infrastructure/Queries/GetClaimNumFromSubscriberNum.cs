﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetClaimNumFromSubscriberNum : FeApiController
    {
        private readonly string _subscriberNum;

        public GetClaimNumFromSubscriberNum(string subscriberNum)
        {
            _subscriberNum = subscriberNum;
        }

        public string Run()
        {
            try
            {
                using (var db = new OracleConnection(GetEmrConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"subscriberNum", Utils.IsNotNull(_subscriberNum) ? _subscriberNum : string.Empty}
                    };

                    var sql = BuildGetClaimNumFromSubscriberNumSql();

                    var claimNum = db.Query<string>(sql, dict).FirstOrDefault();

                    return claimNum;
                }
            }
            catch
            {
                return null;
            }
        }

        private static string BuildGetClaimNumFromSubscriberNumSql()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT claimid FROM sqlmgr.subscriber WHERE subscribernum=:subscriberNum");

            return sb.ToString();
        }
    }
}