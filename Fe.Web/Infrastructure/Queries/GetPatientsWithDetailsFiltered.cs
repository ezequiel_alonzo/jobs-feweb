﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using Fe.Web.Properties;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetPatientsWithDetailsFiltered : FeApiController
    {
        private readonly SearchPatientsRequestModel _request;

        public GetPatientsWithDetailsFiltered(SearchPatientsRequestModel request)
        {
            _request = request;
        }

        public IEnumerable<PatientWithDetailsModel> Run()
        {
            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                return db.Query<PatientWithDetailsModel>(Resources.GetPatientsWithDetailsFiltered, new
                {
                    SHOWPLACEAMOUNT = _request.ShowPlaceAmount,
                    EVERHADSTATUS = _request.EverHadStatus,
                    AFTERHOURS = _request.AfterHours,
                    POSTINGSONLY = _request.PostingsOnly,
                    SPANISH = _request.Spanish,
                    AGEDUNDER20 = _request.AgedUnder20,
                    FIELDVISIT = _request.FieldVisit,
                    DATESEARCH = _request.DateSearch,

                    STARTDATE = _request.DateRangeType == "DAYS"
                        ? DateTime.Now.AddDays(-_request.Days.GetValueOrDefault()).Date
                        : _request.StartDate.HasValue
                            ? _request.StartDate.Value.Date
                            : DateTime.MinValue,

                    ENDDATE = _request.DateRangeType == "DAYS" || !_request.EndDate.HasValue
                        ? DateTime.Now.Date.AddSeconds(86399)
                        : _request.EndDate.Value.Date.AddSeconds(86399),

                    CLIENTNUMS = string.Join(",", _request.ClientNums),
                    STATUSCODES = string.Join(",", _request.StatusCodes)
                });
            }
        }
    }
}