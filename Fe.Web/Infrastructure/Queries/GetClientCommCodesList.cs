﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Queries
{
    public class GetClientCommCodesList : FeApiController
    {
        private readonly string _clientNum;

        public GetClientCommCodesList(string clientNum)
        {
            _clientNum = clientNum;
        }

        public string Run()
        {
            var query = @"SELECT LISTID FROM CLIENTCOMMCODESASSIGN WHERE CLIENTNUM = :CLIENTNUM";

            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                return db.Query<string>(query, new { CLIENTNUM = _clientNum }).FirstOrDefault() ?? "HFMI2";
            }
        }
    }
}