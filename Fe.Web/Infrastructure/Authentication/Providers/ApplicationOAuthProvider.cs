﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Fe.Web.Infrastructure.Common;
using Fe.Web.Models;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Authentication.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientd;
 
        public ApplicationOAuthProvider(string publicClientId)
        {
            if (string.IsNullOrEmpty(publicClientId))
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientd = publicClientId;

        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientd)
            {
                var expectedRootUri = new Uri(context.Request.Uri, "/");
                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
                else if (context.ClientId == "feweb")
                {
                    var expectedUri = new Uri(context.Request.Uri, "/");
                    context.Validated(expectedUri.AbsoluteUri);
                }
            }
            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId, clientSecret;
            //gets the clientid and client secret from request body.If you are providing it in header as basic authentication then use
            //context.TryGetBasicCredentials
            context.TryGetFormCredentials(out clientId, out clientSecret);
            // validate clientid and clientsecret. You can omit validating client secret if none is provided in your request (as in sample client request above)
            if (clientId == "feweb")
                context.Validated();
            else
                context.Rejected();
            return Task.FromResult(0);
        }

        /// <summary>
        /// Create the authentication properties
        /// Create the required properties that would be converted into Claims
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static AuthenticationProperties CreateProperties(UserLoginResponseModel model)
        {

            var user = model.UserModel;

            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", user.UserName },
                { "userModel", JsonConvert.SerializeObject(user)},
                { "errorMessage", model.ErrorMessage},
                { "hasErrors", model.HasError.ToString()}
            };
            return new AuthenticationProperties(data);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                if (Utils.IsNull(context.UserName))
                {
                    context.SetError("invalid_grant", "Missing UserName");
                    return;
                }

                if (Utils.IsNull(context.Password))
                {
                    context.SetError("invalid_grant", "Missing Password");
                    return;
                }

                if (Utils.IsNull(context.ClientId))
                {
                    context.SetError("invalid_grant", "Missing Client Id");
                    return;
                }

                var model = new UserLoginModel()
                {
                    UserName = context.UserName.ToLower(),
                    Password = context.Password,
                    ClientId = context.ClientId
                };

                var handler = new LocalUserLogin(model);
                var result = await handler.Login();

                if (result.HasError)
                {
                    context.SetError("invalid_grant", result.ErrorMessage);
                    return;
                }

                if (result.UserModel == null)
                {
                    context.SetError("invalid_grant", "Missing User Model");
                    return;
                }

                var claims = new List<Claim>()
                {
                    new Claim("UserName", model.UserName),
                    new Claim("UserModel", JsonConvert.SerializeObject(result.UserModel))
                };

                var properties = CreateProperties(result);
                var oAuthIdentity = new ClaimsIdentity(claims, context.Options.AuthenticationType);
                var ticket = new AuthenticationTicket(oAuthIdentity, properties);
                var cookiesIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationType);

                context.Validated(ticket);
                context.Request.Context.Authentication.SignIn((cookiesIdentity));
            }
            catch
            {
                context.SetError("invalid_grant", "Internal Server Error");
            }

        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}