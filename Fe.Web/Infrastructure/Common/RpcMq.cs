﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using Fe.Web.Infrastructure.Choppers;
using Newtonsoft.Json;
using RabbitMQ.Client;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Common
{
    public class RpcMq
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IModel _channel;
        private readonly string _exchangeName;
        private readonly string _queueName;
        private readonly string _routeKey;
        private readonly int _timeout;

        public RpcMq(string exchangeName, string queueName, string routeKey, IConnection rmqConn)
        {
            Log.DebugFormat("[{0}]: Start init RPC client.", queueName);

            _queueName = queueName;
            _exchangeName = exchangeName;
            _routeKey = routeKey;

            var rpcTest = ConfigurationManager.AppSettings["RpcMqTest"];
            var rpcTimeout = ConfigurationManager.AppSettings["RpcMqTimeout"];

            if (!Int32.TryParse(rpcTimeout, out _timeout)) _timeout = 60000;

            if (Utils.IsNull(rpcTest)) _queueName = _queueName + "_test";
            else if (rpcTest.ToLower() == "true") _queueName = _queueName + "_test";

            Log.DebugFormat("[{0}]: Start create channel.", _queueName);
            _channel = rmqConn.CreateModel();
            Log.DebugFormat("[{0}]: End create channel.", _queueName);

            Log.DebugFormat("[{0}]: Start create exchange.", _queueName);
            _channel.ExchangeDeclare(_exchangeName, ExchangeType.Direct);
            Log.DebugFormat("[{0}]: End create exchange.", _queueName);

            Log.DebugFormat("[{0}]: Start create queue.", _queueName);
            _channel.QueueDeclare(_queueName, false, false, false, null);
            Log.DebugFormat("[{0}]: End create queue.", _queueName);

            Log.DebugFormat("[{0}]: Start create binding.", _queueName);
            _channel.QueueBind(_queueName, _exchangeName, _routeKey, null);
            Log.DebugFormat("[{0}]: End create binding.", _queueName);

            Log.DebugFormat("[{0}]: End init RPC client.", queueName);

        }

        public string Receive()
        {
            try
            {
                Log.DebugFormat("[{0}]: Start receive.", _queueName);
                var result = _channel.BasicGet(_queueName, false);

                if (result == null) return null;

                _channel.BasicAck(result.DeliveryTag, false);

                Log.DebugFormat("[{0}]: End receive.", _queueName);

                return Encoding.UTF8.GetString(result.Body);
            }
            catch (Exception ex)
            {
                Log.DebugFormat("[{0}]: Unable complete receive.\r\n{1}", _queueName, ex);
                return null;
            }

        }

        public List<string> ReceiveAll()
        {
            var msgCnt = _channel.MessageCount(_queueName);

            var rList = new List<string>();

            for (var i = 0; i < msgCnt; i++)
            {

                var result = _channel.BasicGet(_queueName, false);

                if (result == null) return null;

                _channel.BasicAck(result.DeliveryTag, false);

                rList.Add(Encoding.UTF8.GetString(result.Body));
            }

            Log.DebugFormat("[{0}]: End receive.", _queueName);


            return rList;
        }

        public void Send(string message)
        {
            try
            {
                Log.DebugFormat("[{0}]: Start Send.", _queueName);
                var msg = Encoding.UTF8.GetBytes(message);
                var props = _channel.CreateBasicProperties();
                props.ContentType = "text/plain";
                props.DeliveryMode = 2;
                _channel.BasicPublish(_exchangeName, _routeKey, props, msg);
                Log.DebugFormat("[{0}]: End Send.", _queueName);
            }
            catch (Exception ex)
            {
                Log.DebugFormat("[{0}]: Unable complete send.\r\n{1}", _queueName, ex);
            }
        }

        public void Close()
        {
            try
            {
                Log.DebugFormat("[{0}]: Start channel close.", _queueName);
                _channel.QueueUnbind(_queueName, _exchangeName, _routeKey, null);
                _channel.QueueDelete(_queueName, true, true);
                _channel.ExchangeDelete(_exchangeName, true);
                _channel.Close(200, "OK");
                Log.DebugFormat("[{0}]: End channel close.", _queueName);
            }
            catch (Exception ex)
            {
                Log.DebugFormat("[{0}]: Unable complete close.\r\n{1}", _queueName, ex);
            }
        }
    }

    public class MqHandler
    {
        public RpcMq Handler { get; set; }
        public string Exchange { get; set; }
        public string Queue { get; set; }
        public string RouteKey { get; set; }
    }
}