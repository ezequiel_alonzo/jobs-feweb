﻿using System;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Newtonsoft.Json;

namespace Fe.Web.Infrastructure.Common
{
    public class DocumentGeneration
    {
        private readonly DocumentGenerationRequestModel _model;
        private readonly string _basePath;
        private string _newDocumentName;
        private string _newDocumentPath;
        private PatientDetails _patientDetails;

        public DocumentGeneration(DocumentGenerationRequestModel model, string basePath)
        {
            _model = model;
            _basePath = basePath;
        }

        public string Run()
        {
            var docName = GetDocument();
            var fullPath = _basePath + docName;

            if (!File.Exists(fullPath)) return null;

            _newDocumentName = Guid.NewGuid().ToString("N").ToUpper() + ".docx";
            _newDocumentPath = _basePath + "generated\\" + _newDocumentName;

            CopyDocument(fullPath);
            if (!File.Exists(_newDocumentPath)) return null;
            GetPatientDetails();
            return _newDocumentName;
        }

        private void GetPatientDetails()
        {
            var model = new PatientRequestModel();
                
              model.patient=  new PatientDemographics()
            {
                SubscriberNum = _model.SubscriberNumber,
                TemplateName = "emr"
            };
              model.userName = "";

            var query = new GetPatientDemographics(model);
            var resp = query.Run();

            _patientDetails = JsonConvert.DeserializeObject<PatientDetails>(resp);

            UpdateDocument();
        }

        private void UpdateDocument()
        {
            try
            {
                using (var doc = WordprocessingDocument.Open(_newDocumentPath, true))
                {
                    var body = doc.MainDocumentPart.Document.Body;
                    var paras = body.Elements<Paragraph>();

                    foreach (var text in from para in paras from run in para.Elements<Run>() from text in run.Elements<Text>() select text)
                    {
                        if (text.Text.Contains("<<Member First Name>>")) 
                            text.Text = text.Text.Replace("<<Member First Name>>", _patientDetails.EmrPatientDemographics[0].PatFirstName);
                        
                        if (text.Text.Contains("<<Member Last Name>>"))
                            text.Text = text.Text.Replace("<<Member Last Name>>", _patientDetails.EmrPatientDemographics[0].PatLastName);

                        if (text.Text.Contains("<<Member DOB>>"))
                            text.Text = text.Text.Replace("<<Member DOB>>", "");

                        if (_model.SelectedAddress == "Pharmscreen")
                        {
                            if (text.Text.Contains("<<Member Address>>"))
                                text.Text = text.Text.Replace("<<Member Address>>", _patientDetails.EmrPatientDemographics[0].PatAddress);

                            if (text.Text.Contains("<<Member City>>"))
                                text.Text = text.Text.Replace("<<Member City>>", _patientDetails.EmrPatientDemographics[0].PatCity);

                            if (text.Text.Contains("<<Member State>>"))
                                text.Text = text.Text.Replace("<<Member State>>", _patientDetails.EmrPatientDemographics[0].PatState);

                            if (text.Text.Contains("<<Member Zip>>"))
                                text.Text = text.Text.Replace("<<Member Zip>>", _patientDetails.EmrPatientDemographics[0].PatZip);
                        }

                        if (_model.SelectedAddress == "Alternate")
                        {
                            if (text.Text.Contains("<<Member Address>>"))
                                text.Text = text.Text.Replace("<<Member Address>>", _patientDetails.FePatientDemographics[0].PatAddress);

                            if (text.Text.Contains("<<Member City>>"))
                                text.Text = text.Text.Replace("<<Member City>>", _patientDetails.FePatientDemographics[0].PatCity);

                            if (text.Text.Contains("<<Member State>>"))
                                text.Text = text.Text.Replace("<<Member State>>", _patientDetails.FePatientDemographics[0].PatState);

                            if (text.Text.Contains("<<Member Zip>>"))
                                text.Text = text.Text.Replace("<<Member Zip>>", _patientDetails.FePatientDemographics[0].PatZip);
                        }

                        if (_model.SelectedAddress == "Third Party")
                        {
                            if (text.Text.Contains("<<Member Address>>"))
                                text.Text = text.Text.Replace("<<Member Address>>", _patientDetails.FePatientDemographics[0].RespAddress);

                            if (text.Text.Contains("<<Member City>>"))
                                text.Text = text.Text.Replace("<<Member City>>", _patientDetails.FePatientDemographics[0].RespCity);

                            if (text.Text.Contains("<<Member State>>"))
                                text.Text = text.Text.Replace("<<Member State>>", _patientDetails.FePatientDemographics[0].RespState);

                            if (text.Text.Contains("<<Member Zip>>"))
                                text.Text = text.Text.Replace("<<Member Zip>>", _patientDetails.FePatientDemographics[0].RespZip);
                        }

                        if (text.Text.Contains("<<Member ID>>"))
                            text.Text = text.Text.Replace("<<Member ID>>", "0000000000");

                        if (text.Text.Contains("<<Member PC>>"))
                            text.Text = text.Text.Replace("<<Member PC>>", "01");

                        if (text.Text.Contains("<<Member Group Number>>"))
                            text.Text = text.Text.Replace("<<Member Group Number>>", "102938475");
                        
                        if (text.Text.Contains("<<Claim Id>>"))
                            text.Text = text.Text.Replace("<<Claim Id>>", "ZZZ123456789");
                        
                        if (text.Text.Contains("<<Adjuster Name>>"))
                            text.Text = text.Text.Replace("<<Adjuster Name>>", "Jane Smith");

                        if (text.Text.Contains("<<Carrier Name>>"))
                            text.Text = text.Text.Replace("<<Carrier Name>>", "Some Carrier");

                        if (text.Text.Contains("<<Episode Number>>"))
                            text.Text = text.Text.Replace("<<Episode Number>>", "ABC123456789");                  
                        
                    }

                }
            }
            catch
            {
                
            }
        }

        private void CopyDocument(string docPath)
        {
            File.Copy(docPath, _newDocumentPath);
        }

        private string GetDocument()
        {
            switch (_model.DocumentName)
            {
                case "Letter To Doctor Out Of Network":
                    return "templates\\EMR_LETTER_DOCTOR_OUT_OF_NETWORK.docx";
                case "Letter To Claimant Out Of Network":
                    return "templates\\EMR_LETTER_CLAIMANT_OUT_OF_NETWORK.docx";                   
                default:
                    return null;
            }
        }
    }
}