﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Fe.Web.Models;
using Newtonsoft.Json;

namespace Fe.Web.Infrastructure.Common
{
    public class LocalUserLogin
    {
        private HttpClient _client;
        private readonly UserLoginModel _loginModel;
        private readonly string _serviceUrl;

        public LocalUserLogin(UserLoginModel loginModel)
        {
            _loginModel = loginModel;
            _serviceUrl = ConfigurationManager.AppSettings["IdentityServerHost"];
        }

        public async Task<UserLoginResponseModel> Login()
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(_loginModel);

                var response = await _client.PostAsync("api/user/userlogin",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                var content = await response.Content.ReadAsStringAsync();

                if (CheckUserLoginResponse(content))
                {
                    var jObj1 = JsonConvert.DeserializeObject(content);
                    var uObj = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
                    return new UserLoginResponseModel(){HasError = false, ErrorMessage = "", UserModel = uObj};
                }
               
                return new UserLoginResponseModel() { HasError = true, ErrorMessage = content};               
 
            }
            catch
            {
                return new UserLoginResponseModel() { HasError = true, ErrorMessage = "Internal Server Error" }; 
            }
        }

        private HttpClient GetHttpClient()
        {
            return _client ?? (_client = new HttpClient() { BaseAddress = new Uri(_serviceUrl) });
        }

        private static bool CheckUserLoginResponse(string content)
        {
            try
            {
                var jObj1 = JsonConvert.DeserializeObject(content);
                var uObj = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}