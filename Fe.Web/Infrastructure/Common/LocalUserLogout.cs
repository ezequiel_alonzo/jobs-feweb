﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Fe.Web.Models;
using Newtonsoft.Json;

namespace Fe.Web.Infrastructure.Common
{
    public class LocalUserLogout
    {
        private HttpClient _client;
        private readonly LocalUserModel _logoutModel;
        private readonly string _serviceUrl;

        public LocalUserLogout(LocalUserModel logoutModel)
        {
            _logoutModel = logoutModel;
            _serviceUrl = ConfigurationManager.AppSettings["IdentityServerHost"];
        }

        public async Task<string> Logout()
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(_logoutModel);

                var response = await _client.PostAsync("api/user/userlogout",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                return response.StatusCode.ToString() == "OK" ? "OK" : "Unable To Logout";
            }
            catch
            {
                return "Internal Server Error";
            }
        }

        private HttpClient GetHttpClient()
        {
            return _client ?? (_client = new HttpClient() { BaseAddress = new Uri(_serviceUrl) });
        }

    }
}