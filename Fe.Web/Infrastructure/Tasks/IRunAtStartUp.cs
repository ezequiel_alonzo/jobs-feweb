﻿namespace Fe.Web.Infrastructure.Tasks
{
    public interface IRunAtStartUp
    {
        void Execute();
    }
}