﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Dapper;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Infrastructure.Spitfire;

namespace Fe.Web.Infrastructure.Commands
{
    public class CreateCampaign : FeApiController
    {
        private readonly SpitfireConnect _spitfire = new SpitfireConnect();

        private readonly string _campaignNum;
        private readonly string _clientNum;
        private readonly IEnumerable<int> _patientNums;

        public CreateCampaign(string campaignNum, string clientNum, IEnumerable<int> patientNums)
        {
            _campaignNum = campaignNum;
            _clientNum = clientNum;
            _patientNums = patientNums;
        }

        public int Run()
        {
            var patients = new GetPatientsWithDetailsById(_patientNums.Select(num => num.ToString()))
                .Run()
                .Where(patient => int.Parse(patient.CurrStatus) < 700);

            var deleteResult = _spitfire.DeleteLeadsUsingListName(_campaignNum);

            var voicePhoneList = new List<string>();

            var results = patients
                .Select(patient => _spitfire
                    .InsertLeadUsingListName(new LiveLead
                    {
                        firstname = patient.FirstName,
                        lastname = patient.LastName,
                        voicephone = patient.PatPhone,
                        listname = _campaignNum,
                        address = patient.PatAddress,
                        state = patient.PatState,
                        zipcode = patient.PatZip,
                        city = patient.PatCity,
                        xmldata = GetXmlData(patient.PatientNum, patient.Dob),
                        username = ConfigurationManager.AppSettings["spitfireUsername"],
                        password = ConfigurationManager.AppSettings["spitfirePassword"]
                    })
                    .Result);

            return results.Count(result => result == "Success");
        }

        public static string GetXmlData(string patientnum, DateTime? dob)
        {
            return string.Format(
                    "<Fields>" +
                    "<Field Name=\"EDP No\" Value=\"{0}\" />" +
                    "<Field Name=\"DOB\" Value=\"{1}\" />" +
                    "</Fields>",
                    patientnum, dob.HasValue ? dob.Value.ToString("dd-MMM-yyyy") : string.Empty);
        }
    }
}