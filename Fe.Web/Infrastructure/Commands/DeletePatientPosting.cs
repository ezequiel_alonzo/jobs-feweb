﻿using Fe.Web.Controllers;
using System.Collections.Generic;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Models;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Commands
{
    public class DeletePatientPosting : FeApiController
    {
        private readonly PostingDeleteModel _model;


        public DeletePatientPosting(PostingDeleteModel model)
        {
            _model = model;
        }


        public string Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                     {
                        {"patientNum", Utils.IsNotNull(_model.PatientNum) ? _model.PatientNum : string.Empty}
                    };

                    var sql = BuildDeletePatientPosting();

                    db.Execute(sql, dict);
                    return null;
                }
            }
            catch
            {
                return null;
            }

        }

        public string BuildDeletePatientPosting()
        {
            var sb = new StringBuilder();

            sb.Append("DELETE FROM posting WHERE ")
               .Append(" patientnum = :patientNum ")
               .AppendFormat(" AND linenum = {0} ", _model.LineNum);

            return sb.ToString();
        }
    }
}