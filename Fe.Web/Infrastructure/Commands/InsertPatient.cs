﻿using Fe.Web.Controllers;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Models;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web.Infrastructure.Commands
{
    public class HFMIInsertPatient : FeApiController
    {

        private readonly HFMIPatientInsertRequestModel _model;

        public HFMIInsertPatient (HFMIPatientInsertRequestModel model){
            _model= model;
        }

        public string Validate(){
            var sb = new StringBuilder();

            if (Utils.IsNull(_model.TemplateName))
            {
                sb.Append("Template Name");
                sb.Insert(0, "Missing required parameters: ");
                return sb.ToString().Substring(0, sb.ToString().Length - 1);
            }
            //if (!_model.PlacementDate.HasValue)
            //{
            //    sb.Append("Placement Date ");
            //    sb.Insert(0, "Missing required parameters: ");
            //    return sb.ToString().Substring(0, sb.ToString().Length - 1);
            //}
            return "";
        }

            
        

        public bool Run()
        {
            if (Utils.IsNull(_model.TemplateName)) return false;

            
            try {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var sql = BuildGetNextPatientNum();

                    var pNum = db.Query<int?>(sql).FirstOrDefault();

                    if (!pNum.HasValue)
                    {
                        _model.PatientNum = 1;
                    }
                    else
                    {
                        _model.PatientNum = (int) pNum;
                    }

                    if (_model.AdmitDate.HasValue) {
                       
                    }

                    sql = BuildHFMIInsertPatient();
                    
                    db.Execute(sql);

                    var firstCommentModel = new CommentInsertModel();
                    firstCommentModel.ActionCode = 0;
                    firstCommentModel.Comment = "New Patient";
                    firstCommentModel.PatientNum = _model.PatientNum.ToString();
                    firstCommentModel.Status = "000";
                    firstCommentModel.SubscriberNum = _model.ActHospAcct.ToUpper();
                    firstCommentModel.TemplateName = _model.TemplateName;
                    firstCommentModel.UserName = _model.Username.ToUpper();
                    firstCommentModel.LineNum = 1;

                    var cmd = new InsertPatientComment(firstCommentModel);
                    cmd.Run();
                    
                }

                return true;
               
            }
            catch
            {
                return false;
            }
        }

        private string BuildGetNextPatientNum()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT Patient_Seq.NextVal ")
               .Append("FROM Dual ");
            return sb.ToString();
        }


        private string BuildHFMIInsertPatient()
        {
            var sb = new StringBuilder();

            sb.Append("INSERT INTO Patient (PATIENTNUM,CLIENTNUM,HOSPACCT,SSN,DOB,SEX,LASTNAME,FIRSTNAME, ")
                .Append("PATPHONE, PATADDRESS, PATCITY, PATSTATE, PATZIP, RESPNAME, RESPPHONE, RESPADDRESS, ")
                .Append("RESPCITY, RESPSTATE, RESPZIP, PLACEDATE, PLACEAMOUNT, ADMITDATE, DISCHDATE, ")
                .Append(" CURRBALANCE, CURRSTATUS, MRN , INSCODE, SPANISH, ")
                .Append("AFTERHOURS, TRANSFER, ENCOUNTER, APPLICATIONDATE, RESPSSN, RESPWPHONE,CAIDNUM,FINCLASS)")
                .Append("values (")
                .AppendFormat("{0},", _model.PatientNum)
                .AppendFormat("'{0}',", _model.ClientNum)
                .AppendFormat("'{0}',", _model.ActHospAcct != null ? _model.ActHospAcct.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.SSN)
                .AppendFormat("'{0}',", _model.DOB.HasValue ? _model.DOB.Value.ToString("dd-MMM-yy") : string.Empty)
                .AppendFormat("'{0}',", _model.Gender != null ? _model.Gender.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.PatLastName != null ? _model.PatLastName.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.PatFirstName != null ? _model.PatFirstName.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.PatPhone)
                .AppendFormat("'{0}',", _model.PatAddress != null ? _model.PatAddress.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.PatCity != null ? _model.PatCity.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.PatState != null ? _model.PatState.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.PatZip)
                .AppendFormat("'{0}',", _model.RespName != null ? _model.RespName.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.RespPhone)
                .AppendFormat("'{0}',", _model.RespAddress != null ? _model.RespAddress.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.RespCity != null ? _model.RespCity.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.RespState != null ? _model.RespState.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.RespZip != null ? _model.RespZip.ToUpper() : string.Empty)
                .Append(" SYSDATE,")
                .AppendFormat("'{0}',", _model.PlaceAmount)
                .AppendFormat("'{0}',", _model.AdmitDate.HasValue ? _model.AdmitDate.Value.ToString("dd-MMM-yy") :  string.Empty)
                .AppendFormat("'{0}',", _model.DischDate.HasValue ? _model.DischDate.Value.ToString("dd-MMM-yy") : string.Empty)
                .AppendFormat("{0},", _model.CurrentBalance)
                .AppendFormat("'000', ")
                .AppendFormat("'{0}',", _model.MRN != null ? _model.MRN.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.InsCode != null ? _model.InsCode.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.Spanish != null ? _model.Spanish.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.AfterHours != null ? _model.AfterHours.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.Transfer != null ? _model.Transfer.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.Encounter != null ? _model.Encounter.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.ApplicationDate.HasValue ? _model.ApplicationDate.Value.ToString("dd-MMM-yy") : string.Empty)
                .AppendFormat("'{0}',", _model.RespSSN != null ? _model.RespSSN.ToUpper() : string.Empty)
                .AppendFormat("'{0}',", _model.RespWPhone)
                .AppendFormat("'{0}',", _model.CaidNum != null ? _model.CaidNum.ToUpper() : string.Empty)
                .AppendFormat("'{0}'", _model.FinClass != null ? _model.FinClass.ToUpper() : string.Empty)
                .Append(" )");

            var aa = sb.ToString();

            return sb.ToString();
        }

    }
}