﻿using Fe.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Fe.Web.Infrastructure.Commands
{
    public class UpdatePassword
    {
        private readonly UserPasswordRequestModel _model;
        private readonly string _serviceUrl;
        private HttpClient _client;

        public UpdatePassword(UserPasswordRequestModel model)
        {
            _model = model;
            _serviceUrl = ConfigurationManager.AppSettings["IdentityServerHost"];

        }

        public async Task<string> Run()
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(_model);

                var response = await _client.PostAsync("api/user/changeuserpassword",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                var content = await response.Content.ReadAsStringAsync();


                if (content != null) { 
                    var jObj1 = JsonConvert.DeserializeObject(content);
                    return content;
                }
                return content;
            }
            catch 
            {
                return null;
            }
        }

        private HttpClient GetHttpClient()
        {
            return _client ?? (_client = new HttpClient() { BaseAddress = new Uri(_serviceUrl) });
        }

    }
}