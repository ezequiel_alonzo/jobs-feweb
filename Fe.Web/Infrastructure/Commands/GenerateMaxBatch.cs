﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Fe.Web.Models;

namespace Fe.Web.Infrastructure.Commands
{
    public class GenerateMaxBatch
    {
        private readonly string _path;
        private readonly IEnumerable<PatientWithDetailsModel> _patients;

        public GenerateMaxBatch(string path, IEnumerable<PatientWithDetailsModel> patients)
        {
            _path = path;
            _patients = patients;
        }

        public int Run()
        {
            var patientsByHospCodeQuery = 
                from patient in _patients
                group patient by patient.HospCode into patientsByHospCode
                select patientsByHospCode;

            foreach (var patientsInHospCode in patientsByHospCodeQuery)
            {
                Directory.CreateDirectory(_path + patientsInHospCode.Key);

                File.WriteAllLines(_path + patientsInHospCode.Key + string.Format(@"\{0}_{1}.fecsv", patientsInHospCode.Key, DateTime.Now.ToString("ddMMyyyy_HHmm")), 
                    from patient in patientsInHospCode
                    select string.Format(";{0};;{1};{2};{3};{4};{5};;{6};{7};;",
                        (patient.HospAcct ?? string.Empty).Trim(), 
                        (patient.LastName ?? string.Empty).Trim(),
                        (patient.FirstName ?? string.Empty).Trim(),
                        (patient.Sex ?? string.Empty).Trim(),
                        patient.Dob.HasValue ? patient.Dob.Value.ToString("MM/dd/yyyy") : "",
                        (patient.Ssn ?? string.Empty).Trim(),
                        patient.AdmitDate.HasValue ? patient.AdmitDate.Value.ToString("MM/dd/yyyy") : "", 
                        patient.PatientNum));
            }

            return patientsByHospCodeQuery.Count();
        }
    }
}