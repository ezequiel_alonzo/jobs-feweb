﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Text;
using Fe.Web.Helpers;

namespace Fe.Web.Infrastructure.Commands
{
    public class ImportInsertPatient : FeApiController
    {
        private readonly PatientProfile _patient;
        public string _userName;

        public ImportInsertPatient(PatientProfile patient, string userName)
        {
            _patient = patient;
            _userName = userName;
        }

        public void Run()
        {
            if (string.IsNullOrWhiteSpace(_patient.HospAcct))
                throw new InvalidOperationException("A hospital account number is required.");

            const string idSql = "SELECT patient_seq.nextval from dual";

            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                int id;

                if (!int.TryParse(_patient.PatientNum, out id))
                {
                    id = int.Parse(db.Query<string>(idSql).FirstOrDefault());
                }

                var iSql = BuildInsertFePatientSql(_patient, id.ToString());

                db.Execute(iSql);

                var cSql = BuildInsertFePatientCommentSql(id.ToString());

                db.Execute(cSql);
            }
        }

        private string BuildInsertFePatientSql(PatientProfile pt, string id)
        {
            /*return
                @"INSERT INTO PATIENT
                    PATIENTNUM      
                    , CLIENTNUM       
                    , HOSPACCT        
                    , SSN             
                    , DOB             
                    , SEX             
                    , LASTNAME        
                    , FIRSTNAME       
                    , PATPHONE        
                    , PATADDRESS      
                    , PATCITY         
                    , PATSTATE        
                    , PATZIP          
                    , EMPNAME         
                    , EMPPHONE        
                    , EMPADDRESS      
                    , EMPCITY         
                    , EMPSTATE        
                    , EMPZIP          
                    , RESPNAME        
                    , RESPPHONE       
                    , RESPADDRESS     
                    , RESPWPHONE      
                    , RESPCITY        
                    , RESPSTATE       
                    , RESPZIP         
                    , MINOR           
                    , PLACEDATE       
                    , PLACEAMOUNT     
                    , ADMITDATE       
                    , DISCHDATE       
                    , CAIDNUM         
                    , BDATE           
                    , EP              
                    , CBDATE          
                    , CBUSER          
                    , CURRBALANCE     
                    , INHDSCH         
                    , CURRSTATUS      
                    , REMDATE         
                    , POSTEDDATE      
                    , AMOUNT          
                    , PAYTYPE         
                    , POSTDESC        
                    , RELCODE         
                    , RELID           
                    , LASTCHANGED     
                    , PERIODSTART     
                    , PERIODEND       
                    , PERIODBAL       
                    , VISITTYPE       
                    , FINCLASSDATE    
                    , CLIENTLOC       
                    , FINCLASS        
                    , DCF             
                    , DCFDATE         
                    , MANAGERSTATUS   
                    , HOSPALERT       
                    , MRN             
                    , INSCODE         
                    , ACTIONTYPE      
                    , SPANISH         
                    , AFTERHOURS      
                    , FIELDVISIT      
                    , TRANSFER        
                    , STARFLAG        
                    , WEBCONFO        
                    , WEBCONFOFLAG    
                    , RESPSSN         
                    , PREVFINCLASS    
                    , ALTFINCLASS     
                    , CURRSTATUSQ     
                    , PRINTQ          
                    , PREBADDEBT      
                    , CONNANCEID      
                    , APPLICATIONDATE 
                    , ENCOUNTER       
                    , RESPCELLPHONE   
                    , PATADDRESS2     
                    , PAYMENTSTARTDATE
                    , PAYMENTAMOUNT   
                    , PAYMENTLASTDATE
                VALUES (
                    :PATIENTNUM      
                    , :CLIENTNUM       
                    , :HOSPACCT        
                    , :SSN             
                    , :DOB             
                    , :SEX             
                    , :LASTNAME        
                    , :FIRSTNAME       
                    , :PATPHONE        
                    , :PATADDRESS      
                    , :PATCITY         
                    , :PATSTATE        
                    , :PATZIP          
                    , :EMPNAME         
                    , :EMPPHONE        
                    , :EMPADDRESS      
                    , :EMPCITY         
                    , :EMPSTATE        
                    , :EMPZIP          
                    , :RESPNAME        
                    , :RESPPHONE       
                    , :RESPADDRESS     
                    , :RESPWPHONE      
                    , :RESPCITY        
                    , :RESPSTATE       
                    , :RESPZIP         
                    , :MINOR           
                    , :PLACEDATE       
                    , :PLACEAMOUNT     
                    , :ADMITDATE       
                    , :DISCHDATE       
                    , :CAIDNUM         
                    , :BDATE           
                    , :EP              
                    , :CBDATE          
                    , :CBUSER          
                    , :CURRBALANCE     
                    , :INHDSCH         
                    , :CURRSTATUS      
                    , :REMDATE         
                    , :POSTEDDATE      
                    , :AMOUNT          
                    , :PAYTYPE         
                    , :POSTDESC        
                    , :RELCODE         
                    , :RELID           
                    , :LASTCHANGED     
                    , :PERIODSTART     
                    , :PERIODEND       
                    , :PERIODBAL       
                    , :VISITTYPE       
                    , :FINCLASSDATE    
                    , :CLIENTLOC       
                    , :FINCLASS        
                    , :DCF             
                    , :DCFDATE         
                    , :MANAGERSTATUS   
                    , :HOSPALERT       
                    , :MRN             
                    , :INSCODE         
                    , :ACTIONTYPE      
                    , :SPANISH         
                    , :AFTERHOURS      
                    , :FIELDVISIT      
                    , :TRANSFER        
                    , :STARFLAG        
                    , :WEBCONFO        
                    , :WEBCONFOFLAG    
                    , :RESPSSN         
                    , :PREVFINCLASS    
                    , :ALTFINCLASS     
                    , :CURRSTATUSQ     
                    , :PRINTQ          
                    , :PREBADDEBT      
                    , :CONNANCEID      
                    , :APPLICATIONDATE 
                    , :ENCOUNTER       
                    , :RESPCELLPHONE   
                    , :PATADDRESS2     
                    , :PAYMENTSTARTDATE
                    , :PAYMENTAMOUNT   
                    , :PAYMENTLASTDATE)";
            */

            var sb = new StringBuilder();
            sb.Append("INSERT INTO patient (patientnum,clientnum,hospacct,currstatus,remdate,lastchanged,")
                .Append("cbdate,cbuser,placedate,admitdate,firstname,lastname,mrn,dob,ssn,")
                .Append("dischdate,sex,respname,pataddress,pataddress2,patcity,patstate,patzip,patphone,")
                .Append("currbalance,placeamount,inscode")
                .Append(") VALUES (")
                .AppendFormat("'{0}',", id)
                .AppendFormat("'{0}',", pt.ClientNum)
                .AppendFormat("'{0}',", CleanValueForSql(pt.HospAcct))
                .AppendFormat("'{0}',", "000")
                .AppendFormat("{0},", "sysdate")
                .AppendFormat("{0},", "sysdate")
                .AppendFormat("{0},", "trunc(sysdate)")
                .AppendFormat("'{0}',", _userName)
                .AppendFormat("{0},", "sysdate");

            if (pt.AdmitDate.HasValue)
            {
                sb.AppendFormat("TO_DATE('{0}','MM/DD/YYYY HH24:MI:SS'),",
                    pt.AdmitDate.Value.ToString("MM/dd/yyyy HH:mm:ss"));
            }
            else
            {
                sb.AppendFormat("{0},", "NULL");
            }

            sb.AppendFormat("'{0}',", CleanValueForSql(pt.FirstName))
                .AppendFormat("'{0}',", CleanValueForSql(pt.LastName))
                .AppendFormat("'{0}',", CleanValueForSql(pt.Mrn));

            if (pt.Dob.HasValue)
            {
                sb.AppendFormat("TO_DATE('{0}','MM/DD/YYYY'),", pt.Dob.Value.ToString("MM/dd/yyyy"));
            }
            else
            {
                sb.AppendFormat("{0},", "NULL");
            }

            sb.AppendFormat("'{0}',", CleanValueForSql(pt.Ssn));

            if (pt.DischDate.HasValue)
            {
                sb.AppendFormat("TO_DATE('{0}','MM/DD/YYYY HH24:MI:SS'),",
                    pt.DischDate.Value.ToString("MM/dd/yyyy HH:mm:ss"));
            }
            else
            {
                sb.AppendFormat("{0},", "NULL");
            }

            sb.AppendFormat("'{0}',", CleanValueForSql(pt.Sex))
                .AppendFormat("'{0}',", CleanValueForSql(pt.RespName))
                .AppendFormat("'{0}',", CleanValueForSql(pt.PatAddress))
                .AppendFormat("'{0}',", CleanValueForSql(pt.PatAddress2))
                .AppendFormat("'{0}',", CleanValueForSql(pt.PatCity))
                .AppendFormat("'{0}',", CleanValueForSql(pt.PatState))
                .AppendFormat("'{0}',", CleanValueForSql(pt.PatZip))
                .AppendFormat("'{0}',", CleanValueForSql(pt.PatPhone))
                .AppendFormat("{0},", pt.CurrBalance.HasValue ? pt.CurrBalance : 0)
                .AppendFormat("{0},", pt.PlaceAmount.HasValue ? pt.PlaceAmount : 0)
                .AppendFormat("'{0}')", CleanValueForSql(pt.InsCode));

            return sb.ToString();
        }

        private string BuildInsertFePatientCommentSql(string id)
        {
            var sb = new StringBuilder();
            sb.Append("INSERT INTO comments (patientnum,linenum,username,status,comdate,comments) VALUES ( ")
                .AppendFormat("'{0}',", id)
                .AppendFormat("{0},", 0)
                .AppendFormat("'{0}',", _userName)
                .AppendFormat("'{0}',", "000")
                .AppendFormat("{0},", "sysdate")
                .AppendFormat("'{0}')", "NEW ACCOUNT");

            return sb.ToString();
        }

        protected static string CleanValueForSql(string value)
        {
            return Utils.IsNull(value) ? "" : value.Trim().ToUpper().Replace("'", "''");
        }
    }
}