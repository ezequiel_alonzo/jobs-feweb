﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fe.Web.Infrastructure.Repositories.Libraries;
using System.Text;
using Dapper;

namespace Fe.Web.Infrastructure.Commands
{
    public class ClearLettersQueue : FeApiController
    {
        private readonly PatientLettersRequestModel _model;

        public ClearLettersQueue(PatientLettersRequestModel model)
        {
            _model = model;
        }

        public void Run()
        {
            var patNums = _model.PatientLetters.Select(x => "'" + x.PatNum + "'");
            var commandText = new StringBuilder();
            commandText.Append(@"DELETE FROM queuedletters WHERE patientNum IN ( ")
                .AppendFormat(" {0} )", string.Join(",", patNums));


            using (var db = new OracleConnection(GetFeConnectionString()))
            {
                db.Execute(commandText.ToString());
            }
        }

    }
}