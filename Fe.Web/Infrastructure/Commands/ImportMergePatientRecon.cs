﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Text;
using Fe.Web.Helpers;
using System.Text.RegularExpressions;

namespace Fe.Web.Infrastructure.Commands
{
    public class ImportMergePatientRecon : FeApiController
    {
        private readonly PatientProfile _patient;
        private static readonly Regex rxNonDigits = new Regex(@"[^\d]+");


        public ImportMergePatientRecon(PatientProfile patient)
        {
            _patient = patient;
        }


        public void Run()
        {
            if (string.IsNullOrWhiteSpace(_patient.PatientNum))
                throw new InvalidOperationException("A hospital Patient number is required.");

            var iSql = BuildMergePatientRecon();

            try
            {
                using (OracleConnection conn = new OracleConnection(GetFeConnectionString()))
                {
                    conn.Open();
                    OracleTransaction t = conn.BeginTransaction();
                    OracleCommand comm = new OracleCommand(iSql);// place here your merge statement
                    comm.Transaction = t;
                    comm.Parameters.Add(new OracleParameter("PatientNum", _patient.PatientNum));
                    comm.Parameters.Add(new OracleParameter("ClientNum", _patient.ClientNum));
                    comm.Parameters.Add(new OracleParameter("ScreenSnap", BuildScreenSnapField()));
                    comm.Parameters.Add(new OracleParameter("TranFlag", String.IsNullOrEmpty(_patient.Transfer) ? "F" : "T"));
                    comm.Connection = conn;
                    comm.ExecuteNonQuery();
                    t.Commit();
                }
            }
            catch (Exception ex)
            {

            }

        }


        public string BuildMergePatientRecon()
        {
            var sb = new StringBuilder();

            sb.Append("MERGE INTO patnotes pn ")
                .Append("USING (SELECT :PatientNum  as pnPatientnum, :Clientnum as pnClientNum from dual) h ")
                .Append("ON (pn.patientnum = h.pnPatientnum and pn.clientnum=pnClientNum ) ")
                .Append("WHEN MATCHED THEN ")
                .Append("UPDATE SET pn.screensnap = :ScreenSnap ")
                .Append("WHEN NOT MATCHED THEN ")
                .Append("  INSERT (patientnum ,clientnum ,screensnap ,tranflag ) ")
                .Append("  VALUES (:PatientNum, :ClientNum, :ScreenSnap, :TranFlag) ");

            return sb.ToString();
        }

        public string BuildScreenSnapField()
        {
            var sb = new StringBuilder();

            var pnName = (string.IsNullOrEmpty(_patient.FirstName) || string.IsNullOrEmpty(_patient.LastName)
                ? "NO PATIENT NAME"
                : Utils.Truncate(_patient.FirstName + _patient.LastName, 37)).PadRight(39, ' ');

            var pnPatientnum = _patient.PatientNum.PadRight(10, ' ');

            var pnDob = (_patient.Dob.HasValue
                ? _patient.Dob.Value.ToString("M/dd/yyyy")
                : !string.IsNullOrEmpty(_patient.Mrn)
                    ? _patient.Mrn
                    : string.Empty).PadRight(15, ' ');

            var pnSex = (string.IsNullOrEmpty(_patient.Sex)
                ? string.Empty
                : _patient.Sex).PadRight(5, ' ');

            var pnAdmitDate = (_patient.AdmitDate.HasValue
                ? _patient.AdmitDate.Value.ToString("M/dd/yyyy")
                : string.Empty).PadRight(15, ' ');

            var pnDischDate = (_patient.DischDate.HasValue
                ? _patient.DischDate.Value.ToString("M/dd/yyyy")
                : string.Empty).PadRight(15, ' ');

            var pnInOut = string.Empty.PadRight(8, ' ');

            var pnSSN = (string.IsNullOrEmpty(_patient.Ssn)
                ? string.Empty
                : rxNonDigits.Replace(_patient.Ssn, "")).PadRight(14, ' ');

            var pnPhone = (string.IsNullOrEmpty(_patient.PatPhone)
                ? string.Empty
                : rxNonDigits.Replace(_patient.PatPhone, "")).PadRight(15, ' ');

            var pnCurrBalance = (_patient.CurrBalance.HasValue
                ? _patient.CurrBalance.ToString()
                : string.Empty).PadRight(11, ' ');

            var pnAddress1 = (string.IsNullOrEmpty(_patient.PatAddress)
                ? string.Empty
                : Utils.Truncate(_patient.PatAddress, 28)).PadRight(30, ' ');

            var pnAddress2 = (string.IsNullOrEmpty(_patient.PatAddress2)
                ? string.Empty
                : Utils.Truncate(_patient.PatAddress2, 28)).PadRight(30, ' ');

            var pnCity = (string.IsNullOrEmpty(_patient.PatCity)
               ? string.Empty
               : Utils.Truncate(_patient.PatCity, 18)).PadRight(18, ' ');

            var pnState = (string.IsNullOrEmpty(_patient.PatState)
               ? string.Empty
               : Utils.Truncate(_patient.PatState, 2)).PadRight(3, ' ');

            var pnZip = (string.IsNullOrEmpty(_patient.PatZip)
              ? string.Empty
              : Utils.Truncate(_patient.PatZip, 10)).PadRight(11, ' ');

            var pnFinClass = (string.IsNullOrEmpty(_patient.FinClass)
              ? string.Empty
              : Utils.Truncate(_patient.PatZip, 10)).PadRight(10, ' ');

            var pnDateImported = DateTime.Now.ToString("M/dd/yyyy h:mm:ss tt");

            sb.Append(pnName)
                .Append(pnPatientnum)
                .Append(pnDob)
                .Append(pnSex)
                .Append(pnAdmitDate)
                .Append(pnDischDate)
                .Append(pnInOut)
                .Append(pnSSN)
                .Append(pnPhone)
                .Append(pnCurrBalance)
                .Append(pnAddress1)
                .Append(pnAddress2)
                .Append(pnCity)
                .Append(pnState)
                .Append(pnZip)
                .Append(pnFinClass)
                .Append(pnDateImported);

            return sb.ToString();
        }


    }
}