﻿using Fe.Web.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Models;
using Utils = Fe.Web.Helpers.Utils;
using Fe.Web.Infrastructure.Queries;

namespace Fe.Web.Infrastructure.Commands
{
    public class InsertPatientPosting : FeApiController
    {
        private readonly PostingInsertModel _model;

        public InsertPatientPosting(PostingInsertModel model)
        {
            _model = model;
        }

        public string Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                     {
                        {"patientNum", Utils.IsNotNull(_model.PatientNum) ? _model.PatientNum : string.Empty}
                    };

                    var sql = BuildGetNextLineNum();

                    var lNum = db.Query<int?>(sql, dict).FirstOrDefault();

                    if (!lNum.HasValue)
                    {
                        _model.LineNum = 1;
                    }
                    else
                    {
                        _model.LineNum = (int)lNum + 1;
                    }
                    var clientInfo = new GetClientInfo(_model.ClientNum).Run();
                    if (clientInfo.First().Continhouse.HasValue)
                    {
                        if ((new[] { "1", "5", "6" }).Contains(_model.Type))
                        {
                            _model.AgencyComm = (_model.Amount * clientInfo.First().Continhouse.Value / 100) ?? 0;
                            if (_model.Type != "1")
                            {
                                _model.HfmiComm = (_model.Amount * 3 / 100) ?? 0;
                            }
                            else
                            {
                                _model.HfmiComm = 0;
                            }
                        }
                    }
                    sql = BuildInsertPosting();

                    db.Execute(sql);

                    sql = BuilUpdatePatientCurrentAmount();

                    db.Execute(sql);

                    sql = BuildUpdatePostingSummary();

                    var sumResult = db.Execute(sql, new
                    {
                        clientNum = _model.ClientNum,
                        postingDate = _model.PostingDate.Value.Year.ToString() + _model.PostingDate.Value.Month.ToString().PadLeft(2, '0'),
                        postingType = _model.Type,
                        postingAmount = _model.Amount
                    });

                    if (sumResult < 1)
                    {
                        sql = BuildInsertPostingSummary();

                        db.Execute(sql, new
                        {
                            clientNum = _model.ClientNum,
                            postingDate = _model.PostingDate.Value.Year.ToString() + _model.PostingDate.Value.Month.ToString().PadLeft(2, '0'),
                            postingType = _model.Type,
                            postingAmount = _model.Amount
                        });
                    }
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        public string Validate()
        {
            if (!_model.PostingDate.HasValue) return "Posting Date Is Required";
            if (!_model.RemitDate.HasValue) return "Remit Date Is Required";
            if (_model.Amount == null) return "Amount is Required";
            return "";
        }

        private static string BuildGetNextLineNum()
        {
            var sb = new StringBuilder();

            sb.Append(
                "SELECT linenum FROM posting WHERE patientnum = :patientNum AND rownum=1 ORDER BY linenum desc");

            return sb.ToString();
        }

        public string BuildInsertPosting()
        {
            var sb = new StringBuilder();

            sb.Append("INSERT INTO posting (PATIENTNUM,LINENUM,USERNAME,REMITDATE,POSTINGDATE,AMOUNT,")
                .Append("TYPE,AGENCYCOMM,HFMIrep,HFMIcomm,TimeStamp ) VALUES (")
                .AppendFormat("'{0}', ", _model.PatientNum)
                .AppendFormat("{0}, ", _model.LineNum)
                .AppendFormat("'{0}', ", _model.UserName.ToUpper())
                .AppendFormat("'{0}', ", _model.RemitDate.HasValue ? _model.RemitDate.Value.ToString("dd-MMM-yy") : string.Empty)
                .AppendFormat("'{0}', ", _model.PostingDate.HasValue ? _model.PostingDate.Value.ToString("dd-MMM-yy") : string.Empty)
                .AppendFormat("'{0}', ", _model.Amount)
                .AppendFormat("'{0}', ", _model.Type)
                .AppendFormat("{0}, ", _model.AgencyComm)
                .AppendFormat("'{0}',", _model.HfmiRep)
                .AppendFormat("{0}, ", _model.HfmiComm)
                .Append("SYSDATE )");

            return sb.ToString();
        }

        public string BuilUpdatePatientCurrentAmount()
        {
            var sb = new StringBuilder();

            sb.Append("UPDATE patient set ")
                .AppendFormat("currBalance = (currBalance - {0}) ", _model.Amount)
                .Append("WHERE ")
                .AppendFormat(" patientNum ='{0}' ", _model.PatientNum);

            return sb.ToString();
        }

        public string BuildUpdatePostingSummary()
        {
            var sb = new StringBuilder();

            sb.Append("Update PostingSummary ")
                .Append("set Amount = Amount + :postingAmount ")
                .Append("where ClientNum = :clientNum ")
                .Append("and PostingDate = :postingDate ")
                .Append("and Type = :postingType ");

            return sb.ToString();
        }

        public string BuildInsertPostingSummary()
        {
            var sb = new StringBuilder();

            sb.Append("insert into PostingSummary ")
                .Append("(Clientnum,PostingDate,Type,Amount ")
                .Append(" ) values ( ")
                .Append(" :clientNum, ")
                .Append(" :postingDate, ")
                .Append(" :postingType, ")
                .Append(" :postingAmount )");

            return sb.ToString();
        }
    }
}