﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Utils = Fe.Web.Helpers.Utils;
using System;

namespace Fe.Web.Infrastructure.Commands
{
    public class InsertPatientComment : FeApiController
    {

        private readonly CommentInsertModel _model;

        public InsertPatientComment(CommentInsertModel model)
        {
            _model = model;
        }

        public string Validate()
        {
            var sb = new StringBuilder();

            if (Utils.IsNull(_model.TemplateName))
            {
                sb.Append("Template Name");
                sb.Insert(0, "Missing required parameters: ");
                return sb.ToString().Substring(0, sb.ToString().Length - 1);
            }
            var tName = _model.TemplateName.ToLower();
            switch (tName)
            {
                case "hfmi":
                    if (Utils.IsNull(_model.PatientNum)) sb.Append("Patient Number,");
                    break;
                case "emr":
                    if (Utils.IsNull(_model.SubscriberNum)) sb.Append("Subscriber Number,");
                    break;
            }
            
            if (Utils.IsNull(_model.Comment)) sb.Append("Comment,");
            if (Utils.IsNull(_model.Status)) sb.Append("Status,");

            if (sb.ToString().Length <= 0) return "";

            sb.Insert(0, "Missing required parameters: ");
            return sb.ToString().Substring(0, sb.ToString().Length - 1);
        }

        public bool Run()
        {

            if (Utils.IsNull(_model.TemplateName)) return false;
            var tName = _model.TemplateName.ToLower();
            switch (tName)
            {
                case "hfmi":
                    return RunInsertPatientComment();
                case "emr":

                    if (Utils.IsNull(_model.PatientNum) && Utils.IsNull(_model.SubscriberNum)) return false;

                    if (Utils.IsNull(_model.PatientNum))
                    {
                        var query = new GetPatientNumFromHospAcct(_model.SubscriberNum);
                        _model.PatientNum = query.Run();
                    }

                    return Utils.IsNotNull(_model.PatientNum) && RunInsertPatientComment();

                default:
                    return false;
            }
            
        }

        public bool RunInsertPatientComment()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, object>
                    {
                        {"patientNum", Utils.IsNotNull(_model.PatientNum) ? _model.PatientNum : string.Empty}
                    };

                    var sql = BuildGetNextLineNum();

                    var lNum = db.Query<int?>(sql, dict).FirstOrDefault();

                    if (!lNum.HasValue)
                    {
                        _model.LineNum = 1;
                    }
                    else
                    {
                        _model.LineNum = (int) lNum + 1;
                    }

                    sql = BuildInsertCommentSql();

                    db.Execute(sql);

                    sql = BuildGetCallBackDaysToAdd();

                    var dict1 = new Dictionary<string, object>
                    {
                        {"ccCode", Utils.IsNotNull(_model.Status) ? _model.Status : string.Empty}
                    };

                    var dtAdd = _model.CbDate.HasValue ? (_model.CbDate.Value - DateTime.Today).TotalDays.ToString() : db.Query<string>(sql, dict1).FirstOrDefault();

                    sql = BuildUpdatePatientSql(dtAdd);

                    db.Execute(sql, dict);
                    
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private static string BuildGetNextLineNum()
        {
            var sb = new StringBuilder();

            sb.Append(
                "SELECT linenum FROM comments WHERE patientnum = :patientNum AND rownum=1 ORDER BY linenum desc");

            return sb.ToString();
        }

        private string BuildInsertCommentSql()
        {
            var sb = new StringBuilder();

            sb.Append("INSERT INTO comments (patientnum,linenum,username,status,comdate,comments,actioncode) VALUES ")
                .Append("(")
                .AppendFormat("'{0}',", _model.PatientNum)
                .AppendFormat("{0},", _model.LineNum)
                .AppendFormat("'{0}',", _model.UserName)
                .AppendFormat("'{0}',", _model.Status)
                .AppendFormat("{0},", "sysdate")
                .AppendFormat("'{0}',", _model.Comment)
                .AppendFormat("{0}", _model.ActionCode)
                .Append(")");

            return sb.ToString();
        }

        private static string BuildGetCallBackDaysToAdd()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT daysadded FROM commcodes WHERE code=:ccCode");

            return sb.ToString();
        }

        private string BuildUpdatePatientSql(string daysToAdd)
        {
            var sb = new StringBuilder();

            sb.Append("UPDATE patient SET ")
            .AppendFormat("currstatus='{0}',", _model.Status)
            .Append(_model.SpanishPat == true ? "spanish = '1'," : "spanish ='0',")
            .Append (_model.FieldVisitPat == true ? "fieldvisit ='1'," : "fieldvisit ='0',")
            .Append(_model.AfterHoursPat == true ? "afterhours ='1'," : "afterhours ='0',")
            .AppendFormat("remdate={0},", "sysdate")
            .AppendFormat("lastchanged={0}", "sysdate");

            if (_model.ActionCode == 19)
                sb.Append(", ApplicationDate = sysdate ");

            if (Utils.IsNotNull(daysToAdd))
                sb.AppendFormat(",cbdate={0}", "sysdate + INTERVAL '" + daysToAdd + "' DAY");

            sb.Append(" WHERE patientnum=:patientNum");
            if(_model.TemplateName =="emr"){
                sb.AppendFormat (" OR hospacct = '{0}'",_model.SubscriberNum);
            }
            return sb.ToString();
        }

    }
}