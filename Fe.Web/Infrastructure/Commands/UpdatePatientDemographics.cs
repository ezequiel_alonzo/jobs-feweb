﻿using System.Collections.Generic;
using System.Text;
using Dapper;
using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using Utils = Fe.Web.Helpers.Utils;
using System.Data;

namespace Fe.Web.Infrastructure.Commands
{
    public class UpdatePatientDemographics : FeApiController
    {
        private readonly PatientDemographics _model;

        public UpdatePatientDemographics(PatientDemographics model)
        {
            _model = model;
        }

        public string Validate()
        {
            var sb = new StringBuilder();

            if (Utils.IsNull(_model.TemplateName))
            {
                sb.Append("Template Name");
                sb.Insert(0, "Missing required parameters: ");
                return sb.ToString().Substring(0, sb.ToString().Length - 1);
            }
            var tName = _model.TemplateName.ToLower();
            switch (tName)
            {
                case "hfmi":
                    if (Utils.IsNull(_model.PatNum)) sb.Append("Patient Number,");
                    break;
                case "emr":
                    if (Utils.IsNull(_model.ActHospAcct)) sb.Append("Subscriber Number,");
                    break;
            }

            if (sb.ToString().Length <= 0) return "";

            sb.Insert(0, "Missing required parameters: ");
            return sb.ToString().Substring(0, sb.ToString().Length - 1);
        }

        public bool Run()
        {

            if (Utils.IsNull(_model.TemplateName)) return false;
            var tName = _model.TemplateName.ToLower();
            switch (tName)
            {
                case "hfmi":
                    return RunUpdatePatientDemographics();
                case "emr":

                    if (Utils.IsNull(_model.PatNum) && Utils.IsNull(_model.ActHospAcct)) return false;

                    if (Utils.IsNull(_model.PatNum))
                    {
                        //var query = new GetPatNumFromHospAcct(_model.ActHospAcct);
                        //_model.PatNum = query.Run();
                    }

                    return Utils.IsNotNull(_model.PatNum) && RunUpdatePatientDemographics();

                default:
                    return false;
            }

        }

        public bool RunUpdatePatientDemographics()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {

                    var sql = BuildUpdatePatientDemographicsSql();

                    var param = new DynamicParameters();
                    param.Add(name: "patFirstName", value: _model.PatFirstName!=null ? _model.PatFirstName.ToUpper() : null , direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patLastName", value: _model.PatLastName != null ? _model.PatLastName.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "currBalance", value: _model.ActCurrBalance, direction: ParameterDirection.Input, dbType: DbType.Double);
                    param.Add(name: "patNum", value: _model.PatNum, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "pataddress", value: _model.PatAddress != null ? _model.PatAddress.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patcity", value: _model.PatCity != null ? _model.PatCity.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patstate", value: _model.PatState != null ? _model.PatState.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patzip", value: _model.PatZip != null ? _model.PatZip.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "patphone", value: _model.PatPhone, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "respname", value: _model.RespName != null ? _model.RespName.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "respaddress", value: _model.RespAddress != null ? _model.RespAddress.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "respcity", value: _model.RespCity != null ? _model.RespCity.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "respstate", value: _model.RespState != null ? _model.RespState.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "respzip", value: _model.RespZip != null ? _model.RespZip.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "respphone", value: _model.RespPhone, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "respssn", value: _model.RespSsn != null ? _model.RespSsn.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "encounter", value: _model.ActEncounter != null ? _model.ActEncounter.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "respwphone", value: _model.RespWorkPhone != null ? _model.RespWorkPhone.ToUpper() : null != null ? _model.PatFirstName.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "sex", value: _model.PatGender != null ? _model.PatGender.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "finclass", value: _model.RespFc != null ? _model.RespFc.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "placeamount", value: _model.ActPlaceAmt, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "hospacct", value: _model.ActHospAcct != null ? _model.ActHospAcct.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "MRN", value: _model.ActMrn != null ? _model.ActMrn.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "clientnum", value: _model.ClientNum , direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "ssn", value: _model.PatSsn != null ? _model.PatSsn.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "caidnum", value: _model.ActMedicaidNum != null ? _model.ActMedicaidNum.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "inscode", value: _model.ActInsCode != null ? _model.ActInsCode.ToUpper() : null, direction: ParameterDirection.Input, dbType: DbType.String);
                    param.Add(name: "placedate", value: _model.ActPlaceDate, direction: ParameterDirection.Input, dbType: DbType.Date);
                    param.Add(name: "admitdate", value: _model.ActAdmitDate, direction: ParameterDirection.Input, dbType: DbType.Date);
                    param.Add(name: "dischdate", value: _model.ActDischDate, direction: ParameterDirection.Input, dbType: DbType.Date);
                    param.Add(name: "Applicationdate", value: _model.ActApplDate, direction: ParameterDirection.Input, dbType: DbType.Date);
                    param.Add(name: "dob", value: _model.PatDob, direction: ParameterDirection.Input, dbType: DbType.Date);
                    
                    

                    db.Execute(sql, param);

                }

                

                return true;
            }
            catch
            {
                return false;
            }
        }

        private string BuildUpdatePatientDemographicsSql()
        {
            var sb = new StringBuilder();

            
            sb.Append("UPDATE patient SET ")
                            .Append("firstname = :patFirstName,")
                            .Append("lastname = :patLastName,")
                            .Append("currbalance = :currBalance, ")
                            .Append("pataddress = :pataddress,")
                            .Append("patcity = :patcity,")
                            .Append("patstate = :patstate,")
                            .Append("patzip = :patzip,")
                            .Append("patphone = :patphone,")
                            .Append("respname = :respname,")
                            .Append("respaddress = :respaddress,")
                            .Append("respcity = :respcity,")
                            .Append("respstate = :respstate,")
                            .Append("respzip = :respzip,")
                            .Append("respphone = :respphone,")
                            .Append("respssn = :respssn,")
                            .Append("encounter = :encounter,")
                            .Append("respwphone = :respwphone,")
                            .Append("sex = :sex,")
                            .Append("finclass = :finclass,")
                            .Append("placeamount = :placeamount,")
                            .Append("hospacct = :hospacct,")
                            .Append("MRN = :MRN, ")
                            .Append("clientnum = :clientnum, ")
                            .Append("ssn = :ssn,")
                            .Append("caidnum = :caidnum,")
                            .Append("inscode = :inscode,")
                            .Append("placedate = :placedate,")
                            .Append("admitdate = :admitdate,")
                            .Append("dischdate = :dischdate,")
                            .Append("Applicationdate = :Applicationdate, ")
                            .Append("dob = :dob, ")
                            .Append("lastchanged = sysdate ")
                            .Append("WHERE patientnum=:patNum");

            return sb.ToString();
        }
    }
}