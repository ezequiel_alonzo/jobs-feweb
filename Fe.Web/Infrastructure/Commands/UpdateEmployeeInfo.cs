﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using Fe.Web.Controllers;
using Fe.Web.Models;
using System.Web.Http;
using System.Text;
using Devart.Data.Oracle;
namespace Fe.Web.Infrastructure.Commands
{
    public class UpdateEmployeeInfo : FeApiController
    {
        private readonly UserInfo _uInfo;

        public UpdateEmployeeInfo(UserInfo uInfo)
        {
            _uInfo = uInfo;
        }

        public IHttpActionResult Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var dict = new Dictionary<string, Object>{
                    {"userName", _uInfo.UserName ?? string.Empty },
                    {"StartDate", _uInfo.StartDate.HasValue ? _uInfo.StartDate.Value.ToString("dd-MMM-yyyy") : string.Empty},
                    {"PersonalRate", _uInfo.PersonalRate.HasValue ? _uInfo.PersonalRate.Value : 0 },
                    {"VacationRate", _uInfo.VacationRate.HasValue ? _uInfo.VacationRate.Value : 0},
                    {"Type", _uInfo.Type ?? string.Empty},
                    {"Percentage", _uInfo.Percentage ?? string.Empty },
                    {"Location", _uInfo.Location ?? string.Empty  },
                    {"Rate", _uInfo.Rate ?? string.Empty },
                    {"SupvName", _uInfo.SupvName ?? string.Empty  },
                    {"Region", _uInfo.Region ?? string.Empty },
                    {"CoachingFreq", _uInfo.CoachingFreq ?? string.Empty  },
                    {"TermDate", _uInfo.TermDate.HasValue ? string.Empty : string.Empty },
                    {"PayType", _uInfo.PayType ?? string.Empty },
                    {"PayrollEmail", _uInfo.PayrollEmail ?? string.Empty  },
                    {"MsgText", _uInfo.MsgText?? string.Empty  },
                    {"AlertFlag", _uInfo.AlertFlag ?? string.Empty },
                    {"HospClientNum", _uInfo.HospClientNum?? string.Empty  },
                    {"Trainer", _uInfo.Trainer ?? string.Empty },
                    {"MgrCoach", _uInfo.MgrCoach ?? string.Empty },
                    {"PayChexEmpNum", _uInfo.PayChexEmpNum ?? string.Empty },
                    {"PhoneRptFlag", _uInfo.PhoneRptFlag ?? string.Empty },
                    {"CardName", _uInfo.CardName ?? string.Empty },
                    {"CardTitle", _uInfo.CardTitle ?? string.Empty },
                    {"Card1Left", _uInfo.Card1Left ?? string.Empty },
                    {"Card2Left", _uInfo.Card2Left ?? string.Empty },
                    {"Card3Left", _uInfo.Card3Left ?? string.Empty },
                    {"Card4Left", _uInfo.Card4Left ?? string.Empty },
                    {"Card1Right", _uInfo.Card1Right ?? string.Empty },
                    {"Card2Right", _uInfo.Card2Right ?? string.Empty },
                    {"Card3Right", _uInfo.Card3Right ?? string.Empty },
                    {"Card4Right", _uInfo.Card4Right ?? string.Empty },
                    {"Name", _uInfo.Name ?? string.Empty },
                    {"Addr1", _uInfo.Addr1 ?? string.Empty },
                    {"Addr2", _uInfo.Addr2 ?? string.Empty },
                    {"City", _uInfo.City ?? string.Empty },
                    {"State", _uInfo.State ?? string.Empty },
                    {"Zipcode", _uInfo.Zipcode ?? string.Empty },
                    {"Ssn", _uInfo.Ssn ?? string.Empty },
                    {"DrivLic", _uInfo.DrivLic ?? string.Empty },
                    {"HPhone", _uInfo.HPhone ?? string.Empty },
                    {"WPhone", _uInfo.WPhone ?? string.Empty },
                    {"Dob", _uInfo.Dob.HasValue ? _uInfo.Dob.Value.ToString("dd-MMM-yyyy") : string.Empty},
                    {"EContact", _uInfo.EContact ?? string.Empty },
                    {"EPhone", _uInfo.EPhone ?? string.Empty },
                    {"MiscInfo", _uInfo.MiscInfo ?? string.Empty }
                };
                    var sql = BuildUpdateEmployeeSql();

                    db.Execute(sql, dict);

                    return Ok();
                }
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public string BuildUpdateEmployeeSql()
        {
            var sb = new StringBuilder();
            sb.Append("UPDATE employee SET ")
                .Append("lastchanged = sysdate ")
                .Append(",StartDate=:StartDate  ")
                .Append(",PersonalRate=:PersonalRate  ")
                .Append(",VacationRate=:VacationRate  ,")
                .Append("Type=:Type  ,")
                .Append("Percentage=:Percentage  ,")
                .Append("Location=:Location  ,")
                .Append("Rate=:Rate  ,")
                .Append("SupvName=:SupvName  ,")
                .Append("Region=:Region  ,")
                .Append("CoachingFreq=:CoachingFreq  ,")
                .Append("TermDate=:TermDate  ,")
                .Append("PayType=:PayType  ,")
                .Append("PayrollEmail=:PayrollEmail  ,")
                .Append("MsgText=:MsgText  ,")
                .Append("AlertFlag=:AlertFlag  ,")
                .Append("HospClientNum=:HospClientNum  ,")
                .Append("Trainer=:Trainer  ,")
                .Append("MgrCoach=:MgrCoach  ,")
                .Append("PayChexEmpNum=:PayChexEmpNum  ,")
                .Append("PhoneRptFlag=:PhoneRptFlag  ,")
                .Append("CardName=:CardName  ,")
                .Append("CardTitle=:CardTitle  ,")
                .Append("Card1Left=:Card1Left  ,")
                .Append("Card2Left=:Card2Left  ,")
                .Append("Card3Left=:Card3Left  ,")
                .Append("Card4Left=:Card4Left  ,")
                .Append("Card1Right=:Card1Right  ,")
                .Append("Card2Right=:Card2Right  ,")
                .Append("Card3Right=:Card3Right  ,")
                .Append("Card4Right=:Card4Right  ,")
                .Append("Name=:Name  ,")
                .Append("Addr1=:Addr1  ,")
                .Append("Addr2=:Addr2  ,")
                .Append("City=:City  ,")
                .Append("State=:State  ,")
                .Append("Zipcode=:Zipcode  ,")
                .Append("Ssn=:Ssn  ,")
                .Append("DrivLic=:DrivLic  ,")
                .Append("HPhone=:HPhone  ,")
                .Append("WPhone=:WPhone  ,")
                .Append("Dob=:Dob  ,")
                .Append("EContact=:EContact  ,")
                .Append("EPhone=:EPhone  ,")
                .Append("MiscInfo=:MiscInfo  ")
                .Append("Where empnum= :userName");
            


            return sb.ToString();
        }
    }
}