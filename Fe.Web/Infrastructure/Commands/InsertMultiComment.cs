﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Dapper;
using System.Text;

namespace Fe.Web.Infrastructure.Commands
{
    public class InsertMultiComment : FeApiController
    {
        private readonly MultiCommentInsertModel _model;

        public InsertMultiComment(MultiCommentInsertModel model)
        {
            _model = model;
        }

        public IHttpActionResult Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {

                    var sql = BuildMultiCommentInsertSql();

                    db.Execute(sql, new
                    {
                        actionCode = _model.ActionCode,
                        info = _model.Comment,
                        statusCode = _model.Status,
                        userName = _model.UserName,
                        patientNums = _model.PatientList,
                    });

                    sql = BuildGetCallBackDaysToAdd();

                    var dtAdd = _model.CbDate.HasValue 
                        ?(_model.CbDate.Value - DateTime.Today).TotalDays.ToString() 
                        :db.Query<string>(sql, new { statusCode = _model.Status }).FirstOrDefault();

                    sql = BuildMultiUpdatePatient(dtAdd);

                    db.Execute(sql, new
                    {
                        statusCode = _model.Status,
                        userName = _model.UserName,
                        patientNums = _model.PatientList,
                        fieldVisitPat = _model.FieldVisitPat ? "Y" : "F",
                        spanishPat = _model.SpanishPat ? "Y" : "F",
                        afterHoursPat = _model.AfterHoursPat ? "Y" : "F",
                        daysToAdd = dtAdd

                    });

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private static string BuildGetCallBackDaysToAdd()
        {
            var sb = new StringBuilder();

            sb.Append("SELECT daysadded FROM commcodes WHERE code= :statusCode ");

            return sb.ToString();
        }

        private string BuildMultiUpdatePatient(string daysToAdd)
        {
            var sb = new StringBuilder();

            sb.Append("update patient set ")
            .Append("fieldVisit = :fieldVisitPat, ")
            .Append("spanish = :spanishPat, ")
            .Append("afterHours = :afterHoursPat, ")
            .Append("currStatus = :statusCode, ")
            .Append("remdate = sysdate, ")
            .Append("lastchanged = sysdate ");
            
            if (!string.IsNullOrEmpty(daysToAdd)) sb.AppendFormat(",cbdate={0}", "sysdate + INTERVAL '" + daysToAdd + "' DAY ");

           sb.Append("where patientnum in :patientNums");
            

            return sb.ToString();
        }

        private string BuildMultiCommentInsertSql()
        {
            var sb = new StringBuilder();
            sb.Append("insert into comments (ACTIONCODE,COMDATE,COMMENTS,PATIENTNUM,STATUS,USERNAME,LINENUM ) ")
            .Append("select ")
            .Append(":actionCode,")
            .Append("sysdate,")
            .Append(":info,")
            .Append("p.patientnum,")
            .Append(":statusCode,")
            .Append(":userName,")
            .Append("(select max(linenum) + 1 from comments c where c.patientnum=p.patientnum) ")
            .Append("FROM patient p ")
            .Append("where patientnum in :patientNums");

            return sb.ToString();
        }

    }
}