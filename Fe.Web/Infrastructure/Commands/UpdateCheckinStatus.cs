﻿using Devart.Data.Oracle;
using Fe.Web.Controllers;
using Fe.Web.Helpers;
using Fe.Web.Models;
using System.Collections.Generic;
using System.Text;
using Dapper;

namespace Fe.Web.Infrastructure.Commands
{
    public class UpdateCheckinStatus : FeApiController
    {
        private readonly UserCheckinModel _model;
        private readonly string _ipAddress;

        public UpdateCheckinStatus(UserCheckinModel model, string ipAddress)
        {
            _model = model;
            _ipAddress = ipAddress;
        }

        public bool Run()
        {
            try
            {
                using (var db = new OracleConnection(GetFeConnectionString()))
                {
                    var sql = BuildUpdateCheckedinStatus();
                    var dict = new Dictionary<string, object> {
                {"userName", (Utils.IsNotNull(_model.UserName) ? _model.UserName.ToUpper() : string.Empty).ToUpper()},
                {"ipAddress", (Utils.IsNotNull(_ipAddress) ? _ipAddress : string.Empty).ToUpper()}
            };
                    db.Execute(sql, dict);

                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private string BuildUpdateCheckedinStatus()
        {
            var sb = new StringBuilder();

            if (_model.IsCheckedIn == true)
            {
                sb.Append("update TIMECARDS ")
                   .Append("set DATEOUT = sysdate, ")
                   .Append("ipaddress= :ipAddress ")
                   .Append("where USERNAME = :userName and DATEOUT is null");
            }
            else
            {
                sb.Append("insert into TIMECARDS (USERNAME,LINENUM,DATEIN,ipaddress) ")
                  .Append("Select ")
                  .Append(":userName, ")
                  .Append("LINENUM +1,")
                  .Append("sysdate, ")
                  .Append(":ipAddress ")
                  .Append("from TIMECARDS where USERNAME = :userName and ROWNUM=1 order by USERNAME, LINENUM desc");
            }

            return sb.ToString();
        }
    }
}