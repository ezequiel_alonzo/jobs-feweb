﻿using System;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using DocumentFormat.OpenXml;
using System.Configuration;
using BarcodeLib;
using System.Drawing;
using Novacode;
using System.Text.RegularExpressions;


namespace Fe.Web.Infrastructure.Common
{
    public class LettersFileGeneration
    {
        private readonly PatientLettersRequestModel _model;
        private readonly string _basePath;
        private string _newDocumentName;
        private string _newDocumentPath;
        private string _baseUrl;
        

        public LettersFileGeneration(PatientLettersRequestModel model, string basePath, string baseUrl)
        {
            _model = model;
            _basePath = basePath;
            _baseUrl = baseUrl;
        }

        public string Run()
        {
            List<string> ListOfBarCodes = new List<string>();
            var tempLettersList = new List<string>();
            var PatientLettersList = _model.PatientLetters;
            var tempNameList = new List<string>();
            string uriAuthority;

            uriAuthority = ConfigurationManager.AppSettings["SiteVersion"] != null ? ConfigurationManager.AppSettings["SiteVersion"] : _baseUrl;

            var getHospInfo = new GetMasterInfo(_model.PatientLetters.First().ClientNum);

            var Hospinfo = getHospInfo.Run().FirstOrDefault();

            PatientLettersList.ForEach(patientletter =>
            {
                var LetterPath = "templates\\" + Hospinfo.HospCode + "\\" + patientletter.LetterName;
                var fullPath = _basePath + LetterPath;

                if (File.Exists(fullPath))
                {
                    var processType = PatientLettersList.Count() > 1 ? "Batch_Letters_" : "Letter_";
                    _newDocumentName = processType + patientletter.HospName.Replace(" ", "_") + "_by_User_" + _model.UserName +
                        string.Format("text-{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now) + Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Replace("/", "_").Replace("+", "_") + ".docx";
                    tempNameList.Add(_newDocumentName);
                    _newDocumentPath = _basePath + "generated\\" + Hospinfo.HospCode + "\\" + _newDocumentName;
                    tempLettersList.Add(_newDocumentPath);

                    var checkSumPreCalc = "000531898" + patientletter.HospAcct.PadLeft(12, '0') + String.Format("{0:0.00}", patientletter.CurrBalance.Value).Replace(".", "").PadLeft(9, '0');
                    Regex regex = new Regex(@"^\d+$");
                    var CheckSum= 0;
                    if (regex.IsMatch(checkSumPreCalc))
                    {
                       CheckSum = GetMod10Digit(checkSumPreCalc);
                    }
                    var templateEngine = new swxben.docxtemplateengine.DocXTemplateEngine();
                    templateEngine.Process(
                        source: fullPath,
                        destination: _newDocumentPath,
                        data: new
                        {
                            HospName = Hospinfo.HospName ?? " ",
                            HospAddress = Hospinfo.HospAddress ?? " ",
                            FirstName = patientletter.FirstName ?? " ",
                            LastName = patientletter.LastName ?? " ",
                            pAddress = patientletter.PAddress ?? " ",
                            HospAcct = patientletter.HospAcct ?? " ",
                            CurrBalance = patientletter.CurrBalance.HasValue ? "$ " + String.Format("{0:0.00}", patientletter.CurrBalance.Value) : " ",
                            AdmitDate = patientletter.AdmitDate.HasValue ? patientletter.AdmitDate.Value.ToString("MM-dd-yyyy") : " ",
                            pCity = patientletter.PCity ?? " ",
                            pState = patientletter.PState ?? " ",
                            pZip = patientletter.PZip ?? " ",
                            HospContact = _model.DisplayName.ToUpper() ?? " ",
                            HospCity = Hospinfo.HospCity ?? " ",
                            HospState = Hospinfo.HospState ?? " ",
                            HospPhone = Hospinfo.HospPhone ?? " ",
                            HospZip = Hospinfo.HospZip ?? " ",
                            DischDate = patientletter.DischDate.HasValue ? patientletter.DischDate.Value.ToString("MM-dd-yyyy") : " ",
                            PaymentDate = patientletter.PaymentStartDate.HasValue ? patientletter.PaymentStartDate.Value.ToString("MM-dd-yyyy") : " ",
                            Payment = patientletter.PaymentAmount ?? " ",
                            TodaysDate = DateTime.Now.ToString("MM-dd-yyyy"),
                            ScanLine = checkSumPreCalc + CheckSum.ToString()
                        }
                    );
                    //******BARCODE COMMENTED UNTIL FURTHER NOTICE*****
                    /*if (patientletter.ClientNum.Contains("90406") || patientletter.ClientNum.Contains("88881"))
                    {
                        try
                        {
                            ListOfBarCodes.Add(AddBarCode(_newDocumentPath, "000531898" + patientletter.HospAcct.PadLeft(12, '0') + String.Format("{0:0.00}", patientletter.CurrBalance.Value).Replace(".", "").PadLeft(9, '0') + "9", _basePath + "generated\\barcodes\\"));
                            
                        }
                        catch (Exception ex)
                        {
                            var xx = ex.Message;
                        }
                    }*/
                }
            });
            JoinLetters(tempLettersList.Select(i => i.ToString()).ToArray());
            ReleaseFiles(ListOfBarCodes, _basePath + "generated\\barcodes\\");

            return uriAuthority + "/content/documents/generated/" + Hospinfo.HospCode + "/" + tempNameList.First();
        }


        public static int GetMod10Digit(string checkSumPreCalc)
        {
            int[] checkSumPosibleResults = { 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 };
            List<int> digits = new List<int>();
            foreach (var x in checkSumPreCalc)
            {
                digits.Add(Int32.Parse(x.ToString()));
            }
            var i = 0;
            var lengthMod = digits.Count % 2;
            return (digits.Sum(d => i++ % 2 == lengthMod ? d : checkSumPosibleResults[d]) * 9) % 10;
        }

        public static void ReleaseFiles(List<string> barcodes, string basePath)
        {
            try { 
            AddBarCode(@"c:\git\Dummy.docx", "000000000001,", basePath);
            foreach (var barcode in barcodes)
            {
                File.Delete(barcode);
            }
            }
            catch
            {
                
            }
        }

        public static string AddBarCode(string docPath,string encodingText, string barcodesPath)
        {
            var tempImagefullPath = barcodesPath + Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Replace("/", "_").Replace("+", "_") + ".jpg";

            try {
                if (!docPath.Contains("Dummy")) { 
                    var barcodeText = encodingText;
                    BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
                    {
                        IncludeLabel = true,
                        Alignment = AlignmentPositions.CENTER,
                        Width = 800,
                        Height = 200,
                        RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                        BackColor = System.Drawing.Color.White,
                        ForeColor = System.Drawing.Color.Black
                    };
                    barcode.Encode(TYPE.CODE128, barcodeText);
                    barcode.SaveImage(tempImagefullPath, BarcodeLib.SaveTypes.JPG);
                    }
                using (DocX doc = DocX.Load(docPath))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {

                        System.Drawing.Image myImg = docPath.Contains("Dummy")
                            ? System.Drawing.Image.FromFile(barcodesPath + "Dummy.jpg")
                            : System.Drawing.Image.FromFile(tempImagefullPath);

                        myImg.Save(ms, myImg.RawFormat);  // Save your picture in a memory stream.
                        ms.Seek(0, SeekOrigin.Begin);

                        Novacode.Image cc = doc.AddImage(ms); // Create image.
                        Novacode.Header header_default = doc.Headers.odd;
                        Novacode.Paragraph p1 = header_default.InsertParagraph();
                        p1.Direction = Novacode.Direction.LeftToRight;
                        Novacode.Picture pic1 = cc.CreatePicture();
                        pic1.Height = 80;
                        pic1.Width = 250;
                        p1.AppendPicture(pic1);
                        doc.Save();
                    }
                }
                return tempImagefullPath;
                }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static void JoinLetters(params string[] filepaths)
        {
            if (filepaths != null && filepaths.Length > 1)

                using (WordprocessingDocument myDoc = WordprocessingDocument.Open(@filepaths[0], true))
                {
                    MainDocumentPart mainPart = myDoc.MainDocumentPart;

                    for (int i = 1; i < filepaths.Length; i++)
                    {
                        string altChunkId = "AltChunkId" + i;
                        AlternativeFormatImportPart chunk = mainPart.AddAlternativeFormatImportPart(
                            AlternativeFormatImportPartType.WordprocessingML, altChunkId);
                        using (FileStream fileStream = File.Open(@filepaths[i], FileMode.Open))
                        {
                            chunk.FeedData(fileStream);
                        }
                        DocumentFormat.OpenXml.Wordprocessing.AltChunk altChunk = new DocumentFormat.OpenXml.Wordprocessing.AltChunk();
                        altChunk.Id = altChunkId;
                        //new page, if you like it...
                        mainPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new Break() { Type = BreakValues.Page })));
                        //next document
                        mainPart.Document.Body.InsertAfter(altChunk, mainPart.Document.Body.Elements<DocumentFormat.OpenXml.Wordprocessing.Paragraph>().Last());
                    }
                    mainPart.Document.Save();
                    myDoc.Close();
                }
            for (var i = 1; i < filepaths.Length; i++)
            {
                File.Delete(filepaths[i]);
            }
        }
    }
}