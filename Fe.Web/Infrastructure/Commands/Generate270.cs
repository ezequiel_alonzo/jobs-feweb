﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Collections.Generic;
using Fe.Web.Models;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using Fe.Web.Infrastructure.Repositories.Libraries;
using Fe.Web.Infrastructure.Queries;

namespace Fe.Web.Infrastructure.Commands
{
    public class Generate270
    {
        private readonly string _path;
        private readonly IEnumerable<PatientWithDetailsModel> _patients;

        public Generate270(string path, IEnumerable<PatientWithDetailsModel> patients)
        {
            _path = path;
            _patients = patients;
        }

        public int Run()
        {
            int count = 0;

            var patientsByHospCodeQuery = (
                from patient in _patients
                group patient by patient.HospCode into patientsByHospCode
                select patientsByHospCode).ToList();

            foreach (var patientsInHospCode in patientsByHospCodeQuery)
            {
                var samplePatient = patientsInHospCode.First();

                var hospInfo = GetHospInfo(samplePatient.ClientNum);

                Directory.CreateDirectory(_path + patientsInHospCode.Key);

                foreach (var n in Enumerable.Range(0, (int) Math.Ceiling(patientsInHospCode.Count() / 999d)))
                {
                    count++;

                    var fileContent = Get270(samplePatient.ClientNum, hospInfo, patientsInHospCode.Skip(n * 999).Take(999));

                    File.WriteAllText(_path + string.Format(@"{0}\{1}_{0}_{2}{3}.x12",
                        patientsInHospCode.Key,
                        hospInfo.HospState,
                        DateTime.Now.ToString("yyyyMMddHHmmss"),
                        patientsInHospCode.Count() < 1000 ? string.Empty : "_" + (n + 1)), fileContent);
                }
            }

            return count;
        }

        public string Get270(string clientNum, HospInfo hospInfo, IEnumerable<PatientWithDetailsModel> patients)
        {
            var seCounter = 0;
            var sLine = new StringBuilder();

            var date = DateTime.Now.ToString("yyyyMMdd");
            var time = DateTime.Now.ToString("HHmm");
            var seconds = DateTime.Now.ToString("ss");

            var npi = GetNpi(clientNum);
            var caid270Vals = GetCaid270Vals(hospInfo.HospCode);
            var controlNum = GetNextControlNum().ToString();

            var hlLevel = 0;
            var hlParent = 0;

            var repSep = ExtractCSV(caid270Vals, 4);
            var componentSep = ExtractCSV(caid270Vals, 5);
            var elementSep = ExtractCSV(caid270Vals, 3);
            var isaSendID = ExtractCSV(caid270Vals, 7);
            var isaSendQual = ExtractCSV(caid270Vals, 6);
            var isaRecvID = ExtractCSV(caid270Vals, 9);
            var isaRecvQual = ExtractCSV(caid270Vals, 8);
            var segmentSep = ExtractCSV(caid270Vals, 2);
            var appSend = ExtractCSV(caid270Vals, 10);
            var appRecv = ExtractCSV(caid270Vals, 11);
            var recvName = ExtractCSV(caid270Vals, 12);
            var payorId = ExtractCSV(caid270Vals, 13);

            var BHT03 = "HFMI" + clientNum + controlNum + date + time + seconds;

            //'ISA 'Example:ISA*00*          *00*          *ZZ*300354         *ZZ*77034          *130919*1511*^*00501*000011046*1*P*:
            sLine.Append(BuildISASegment(elementSep, isaSendID, isaSendQual, isaRecvID, isaRecvQual, date, time, repSep, controlNum, componentSep, "P"));
            sLine.Append(segmentSep);

            //'GS 'Example:GS*HS*300354*77034*20130919*1511*11046*X*005010X279A1
            sLine.Append(BuildGenericSegment(elementSep, "GS", "HS", appSend, appRecv, date, time, controlNum, "X", "005010X279A1"));
            sLine.Append(segmentSep);

            //'ST 'Example:ST*270*0001*005010X279A1
            seCounter++;
            sLine.Append(BuildGenericSegment(elementSep, "ST", "270", "0001", "005010X279A1"));
            sLine.Append(segmentSep);

            //'BHT 'Example:BHT*0022*13*0001*20130919*150949
            seCounter++;
            sLine.Append(BuildGenericSegment(elementSep, "BHT", "0022", "13", BHT03, date, time + seconds));
            sLine.Append(segmentSep);

            //'Reciever Info (INFORMATION SOURCE) 'Example:HL*1**20*1~NM1*PR*2*Georgia Health Partnership*****PI*77034
            seCounter++;
            sLine.Append(BuildHLSegment(elementSep, ref hlLevel, hlParent, "20", 1));
            sLine.Append(segmentSep);

            seCounter++;
            sLine.Append(BuildGenericSegment(elementSep, "NM1", "PR", "2", recvName, "", "", "", "", "PI", payorId));
            sLine.Append(segmentSep);

            //'Sender Info (INFORMATION RECEIVER) 'Example:HL*2*1*21*1~NM1*1P*2*ST. MARY'S HOSPITAL*****XX*1871556621~N4*ATHENS*GA*306063712
            seCounter++;
            hlParent = hlLevel;
            sLine.Append(BuildHLSegment(elementSep, ref hlLevel, hlParent, "21", 1));
            sLine.Append(segmentSep);

            seCounter++;
            hlParent = hlLevel;
            sLine.Append(BuildGenericSegment(elementSep, "NM1", "1P", "2", hospInfo.HospName, "", "", "", "", "XX", npi));
            sLine.Append(segmentSep);

            if (!string.IsNullOrWhiteSpace(hospInfo.HospAddress))
            {
                seCounter++;
                sLine.Append(BuildGenericSegment(elementSep, "N3", hospInfo.HospAddress));
                sLine.Append(segmentSep);
            }

            if (!string.IsNullOrWhiteSpace(hospInfo.HospCity + hospInfo.HospState + hospInfo.HospZip))
            {
                seCounter++;
                sLine.Append(BuildGenericSegment(elementSep, "N4", hospInfo.HospCity, hospInfo.HospState, hospInfo.HospZip));
                sLine.Append(segmentSep);
            }

            foreach (var patient in patients)
            {
                foreach (var trys in Enumerable.Range(1, 8))
                {
                    var sCaidNum = patient.CaidNum ?? string.Empty;
                    var slastname = patient.LastName ?? string.Empty;
                    var sFirstName = patient.FirstName ?? string.Empty;
                    var sSex = patient.Sex == "F" || patient.Sex == "M" ? patient.Sex : string.Empty;
                    var sDOB = patient.Dob.HasValue ? patient.Dob.Value.ToString("yyyyMMdd") : string.Empty;
                    var sSSN = patient.Ssn != null && patient.Ssn.Length == 9 ? patient.Ssn : string.Empty;
                    var sDOS = patient.AdmitDate.HasValue ? patient.AdmitDate.Value.ToString("yyyyMMdd") : string.Empty;
                    var sMIS = string.Empty; //TODO: Find this out

                    if (CheckLookUpWay(hospInfo.HospState, ref sCaidNum, ref slastname, ref sFirstName, ref sSex, ref sDOB, ref sSSN, ref sMIS, trys))
                        sLine.Append(BuildSearchSegments(segmentSep, elementSep, ref seCounter, hlParent, ref hlLevel, patient.PatientNum, clientNum, trys, sCaidNum, slastname, sFirstName, sSex, sDOB, sSSN, sMIS, sDOS));
                }

            }

            //'Add SE  segment  'SE*225*0001
            seCounter++;
            sLine.Append(BuildGenericSegment(elementSep, "SE", seCounter.ToString(), "0001"));
            sLine.Append(segmentSep);

            //'Add GE  segment  'GE*1*11046
            sLine.Append(BuildGenericSegment(elementSep, "GE", "1", controlNum));
            sLine.Append(segmentSep);

            //'Add IEA segment  'IEA*1*000011046
            sLine.Append(BuildGenericSegment(elementSep, "IEA", "1", Lpad(controlNum, "0", 9)));
            sLine.Append(segmentSep);

            return sLine.ToString();
        }

        static string ExtractCSV(string csv, int index)
        {
            return csv.Split(',')[--index];
        }

        static string GetCaid270Vals(string hospCode)
        {
            var query = "SELECT CAID270VALS FROM HOSPMASTER WHERE HOSPCODE = :HOSPCODE";

            return Oracle.Query<string>(query, new { HOSPCODE = hospCode }).First();
        }

        static HospInfo GetHospInfo(string clientNum)
        {
            return new GetMasterInfo(clientNum).Run().First();
        }

        static long GetNextControlNum()
        {
            var query = "SELECT EDI_ISA_CTRL_SEQ.NEXTVAL FROM DUAL";

            return Oracle.Query<long>(query).First();
        }

        static string GetNpi(string clientNum)
        {
            var query = "SELECT NPI FROM CLIENTS WHERE CLIENTNUM = :CLIENTNUM";

            return Oracle.Query<string>(query, new { CLIENTNUM = clientNum }).First();
        }

        static string BuildGenericSegment(string elementSep, params string[] elements)
        {
            return string.Join(elementSep, elements).Trim(elementSep.First());
        }

        static string BuildHLSegment(string elementSep, ref int hlLevel, int hlParent, string sLevelCode, int child)
        {
            var rv = string.Empty;
            var sParent = string.Empty;

            //HL*1**20*1

            if (hlParent != 0)
                sParent = Trim(Str(hlParent));

            hlLevel = hlLevel + 1;

            rv = "HL" + elementSep;
            rv = rv + Trim(Str(hlLevel)) + elementSep;
            rv = rv + sParent + elementSep;
            rv = rv + sLevelCode + elementSep;
            rv = rv + Trim(Str(child));

            return rv;
        }

        static string BuildISASegment(string elementSep, string sendID, string sendQual, string recvID, string recvQual, string intDate, string intTime, string repSep, string controlnum, string compSep, string prod)
        {
            var rv = string.Empty;

            rv = "ISA";
            rv = rv + elementSep + "00" + elementSep + "          " + elementSep;
            rv = rv + "00" + elementSep + "          " + elementSep;
            rv = rv + sendQual + elementSep + Rpad(sendID, " ", 15) + elementSep;
            rv = rv + recvQual + elementSep + Rpad(recvID, " ", 15) + elementSep;
            rv = rv + Mid(intDate, 3, 6) + elementSep + intTime + elementSep;
            rv = rv + repSep + elementSep;
            rv = rv + "00501" + elementSep + Lpad(Trim(controlnum), "0", 9) + elementSep;
            rv = rv + "0" + elementSep + prod + elementSep + compSep;

            return rv;
        }


        static string BuildSearchSegments(string segmentSep, string elementSep, ref int seCounter, int hlParent, ref int hlLevel, string iPatientNum, string HospName, int trys, string sCaidNum, string slastname, string sFirstName, string sSex, string sDOB, string sSSN, string sMI, string sDOS)
        {
            var rv = string.Empty;
            var trace = string.Empty;
            var iTemp = 0;

            //'HL segment
            seCounter++;
            rv = BuildHLSegment(elementSep, ref hlLevel, hlParent, "22", 0);
            rv = rv + segmentSep;

            //'TRN segment (Trace number)
            seCounter++;
            HospName = Rpad(HospName, "0", 10);

            iTemp = HospName.Length > 0 ? 4 : 3;

            trace = iPatientNum + "." + Trim(Str(trys));
            HospName = Rpad(HospName, "0", 10);
            rv = rv + BuildGenericSegment(elementSep, "TRN", "1", trace, HospName);
            rv = rv + segmentSep;

            //'NM1 Segment (Name/ID)
            iTemp = 3;
            if (!string.IsNullOrWhiteSpace(sCaidNum))
            {
                iTemp = 10;
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(sMI))
                    iTemp = 6;
                else
                {
                    if (!string.IsNullOrWhiteSpace(sFirstName))
                        iTemp = 5;
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(slastname))
                            iTemp = 4;
                    }
                }
            }

            if (iTemp > 0)
            {
                seCounter++;
                rv = rv + BuildGenericSegment(elementSep, (new[] { "NM1", "IL", "1", slastname, sFirstName, sMI, "", "", "MI", sCaidNum }).Take(iTemp).ToArray());
                rv = rv + segmentSep;
            }

            //'REF segment (SSN)
            iTemp = 0;
            if (!string.IsNullOrWhiteSpace(sSSN) && sSSN.Length == 9)
                iTemp = 3;

            if (iTemp > 0)
            {
                seCounter++;
                rv = rv + BuildGenericSegment(elementSep, "REF", "SY", sSSN);
                rv = rv + segmentSep;
            }

            //'DMG segment (DOB/Gender)
            iTemp = 0;

            if (!string.IsNullOrWhiteSpace(sSex))
                iTemp = 4;
            else
            {
                if (!string.IsNullOrWhiteSpace(sDOB))
                    iTemp = 3;
            }
            if (iTemp > 0)
            {
                seCounter++;
                rv = rv + BuildGenericSegment(elementSep, "DMG", "D8", sDOB, sSex);
                rv = rv + segmentSep;
            }

            //'DTP segment (DOS)
            seCounter++;
            rv = rv + BuildGenericSegment(elementSep, "DTP", "291", "D8", sDOS);
            rv = rv + segmentSep;

            //'EQ segment
            seCounter++;
            rv = rv + BuildGenericSegment(elementSep, "EQ", "30");
            rv = rv + segmentSep;

            return rv;
        }

        bool CheckLookUpWay(string sState, ref string sCaidNum, ref string slastname, ref string sFirstName, ref string sSex, ref string sDOB, ref string sSSN, ref string sMI, int trys)
        {
            bool rv = false;

            switch (trys)
            {
                case 1:
                    switch (sState)
                    {
                        case "TX":
                        case "NJ":
                        case "GA":
                        case "MS":
                        case "WV":
                        case "MO":
                        case "PA":
                        case "IL":
                        case "NC":
                        case "TN":
                        case "VA":
                        case "KY":
                        case "AL":
                        case "WI":
                        case "FL":
                        case "IN":
                        case "SC":
                        case "DC":
                            rv = true;
                            break;
                        default:
                            rv = false;
                            break;
                    }
                    break;
                case 2:
                    switch (sState)
                    {
                        case "TX":
                        case "NJ":
                        case "LA":
                        case "MS":
                        case "MO":
                        case "PA":
                        case "IL":
                        case "NC":
                        case "TN":
                        case "VA":
                        case "KY":
                        case "AL":
                        case "WI":
                        case "IN":
                        case "SC":
                            rv = true;
                            break;
                        default:
                            rv = false;
                            break;
                    }
                    break;
                case 3:
                    switch (sState)
                    {
                        case "TX":
                        case "NJ":
                        case "LA":
                        case "GA":
                        case "MS":
                        case "PA":
                        case "IL":
                        case "NC":
                        case "AL":
                        case "WI":
                        case "FL":
                        case "IN":
                        case "DC":
                            rv = true;
                            break;
                        default:
                            rv = false;
                            break;
                    }
                    break;
                case 4:
                    switch (sState)
                    {
                        case "TX":
                        case "NJ":
                        case "LA":
                        case "GA":
                        case "WV":
                        case "PA":
                        case "IL":
                        case "NC":
                        case "VA":
                        case "AL":
                        case "WI":
                        case "FL":
                        case "MO":
                        case "SC":
                        case "DC":
                            rv = true;
                            break;
                        default:
                            rv = false;
                            break;
                    }
                    break;
                case 5:
                    switch (sState)
                    {
                        case "FL":
                        case "KY":
                        case "GA":
                        case "DC":
                            rv = true;
                            break;
                        default:
                            rv = false;
                            break;
                    }
                    break;
                case 6:
                    switch (sState)
                    {
                        case "IN":
                        case "TN":
                            rv = true;
                            break;
                        default:
                            rv = false;
                            break;
                    }
                    break;
                case 7:
                    switch (sState)
                    {
                        case "KY":
                            rv = true;
                            break;
                        default:
                            rv = false;
                            break;
                    }
                    break;
                case 8:
                    switch (sState)
                    {
                        case "SC":
                            rv = true;
                            break;
                        default:
                            rv = false;
                            break;
                    }
                    break;
            }

            switch (trys)
            {
                case 1:
                    if (Len(sCaidNum) > 0 && rv)
                    {
                        slastname = "";
                        sFirstName = "";
                        sSex = "";
                        sDOB = "";
                        sSSN = "";
                        sMI = "";
                    }
                    else
                    {
                        rv = false;
                    }

                    break;
                case 2:
                    if (Len(sDOB) == 8 && Trim(slastname) != "" && Trim(sFirstName) != "" && rv)
                    {
                        sCaidNum = "";
                        sSex = "";
                        sSSN = "";
                        sMI = "";
                    }
                    else
                    {
                        rv = false;
                    }

                    break;
                case 3:
                    if (Len(sSSN) == 9 && Trim(slastname) != "" && Trim(sFirstName) != "" && rv)
                    {
                        sCaidNum = "";
                        sSex = "";
                        sDOB = "";
                        sMI = "";
                    }
                    else
                    {
                        rv = false;
                    }

                    break;
                case 4:
                    if (Len(sSSN) == 9 && Len(sDOB) == 8 && rv)
                    {
                        sCaidNum = "";
                        slastname = "";
                        sFirstName = "";
                        sSex = "";
                        sMI = "";
                    }
                    else
                    {
                        rv = false;
                    }

                    break;
                case 5:
                    if (Len(sDOB) == 8 && Trim(sSex) != "" && Trim(slastname) != "" && Trim(sFirstName) != "" && rv)
                    {
                        sCaidNum = "";
                        sSSN = "";
                        sMI = "";
                    }
                    else
                    {
                        rv = false;
                    }

                    break;
                case 6:
                    if (Len(sSSN) == 9 && rv)
                    {
                        sCaidNum = "";
                        slastname = "";
                        sFirstName = "";
                        sSex = "";
                        sDOB = "";
                        sMI = "";
                    }
                    else
                    {
                        rv = false;
                    }

                    break;
                case 7:
                    if (Len(sCaidNum) > 0 && Len(sDOB) == 8 && rv)
                    {
                        slastname = "";
                        sFirstName = "";
                        sSex = "";
                        sSSN = "";
                        sMI = "";
                    }
                    else
                    {
                        rv = false;
                    }

                    break;
                case 8:
                    if (Len(sDOB) == 8 && Trim(sMI) != "" && Trim(slastname) != "" && Trim(sFirstName) != "" && rv)
                    {
                        sCaidNum = "";
                        slastname = "";
                        sFirstName = "";
                        sDOB = "";
                    }
                    else
                    {
                        rv = false;
                    }

                    break;
            }

            return rv;
        }

        static string Rpad(string value, string paddingChar, int totalWidth)
        {
            return value.PadRight(totalWidth, paddingChar.First());
        }

        static string Lpad(string value, string paddingChar, int totalWidth)
        {
            return value.PadLeft(totalWidth, paddingChar.First());
        }

        static string Mid(string value, int startIndex, int length)
        {
            return value.Substring(--startIndex, length);
        }

        static string Trim(string value)
        {
            return value.Trim();
        }

        static string Str(object value)
        {
            return value.ToString();
        }

        static int Len(string value)
        {
            return value.Length;
        }
    }
}