﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Fe.Web.Infrastructure.Common
{
    public class FileDelete
    {

        private readonly string _filelocation;

        public FileDelete(string filelocation)
        {
            _filelocation = filelocation;
        }

        public string Run()
        {

            try {
                File.Delete(_filelocation);
                return "ok";
            }
            catch {
                return null;
            }
        } 
    }
}