﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fe.Web.Infrastructure.Choppers
{
    public interface IChopper<T>
    {
        IEnumerable<T> GetResult();
        void Process();
    }
}
