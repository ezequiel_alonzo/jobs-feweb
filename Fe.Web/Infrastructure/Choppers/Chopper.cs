﻿using Fe.Web.Infrastructure.Commands;
using Fe.Web.Infrastructure.Common;
using Fe.Web.Infrastructure.Queries;
using Fe.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fe.Web.Infrastructure.Choppers
{
    public class Chopper : IChopper<PatientProfile>
    {
        protected readonly string ClientNum;
        protected readonly string FileName;
        protected readonly string UserName;
        protected readonly bool IsRecon;

        private MqHandler _mq;

        public Chopper(string fileName, string clientNum, string clientRequestId, string userName, bool isRecon)
        {
            FileName = fileName;
            ClientNum = clientNum;
            UserName = userName;
            IsRecon = isRecon;

            if (clientRequestId != null)
            {
                CreateResponseChannel(clientRequestId);
            }
        }

        public virtual IEnumerable<PatientProfile> GetResult()
        {
            throw new NotImplementedException();
        }

        public virtual void Process()
        {
            var patients = GetResult();

            foreach (var patient in patients)
            {
                try
                {
                    var result = InsertPatient(patient, UserName, IsRecon);

                    SendResponseToChannel(result);
                }
                catch (Exception ex)
                {
                }
            }
        }

        protected void CreateResponseChannel(string clientRequestId)
        {
            if (clientRequestId == null)
                throw new InvalidOperationException();

            var exchangeName = "fe.import";
            var queueName = "fe.chopper";
            var routeKey = clientRequestId;

            _mq = new MqHandler
            {
                Handler = new RpcMq(exchangeName, queueName, routeKey, MvcApplication._rmqConn),
                Exchange = exchangeName,
                Queue = queueName,
                RouteKey = queueName
            };
        }

        protected void SendResponseToChannel(string msg)
        {
            if (_mq == null) return;

            _mq.Handler.Send(msg);
        }

        protected void CloseResponseChannel()
        {
            if (_mq == null) return;

            _mq.Handler.Close();
        }

        private string InsertPatient(PatientProfile patient, string userName, bool isRecon)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(patient.HospAcct))
                {
                    return JsonConvert.SerializeObject(new { status = "ERROR" });
                }

                if (CheckExistingPatient(patient) && !isRecon)
                {
                    return JsonConvert.SerializeObject(new
                    {
                        hospAcct = patient.HospAcct,
                        status = "DUPLICATE"
                    });
                }

                if (isRecon)
                {
                    if (!String.IsNullOrEmpty(GetPatientNum(patient))) {
                        patient.PatientNum = GetPatientNum(patient);
                        var command = new ImportMergePatientRecon(patient);
                        command.Run();
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            hospAcct = patient.HospAcct,
                            status = "DUPLICATE"
                        });
                    }
                }
                else
                {
                    var command = new ImportInsertPatient(patient, userName);
                    command.Run();
                }

                return JsonConvert.SerializeObject(new
                {
                    hospAcct = patient.HospAcct,
                    status = "SUCCESS"
                });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new
                {
                    hospAcct = patient.HospAcct,
                    status = "ERROR",
                    message = ex.Message
                });
            }
        }

        private static bool CheckExistingPatient(PatientProfile patient)
        {
            var command = new CheckPatientExists(patient.HospAcct, patient.ClientNum);

            return command.Run();
        }

        private static string GetPatientNum(PatientProfile patient)
        {
            var command = new CheckPatientExists(patient.HospAcct, patient.ClientNum);

            return command.GetPatientNum();
        }
    }
}
