﻿using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Fe.Web.Infrastructure.Choppers
{
    public class Chopper210 : CsvChopper
    {
        public Chopper210(string fileName, string clientNum, string clientRequestId,string userName, bool isRecon) 
            : base(fileName, clientNum, clientRequestId, userName: userName,isRecon: isRecon, separator: '\t') 
        {
            MappingExceptions.Add("PatientNum", FieldMapper.Create("AcctID"));
            MappingExceptions.Add("HospAcct", FieldMapper.Create("ClientRef1"));
            MappingExceptions.Add("Ssn", FunctionMapper.Create(row => Truncate(row["SocialSec"].ToString(), 11)));
            MappingExceptions.Add("Dob", FieldMapper.Create("DateOfBirth"));
            MappingExceptions.Add("Sex", FieldMapper.Create("Gender"));
            MappingExceptions.Add("FirstName", FunctionMapper.Create(row => string.Format("{0} {1}", row["FirstName"], row["MiddleName"]).Trim()));
            MappingExceptions.Add("PatPhone", FieldMapper.Create("Phone1"));
            MappingExceptions.Add("PatAddress", FunctionMapper.Create(row => Truncate(row["Address1"].ToString(), 38)));
            MappingExceptions.Add("PatCity", FieldMapper.Create("City"));
            MappingExceptions.Add("PatState", FieldMapper.Create("State"));
            MappingExceptions.Add("PatZip", FieldMapper.Create("Zip"));
            MappingExceptions.Add("EmpName", FieldMapper.Create("EmployerName"));
            MappingExceptions.Add("EmpAddress", FieldMapper.Create("EmpAddress1"));
            MappingExceptions.Add("RespName", FunctionMapper.Create(row => string.Format("{0} {1}", row["RPFirstName"], row["RPMiddleName"]).Trim()));
            MappingExceptions.Add("RespPhone", FieldMapper.Create("RPPhone1"));
            MappingExceptions.Add("RespAddress", FieldMapper.Create("RPAddress1"));
            MappingExceptions.Add("RespCity", FieldMapper.Create("RPCity"));
            MappingExceptions.Add("RespState", FieldMapper.Create("RPState"));
            MappingExceptions.Add("RespZip", FieldMapper.Create("RPZip"));
            MappingExceptions.Add("PlaceDate", FieldMapper.Create("PlacementDate"));
            MappingExceptions.Add("CurrBalance", FieldMapper.Create("BalanceDue"));
            MappingExceptions.Add("RespSsn", FieldMapper.Create("RPSocialSec"));
            MappingExceptions.Add("PatAddress2", FieldMapper.Create("Address2"));
            MappingExceptions.Add("PaymentAmount", FieldMapper.Create("LastPayAmt"));
            MappingExceptions.Add("PaymentLastDate", FieldMapper.Create("LastPayDate"));
        }

        public static string Truncate(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }
}