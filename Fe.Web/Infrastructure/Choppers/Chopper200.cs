﻿using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Fe.Web.Infrastructure.Choppers
{
    public class Chopper200 : CsvChopper
    {
        public Chopper200(string fileName, string clientNum, string clientRequestId, string userName,bool isRecon)
            : base(fileName, clientNum, clientRequestId, userName: userName, isRecon: isRecon, separator: '|') 
        {
            MappingExceptions.Add("HospAcct", IndexMapper.Create(0));
            MappingExceptions.Add("Mrn", IndexMapper.Create(1));
            MappingExceptions.Add("LastName", IndexMapper.Create(2));
            MappingExceptions.Add("FirstName", IndexMapper.Create(3));
            MappingExceptions.Add("Dob", IndexMapper.Create(5));
            MappingExceptions.Add("Ssn", IndexMapper.Create(6));
            MappingExceptions.Add("AdmitDate", IndexMapper.Create(7));
            MappingExceptions.Add("DischDate", IndexMapper.Create(8));
            MappingExceptions.Add("Sex", IndexMapper.Create(9));
            MappingExceptions.Add("RespName", IndexMapper.Create(11));
            MappingExceptions.Add("PatAddress", IndexMapper.Create(13));
            MappingExceptions.Add("PatAddress2", IndexMapper.Create(14));
            MappingExceptions.Add("PatCity", IndexMapper.Create(15));
            MappingExceptions.Add("PatState", IndexMapper.Create(16));
            MappingExceptions.Add("PatZip", IndexMapper.Create(18));
            MappingExceptions.Add("PatPhone", IndexMapper.Create(20));
            MappingExceptions.Add("CurrBalance", IndexMapper.Create(22));
            MappingExceptions.Add("PlaceAmount", IndexMapper.Create(22));
            MappingExceptions.Add("InsCode", IndexMapper.Create(23));
        }
    }
}