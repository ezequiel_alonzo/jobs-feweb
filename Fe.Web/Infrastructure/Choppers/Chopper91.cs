﻿using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Fe.Web.Infrastructure.Choppers
{
    public class Chopper91 : SpacedChopper
    {
        public Chopper91(string fileName, string clientNum, string clientRequestId,string userName, bool isRecon)
            : base(fileName, clientNum, clientRequestId, userName: userName, isRecon: isRecon, separator: ',', containsHeaders : false)
        {
            
            MappingExceptions.Add("HospAcct", IndexMapper.Create(1));
            MappingExceptions.Add("CurrBalance", IndexMapper.Create(3));
            MappingExceptions.Add("mrn", IndexMapper.Create(2));

    }

        public static string Truncate(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }
}