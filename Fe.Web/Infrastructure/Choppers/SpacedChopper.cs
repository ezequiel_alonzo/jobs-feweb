﻿using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Fe.Web.Infrastructure.Choppers
{
    public class SpacedChopper : Chopper
    {
        public Dictionary<string, ICsvChopperMapper> MappingExceptions = new Dictionary<string, ICsvChopperMapper>(StringComparer.OrdinalIgnoreCase);

        private readonly string[] _lines;
        private readonly bool _containsHeaders;
        private readonly char _separator;

        private static RegexOptions options = RegexOptions.None;
        private readonly Regex spacedRegex = new Regex("[ ]{1,}", options);


        protected Func<string, string> TrimFunction = value => value.Trim();

        protected SpacedChopper(string fileName, string clientNum, string clientRequestId, string userName, bool isRecon, char separator = ',', bool containsHeaders = false)
            : base(fileName, clientNum, clientRequestId, userName, isRecon)
        {
            _separator = separator;
            _containsHeaders = containsHeaders;

            _lines = File
                .ReadAllText(fileName)
                .Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);

            _lines = _lines.Select(line => spacedRegex.Replace(line, ",")).ToArray();

            if (!_lines.Any())
                throw new InvalidOperationException("File not loaded or empty");
        }

        private IEnumerable<string> GetColumnHeaders()
        {
            var firstLine = _lines.First().Split(_separator);

            return !_containsHeaders
                ? EnumerateColumns(firstLine)
                : firstLine;
        }

        private IEnumerable<string> EnumerateColumns(IEnumerable<string> values)
        {

            var aa = Enumerable
                .Range(0, values.Count())
                .Select(n => string.Format("Column{0}", n));

            return aa;
        }

        protected virtual DataTable GetDataTable()
        {
            var dataTable = new DataTable();

            foreach (var columnHeader in GetColumnHeaders())
                dataTable.Columns.Add(columnHeader.Trim());

            foreach (var line in _lines.Skip(_containsHeaders ? 1 : 0))
            {
                var row = dataTable.NewRow();
                if (line.Split(_separator).Count() == GetColumnHeaders().Count())
                {
                    row.ItemArray = line.Split(_separator)
                        .Select(TrimFunction)
                        .ToArray();

                    dataTable.Rows.Add(row);

                }
            }

            return dataTable;
        }

        public override IEnumerable<PatientProfile> GetResult()
        {
            var dataTable = GetDataTable();

            var patients = new List<PatientProfile>();

            var columns = dataTable.Columns
                .Cast<DataColumn>()
                .Select(column => column.ColumnName.ToLowerInvariant());

            var properties = typeof(PatientProfile)
                .GetProperties()
                .Where(prop => columns.Contains(prop.Name, StringComparer.OrdinalIgnoreCase) || MappingExceptions.Keys.Contains(prop.Name))
                .ToArray();

            foreach (DataRow row in dataTable.Rows)
            {
                var patient = new PatientProfile
                {
                    ClientNum = ClientNum
                };

                foreach (var prop in properties)
                {
                    try
                    {
                        var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;

                        ICsvChopperMapper mapper;

                        var rowValue = !MappingExceptions.TryGetValue(prop.Name, out mapper)
                            ? row[columns.First(column => string.Equals(column, prop.Name, StringComparison.OrdinalIgnoreCase))].ToString()
                            : mapper.GetValue(row);

                        prop.SetValue(patient, ConvertProperty(type, rowValue), null);
                    }
                    catch
                    {
                        continue;
                    }
                }

                patients.Add(patient);
            }

            return patients;
        }

        private object ConvertProperty(Type type, string rowValue)
        {
            object value = null;

            switch (type.Name)
            {
                case "DateTime":
                    DateTime dateTime;
                    if (DateTime.TryParse(rowValue, out dateTime))
                        value = dateTime;

                    break;

                case "Double":
                    Double doubleNumber;
                    if (Double.TryParse(rowValue, out doubleNumber))
                        value = doubleNumber;

                    break;

                case "Int32":
                    Int32 intNumber;
                    if (Int32.TryParse(rowValue, out intNumber))
                        value = intNumber;

                    break;

                default:
                    value = rowValue;
                    break;
            }

            return value;
        }
    }
}