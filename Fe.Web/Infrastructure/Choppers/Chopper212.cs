﻿using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Fe.Web.Infrastructure.Choppers
{
    public class Chopper212 : CsvChopper
    {
        public Chopper212(string fileName, string clientNum, string clientRequestId,string userName, bool isRecon)
            : base(fileName, clientNum, clientRequestId, userName: userName, isRecon: isRecon, separator: '\t')
        {
            TrimFunction = value => value.Trim('"');

            MappingExceptions.Add("HospAcct", FieldMapper.Create("AcctNo"));
            MappingExceptions.Add("Ssn", FunctionMapper.Create(row => Truncate(row["PSSN"].ToString(), 11)));
            MappingExceptions.Add("Dob", FieldMapper.Create("PDOB"));
            MappingExceptions.Add("Sex", FieldMapper.Create("PGEN"));
            MappingExceptions.Add("AdmitDate", FieldMapper.Create("AdmDate"));
            MappingExceptions.Add("LastName", IndexMapper.Create(1));
            MappingExceptions.Add("FirstName", FunctionMapper.Create(row => string.Format("{0} {1}", row[2], row[3]).Trim()));
            MappingExceptions.Add("PatPhone", FieldMapper.Create("PHome"));
            MappingExceptions.Add("PatAddress", FunctionMapper.Create(row => string.Format("{0} {1}", row["PAdd1"], row["PAdd12"]).Trim()));
            MappingExceptions.Add("PatCity", FieldMapper.Create("PCity"));
            MappingExceptions.Add("PatState", FieldMapper.Create("PST"));
            MappingExceptions.Add("PatZip", FieldMapper.Create("PZip"));
            MappingExceptions.Add("PlaceAmount", FieldMapper.Create("TotChgs"));
            MappingExceptions.Add("DischDate", FieldMapper.Create("TotChgs"));
            MappingExceptions.Add("FinClass", FieldMapper.Create("FinanCl"));
            MappingExceptions.Add("RespName", FunctionMapper.Create(row => string.Format("{0} {1} {2}", row["GuarFirstNam"], row["GuarLastName"],row["GuarMiddle"]).Trim()));
            MappingExceptions.Add("RespPhone", FieldMapper.Create("GuarPhone"));
            MappingExceptions.Add("RespAddress", FunctionMapper.Create(row => string.Format("{0} {1}", row["GuarAdd1"], row["GuarAdd2"]).Trim()));
            MappingExceptions.Add("RespCity", FieldMapper.Create("GuarCity"));
            MappingExceptions.Add("RespState", FieldMapper.Create("GuarSt"));
            MappingExceptions.Add("RespZip", FieldMapper.Create("GuarZip"));
            MappingExceptions.Add("PlaceDate", FieldMapper.Create("PlacementDate"));
            MappingExceptions.Add("CurrBalance", FieldMapper.Create("CurBal"));
            MappingExceptions.Add("RespSsn", FieldMapper.Create("GuarSSN"));
        }

        public static string Truncate(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }
}