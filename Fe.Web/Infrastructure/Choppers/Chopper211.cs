﻿using Fe.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
namespace Fe.Web.Infrastructure.Choppers
{
    public class Chopper211 : CsvChopper
    {

        public Chopper211(string fileName, string clientNum, string clientRequestId, string userName, bool isRecon)
            : base(fileName, clientNum, clientRequestId, userName: userName, isRecon: isRecon, separator: '\t', containsHeaders: false)
        {

            MappingExceptions.Add("HospAcct", IndexMapper.Create(0));
            MappingExceptions.Add("Ssn", FunctionMapper.Create(row => Truncate(row[11].ToString(), 11)));
            MappingExceptions.Add("Dob", FunctionMapper.Create(row => FormatDate(row[12])));
            MappingExceptions.Add("Sex", IndexMapper.Create(13));
            MappingExceptions.Add("FirstName", FunctionMapper.Create(row => string.Format("{0}, {1}", row[2], row[3]).Trim()));
            MappingExceptions.Add("LastName", IndexMapper.Create(1));
            MappingExceptions.Add("PatPhone", IndexMapper.Create(9));
            MappingExceptions.Add("PatAddress", FunctionMapper.Create(row => string.Format("{0}, {1}", row[4], row[5]).Trim()));
            MappingExceptions.Add("PatCity", IndexMapper.Create(6));
            MappingExceptions.Add("PatState", IndexMapper.Create(7));
            MappingExceptions.Add("PatZip", IndexMapper.Create(8));
            MappingExceptions.Add("AdmitDate", FunctionMapper.Create(row => FormatDate(row[14])));
            MappingExceptions.Add("DischDate", IndexMapper.Create(15));
            MappingExceptions.Add("PlaceAmount", IndexMapper.Create(16));
            MappingExceptions.Add("FinClass", IndexMapper.Create(21));
            MappingExceptions.Add("RespName", FunctionMapper.Create(row => string.Format("{0}, {1}", row[22], row[23]).Trim()));
            MappingExceptions.Add("RespAddress", FunctionMapper.Create(row => string.Format("{0} {1}", row[25], row[26]).Trim()));
            MappingExceptions.Add("RespPhone", IndexMapper.Create(30));
            MappingExceptions.Add("RespCity", IndexMapper.Create(27));
            MappingExceptions.Add("RespState", IndexMapper.Create(28));
            MappingExceptions.Add("RespZip", IndexMapper.Create(29));
            MappingExceptions.Add("RespSsn", IndexMapper.Create(32));
            MappingExceptions.Add("ClientNum", FunctionMapper.Create(row => GetClientNumFHR(row[33])));
        }

        public static string Truncate(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string GetClientNumFHR(Object serviceCode)
        {
            switch ((serviceCode ?? string.Empty).ToString())
            {
                case "Inpatient":
                    return "90413B";
                case "Emergency":
                    return "90413D";
                case "Clinic ":
                    return "90413C";
                default:
                    return "90413B";
            }
        }

        public static string FormatDate(Object fileDate)
        {
            DateTime date;
            return DateTime.TryParseExact(fileDate.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date)
                ? date.ToString()
                : string.Empty;
        }
    }
}