﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Fe.Web.Infrastructure.Choppers
{
    public interface ICsvChopperMapper
    {
        string GetValue(DataRow row);
    }
    
    public class FieldMapper : ICsvChopperMapper
    {
        string _field;

        private FieldMapper(string field) 
        {
            _field = field;
        }

        public static FieldMapper Create(string field) 
        {
            return new FieldMapper(field);
        }

        public string GetValue(DataRow row)
        {
            return row[_field].ToString();
        }
    }

    public class FunctionMapper : ICsvChopperMapper
    {
        Func<DataRow, string> _func;

        private FunctionMapper(Func<DataRow, string> func) 
        {
            _func = func;
        }

        public static FunctionMapper Create(Func<DataRow, string> func)
        {
            return new FunctionMapper(func);
        }

        public string GetValue(DataRow row)
        {
            return _func(row);
        }
    }

    public class IndexMapper : ICsvChopperMapper
    {
        int _index;

        private IndexMapper(int index)
        {
            _index = index;
        }

        public static IndexMapper Create(int index)
        {
            return new IndexMapper(index);
        }

        public string GetValue(DataRow row)
        {
            return row.ItemArray[_index].ToString();
        }
    }
}
