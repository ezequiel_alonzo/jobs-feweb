﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Fe.Web.Infrastructure.Spitfire
{

    public class SpResponse
    {
        public int ID { get; set; }
        public DateTime PostTime { get; set; }
        public string Result { get; set; }
    }

    public class SpDefaultRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class CreateDialingListUsingName : SpDefaultRequest
    {
        public string listName { get; set; }
    }

    public class GetAllCampaignsResponse
    {
        public List<SpCampaign> CampaignCollection { get; set; }
        public List<ClientItem> ClientItemCollection { get; set; }
        public List<CustomField> CustomFieldCollection { get; set; }
        public List<DialingList> DialingListCollection { get; set; }
        public int Id { get; set; }
        public DateTime PostTime { get; set; }
        public string Result { get; set; }
    }

    public class SpCampaign
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class CustomField
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class DialingList
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class ClientItem
    {
        public string Address { get; set; }
        public long CallLength { get; set; }
        public DateTime? CallStartTime { get; set; }
        public string CampaignName { get; set; }
        public string Cellphone { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public string Country { get; set; }
        public string CustomerName { get; set; }
        public string Disposition { get; set; }
        public string EMail { get; set; }
        public string Faxphone { get; set; }
        public List<Field> Fields { get; set; }
        public string FirstName { get; set; }
        public string Homephone { get; set; }
        public string IsHit { get; set; }
        public string LastName { get; set; }
        public string ListName { get; set; }
        public string Notes { get; set; }
        public string OutboundCallerId { get; set; }
        public string SecVoicephone { get; set; }
        public string State { get; set; }
        public string UserName { get; set; }
        public string Voicephone { get; set; }
        public string Website { get; set; }
        public long WrapUpTime { get; set; }
        public string Zipcode { get; set; }
    }

    public class LiveLead
    {
        public string username { get; set; }
        public string password { get; set; }
        public string campaign { get; set; }
        public string name { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string company { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string country { get; set; }
        public string voicephone { get; set; }
        public string listname { get; set; }
        public string xmldata { get; set; }
    }

    public class Field
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class Lead
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Campaign { get; set; }
        public string ListName { get; set; }
        public string VoicePhone { get; set; }
        //rest are demographic info
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string SecVoicePhone { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string FaxPhone { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public string XmlData { get; set; }
        public string Notes { get; set; }
    }
}