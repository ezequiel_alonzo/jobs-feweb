﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace Fe.Web.Infrastructure.Spitfire
{
    public class SpitfireConnect
    {
        public const string ApiUrl = @"http://MNED2K12-SPITFIRE.ad.hfmi.org:8090/SpitfireAPI/";

        public GetAllCampaignsResponse GetAllCampaigns()
        {
            string uri = "GetAllCampaigns";
            var req = new SpDefaultRequest { Username = "API@SpitFire.com", Password = "SpitFireAPI" };
            // var client = new RestClient();
            var c = new RestClient(ApiUrl);
            var r = new RestRequest(uri, Method.GET);

            r.AddObject(req);

            var resp = c.Execute<GetAllCampaignsResponse>(r).Data;

            return resp;
        }

        public GetAllCampaignsResponse GetAllLists()
        {
            var uri = "GetAllLists";
            var req = new SpDefaultRequest { Username = "API@SpitFire.com", Password = "SpitFireAPI" };
            // var client = new RestClient();
            var c = new RestClient(ApiUrl);
            var r = new RestRequest(uri, Method.GET);

            r.AddObject(req);

            var resp = c.Execute<GetAllCampaignsResponse>(r).Data;

            return resp;

        }

        public List<CustomField> GetAllCustomFields()
        {
            var uri = "GetAllCustomFields";
            var req = new SpDefaultRequest { Username = "API@SpitFire.com", Password = "SpitFireAPI" };
            // var client = new RestClient();
            var c = new RestClient(ApiUrl);
            var r = new RestRequest(uri, Method.GET);

            r.AddObject(req);

            var resp = c.Execute<GetAllCampaignsResponse>(r).Data;

            return resp.CustomFieldCollection;

        }

        public GetAllCampaignsResponse CreateNewListUsingName(string listName)
        {
            var uri = "CreateDialingListUsingListName";
            var req = new CreateDialingListUsingName
            {
                Username = "API@SpitFire.com",
                Password = "SpitFireAPI",
                listName = listName
            };

            // var client = new RestClient();
            var c = new RestClient(ApiUrl);
            var r = new RestRequest(uri, Method.GET);

            r.AddObject(req);

            var resp = c.Execute<GetAllCampaignsResponse>(r).Data;

            return resp;
        }

        public GetAllCampaignsResponse DeleteDialingListUsingName(string listName)
        {
            var uri = "DeleteDialingListUsingListName";
            var req = new CreateDialingListUsingName
            {
                Username = "API@SpitFire.com",
                Password = "SpitFireAPI",
                listName = listName
            };

            // var client = new RestClient();
            var c = new RestClient(ApiUrl);
            var r = new RestRequest(uri, Method.GET);

            r.AddObject(req);

            var resp = c.Execute<GetAllCampaignsResponse>(r).Data;

            return resp;
        }

        public GetAllCampaignsResponse DeleteLeadsUsingListName(string listName)
        {
            var uri = "DeleteLeadsUsingListName";
            var req = new CreateDialingListUsingName
            {
                Username = "API@SpitFire.com",
                Password = "SpitFireAPI",
                listName = listName
            };

            // var client = new RestClient();
            var c = new RestClient(ApiUrl);
            var r = new RestRequest(uri, Method.GET);



            r.AddObject(req);

            var resp = c.Execute<GetAllCampaignsResponse>(r).Data;

            return resp;
        }

        public GetAllCampaignsResponse PostLiveLead(LiveLead lead)
        {
            var uri = "LiveLeadInsert";
            var c = new RestClient(ApiUrl);
            var r = new RestRequest(uri, Method.GET) { RequestFormat = DataFormat.Json };
            //r.AddQueryParameter("UserName", "API@SpitFire.com");

            //            r.AddQueryParameter("Password", "SpitFireAPI");
            r.AddObject(lead);

            var resp = c.Execute<GetAllCampaignsResponse>(r).Data;

            return resp;
        }

        public GetAllCampaignsResponse InsertLeadUsingListName(LiveLead lead)
        {
            var uri = "LiveLeadInsertUsingListName";
            var c = new RestClient(ApiUrl);
            var r = new RestRequest(uri, Method.GET) { RequestFormat = DataFormat.Json };

            r.AddObject(lead);

            var resp = c.Execute<GetAllCampaignsResponse>(r).Data;

            return resp;
        }
    }

}