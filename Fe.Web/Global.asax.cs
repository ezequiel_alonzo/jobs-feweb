﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Configuration;
using RabbitMQ.Client;
using Utils = Fe.Web.Helpers.Utils;

namespace Fe.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static IConnection _rmqConn;
        private bool _disposed;

        protected void Application_Start()
        {
            Telerik.Reporting.Services.WebApi.ReportsControllerConfiguration.RegisterRoutes(System.Web.Http.GlobalConfiguration.Configuration);
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure();
            CreateRabbitMqConnection();
        }

        private static void CreateRabbitMqConnection()
        {
            var hName = ConfigurationManager.AppSettings["RpcMqHost"];
            if (Utils.IsNull(hName)) return;

            var tmpPort = ConfigurationManager.AppSettings["RpcMqPort"];
            if (Utils.IsNull(tmpPort)) return;

            int hPort;

            if (!Int32.TryParse(tmpPort, out hPort)) return;

            var factory = new ConnectionFactory() { HostName = hName, Port = hPort };
            _rmqConn = factory.CreateConnection();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                _rmqConn.Close();
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~MvcApplication()
        {
            Dispose(false);
        }
    }
}
