﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fe.Web.Models
{
    public class ClientInfo{
        public string Clientnum { get; set; }
        public string Phyname { get; set; }
        public string Phyaddress { get; set; }
        public string Phycity { get; set; }
        public string Phystate { get; set; }
        public string Phyzip { get; set; }
        public string Phycontact { get; set; }
        public string Phyphone { get; set; }
        public string Phytitle { get; set; }
        public string Rptname { get; set; }
        public string Rptaddress { get; set; }
        public string Rptcity { get; set; }
        public string Rptstate { get; set; }
        public string Rptzip { get; set; }
        public string Rptcontact { get; set; }
        public string Rptphone { get; set; }
        public string Rpttitle { get; set; }
        public string Billname { get; set; }
        public string Billaddress { get; set; }
        public string Billcity { get; set; }
        public string Billstate { get; set; }
        public string Billzip { get; set; }
        public string Billcontact { get; set; }
        public string Billphone { get; set; }
        public string Billtitle { get; set; }
        public string Providernum { get; set; }
        public string Minimum { get; set; }
        public string Cap { get; set; }
        public double? Continhouse { get; set; }
        public double? Contdschrg { get; set; }
        public string Flatrate { get; set; }
        public string Pgm { get; set; }
        public string Importchopper { get; set; }
        public string Inactive { get; set; }
        public string Optmevs { get; set; }
        public string Statecode { get; set; }
        public string Patscreenopt { get; set; }
        public string Regioncode { get; set; }
        public string Userrequired { get; set; }
        public string Sortgroup { get; set; }
        public string Formflags { get; set; }
        public string Formsorder { get; set; }
        public string Cbdaysadd { get; set; }
        public string Comments { get; set; }
        public string Tpg { get; set; }
        public string Serialnum { get; set; }
        public string Applid { get; set; }
        public string Mastate { get; set; }
        public string Hosppgm { get; set; }
        public string Hosptimeout { get; set; }
        public string Phonemsgs { get; set; }
        public string Keproclient { get; set; }
        public string Campuscode { get; set; }
        public string Touchstarbase { get; set; }
        public string Starautoclose { get; set; }
        public string Acctnolength { get; set; }
        public string Importlimit { get; set; }
        public string Clipboardchopper { get; set; }
        public string Basebillamt { get; set; }
        public string Testclient { get; set; }
        public string Altletters { get; set; }
        public string Budget { get; set; }
        public string Chain { get; set; }
        public string Hospname { get; set; }
        public string Hospid { get; set; }
        public string Basebudget { get; set; }
        public string Pdforder { get; set; }
        public string Pt_custid { get; set; }
        public string Pt_description { get; set; }
        public string Hospnamefull { get; set; }
        public string Name1 { get; set; }
        public string Title1 { get; set; }
        public string Email1 { get; set; }
        public string Name2 { get; set; }
        public string Title2 { get; set; }
        public string Email2 { get; set; }
        public string Name3 { get; set; }
        public string Title3 { get; set; }
        public string Email3 { get; set; }
        public string Restricted { get; set; }
        public string Clienttype { get; set; }
        public string Commissionovr { get; set; }
        public string Noletterq { get; set; }
        public string Defssconfig { get; set; }
        public string Startdate { get; set; }
        public string Hospcode { get; set; }
        public string Npi { get; set; }
        public string Taxatomynbr { get; set; }
        public string Socsvcurl { get; set; }
        public string Socsvcfacility { get; set; }
        public string Socsvcusername { get; set; }
        public string Socsvcpassword { get; set; }
        public string Recon { get; set; }
        public string Reconquestion { get; set; }
        public string Ltr1q { get; set; }
        public string Ltr2q { get; set; }
        public string Ltr3q { get; set; }
        public string Actioncodes { get; set; }
        public string Bitflag { get; set; }

    }

    public class ClientsCallCounter
    {
        public string Campaign { get; set; }
        public int? Numleft { get; set; }
        public string Time { get; set; }
       
    }
}