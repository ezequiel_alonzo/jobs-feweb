﻿using System;
namespace Fe.Web.Models
{
    internal class PostingModel
    {
        public string PstCode { get; set; }
        public string PstRemitDate { get; set; }
        public string PstPostDate { get; set; }
        public string PstAmount { get; set; }
        public string PstCommish { get; set; }
        public string PstDescription { get; set; }
        public string PstUserName { get; set; }
        public string PstHfmiRep { get; set; }
        public int LineNum { get; set; }
        public string PatientNum { get; set; }
    }

    public class PostingRequestModel
    {
        public string TemplateName { get; set; }
        public string PatientNum { get; set; }
        public string SubscriberNum { get; set; }
        public string ClaimNum { get; set; }
        public string HospAcct { get; set; }
        public string Mrn { get; set; }
        public string MedicaidNum { get; set; }
        public string PatFirstName { get; set; }
        public string PatLastName { get; set; }
        public string PatAddress { get; set; }
        public string PatCity { get; set; }
        public string PatState { get; set; }
        public string PatZip { get; set; }
        public string PatDob { get; set; }
        public string PatPhone { get; set; }
        public string PatSsn { get; set; }
    }

    public class PostingInsertModel
    {
        public string PatientNum { get; set; }
        public int LineNum { get; set; }
        public string UserName { get; set; }
        public DateTime? RemitDate { get; set; }
        public DateTime? PostingDate { get; set; }
        public double? Amount { get; set; }
        public string Type { get; set; }
        public double AgencyComm { get; set; }
        public string HfmiRep { get; set; }
        public double HfmiComm { get; set; }
        public string ClientNum { get; set; }
    }

    public class PostingDeleteModel
    {
        public string PatientNum { get; set; }
        public string LineNum { get; set; }
    }
}