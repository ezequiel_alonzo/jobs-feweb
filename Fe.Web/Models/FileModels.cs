﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fe.Web.Models
{
    public class FileListRequest
    {
        public FileListRequest(string ex,string bf,string us) {
            Extention = ex;
            BaseFolder = bf;
            UserName = us;
        }
        public string Extention { get; set; }
        public string BaseFolder { get; set; }
        public string UserName { get; set; }
    }

    public class FileListRequestT
    {
        
        public string Extention { get; set; }
        public string BaseFolder { get; set; }
        public string UserName { get; set; }
    }
    public class FileModels
    {

    }
}