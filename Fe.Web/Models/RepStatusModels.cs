﻿using System;

namespace Fe.Web.Models
{
    public class RepStatusModel
    {
        public bool HasViewed { get; set; }
        public string PatientName { get; set; }
        public string PatientNumber { get; set; }
        public string SubscriberNumber { get; set; }
        public string Status { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? CallBackDate { get; set; }
        public DateTime? LastChanged { get; set; }
        public double AmountPaid { get; set; }
        public int ClaimCount { get; set; }
    }

    public class RepStatusModelHFMI
    {
        public bool HasViewed { get; set; }
        public string HospAlert { get; set; }
        public string PatientName { get; set; }
        public string PatientNumber { get; set; }
        public string SubscriberNumber { get; set; }
        public string Status { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? CallBackDate { get; set; }
        public DateTime? LastChanged { get; set; }
        public DateTime? CbDate { get; set; }
        public double CurrentBalance { get; set; }
        public DateTime? Admitted { get; set; }
        public int CurrentStatus { get; set; }
        public string FinClass { get; set; }
        public string CbUser { get; set; }
        public string MGR { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? DischargedDate { get; set; }
        public double? PlaceAmount { get; set; }
        public string InsCode { get; set; }
        public string QColorCode { get; set; }
        public string PrevFinClass { get; set; }
        public string Qltr { get; set; }
        public DateTime? QLtrPrint { get; set; }
        public string ConnanceID { get; set; }
        public DateTime? ApplicationDate{ get; set; }
        public double PaymentAmount { get; set; }
        public DateTime? PaymentStartDate { get; set; }
        public DateTime? PaymentLastDate { get; set; }
        public string ClientNum { get; set; }
}
    
    public class RepStatusClaimDetailModelHFMI
    {
        public string SubscriberNumber { get; set; }
        public int ClaimCount { get; set; }
        public double AmountPaid { get; set; }
        public double CurrentBalance { get; set; }
        public DateTime? Admitted { get; set; }
        public string CurrentStatus { get; set; }
        public string FinClass { get; set; }
        public string CbUser { get; set; }
        public DateTime? CbDate { get; set; }
        public string MGR { get; set; }
        public string Hosp { get; set; }
        public string letter { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? DischargedDate { get; set; }
        public double? PlaceAmount { get; set; }
}
    public class RepStatusClaimDetailModel
    {
        public string SubscriberNumber { get; set; }
        public int ClaimCount { get; set; }
        public double AmountPaid { get; set; }

    }

    public class RepStatusRequestModel
    {
        public string TemplateName { get; set; }
        public string ClientNumber { get; set; }
    }
}
