﻿using System;

namespace Fe.Web.Models
{

    public class ClaimModel
    {
        public string ClientName { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime? DateOfInjury { get; set; }
        public DateTime? DateOfService { get; set; }
        public string BillType { get; set; }
        public string BillNumber { get; set; }
        public int? LineNumber { get; set; }
        public string NdcNumber { get; set; }
        public string ServiceDescription { get; set; }
        public int Units { get; set; }
        public double AmountBilled { get; set; }
        public double FsReduction { get; set; }
        public double AuditReduction { get; set; }
        public double PpoReduction { get; set; }
        public double AmountPaid { get; set; }
        public DateTime? PrintDate { get; set; }
        public string Network { get; set; }
        public string ProviderTin { get; set; }
        public string ProviderNpi { get; set; }
        public string ProviderName { get; set; }
        public string ProviderAddress { get; set; }
        public string ProviderCity { get; set; }
        public string ProviderState { get; set; }
        public string ProviderZip { get; set; }
        public string PrescriberName { get; set; }
        public string PrescriberDeaNumber { get; set; }
    }

    public class ClaimRequestModel
    {
        public string TemplateName { get; set; }
        public string PatientNum { get; set; }
        public string SubscriberNum { get; set; }
        public string ClaimNum { get; set; }
        public string HospAcct { get; set; }
        public string Mrn { get; set; }
        public string MedicaidNum { get; set; }
        public string PatFirstName { get; set; }
        public string PatLastName { get; set; }
        public string PatAddress { get; set; }
        public string PatCity { get; set; }
        public string PatState { get; set; }
        public string PatZip { get; set; }
        public string PatDob { get; set; }
        public string PatPhone { get; set; }
        public string PatSsn { get; set; }
    }

}