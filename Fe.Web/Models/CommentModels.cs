﻿using System;
using System.Collections.Generic;
namespace Fe.Web.Models
{
    public class CommentModel
    {
        public string PatientNum { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
        public int ActionCode { get; set; }
        public int LineNum { get; set; }
        public string UserName { get; set; }
    }

    public class CommentRequestModel
    {
        public string TemplateName { get; set; }
        public string PatientNum { get; set; }
        public string SubscriberNum { get; set; }
        public string ClaimNum { get; set; }
        public string HospAcct { get; set; }
        public string Mrn { get; set; }
        public string MedicaidNum { get; set; }
        public string PatFirstName { get; set; }
        public string PatLastName { get; set; }
        public string PatAddress { get; set; }
        public string PatCity { get; set; }
        public string PatState { get; set; }
        public string PatZip { get; set; }
        public string PatDob { get; set; }
        public string PatPhone { get; set; }
        public string PatSsn { get; set; }
    }

    public class CommentInsertModel
    {
        public string TemplateName { get; set; }
        public string PatientNum { get; set; }
        public string SubscriberNum { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
        public int ActionCode { get; set; }
        public int LineNum { get; set; }
        public string UserName { get; set; }
        public DateTime? CbDate { get; set; }
        public bool? SpanishPat { get; set; }
        public bool? FieldVisitPat { get; set; }
        public bool? AfterHoursPat { get; set; }

    }

    public class MultiCommentInsertModel
    {
        public List<string> PatientList { get; set; }
        public string  Status { get; set; }
        public string Comment { get; set; }
        public int ActionCode { get; set; }
        public string UserName { get; set; }
        public DateTime? CbDate { get; set; }
        public bool SpanishPat { get; set; }
        public bool FieldVisitPat { get; set; }
        public bool AfterHoursPat { get; set; }
    }

    public class CommCodeModel
    {
        public string CcCode { get; set; }
        public string CcDescription { get; set; }
        public string CcComments { get; set; }
    }
}