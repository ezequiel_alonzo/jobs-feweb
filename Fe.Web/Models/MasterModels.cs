﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fe.Web.Models
{
    public class MasterModels
    {
    }


    public class HospInfo
    {
        public string HospCode { get; set; }
        public string HospAddress { get; set; }
        public string HospName { get; set; }
        public string HospContact { get; set; }
        public string HospCity { get; set; }
        public string HospZip { get; set; }
        public string HospState { get; set; }
        public string HospPhone { get; set; }
    }

    
}