﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fe.Web.Models
{
    public class UserPasswordRequestModel
    {
        public string UserName { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
    }

    public class UserInfo
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? StartDate { get; set; }
        public double? PersonalRate { get; set; }
        public double? VacationRate { get; set; }
        public string Type { get; set; }
        public DateTime? LastChanged { get; set; }
        public string Percentage { get; set; }
        public string Location { get; set; }
        public string Rate { get; set; }
        public string SupvName { get; set; }
        public string Region { get; set; }
        public string CoachingFreq { get; set; }
        public DateTime? TermDate { get; set; }
        public string PayType { get; set; }
        public string PayrollEmail { get; set; }
        public string MsgText { get; set; }
        public string AlertFlag { get; set; }
        public string HospClientNum { get; set; }
        public string Trainer { get; set; }
        public string MgrCoach { get; set; }
        public string PayChexEmpNum { get; set; }
        public string PhoneRptFlag { get; set; }
        public string CardName { get; set; }
        public string CardTitle { get; set; }
        public string Card1Left { get; set; }
        public string Card2Left { get; set; }
        public string Card3Left { get; set; }
        public string Card4Left { get; set; }
        public string Card1Right { get; set; }
        public string Card2Right { get; set; }
        public string Card3Right { get; set; }
        public string Card4Right { get; set; }
        public string Name { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string Ssn { get; set; }
        public string DrivLic { get; set; }
        public string HPhone { get; set; }
        public string WPhone { get; set; }
        public DateTime? Dob { get; set; }
        public string EContact { get; set; }
        public string EPhone { get; set; }
        public string MiscInfo { get; set; }
    }
}