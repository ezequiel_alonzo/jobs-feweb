﻿
namespace Fe.Web.Models
{
    public class DocumentGenerationRequestModel
    {
        public string DocumentName { get; set; }
        public string PatientNumber { get; set; }
        public string SubscriberNumber { get; set; }
        public string SelectedAddress { get; set; }
    }
}