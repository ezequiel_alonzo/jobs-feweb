﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fe.Web.Models
{
    public class HistoryModel
    {
        public string MRN { get; set; }
        public string PatientNum { get; set; }
        public string HospAcct { get; set; }
        public string PatName { get; set; }
        public string CurrStatus { get; set; }
        public string FinClass { get; set; }
        public DateTime? AdmitDate { get; set; }
        public DateTime? DischDate { get; set; }
        public double CurrBalance { get; set; }
        public string ClientNum {get; set;}
        public string ClientName { get; set; }
        public string SSN {get; set;}
    }

    public class HistoryRequestModel
    {
        public string MRN { get; set; }
        public string SSN { get; set; }
        public string MedicaidNum { get; set; }
        public string ClientNum { get; set; }
        public string Template { get; set; }
        public string PatientNum { get; set; }
        public string HospCode { get; set; }
    }
}