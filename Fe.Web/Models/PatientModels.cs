﻿using System;
namespace Fe.Web.Models
{
    public class PatientRequestModel
    {
        public PatientDemographics patient { get; set; }
        public string userName { get; set; }
        
    }

    public class PatientUpdateRequestModel
    {
        public string TemplateName { get; set; }
        public string PatientNum { get; set; }
        public string SubscriberNum { get; set; }
        public string PatFirstName { get; set; }
        public string PatLastName { get; set; }
        public string PatAddress { get; set; }
        public string PatCity { get; set; }
        public string PatState { get; set; }
        public string PatZip { get; set; }
        public string PatPhone { get; set; }
        public string PatEmail { get; set; }
        public string RespName { get; set; }
        public string RespAddress { get; set; }
        public string RespCity { get; set; }
        public string RespState { get; set; }
        public string RespZip { get; set; }
        public string RespPhone { get; set; }
        public string RespEmail { get; set; }
        public DateTime? DOB { get; set; }
        public string SSN { get; set; }
        public string MRN { get; set; }
        public string CaidNum { get; set; }
        public DateTime? PlacementDate { get; set;}
        public string PlaceAmount { get; set; }
        public DateTime? AdmitDate { get; set; }
        public DateTime? DischDate { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string Encounter { get; set; }
        public string RespWPhone { get; set; }
        public string Gender { get; set; }
        public string FinClass { get; set; }
        public string RespSSN { get; set; }
        public string CurrBalance { get; set; }
        public string ClientNum { get; set; }
    }

    public class PatientDemographics
    {
        public string PatNum { get; set; }
        public string PatLastName { get; set; }
        public string PatFirstName { get; set; }
        public string PatAddress { get; set; }
        public string PatCity { get; set; }
        public string PatState { get; set; }
        public string PatZip { get; set; }
        public string PatPhone { get; set; }
        public DateTime? PatDob { get; set; }
        public string PatGender { get; set; }
        public string PatSsn { get; set; }
        public string RespName { get; set; }
        public string RespAddress { get; set; }
        public string RespCity { get; set; }
        public string RespState { get; set; }
        public string RespZip { get; set; }
        public string RespPhone { get; set; }
        public string RespFc { get; set; }
        public string RespWorkPhone { get; set; }
        public string RespSsn { get; set; }
        public string ActHospAcct { get; set; }
        public string ActMrn { get; set; }
        public string ActInsCode { get; set; }
        public string ActCurrBalance { get; set; }
        public string ActPlaceAmt { get; set; }
        public DateTime? ActPlaceDate { get; set; }
        public DateTime? ActAdmitDate { get; set; }
        public DateTime? ActDischDate { get; set; }
        public DateTime? ActApplDate { get; set; }
        public string ActEncounter { get; set; }
        public string ActMedicaidNum { get; set; }
        public string CmtCurrStatus { get; set; }
        public string CmtStatusDate { get; set; }
        public string CmtCallBackUser { get; set; }
        public DateTime? CmtCallBackDate { get; set; }
        public DateTime? CmtLastChanged { get; set; }
        public string ClientNum { get; set; }
        public string TemplateName { get; set; }
        public string ActHospCode { get; set; }
        public string SubscriberNum { get; set; }
        public string PaymentAmount { get; set; }
        public string Spanish { get; set; }
        public string FieldVisit { get; set; }
        public string AfterHours { get; set; }
    }

    public class HFMIPatientInsertRequestModel
    {
        public string TemplateName { get; set; }
        public int  PatientNum { get; set; }
        public string ClientNum { get; set; }
        public string PatFirstName { get; set; }
        public string PatLastName { get; set; }
        public string ActHospAcct { get; set; }
        public string PatAddress { get; set; }
        public string PatCity { get; set; }
        public string PatState { get; set; }
        public string PatZip { get; set; }
        public string PatPhone { get; set; }
        public string PatEmail { get; set; }
        public string RespName { get; set; }
        public string RespAddress { get; set; }
        public string RespCity { get; set; }
        public string RespState { get; set; }
        public string RespZip { get; set; }
        public string RespPhone { get; set; }
        public string RespEmail { get; set; }
        public string RespSSN { get; set; }
        public DateTime? DOB { get; set; }
        public string CurrentStatus { get; set; }
        public string SSN { get; set; }
        public DateTime? BDate { get; set; }
        public string Gender { get; set; }
        public string MRN { get; set; }
        public string CaidNum { get; set; }
        public DateTime? PlacementDate { get; set; }
        public double? PlaceAmount { get; set; }
        public DateTime? AdmitDate { get; set; }
        public DateTime? DischDate{ get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string ManagerStatus { get; set; }
        public string InsCode { get; set; }
        public string Spanish {get;set;}
        public string AfterHours { get; set; }
        public string FieldVisit { get; set; }
        public string Transfer { get; set; }
        public string Encounter { get; set; }
        public double CurrentBalance { get; set; }
        public string RespWPhone { get; set; }
        public string FinClass { get; set; }
        public string Username { get; set; }
    }

    public class PatientProfile
    {
        public string PatientNum { get; set; }
        public string ClientNum { get; set; }
        public string HospAcct { get; set; }
        public string Ssn { get; set; }
        public DateTime? Dob { get; set; }
        public string Sex { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PatPhone { get; set; }
        public string PatAddress { get; set; }
        public string PatCity { get; set; }
        public string PatState { get; set; }
        public string PatZip { get; set; }
        public string EmpName { get; set; }
        public string EmpPhone { get; set; }
        public string EmpAddress { get; set; }
        public string EmpCity { get; set; }
        public string EmpState { get; set; }
        public string EmpZip { get; set; }
        public string RespName { get; set; }
        public string RespPhone { get; set; }
        public string RespAddress { get; set; }
        public string RespWPhone { get; set; }
        public string RespCity { get; set; }
        public string RespState { get; set; }
        public string RespZip { get; set; }
        public string Minor { get; set; }
        public DateTime? PlaceDate { get; set; }
        public Double? PlaceAmount { get; set; }
        public DateTime? AdmitDate { get; set; }
        public DateTime? DischDate { get; set; }
        public string CaidNum { get; set; }
        public DateTime? BDate { get; set; }
        public string Ep { get; set; }
        public DateTime? CbDate { get; set; }
        public string CbUser { get; set; }
        public Double? CurrBalance { get; set; }
        public string Inhdsch { get; set; }
        public string CurrStatus { get; set; }
        public DateTime? RemDate { get; set; }
        public DateTime? PostedDate { get; set; }
        public Double? Amount { get; set; }
        public string PayType { get; set; }
        public string PostDesc { get; set; }
        public string RelCode { get; set; }
        public string RelId { get; set; }
        public DateTime? LastChanged { get; set; }
        public DateTime? PeriodStart { get; set; }
        public DateTime? PeriodEnd { get; set; }
        public Double? PeriodBal { get; set; }
        public string VisitType { get; set; }
        public DateTime? FinClassDate { get; set; }
        public string ClientLoc { get; set; }
        public string FinClass { get; set; }
        public string Dcf { get; set; }
        public DateTime? DcfDate { get; set; }
        public string ManagerStatus { get; set; }
        public string HospAlert { get; set; }
        public string Mrn { get; set; }
        public string InsCode { get; set; }
        public string ActionType { get; set; }
        public string Spanish { get; set; }
        public string AfterHours { get; set; }
        public string FieldVisit { get; set; }
        public string Transfer { get; set; }
        public string StarFlag { get; set; }
        public int? WebConfo { get; set; }
        public string WebConfoFlag { get; set; }
        public string RespSsn { get; set; }
        public string PrevFinClass { get; set; }
        public string AltFinClass { get; set; }
        public string CurrStatusQ { get; set; }
        public DateTime? PrintQ { get; set; }
        public DateTime? PreBadDebt { get; set; }
        public string ConnanceId { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string Encounter { get; set; }
        public string RespCellPhone { get; set; }
        public string PatAddress2 { get; set; }
        public DateTime? PaymentStartDate { get; set; }
        public int? PaymentAmount { get; set; }
        public DateTime? PaymentLastDate { get; set; }
    }

    public class PatientWithDetailsModel
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public double? Balance { get; set; }
        public DateTime? AdmitDate { get; set; }
        public string CurrStatus { get; set; }
        public DateTime? CbDate { get; set; }
        public string HospAcct { get; set; }
        public string PatientNum { get; set; }
        public DateTime? RemDate { get; set; }
        public string CbUser { get; set; }
        public string ClientNum { get; set; }
        public string InsCode { get; set; }
        public string Mrn { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string CaidNum { get; set; }
        public DateTime? PlaceDate { get; set; }
        public string PostType { get; set; }
        public string Sex { get; set; }
        public string HospCode { get; set; }
        public string PhyName { get; set; }
        public DateTime? DischDate { get; set; }
        public string PatPhone { get; set; }
        public DateTime? Dob { get; set; }
        public string Ssn { get; set; }
        public string Minor { get; set; }
        public string PatAddress { get; set; }
        public string PatState { get; set; }
        public string PatCity { get; set; }
        public string PatZip { get; set; }
        public string RespWPhone { get; set; }
        public string RespCellPhone { get; set; }
        public string FinClass { get; set; }
        public string PrevFinClass { get; set; }
        public double? PlaceAmount { get; set; }
        public DateTime? PaymentStartDate { get; set; }
        public DateTime? PaymentLastDate { get; set; }
        public double? PaymentAmount { get; set; }
        public DateTime? ReturnDate { get; set; }
        public int? DaysIn300 { get; set; }
        public int? DaysIn500 { get; set; }
        public int? DaysIn700 { get; set; }
        public DateTime? Date800 { get; set; }
        public string PrevStatus { get; set; }
        public string Letter { get; set; }
        public string PrintQ { get; set; }
        public bool HospAlert { get; set; }
        public bool ManagerStatus { get; set; }
        public string ScreenSnap { get; set; }
        public string ReconBalance { get; set; }
        public bool ReconDiff { get; set; }
        public bool NoReconBalance { get; set; }
        public bool Transfer { get; set; }
        public bool FinClassChange { get; set; }
    }

    public class SearchPatientsRequestModel
    {
        public bool ShowPlaceAmount { get; set; }
        public bool EverHadStatus { get; set; }
        public bool AfterHours { get; set; }
        public bool PostingsOnly { get; set; }
        public bool Spanish { get; set; }
        public bool AgedUnder20 { get; set; }
        public bool FieldVisit { get; set; }

        public string DateSearch { get; set; }
        public string DateRangeType { get; set; }
        public int? Days { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string[] ClientNums { get; set; }
        public string[] StatusCodes { get; set; }

    }
}