﻿
using System;
using System.Collections.Generic;

namespace Fe.Web.Models
{

    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }

    public class UserSessionState
    {
        public bool SessionExpired;
        public bool AuthExpired;
        public double TimeoutInMinutes;
        public double TimeoutInSeconds;
    }

    public class UserLoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ClientId { get; set; }
        public string Scope { get; set; }
    }

    public class UserChangePasswordModel
    {
        public string UserName { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
    }

    public class ClientLoginModel
    {
        public string ClientId { get; set; }
        public string Scope { get; set; }
        public string Secret { get; set; }
    }

    public class LocalUserModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public UserClaimModel Claims { get; set; }
        public bool IsEnabled { get; set; }
    }
    public class UserCheckinModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public UserClaimModel Claims { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsCheckedIn { get; set; }
    }
    public class UserCheckinIp 
    {
        public string Username { get; set; }
        public DateTime? Datein { get; set; }
        public DateTime? Dateout { get; set; }
        public string Ipaddress { get; set; }
    }

    public class UserClaimModel
    {
        public string FirstName { get; set; } //Given Name
        public string MiddleName { get; set; } //Middle Name
        public string LastName { get; set; } //Family Name
        public string FullName { get; set; } //Name
        public string NickName { get; set; } //Nick Name
        public string Address { get; set; } //Address
        public DateTime? BirthDate { get; set; } //Birthdate
        public string Gender { get; set; } //Gender
        public string Email { get; set; } //Email
        public bool EmailVerified { get; set; } //Email Verified
        public string PhoneNumber { get; set; } //Phone Number
        public string PhotoUrl { get; set; } //Picture
        public DateTime? Expiration { get; set; } //Expiration
        public List<string> ExternalProviderUserIds { get; set; } //External Provider User Ids
        public List<string> Roles { get; set; } //Role
        public List<string> Scopes { get; set; } //Scope 
        public List<string> Clients { get; set; } //Client Ids
    }

    public class UserLoginResponseModel
    {
        public bool HasError { get; set; }
        public string ErrorMessage { get; set; }
        public LocalUserModel UserModel { get; set; }
    }

    public class UserAuthModel
    {
        public string UserName { get; set; }
        public bool IsAuthenticated { get; set; }
        public bool ChangePassword { get; set; }
        public List<string> Roles { get; set; }
        public List<string> Permissions { get; set; }
        public string UserModel { get; set; }
    }


}