﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fe.Web.Models 
{
    

        public class LettersRequestModel
        {
            public string aa { get; set; }
            
        }

        public class PatientLettersRequestModel
        {
            public List<PatientLetter> PatientLetters{get; set;}
            public string UserName { get; set; }
            public string DisplayName { get; set; }
        }

        public class PatientLetterFilesListRequest
        {
            public string UserName { get; set; }
        }

        public class PatientLetterFilesList
        {
            public List<PatientLetterFile> PatientLetterList { get; set; }
        }

        public class PatientLetterFile
        {
            public PatientLetterFile(string bl,string fn){
                BaseLocation = bl;
                FileName = fn;
            }
            public string BaseLocation { get; set; }
            public string FileName { get; set; }
        }

        public class PatientLetter
        {
            public string PatNum { get; set; }
            public string LetterName { get; set; }
            public string HospName {get;set;}
            public string HospAddress { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set;}
            public string PAddress { get; set;}
            public string HospAcct { get; set; }
            public double? CurrBalance { get; set; }
            public DateTime? AdmitDate { get; set; }
            public DateTime? DischDate { get; set; }
            public string PCity { get; set; }
            public string PState { get; set;}
            public string PZip { get; set; }
            public string HospContact { get; set; }
            public string ClientNum { get; set; }
            public string PaymentAmount { get; set; }
            public DateTime? PaymentStartDate { get; set; }
            public string CurrStatus { get; set; }
            

        }
    
}