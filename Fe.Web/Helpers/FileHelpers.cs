﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Fe.Web.Helpers
{
    public class FileHelpers
    {
        public static string LastDailySubDirectory { get; private set; }
        public static string LastDailyDirectory { get; private set; }
        private const int MaxFileCount = 1000;

        public static string MapAbsolutePath(string relativePath, string appendPartialPathModifier)
        {
            if (!relativePath.StartsWith("~")) return relativePath;
            var assemblyDirectoryPath = Path.GetDirectoryName(new Uri(typeof(FileHelpers).Assembly.EscapedCodeBase).LocalPath);

            // Escape the assembly bin directory to the hostname directory
            var hostDirectoryPath = assemblyDirectoryPath + appendPartialPathModifier;

            return Path.GetFullPath(relativePath.Replace("~", hostDirectoryPath));
        }

        public static string MapAbsolutePath(string relativePath)
        {
            var mapPath = MapAbsolutePath(relativePath, string.Format("{0}..{0}..", Path.DirectorySeparatorChar));
            return mapPath;
        }

        public static string MapHostAbsolutePath(string relativePath)
        {
            var mapPath = MapAbsolutePath(relativePath, string.Format("{0}..", Path.DirectorySeparatorChar));
            return mapPath;
        }

        public static string GetSafePath(string filePath)
        {
            if (string.IsNullOrEmpty(filePath)) return string.Empty;

            //Strip invalid chars
            filePath = Path.GetInvalidPathChars()
                .Aggregate(filePath, (current, invalidChar)
                    => current.Replace(invalidChar.ToString(CultureInfo.InvariantCulture), String.Empty));

            return filePath
                .TrimStart('.', '/', '\\') //Remove illegal chars at the start
                .Replace('\\', '/') //Switch all to use the same seperator
                .Replace("../", String.Empty) //Remove access to top-level directories anywhere else 
                .Replace('/', Path.DirectorySeparatorChar); //Switch all to use the OS seperator
        }

        public static FileInfo GetPath(string rootDirectory, string filePath)
        {
            return new FileInfo(Path.Combine(rootDirectory, GetSafePath(filePath)));
        }

        public static void VerifyDirectory(string rootDirectory)
        {
            if (string.IsNullOrWhiteSpace(rootDirectory)) throw new ArgumentNullException("rootDirectory");
            if (!Directory.Exists(rootDirectory))
                throw new DirectoryNotFoundException(string.Format("Directory {0} not found.", rootDirectory));
        }

        public static FileInfo GetAndValidateExistingPath(string rootDirectory, string filePath)
        {
            var targetFile = GetPath(rootDirectory, filePath);
            if (!Directory.Exists(rootDirectory))
                throw new DirectoryNotFoundException(string.Format("Directory {0} not found.", rootDirectory));
            if (!targetFile.Exists && !Directory.Exists(targetFile.FullName))
                throw new FileNotFoundException("Could not find: " + filePath);

            return targetFile;
        }

        public static DirectoryInfo GetCurrentImageDirectory(string rootDirectory)
        {
            var dts = DateTime.Now.ToString("yyyy_MM");
            var ddIsNull = string.IsNullOrWhiteSpace(LastDailyDirectory);
            var dsIsNull = string.IsNullOrWhiteSpace(LastDailySubDirectory);
            int i;
            DirectoryInfo di;

            if (!ddIsNull)
            {
                //verify that the directory is for today
                di = new DirectoryInfo(LastDailyDirectory);
                if (di.Name != DateTime.Now.ToString("yyyy_MM"))
                {
                    ddIsNull = true;
                    dsIsNull = true;
                }
            }

            if (ddIsNull)
            {
                //make a new daily directory
                LastDailyDirectory = Path.Combine(rootDirectory, dts);
                if (!Directory.Exists(LastDailyDirectory)) Directory.CreateDirectory(LastDailyDirectory);
            }
            //at this point the daily directory is set

            if (dsIsNull)
            {
                //Since it is null we should make it
                LastDailySubDirectory = Path.Combine(LastDailyDirectory, "0000");
                Directory.CreateDirectory(LastDailySubDirectory);
                return new DirectoryInfo(LastDailySubDirectory);
            }

            di = new DirectoryInfo(LastDailySubDirectory);

            if (di.GetFiles().Count() < MaxFileCount)
            {
                //this directory is still valid
                return di;
            }

            //we have more than the MaxFileCount, make a new directory
            if (!Int32.TryParse(di.Name, out i))
            {
                throw new InvalidCastException(
                    string.Format("Error while creating daily sub directory.  Cannot cast {0} to int32", di.Name));
            }

            LastDailySubDirectory = Path.Combine(LastDailyDirectory, string.Format("{0:0000}", ++i));

            return Directory.CreateDirectory(LastDailySubDirectory);
        }

        public static FileInfo SaveFileToCurrentDirectory(string rootDirectory, string fileName, MemoryStream stream)
        {
            fileName = GetSafePath(fileName);

            using (var file = new FileStream(Path.Combine(rootDirectory, fileName), FileMode.Create, FileAccess.Write))
                stream.WriteTo(file);
            return new FileInfo(fileName);
        }

        public static FileInfo SaveFileToCurrentDirectory(string rootDirectory, string fileName, FileInfo fileInfo)
        {
            return fileInfo.CopyTo(Path.Combine(rootDirectory, fileName));
        }

        public static FileInfo GetNewImageFileName(string rootDirectory, string fileName)
        {
            var di = GetCurrentImageDirectory(rootDirectory);
            var nameNoExt = Path.GetFileNameWithoutExtension(fileName);
            var files = di.GetFiles(nameNoExt + "*");
            if (!files.Any())
                return new FileInfo(Path.Combine(di.FullName, fileName));

            var noexts = files.Select(x => Path.GetFileNameWithoutExtension(x.Name));
            var i = 0;

            foreach (var item in noexts)
            {
                var l = item.LastIndexOf('-');

                if (l <= -1) continue;

                var substr = item.Substring(l + 1);
                int iout;
                Int32.TryParse(substr, out iout);
                if (iout > i) i = iout;
            }

            //var fi = new FileInfo(files.Max(x => x.Name));
            var newFn = Path.GetFileNameWithoutExtension(fileName);
            //nameNoExt = newFn;

            return
                new FileInfo(Path.Combine(di.FullName,
                    string.Format("{0}-{1}{2}", newFn, ++i, Path.GetExtension(fileName))));
        }

        public static string GetNewImageFilePath(string rootDirectory, string fileName)
        {
            var newFn = GetNewImageFileName(rootDirectory, fileName);
            return newFn.FullName.Replace(new DirectoryInfo(rootDirectory).FullName, "");
        }
    }
}
