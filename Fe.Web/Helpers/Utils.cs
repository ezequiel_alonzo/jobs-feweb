﻿using System;
using System.IO;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Security;
using WebMatrix.WebData;

namespace Fe.Web.Helpers
{
    public class Utils
    {

        public static string Truncate(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static bool IsNull(string item)
        {
            return string.IsNullOrWhiteSpace(item);
        }

        public static bool IsNotNull(string item)
        {
            return !string.IsNullOrWhiteSpace(item);
        }


        public static bool ToBoolean(string value)
        {
            switch (value.ToLower())
            {
                case "true":
                    return true;
                case "t":
                    return true;
                case "1":
                    return true;
                case "0":
                    return false;
                case "false":
                    return false;
                case "f":
                    return false;
                default:
                    throw new InvalidCastException("You can't cast a weird value to a bool!");
            }
        }

        public static string FormatLocalDateTime(DateTime tStamp)
        {
            return tStamp.ToLocalTime().ToString("MM/dd/yyyy hh:mm:ss");
        }

        public static string FormatLocalDate(DateTime tStamp)
        {
            return tStamp.ToLocalTime().ToString("MM/dd/yyyy");
        }

        public static string FormatLocalTime(DateTime tStamp)
        {
            return tStamp.ToLocalTime().ToString("hh:mm:ss");
        }

        public static string CheckFileExists(string path, string fileName)
        {
            var fCtr = 0;
            var orgName = fileName;
            while (File.Exists(path + "\\" + fileName))
            {
                fileName = orgName;
                var insInd = fileName.LastIndexOf(".", StringComparison.CurrentCulture);
                fileName = fileName.Insert(insInd, "(" + fCtr + ")");
                fCtr++;
            }

            return fileName;
        }

        public static bool IsAuthenticated()
        {
            return WebSecurity.IsAuthenticated;
        }

        
        public static Int32[] Shuffle(Int32[] sequence)
        {
            var random = new Random();
            var retArray = sequence.ToArray();

            for (var i = 0; i < retArray.Length - 1; i++)
            {
                var swapIndex = random.Next(i, retArray.Length);
                if (swapIndex == i) continue;
                var temp = retArray[i];
                retArray[i] = retArray[swapIndex];
                retArray[swapIndex] = temp;
            }

            return retArray;
        }

        public static string IgnoreInvalidSsn(string sItem)
        {
            if (IsNull(sItem)) return null;

            var nItem = sItem.Replace("-", "").Trim();

            switch (nItem.ToUpper())
            {
                case "999999999":
                    return null;
                case "000000000":
                    return null;
            }
            return RemoveFromString(nItem, "\"");
        }

        public static string RemoveFromString(string oItem, string rItem)
        {
            return IsNull(oItem) ? null : oItem.Replace(rItem, "");
        }

        public static bool IsValidEmail(string strIn)
        {
            var eValid = new EmailValidator();

            return eValid.IsValidEmail(strIn);
        }

        public static string ToTitleCase(string strIn)
        {
            if (IsNull(strIn)) return strIn;
            var tInfo = new CultureInfo("en-us", false).TextInfo;
            return tInfo.ToTitleCase(strIn.Trim().ToLower());
        }

        public static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        public static string KeepOnlyAlphaNumeric(string str)
        {
            var sb = new StringBuilder();
            foreach (var c in str)
            {
                if (char.IsLetterOrDigit(c)) sb.Append(c);
                if (char.IsWhiteSpace(c)) sb.Append(c);
            }

            return sb.ToString();
        }

        public static string FormatDateString(string date)
        {
            if (IsNull(date)) return "";
            try
            {
                var tmpDate = Convert.ToDateTime(date);
                return tmpDate.ToString("MM/dd/yyyy");
            }
            catch
            {
                return "";
            }
        }

        public static string FormatDateTimeString(string dateTime)
        {
            if (IsNull(dateTime)) return "";
            try
            {
                var tmpDate = Convert.ToDateTime(dateTime);
                return tmpDate.ToString("MM/dd/yyyy HH:mm");
            }
            catch
            {
                return "";
            }
        }

        public static string FormatMoneyString(string amt)
        {
            if (IsNull(amt)) return "0.00";
            try
            {
                var ind = amt.IndexOf(".");

                if (ind == -1) return amt + ".00";
                if (ind == 0 && amt.Length == 2) return "0" + amt + "0";
                if (ind == 0 && amt.Length == 3) return "0" + amt;
                if (amt.Length - ind == 1) return amt + "0";

                return amt;
            }
            catch
            {
                return "";
            }
        }

        public static string FormatPhoneString(string phone)
        {
            if (IsNull(phone)) return "";
            try
            {
                if (phone.Length != 10) return phone;

                return phone.Substring(0, 3) + "-" + phone.Substring(3, 3) + "-" + phone.Substring(6, 4);

            }
            catch
            {
                return "";
            } 
        }

    }

    public class EmailValidator
    {
        bool invalid = false;

        public bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names. 
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format. 
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }
    }
}