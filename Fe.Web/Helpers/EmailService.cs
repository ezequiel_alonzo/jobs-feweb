﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace WebExpress.Web.Helpers
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            if (ConfigurationManager.AppSettings["EmailServer"] != "{EmailServer}" &&
                ConfigurationManager.AppSettings["EmailUser"] != "{EmailUser}" &&
                ConfigurationManager.AppSettings["EmailPassword"] != "{EmailPassword}")
            {
                var mailMsg = new MailMessage();

                // To
                mailMsg.To.Add(new MailAddress(message.Destination, ""));

                // From
                mailMsg.From = new MailAddress("donotreply@maxrte.com", "Max RTE Administrator");

                // Max Rte Logo
                var fPath = HttpContext.Current.Server.MapPath("/") + "\\Images\\max-rte-logo-sm.png";
                LinkedResource LinkedImage = new LinkedResource(fPath);
                LinkedImage.ContentId = "MaxRteLogo";
                LinkedImage.ContentType = new ContentType(MediaTypeNames.Image.Jpeg);                             

                // Subject and multipart/alternative Body
                mailMsg.Subject = message.Subject;
                var html = message.Body;

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(
                  html, null, "text/html");

                htmlView.LinkedResources.Add(LinkedImage);
                mailMsg.AlternateViews.Add(htmlView);

                // Init SmtpClient and send
                var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"], Convert.ToInt32((587)));
                var credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailUser"],
                    ConfigurationManager.AppSettings["EmailPassword"]);
                smtpClient.Credentials = credentials;

                return Task.Factory.StartNew(() => smtpClient.SendAsync(mailMsg, "token"));
            }
            else
            {
                return Task.FromResult(0);
            }
        }
    }
}