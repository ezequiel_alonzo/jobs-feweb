﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Fe.Web.Startup))]
namespace Fe.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
